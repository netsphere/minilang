#                                               -*- Autoconf -*-
# Process this file with autoconf to produce a configure script.

# mingw: autoconf 2.69
AC_PREREQ([2.69])

# 新しい autoconf では AC_INIT に 2引数が必須.
AC_INIT(minilang, [0.7], [hisashi dot horikawa at gmail dot com])
AC_CONFIG_AUX_DIR([build-aux])
AC_CONFIG_SRCDIR([configure.ac])
# 新しい automake では, AM_INIT_AUTOMAKE は引数 OPTIONS のみを持つ.
AM_INIT_AUTOMAKE

# m4_ifdef([AM_SILENT_RULES], [AM_SILENT_RULES([yes])])

AM_MAINTAINER_MODE
AC_CONFIG_HEADERS([config.h])

AC_CANONICAL_HOST

#########################################################################
# Checks for programs.

AC_LANG(C++)
AC_PROG_CXX
AC_PROG_YACC
IT_PROG_INTLTOOL([0.50.2])
#AC_PROG_CC
AC_PROG_RANLIB


AC_ARG_ENABLE( debug,
	       [  --enable-debug            DEBUG mode.],
	       [ RELCFLAGS="-g3 -O1 -D_DEBUG -DDEBUG=1 -fexcess-precision=fast " ],
	       [ RELCFLAGS="-DNDEBUG -O3 -fexcess-precision=fast " ] )

# See https://best.openssf.org/Compiler-Hardening-Guides/Compiler-Options-Hardening-Guide-for-C-and-C++

CXXFLAGS="-Wall -Wextra  -Wno-unused-parameter  $RELCFLAGS \
  -Wformat -Wformat=2  -Wimplicit-fallthrough \
  -Werror=format-security \
  -U_FORTIFY_SOURCE -D_FORTIFY_SOURCE=3 \
  -D_GLIBCXX_ASSERTIONS \
  -fstack-clash-protection -fstack-protector-strong \
  -Wl,-z,nodlopen -Wl,-z,noexecstack \
  -Wl,-z,relro -Wl,-z,now \
  -Wl,--as-needed -Wl,--no-copy-dt-needed-entries"

case "$host" in
  x86_64-*-*)
    # `-march=x86-64-v2` has been added from gcc 11.0.
    CXXFLAGS="$CXXFLAGS -march=nehalem -mfma"
    ;;
  *)
    CXXFLAGS="$CXXFLAGS -march=i686 -mmmx -msse -msse2"
    ;;
esac


#AC_DEFINE_UNQUOTED( PACKAGE_LIB_DIR, "${libdir}",
#                    [Installation directory for libraries] )
#AC_DEFINE_UNQUOTED( PACKAGE_LOCALE_DIR, "${localedir}",
#                    [Installation directory for locales] )


#######################################################################
# Checks for libraries.

# libffi.pc  -> libffi-devel package
# bdw-gc.pc  -> gc-devel package
# icu-regex is in libicui18n.so. 
#   icu-i18n.pc -> libicu-devel package
# TODO: change libevent -> libuv
pkg_modules="icu-i18n icu-io libffi bdw-gc libuv gmp libpcre2-16"
PKG_CHECK_MODULES(PACKAGE, [$pkg_modules])
AC_SUBST(PACKAGE_CFLAGS)
AC_SUBST(PACKAGE_LIBS)

AC_CHECK_HEADERS([pthread.h])
# AC_CHECK_LIB(pthread, pthread_create)

AC_CHECK_HEADERS([stdint.h stdlib.h string.h])

# Checks for typedefs, structures, and compiler characteristics.
AC_HEADER_STDBOOL
AC_TYPE_INT64_T
# AC_TYPE_SIZE_T

# Checks for library functions.
#AC_FUNC_MALLOC
#AC_FUNC_REALLOC
#AC_CHECK_FUNCS([memset strdup])

# gettext
GETTEXT_PACKAGE=minilang
AC_SUBST(GETTEXT_PACKAGE)
AC_DEFINE_UNQUOTED(GETTEXT_PACKAGE, "$GETTEXT_PACKAGE", [Package name for gettext.])
# ALL_LINGUAS="ja"
# Use of AM_GNU_GETTEXT without [external] argument is no longer supported.
AM_GNU_GETTEXT([external])
AM_GNU_GETTEXT_VERSION([0.18])

AC_CONFIG_FILES([
  Makefile
  libparser/Makefile
  libminilang/Makefile
  cui/Makefile
  misc/Makefile
  po/Makefile.in
  minilangcore/Makefile
])
AC_OUTPUT
