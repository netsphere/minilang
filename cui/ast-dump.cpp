﻿// -*- coding:utf-8-with-signature; mode:c++; tab-width:4; -*-
// SPDX-License-Identifier: LGPL-3.0-or-later
/*
  minilang -- A small programming language, minilang interpreter.
  Copyright (c) 2011-2013, 2023 Netsphere Laboratories, Hisashi Horikawa.
  All rights reserved.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

// AST を出力する

#include <config.h> // GETTEXT_PACKAGE

#include <stdio.h>
#include <string.h>
//#include "CRB.h"
#include <libintl.h>
//#include "execute.h"
#include "../libparser/parser.h"
#include <unicode/uclean.h>
#include <unicode/ustdio.h>
#include "dump.h"

using namespace std;
using namespace icu;

// @param level top-level は 0
void print_list(UnicodeString& out, const SExp& list, int level)
{
  // top-levelは "(" を省略
  if (level)
      out += "(";
  for (int i = 0; i < list.size(); ++i) {
    if (level && i != 0)
        out += " ";

    switch (list[i].decoration) {
    case Cell::QUOTE:
        out += "'"; break;
    case Cell::QUASIQUOTE:
        out += "`"; break;
    case Cell::UNQUOTE:
        out += ","; break;
    case Cell::UNQUOTE_SPLICING:
        out += ",@"; break;
    default: ; // empty
    }

    if (list[i].tag == Cell::Tag::CONS)
        print_list(out, list[i].list(), level + 1);
    else
        out += list[i].value();

    if (level < 2 && list[i].tag == Cell::Tag::CONS)
        out += "\n";
  }

  if (level)
      out += ")";
}

int main( int argc, char** argv )
{
    setlocale( LC_ALL, "" );
    bindtextdomain( GETTEXT_PACKAGE, PACKAGE_LOCALE_DIR );
    bind_textdomain_codeset( GETTEXT_PACKAGE, "UTF-8" );
    textdomain( GETTEXT_PACKAGE );

    if (argc != 2) {
        fprintf(stderr, "usage:%s filename", argv[0]);
        exit(1);
    }

    //CRB_Interpreter* interp = new CRB_Interpreter();
    //interp->load_path.push_back( PACKAGE_LIB_DIR "/0" );
    //interp->setup_environment();

    Parser parser;
    parser.compile_file(argv[1]);

    ParsedScript* ps = parser.detach_parsed_script();
    SExp dumped = dump_AST_stmt_list( 0, ps->stmt_list );
    UnicodeString dump;
    print_list(dump, dumped, 0);

    UFILE* out = u_finit( stdout, nullptr, nullptr );
    u_fprintf( out, "Parsed script::\n%S\n", dump.getTerminatedBuffer() );
    u_fclose( out );

    Parser::cleanup();
    delete ps;

    u_cleanup();
    return 0;
}



// Local variables:
// tab-width: 4
// c-basic-offset: 4
// indent-tabs-mode: nil
// End:
