﻿// -*- coding:utf-8-with-signature; mode:c++; tab-width:4; -*-
// SPDX-License-Identifier: LGPL-3.0-or-later
/*
  minilang -- A small programming language, minilang interpreter.
  Copyright (c) 2011-2013, 2023 Netsphere Laboratories, Hisashi Horikawa.
  All rights reserved.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

// CUIコマンド

#include "config.h"
#include <cstdio>
#include <cstring>
#include "CRB.h"
#include <libintl.h>
#include "execute.h"
#include <unicode/uclean.h>

using namespace std;
using namespace icu;


int main( int argc, char** argv )
{
    setlocale( LC_ALL, "" );
    bindtextdomain( GETTEXT_PACKAGE, PACKAGE_LOCALE_DIR );
    bind_textdomain_codeset( GETTEXT_PACKAGE, "UTF-8" );
    textdomain( GETTEXT_PACKAGE );

    if (argc != 2) {
        fprintf(stderr, "usage:%s filename", argv[0]);
        exit(1);
    }

    CRB_Interpreter* interp = new CRB_Interpreter();
    interp->load_path.push_back( PACKAGE_LIB_DIR "/0" );
    interp->setup_environment();

    // libminilang/interpreter.cc
    CRB_interp_run( interp, argv[1] );
    printf("%s: 99\n", __func__); // DEBUG
    delete interp;

    u_cleanup();
    return 0;
}



// Local variables:
// tab-width: 4
// c-basic-offset: 4
// indent-tabs-mode: nil
// End:
