﻿// -*- coding:utf-8-with-signature; mode:c++; tab-width:4; -*-

#include <config.h>

//#include "CRB.h"
#include "../libparser/ast.h"
#include "dump.h"
#include "sexp.h"
#include <unicode/uchar.h> // u_isprint()
#include <unicode/schriter.h>
#include <assert.h>
using namespace icu;


static SExp& append( SExp& list, const SExp& a)
{
    list.insert(list.end(), a.begin(), a.end());
    return list;
}

/** unescapeするメソッドはあるが, 逆がない */
static UnicodeString escape_string( const UnicodeString& str )
{
    UnicodeString result;
    StringCharacterIterator ci( str );
    while ( ci.hasNext() ) {
        UChar32 c = ci.next32PostInc();
        if ( u_iscntrl(c) ||
                             // ISO 8bit control character
                             // Control (Cc)
                             // Format (Cf)
                             // Line_Separator (Zl)
                             // Paragraph_Separator (Zp)
             !u_isprint(c) ) {
                             // u_isprint() ... other than "C" (Other)
                             //     Unassigned (Cn)
                             //     Private_Use (Co)
                             //     Surrogate (Cs)
            char buf[100];
            sprintf(buf, (c > 0xffff ? "\\U%08x" : "\\u%04x"), c);
            result += buf;
        }
        else if ( c == '\\' )
            result += "\\\\";
        else
            result += c;
    }

    return result;
}


// @return (a b &optional c d)
//         (a b &optional (c 99) (d a))
static SExp dump_parameter_list( int level, const ParameterList* param_list)
{
    assert( param_list );
    SExp result;

    // 必須パラメータ
    for (StringList::const_iterator i1 = param_list->params.begin(); i1 != param_list->params.end(); ++i1) {
        result.push_back(Cell(**i1));
    }

    // オプション引数
    if (param_list->optional_params.size() > 0) {
        result.push_back(Cell("&optional"));

        ptr_list<OptionalParamNode*>::const_iterator i2 =
                                        param_list->optional_params.begin();
        for ( ; i2 != param_list->optional_params.end(); ++i2 ) {
            if ( (*i2)->default_value ) {
                SExp d = SExp { Cell(*(*i2)->var_name),
                                dump_AST_expr(level + 1, (*i2)->default_value) };
                result.push_back(d);
            }
            else
                result.push_back(*(*i2)->var_name);
        }
    }

    // 残りを配列
    if ( param_list->rest_param ) {
        result.push_back(Cell("&rest")); result.push_back(*param_list->rest_param);
    }

    // ブロック引数
    if ( param_list->block_param ) {
        result.push_back(Cell("&BLOCK")); result.push_back(*param_list->block_param);
    }

    return result;
}

/**
 * ラムダ式 (無名関数), メソッド定義
 * @return (lambda (x) (+ x 3))
 */
static Cell dump_nameless_func( int level, const LambdaExprNode* st )
{
    assert( st );
    Cell result = SExp { Cell("lambda") };

    // 仮引数
    SExp param_list = st->param_list ?
                        dump_parameter_list(level + 1, st->param_list) : SExp();
    result.list().push_back(param_list);

    // 本体
    append(result.list(), dump_AST_stmt_list(level + 1, st->block));

    return result;
}


// if 式
static Cell dump_if_expr( int level, const IfStatement* st )
{
    assert( st );
    Cell result = SExp { Cell("cond") };

    // if 節, elsif 節
    CondList::const_iterator ii = st->cond_list->begin();
    for ( ; ii != st->cond_list->end(); ++ii ) {
        SExp c = SExp { dump_AST_expr(level + 1, (*ii)->condition) };
        append(c, dump_AST_stmt_list(level + 1, (*ii)->block));
        result.list().push_back(c);
    }

    if ( st->else_block ) {
        SExp c = SExp { Cell("t") };
        append(c, dump_AST_stmt_list(level + 1, st->else_block));
        result.list().push_back(c);
    }

    return result;
}

static SExp dump_arguments( int level, const MethodArgument* args )
{
    assert( args );
    SExp result;

    if (args->normal) {
        ArgumentList::const_iterator i = args->normal->begin();
        for ( ; i != args->normal->end(); ++i ) {
            if ( (*i)->type == NORMAL_ARG )
                result.push_back( dump_AST_expr(level + 1, (*i)->expr) );
            else if ( (*i)->type == SPLAT_ARG ) {
                result.push_back( Cell("&SPLAT"));
                result.push_back( dump_AST_expr(level + 1, (*i)->expr) );
            }
        }
    }

    // キーワード引数
    if (args->key) {
        AssociationList::const_iterator i = args->key->begin();
        for ( ; i != args->key->end(); ++i) {
            if ((*i)->key) {
                result.push_back(SExp {dump_AST_expr(level + 1, (*i)->key),
                                   dump_AST_expr(level + 1, (*i)->value)});
            }
            else {
                result.push_back( Cell("&double-splat"));
                result.push_back( dump_AST_expr(level + 1, (*i)->value));
            }
        }
    }

    // ブロック引数
    if ( args->block_arg ) {
        result.push_back( Cell("&BLOCK") );
        result.push_back( dump_AST_expr(level + 1, args->block_arg) );
    }

    return result;
}

// @return (funcall #'+ 1 2 3)
static Cell dump_method_call( int level, const MethodCallNode* mcall )
{
    assert( mcall );
    Cell result = Cell("<<unknown>>");

    // TODO: new() は予約語にして、再定義できないようにすること。
    //       initialize() にしか型アノティションできない
    if ( *mcall->method_name == "new" ) {
        result = SExp { Cell("make-instance") };
        Cell klass = dump_AST_expr(level + 1, mcall->receiver);
        klass.decoration = Cell::QUOTE;
        result.list().push_back(klass);
    }
    else {
        result = SExp { Cell("funcall"),
                    Cell(*mcall->method_name, Cell::QUOTE),
                    mcall->receiver ? dump_AST_expr(level + 1, mcall->receiver) :
                                      Cell("self") };
    }

    // レシーバ以外の実引数
    if (mcall->args)
        append(result.list(), dump_arguments(level + 1, mcall->args));

    // ブロック引数は dump_arguments() へ移動

    return result;
}


// 複数の場合, special operator `progn` で囲む
static Cell dump_stmt_list_as_expr( int level, const StatementList* sl)
{
    if (!sl || sl->size() == 0)
        return Cell( SExp() ); // ()
    else if (sl->size() == 1)
        return dump_AST_expr(level, sl->front());

    Cell result = SExp { Cell("progn") };
    append(result.list(), dump_AST_stmt_list(level, sl));
    return result;
}


/////////////////////////////////////////////////////////////////////////////
// 文

// begin .. rescue 文
static Cell dump_begin_rescue_stmt(int level, const BeginRescueStmt* stmt)
{
    assert( stmt );
    Cell result = SExp { Cell("handler-case") };

    result.list().push_back(dump_stmt_list_as_expr(level + 1, stmt->try_block) );

    // rescue 節. NULL のことがある
    if (stmt->rescue_list) {
        RescueList::const_iterator ii = stmt->rescue_list->begin();
        for ( ; ii != stmt->rescue_list->end(); ii++ ) {
            const RescueBlock* block = *ii;
            SExp rb;
            if (block->class_name)
                rb.push_back(*(*ii)->class_name);
            else {
                // 型名を省略したとき
                rb.push_back(Cell("Exception")); // ruby は StandardError [incompat]
            }
            // opt. 変数で受け取る
            if ( (*ii)->var_ref )
                rb.push_back( SExp { dump_AST_expr(level + 1, (*ii)->var_ref) } );
            else
                rb.push_back( SExp() );

            append(rb, dump_AST_stmt_list(level + 1, (*ii)->block));
            result.list().push_back(rb);
        }
    }

    return result;
}


static Cell dump_variable_ref(int level, const VariableRef* vref)
{
    assert( vref );
    Cell result = Cell("<<unknown>>");

    switch (vref->var_type) {
    case LOCAL_VAR:
    case CONST_VAR:
        result = Cell(*vref->name);   break;
    case INSTANCE_VAR:
        result = Cell("@" + *vref->name);  break;
    case CLASS_VAR:
        result = Cell("@@" + *vref->name);  break;
    case SYMBOL:
        result = Cell(":" + *vref->name);   break;
    }

    return result;
}


static Cell dump_object_slot_ref(int level, const ObjectSlotRef* expr)
{
    assert(expr);

    if (expr->bracket_arguments) {
        // object.[](x)
        Cell result = SExp { Cell(*expr->name), dump_AST_expr(level + 1, expr->object) };
        append(result.list(), dump_arguments(level + 1, expr->bracket_arguments));
        return result;
    }
    else {
        Cell result = SExp { Cell("slot-value"), dump_AST_expr(level + 1, expr->object) };

        Cell slot = dump_variable_ref(level + 1, expr);
        slot.decoration = Cell::QUOTE;
        result.list().push_back(slot);

        return result;
    }
}


// ruby: 関数定義を入れ子にできる [incompat]
static Cell dump_method_definition(int level, const MethodDefinition* method_def)
{
    assert(method_def);
    Cell result = SExp { Cell("defmethod"), Cell(*method_def->method_name) };

    int found = 0;
    const ModuleDefinitionNode* mod_node = NULL;
    const AST_Node* node;
    while ( (node = method_def->parent) != nullptr ) {
        if ( node->m_type == METHOD_DEFINITION ) {
            found = -1;
            break; // メソッド内メソッドはコンパイル時エラーにする. [incompat]
        }
        else if ( node->m_type == CLASS_DEFINITION ||
                                         node->m_type == MODULE_DEFINITION ) {
            found = +1;
            mod_node = static_cast<const ModuleDefinitionNode*>(node);
            break;
        }
    }
    if ( found < 0 ) {
        result = UnicodeString("<<DEFMETHOD-COMPILING-ERROR: inside def>>");
        return result;
    }
    // toplevel の場合がある. self は `main`, そのクラスは `Object`

    // 仮引数
    SExp param_list;

    SExp s = SExp { Cell("self") };
    if (method_def->specializer) {     // eql-specializer
        s.push_back(SExp { Cell("eql"),
                           dump_variable_ref(level +1, method_def->specializer)});
    }
    else if (mod_node)
        s.push_back( Cell(*mod_node->name) );
    else
        s.push_back( Cell("Object") );
    param_list.push_back(s);

    if (method_def->param_list) {
        append(param_list, dump_parameter_list(level + 1, method_def->param_list));
    }
    result.list().push_back(param_list);

    // 暗黙のブロックタグ
    //result.list().push_back(Cell(":tag"));
    //result.list().push_back(method_def->return_tag);

    // 本体
    append(result.list(), dump_AST_stmt_list(level + 1, method_def->block));

    return result;
}


// 値版と条件式版がある
static Cell dump_case_expr(int level, const CaseExpr* st)
{
    assert(st);
    Cell result = st->condition ?
        SExp { Cell("case"), dump_AST_expr(level + 1, st->condition) } :
        SExp { Cell("cond") };

    if (st->condition) {
        WhenClauseList::const_iterator i;
        for ( i = st->when_list->begin(); i != st->when_list->end(); ++i ) {
            SExp when;
            if ( (*i)->patterns->size() > 1) {
                SExp pat;
                ArgumentList::const_iterator j = (*i)->patterns->cbegin();
                for ( ; j != (*i)->patterns->cend(); j++ )
                    pat.push_back( dump_AST_expr(level + 1, (*j)->expr) );
                when.push_back(pat);
            }
            else
                when.push_back( dump_AST_expr(level + 1, (*i)->patterns->front()->expr));
            append(when, dump_AST_stmt_list(level + 1, (*i)->block));

            result.list().push_back(when);
        }

        if (st->else_block) {
            SExp o = SExp { Cell("otherwise") };
            append(o, dump_AST_stmt_list(level + 1, st->else_block));
            result.list().push_back(o);
        }
    }
    else {
        // 条件式版
        WhenClauseList::const_iterator i;
        for ( i = st->when_list->begin(); i != st->when_list->end(); ++i ) {
            SExp when;
            when.push_back( dump_AST_expr(level + 1, (*i)->patterns->front()->expr) );  // 条件式
            append(when, dump_AST_stmt_list(level + 1, (*i)->block));

            result.list().push_back(when);
        }

        if (st->else_block) {
            SExp o = SExp { Cell("t") };
            append(o, dump_AST_stmt_list(level + 1, st->else_block));
            result.list().push_back(o);
        }
    }

    return result;
}

static Cell dump_string_expr( int level, const StringExpr* st )
{
    assert( st );

    if ( st->literals.size() == 1 ) {
        Cell result = Cell("\"" + escape_string(*st->literals.front()) + "\"");
        return result;
    }

    // 交互に編み込む.
    assert( st->embeds.size() == st->literals.size() - 1);
    Cell result = SExp {Cell("concatenate"), Cell("string", Cell::QUOTE)};

    ptr_vector<UnicodeString*>::const_iterator i;
    ptr_vector<Expression*>::const_iterator j = st->embeds.cbegin();
    for ( i = st->literals.cbegin(); i != st->literals.cend() - 1; ++i ) {
        if (**i != "")
            result.list().push_back(Cell("\"" + escape_string(**i) + "\""));

        result.list().push_back(dump_AST_expr(level + 1, *j) );
        ++j;
    }
    if (**i != "")
        result.list().push_back(Cell("\"" + escape_string(**i) + "\""));

    return result;
}

// return, next, break
static Cell dump_return_stmt(int level, const ReturnStatement* st)
{
    assert( st );
    Cell result = SExp { Cell("return-from") };

    if (st->block_tag != "") {
        // この場合は、レキシカルに対応OK
        result.list().push_back( st->block_tag );
    }
    else { // return-from nil  -> for, while, until からの脱出
        bool found = false;
        const AST_Node* node = st->parent;
        while ( node != nullptr ) {
            if ( node->m_type == METHOD_DEFINITION ) {
                result.list().push_back(*static_cast<const MethodDefinition*>(node)->method_name);
                found = true;
                break;
            }
            else if ( node->m_type == CLASS_DEFINITION ||
                                          node->m_type == MODULE_DEFINITION )
                break; // runtime error
            node = node->parent;
        }
        if ( !found )
            result.list().push_back(Cell("<<COMPILING-ERROR>>"));
    }

    if ( st->return_value )
        result.list().push_back(dump_AST_expr(level + 1, st->return_value));

    return result;
}

static Cell dump_assign_expr(int level, const AssignExpression* st )
{
    assert( st );
    Cell result = SExp { };

    result.list().push_back(
            st->lefthand->m_type == VARIABLE_REF ? Cell("setq") : Cell("setf"));

    if (st->lefthand->m_type == VARIABLE_REF)
        result.list().push_back(dump_variable_ref(level + 1, st->lefthand));
    else {
        assert( st->lefthand->m_type == OBJECT_SLOT_REF);
        result.list().push_back(dump_object_slot_ref(level + 1, static_cast<const ObjectSlotRef*>(st->lefthand)));
    }
    result.list().push_back(dump_AST_expr(level + 1, st->operand) );

    return result;
}

static Cell dump_assign_op_expr(int level, const AssignOpExpr* st )
{
    assert( st );
    assert( st->op);
    Cell result = SExp { };

    result.list().push_back(Cell(*st->op));

    if (st->lefthand->m_type == VARIABLE_REF)
        result.list().push_back(dump_variable_ref(level + 1, st->lefthand));
    else {
        assert( st->lefthand->m_type == OBJECT_SLOT_REF);
        result.list().push_back(dump_object_slot_ref(level + 1, static_cast<const ObjectSlotRef*>(st->lefthand)));
    }
    result.list().push_back(dump_AST_expr(level + 1, st->operand) );

    return result;
}


static Cell dump_boolean_expr(int level,  const LiteralExpression* st) {
    return st->u.boolean_value ? Cell("t") : Cell("false"); // "nil" と区別する
}

static Cell dump_int_expr(int level,  const LiteralExpression* st) {
    char* str = mpz_get_str( nullptr, 10, st->u.int_value);
    Cell result = Cell(str);
    free(str); str = NULL;
    return result;
}

static Cell dump_double_expr(int level,  const LiteralExpression* st) {
    char str[100];
    sprintf(str, "%f", st->u.double_value);
    return Cell(str);
}

static Cell dump_nil_expr(int level,  const LiteralExpression* st) {
    return Cell("NIL");
}

static Cell dump_hash_ctor_expr(int level, const HashExpr* st) {
    Cell result = SExp {Cell("HASH")};
    if (st->list) { // NULL のことがある
        for (AssociationList::const_iterator i = st->list->begin();
             i != st->list->end(); ++i) {
            result.list().push_back(SExp{dump_AST_expr(level + 1, (*i)->key),
                                           dump_AST_expr(level + 1, (*i)->value)});
        }
    }
    return result;
}

static Cell dump_array_ctor_expr(int level, const ArrayExpr* st) {
    Cell result = SExp { Cell("vector") };  // 空でもよい
    if ( st->list ) {
        ArgumentList::iterator i = st->list->begin();
        for ( ; i != st->list->end(); i++ )
            result.list().push_back( dump_AST_expr(level + 1, (*i)->expr));
    }
    return result;
}

static Cell dump_yield_expr(int level, const FunctionCallNode* st) {
    // ブロック実引数は禁止
    Cell result = SExp { Cell("YIELD") };
    append(result.list(), dump_arguments(level, st->args));
    return result;
}

static Cell dump_super_expr(int level, const FunctionCallNode* st) {
    // ブロック実引数は禁止
    Cell result = SExp { Cell("call-next-method") };
    append(result.list(), dump_arguments(level, st->args));
    return result;
}

static Cell dump_unary_not_expr(int level, const UnaryExpr* st) {
    return SExp {Cell("not"), dump_AST_expr(level + 1, st->operand)};
}

// 遅延評価, 最後の値を返すのは同じ
static Cell dump_logical_and_expr(int level, const LogicalOpExpr* st) {
    return SExp {Cell("and"), dump_AST_expr(level + 1, st->left),
                 dump_AST_expr(level + 1, st->right)};
}

// 遅延評価, 最後の値を返すのは同じ
static Cell dump_logical_or_expr(int level, const LogicalOpExpr* st) {
    return SExp {Cell("or"), dump_AST_expr(level + 1, st->left),
                 dump_AST_expr(level + 1, st->right)};
}

static Cell dump_while_statement(int level, const WhileStatement* while_) {
    Cell result = SExp { Cell("while"),
                            Cell(":tag"), Cell(while_->block_tag),
                            dump_AST_expr(level + 1, while_->condition) };
    append(result.list(), dump_AST_stmt_list(level + 1, while_->block));
    return result;
}

static Cell dump_for_statement(int level, const ForStatement* for_) {
    Cell result = SExp { Cell("dolist"),
                            Cell(":tag"), Cell(for_->block_tag) };
    SExp c = SExp { dump_AST_expr(level + 1, for_->var_ref),
                            dump_AST_expr(level + 1, for_->range) };
    result.list().push_back(c);
    append(result.list(), dump_AST_stmt_list(level + 1, for_->block));
    return result;
}

static Cell dump_class_definition(int level, const ClassDefinitionNode* st) {
    assert( st->name );

    Cell result = SExp { Cell("defclass"), *st->name };

    SExp super = SExp { };
    // Ruby はモジュールの下のほうから、その後スーパクラスの順序.
    if (st->include_list) {
        for (auto x : *st->include_list)
            super.push_back(dump_variable_ref(level + 1, x));
    }
    if ( st->superclass )
        super.push_back(*st->superclass);

    // Crystal でも可 https://crystal-lang.org/reference/1.13/syntax_and_semantics/modules.html
    if (st->extend_list) {
        super.push_back(Cell("&EXTEND"));
        for (auto x : *st->extend_list)
            super.push_back(dump_variable_ref(level + 1, x));
    }
    result.list().push_back(super);

    // Accessors
    SExp accessors = SExp { };
    if (st->slot_specs) {
        for (auto x : *st->slot_specs)
            accessors.push_back(*x->name);
    }
    result.list().push_back(accessors);

    append(result.list(), dump_AST_stmt_list(level + 1, st->block));
    return result;
}

static Cell dump_module_definition(int level, const ModuleDefinitionNode* st) {
    Cell result = SExp { Cell("DEF-MODULE"), *st->name };

    SExp super = SExp { };
    if (st->include_list) {
        for (auto x : *st->include_list)
            super.push_back(dump_variable_ref(level + 1, x));
    }
    if (st->extend_list) {
        super.push_back(Cell("&EXTEND"));
        for (auto x : *st->extend_list)
            super.push_back(dump_variable_ref(level + 1, x));
    }
    result.list().push_back(super);

    SExp accessors = SExp { };
    if (st->slot_specs) {
        for (auto x : *st->slot_specs)
            accessors.push_back(*x->name);
    }
    result.list().push_back(accessors);

    append(result.list(), dump_AST_stmt_list(level + 1, st->block));
    return result;
}
/*
    case INCLUDE_STMT: // 多重継承 (defclass baz (foo bar) ())
        {
            const IncludeStmt* st = static_cast<const IncludeStmt*>(expr);
            result = SExp { Cell("INCLUDE"), Cell(*st->mod_name) };
        }
        break;

    case EXTEND_STMT:   // eql-specializer
        {
            const ExtendStmt* st = static_cast<const ExtendStmt*>(expr);
            result = SExp { Cell("EXTEND"), Cell(*st->mod_name) };
        }
        break;

    case ATTR_ACCESSOR_STMT:  // defclass :accessor
    case ATTR_READER_STMT:
    case ATTR_WRITER_STMT:
        {
            const AttrAccessorStmt* st = static_cast<const AttrAccessorStmt*>(expr);
            result = SExp { st->m_type == ATTR_ACCESSOR_STMT ? Cell("ATTR-ACCESSOR") :
                            (st->m_type == ATTR_READER_STMT ? Cell("ATTR-READER") : Cell("ATTR-WRITER")) };
            ArgumentList::iterator i = st->arguments->begin();
            for ( ; i != st->arguments->end(); ++i )
                result.list().push_back( dump_AST_expr(level + 1, (*i)->expr));
        }
        break;
*/

static Cell dump_block_node(int level, const BlockNode* st) {
    Cell result = SExp { Cell("block"), Cell(st->name) };
    append(result.list(), dump_AST_stmt_list(level + 1, st->body));
    return result;
}

static Cell dump_unwind_protect_node(int level, const UnwindProtectNode* st) {
    Cell result = SExp { Cell("unwind-protect") };
    if (st->block)
        result.list().push_back(dump_stmt_list_as_expr(level + 1, st->block));
    if (st->cleanup_block) {
        append(result.list(),
                       dump_AST_stmt_list(level + 1, st->cleanup_block));
    }
    return result;
}


#define DISPATCH(_Node, _Func, _Type) \
    case _Node:                       \
        result = _Func(level, static_cast<const _Type*>(expr) ); \
        break;

Cell dump_AST_expr( int level, const AST_Node* expr )
{
    assert( expr );
    // printf("%s: type = %d\n", __func__, expr->type);

    Cell result = Cell("<<compiling-error>>");

    switch (expr->m_type)
    {
    // リテラル
    DISPATCH(BOOLEAN_LITERAL,   dump_boolean_expr, LiteralExpression)
    DISPATCH(INT_LITERAL,       dump_int_expr, LiteralExpression)
    DISPATCH(FLOAT_LITERAL,     dump_double_expr, LiteralExpression)
    DISPATCH(NULL_EXPRESSION,   dump_nil_expr, LiteralExpression)

    DISPATCH(STRING_EXPRESSION, dump_string_expr, StringExpr)
    DISPATCH(LAMBDA_EXPR, dump_nameless_func, LambdaExprNode)
    DISPATCH(VARIABLE_REF, dump_variable_ref, VariableRef)  // 右辺値
    DISPATCH(OBJECT_SLOT_REF, dump_object_slot_ref, ObjectSlotRef)  // 右辺値
    DISPATCH(ARRAY_EXPR, dump_array_ctor_expr, ArrayExpr)
    DISPATCH(HASH_EXPR, dump_hash_ctor_expr, HashExpr)

    DISPATCH(ASSIGN_EXPRESSION, dump_assign_expr, AssignExpression)
    DISPATCH(ASSIGN_OP_EXPR, dump_assign_op_expr, AssignOpExpr)
    DISPATCH(LOGICAL_AND_EXPRESSION, dump_logical_and_expr, LogicalOpExpr)
    DISPATCH(LOGICAL_OR_EXPRESSION, dump_logical_or_expr, LogicalOpExpr)
    DISPATCH(METHOD_CALL_EXPR,  dump_method_call, MethodCallNode)
    DISPATCH(UNARY_NOT_EXPR,    dump_unary_not_expr, UnaryExpr)
    DISPATCH(YIELD_EXPR,        dump_yield_expr, FunctionCallNode)
    DISPATCH(SUPER_EXPR,        dump_super_expr, FunctionCallNode)
    DISPATCH(IF_EXPR,           dump_if_expr, IfStatement)
    DISPATCH(CASE_EXPR,         dump_case_expr, CaseExpr)

    // 文 //////////////////////////////////////////////////////////////

    DISPATCH(WHILE_STATEMENT, dump_while_statement, WhileStatement)
    DISPATCH(FOR_STATEMENT, dump_for_statement, ForStatement)
    DISPATCH(RETURN_STATEMENT, dump_return_stmt, ReturnStatement)
    DISPATCH(NEXT_STATEMENT, dump_return_stmt, ReturnStatement)
    DISPATCH(BREAK_STATEMENT, dump_return_stmt, ReturnStatement)

    DISPATCH(METHOD_DEFINITION, dump_method_definition, MethodDefinition)
    DISPATCH(CLASS_DEFINITION, dump_class_definition, ClassDefinitionNode)
    DISPATCH(MODULE_DEFINITION, dump_module_definition, ModuleDefinitionNode)
    DISPATCH(BEGIN_RESCUE_STMT, dump_begin_rescue_stmt, BeginRescueStmt)

    DISPATCH(BLOCK_NODE, dump_block_node, BlockNode)
    DISPATCH(UNWIND_PROTECT_NODE, dump_unwind_protect_node, UnwindProtectNode)

    default:
        printf("bug: unknown type: %d\n", expr->m_type);
        abort();
    }

    return result;
}


/**
 * AST (抽象構文木) をダンプする
 * @param level 0にして呼び出す.
 */
SExp dump_AST_stmt_list( int level, const StatementList* sl )
{
    // printf("%s: enter.\n", __func__); // DEBUG
    SExp result;
    if ( !sl )
        return result; // ()

    StatementList::const_iterator it;
    for (it = sl->cbegin(); it != sl->cend(); it++) {
        assert(*it);
        result.push_back( dump_AST_expr(level + 1, *it) );
    }

    return result;
}



// Local variables:
// tab-width: 4
// c-basic-offset: 4
// indent-tabs-mode: nil
// End:
