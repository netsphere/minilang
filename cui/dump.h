﻿// -*- coding:utf-8-with-signature; mode:c++; tab-width:4; -*-

#include <unicode/unistr.h>
#include "sexp.h"

SExp dump_AST_stmt_list( int level, const StatementList* sl );

Cell dump_AST_expr( int level, const AST_Node* expr );


// Local variables:
// tab-width: 4
// c-basic-offset: 4
// indent-tabs-mode: nil
// End:
