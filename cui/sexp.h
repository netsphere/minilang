﻿
// SPDX-License-Identifier: LGPL-3.0-or-later
/*
  minilang -- A small programming language, minilang interpreter.
  Copyright (c) 2011-2013, 2023 Netsphere Laboratories, Hisashi Horikawa.
  All rights reserved.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef SEXP_H__
#define SEXP_H__ 1

#include <unicode/unistr.h>
#include <vector>
#include <variant>

struct Cell;

// 簡単に vector で済ます.
typedef std::vector<Cell> SExp;

struct Cell
{
    enum class Tag { ATOM, CONS } tag;  // enum class は型名必須.

    enum Deco { None, QUOTE, QUASIQUOTE, UNQUOTE, UNQUOTE_SPLICING } decoration;

private:
    // anonymous union: 無名 (unnamed) かつオブジェクト定義もなし。
    //  union {
    //      icu::UnicodeString value;
    //    SExp list;
    std::variant<icu::UnicodeString, SExp> u;

public:
    Cell(const icu::UnicodeString& s, Deco d = None):
                                tag(Tag::ATOM), decoration(d), u(s) {
    //new(&value) icu::UnicodeString(s);
    }

    Cell(const SExp& lst, Deco d = None):
                                tag(Tag::CONS), decoration(d), u(lst) {
        //new(&list) std::vector(lst);
    }

    icu::UnicodeString& value() {
        return std::get<icu::UnicodeString>(u);
    }

    const icu::UnicodeString& value() const {
        return std::get<icu::UnicodeString>(u);
    }

    SExp& list() {
        return std::get<SExp>(u);
    }

    const SExp& list() const {
        return std::get<SExp>(u);
    }

/*
    // コピーコンストラクタが必要
    Cell(const Cell& src): tag(src.tag), decoration(src.decoration) {
    switch (tag) {
    case Tag::ATOM:
      // 配置 new 構文で, 明示的にコンストラクタを呼び出す
      new (&value) icu::UnicodeString(src.value);  break;
    case Tag::CONS:
      new (&list) SExp(src.list);  break;
    }
  }

    Cell& operator = (const Cell& src) {
        if (this == &src)
            return *this;

        switch (tag) {
        case Tag::ATOM: value.~UnicodeString();  break;
        case Tag::CONS: list.~SExp(); break;
        }

        tag = src.tag; decoration = src.decoration;
        switch (tag) {
        case Tag::ATOM:
            new (&value) icu::UnicodeString(src.value);  break;
        case Tag::CONS:
            new (&list) SExp(src.list);  break;
        }
    }

  ~Cell() {
    switch (tag) {
    case Tag::ATOM:
        // デストラクタも自分で呼び出す. string ではなく basic_string
        //value.~basic_string();  break;
        value.~UnicodeString();  break;
    case Tag::CONS:
      list.~SExp(); break;
    }
  }
*/
};

#endif
