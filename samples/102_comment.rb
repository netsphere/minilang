
p 1

# comment.

# Crystal は =begin ... =end 未サポート.
=begin
  multi line @@@!!!
=end

# これはダメ.
#=beginhoge

# 行頭に書かなければならない. `=end` とも, 後ろに何を書いてもよい.
# `ruby-parse` v3.2.2 はこのパースに失敗する。
=begin  hoge
hoge
=endhoge  これはコメント終わりではない。
=end  fuga

p(1)

