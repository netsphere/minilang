
# 仮引数にインスタンス変数を書けるか。
# -> ruby は通らない。わざわざサポートしない。

class C
  #@iv : Int32?

  # Crystal: これは通る!
  # ruby: 通らない. formal argument cannot be an instance variable
  def set(@iv) end
  
  def get; return @iv end
end

obj = C.new
obj.set 100
p obj.get
