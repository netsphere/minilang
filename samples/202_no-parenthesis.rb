
# Crystal: unexpected token: "x" (parentheses are mandatory for def parameters)
def f x; x * 2 end

def g y; y + 5 end

def h x, y; x * y end

# これは曖昧ではない
puts f g 3  #=> 16

# 曖昧なケース. h(g 3, 2) と h((g 3), 2)
# Ruby: 前者と解釈され,
#       in `g': wrong number of arguments (given 2, expected 1) (ArgumentError)
puts h g 3, 2
