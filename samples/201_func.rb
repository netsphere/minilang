# -*- mode:ruby -*-

# 識別子はUnicode文字でもOK

class Cふが漢字ふが
  def ほげほげ
    print("1 ok\n");
  end

  def foo # ここにコメントでも大丈夫？
    print("2 ok!\n");
  end
end

o = Cふが漢字ふが.new()
o.ほげほげ()   #=> 1 ok
o.foo()       #=> 2 ok!
