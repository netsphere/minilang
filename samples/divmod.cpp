
// divmod の挙動を見る

// いろいろな定義の方法がありえる。
// http://www.gem.hi-ho.ne.jp/joachim/floorandceiling/remainder.html
//   -> C/C++ は「6. 被除数と同符号の剰余」を採用. 商を rounddown する.
//      fmod() も同じ挙動。ちょっとイケてない

// 他方, remainder() は絶対値最小剰余を得る。商は四捨五入。これは良い。
// 商と剰余を同時に得るのは remquo()

// 絶対値最小剰余が得られるプログラミング言語は少ない。
// Scheme R6RS の div0, mod0, div0-and-mod0 ぐらい。
//   -> 絶対値最小剰余を使うと、ユークリッド互除法をより高速に行える
//      http://keisanwes.casio.jp/exec/user/1648589573

#include <stdlib.h>
#include <stdio.h>

int main()
{
  div_t r = div(5, 2);
  printf("quot = %d, rem = %d\n", r.quot, r.rem);   //=> 2, 1
  r = div(5, -2);
  printf("quot = %d, rem = %d\n", r.quot, r.rem);   //=> -2, 1
  r = div(-5, 2);
  printf("quot = %d, rem = %d\n", r.quot, r.rem);   //=> -2, -1 実用上上手くない.
  r = div(-5, -2);
  printf("quot = %d, rem = %d\n", r.quot, r.rem);   //=> 2, -1
  
  return 0;
}
