
# Crystal も duck typing 可能

class Cat
  def speak() puts "meow" end
end

class Cow
  def speak() puts "moo" end
  def num_stomachs() 4 end
end

class C
  #@hoge : Array(T) forall T   # Crystal: ここを any 型にできない。
                           # 新しいクラスに対して開かれた形にできない。
                           # インタフェイス継承などクラスベースの設計が必要.
  
  def hear_what_it_has_to_say(obj )
    @hoge ||= [] #of T
    @hoge << obj
    #p typeof(obj)
    obj.speak
  end
end

C.new.hear_what_it_has_to_say(Cat.new)
C.new.hear_what_it_has_to_say(Cow.new)
