
# ruby の eval() はスクリプト文字列を渡すので、逆に、プログラムから扱いにくい。

def method_a; return "method_a" end
def method_b; return "method_b" end

eval("puts method_" + "a")

# 実行時でないとエラーを検出できない
eval("puts method_" + "c") #=> undefined local variable or method `method_c' for main:Object (NameError)
