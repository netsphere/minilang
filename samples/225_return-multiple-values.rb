
# 多値の返却 ############################

def f
  return 1, 2   # Ruby: 多値に見えるが, 単に配列で返すだけ.
end

def g
  return [1, 2]
end

x = f    # ここの書き分けがないので、真の多値になっていない。
y = g
p x, y   #=> Ruby [1, 2], [1, 2]
         #=> Crystal {1, 2}, [1, 2]   真の多値がある。

%{
CommonLisp には多値がある。例えば floor 関数が多値を返す.
(values ...) で多値を返せる。受け取った多値を展開して関数を呼び出すのは
multiple-value-call.
Ruby のは「多重代入」。

* (defun myfunc (x) (values (+ x x) (* x x)))
MYFUNC
* (multiple-value-call (lambda (a b) (list a b)) (myfunc 7))
(14 49)
* (myfunc 7)
14
49
}


# 受け取る代入はパタンマッチか ########################

# Ruby: syntax error, unexpected '=', expecting end-of-input
#       代入はパターンマッチングではない。
#[x, y] = f


def h(**k)
  p k
  return {x:1, y:2}
end
h a:1, b:2   #=> Ruby {:a=>1, :b=>2}
             #=> Crystal {a: 1, b: 2}
# Ruby: syntax error, unexpected ':', expecting end-of-input
#       やはり、代入はパタンマッチングではない。
# x:, y: = h()

# Crystal: こちらがキーワード引数を兼ねる
def j(a, b)
  puts a, b
end
j 1, 2
#j a:1, b:1   # in `j': wrong number of arguments (given 1, expected 2) (ArgumentError)

# Ruby: キーワード引数. パターンマッチと同じ書き方
#       通常の引数とキーワード引数を混在できるため、書き方で区別できるように
#       なっている
# Crystal: 構文エラー
def k(a:, b:)
  puts a, b
end
k a:1, b:2
#k 1, 2  # in `k': wrong number of arguments (given 2, expected 0; required keywords: a, b) (ArgumentError)

# パターンマッチ. `a:, b:` と省略すると、同名の変数に格納される。JavaScript と省略が逆。
{a:"hoge", b:"fuga"} => {a:s, b:t}
p s, t  #=> "hoge", "fuga"

%{
;; CommonLisp は、キーワード引数と併用できる!

(defun fullname (sei &key mei)
   (princ sei)
   (princ mei)
   (list sei mei)
)

(fullname "伊藤" :mei "太郎")
(fullname "伊藤" )
;; (fullname :mei "太郎") これはエラー. sei は必須
}
