
;; Common Lisp ではインスタンス変数は "スロット" という。
;; クラス変数は class-allocated slots
;; Ruby でも, クラス変数が継承される。同じ。

(defclass Origin () ())

(defclass Person (Origin)
  ((name :initarg :name :accessor name)
   (species
     :initform 'homo-sapiens
     :accessor the-species
     :allocation :class)))

;; Slot はサブクラスに継承されるか?
(defclass child (Person)
  ())

(defvar o1 (make-instance 'Origin ))
(defvar p1 (make-instance 'Person :name "me"))
(defvar c1 (make-instance 'child :name "Alice"))
(inspect c1)
;; The object is a STANDARD-OBJECT of type CHILD.
;; 0. NAME: "Alice"
;; 1. SPECIES: HOMO-SAPIENS    -- クラス変数はサブクラスから見える

;; 1. スーパークラスのほうを更新する
(setf (slot-value p1 'species) 'hoge)

(print (the-species p1))  ;; HOGE
(print (the-species c1))  ;; HOGE  -- 値はサブクラスと共有。

;; 2. サブクラスのほうから更新する
(setf (slot-value c1 'species) 'fugafuga)
(print (the-species p1))  ;; FUGAFUGA   値は共有

;; これはエラー. Slot はスーパークラスの側には共有されない.
;;(print (slot-value o1 'species))

