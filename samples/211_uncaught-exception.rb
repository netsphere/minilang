
# Catch されない例外がメモリリークしないか

class C
  def f
    raise "hoge"
  end

  def g 
    print 1, "\n"
  end
end

obj = C.new
x = obj.g

obj.f   # ruby: hoge (RuntimeError)
