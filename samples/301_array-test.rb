
# 配列のテスト

class C
end

# for inの後ろは配列でもok. each メソッドが定義されていること。
for i in [1, 3, 5]
  print(i, " ")
end

for i in C.new   #=> undefined method `each' for #<C:...> (NoMethodError)
  print i, " "
end
