# -*- coding:utf-8 -*-

# for文は新しいスコープも導入せず、ただの地の文でよい

class T
  def int_func
    print("enter int_func()\n");
    # for文の中でreturn
    for i in 1..10
      return 5 end
    print("int func out\n");
  end
end

x = T.new().int_func()
print("return value = ", x, "\n")  #=> 5
