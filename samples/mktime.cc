
// Epoch (1970-01-01 00:00:00 UTC) より前の時刻についてmktime() が何を返すか
//     => Linuxでは普通に負値. 問題なさそう
//     => 指定できる時刻は local timeのみ

#include <time.h>
#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include <assert.h>

// http://ufcpp.net/study/algorithm/o_days.html
int64_t mktime_utc( struct tm* req_time )
{
  assert( req_time );
  
  int y = req_time->tm_year + 1900;
  int m = req_time->tm_mon + 1;
  if ( m <= 2 ) {
    --y;
    m += 12;
  }
  int dy = 365 * (y - 1);
  int c = y / 100;
  int dl = (y >> 2) - c + (c >> 2);
  int dm = (m * 979 - 1033) >> 5;

  return (dy + dl + dm + req_time->tm_mday - 719163) * 86400; // Epochに合わせる
}


int main()
{
  struct tm req_time;
  
  memset( &req_time, 0, sizeof(req_time));
  req_time.tm_year = 1970 - 1900;
  req_time.tm_mon = 1 - 1;
  req_time.tm_mday = 1; // 1始まり
  req_time.tm_hour = 9; // JST
  
  printf("%ld\n", mktime(&req_time));
  printf("%ld\n", mktime_utc(&req_time) );

  memset( &req_time, 0, sizeof(req_time));
  req_time.tm_year = 2012 - 1900;
  req_time.tm_mon = 12 - 1;
  req_time.tm_mday = 6; // 1始まり
  req_time.tm_hour = 9; // JST

  printf("%ld\n", mktime(&req_time));
  printf("%ld\n", mktime_utc(&req_time) );

  return 0;
}
