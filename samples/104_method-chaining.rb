
# 改行の先を先読みしないといけない

class C
  def bar()
    return 1
  end
end
  
def foo() return C.new end

p foo()
    .bar()  #=> 1

