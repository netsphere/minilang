
;; 高階関数

;; 全部を足しあげる
* (reduce '+ '(1 2 3 4 5))
15

;; 最大値を得る.
* (reduce (lambda (curbest item)
            (if (> item curbest) item curbest))
          '(7 10 3 -2 1/3 5 2) :initial-value -100)
10

;; return でどこまで抜ける?
(setq fn (lambda (x) (if (= x 100) (return 50) x)))
  -> これはエラー. `lambda` は暗黙のブロックを作らない.
     ●●暗黙のブロック implicit block の一覧とかないかな?
       See https://stackoverflow.com/questions/4394942/which-standard-common-lisp-macros-special-forms-establish-implicit-blocks-named


;; 関数定義は暗黙に block を作る
(defun foo (x)
  (print x)
  (return-from foo 50)  ;; 暗黙に関数名の block が作られる
  (print 'hoge))
(funcall 'foo 20)

;; 関数定義 `DEFUN` はマクロになっている。展開すると, λ式と `block` になる。
* (macroexpand '(defun test () (return-from :test 0)))
(PROGN
 (EVAL-WHEN (:COMPILE-TOPLEVEL) (SB-C:%COMPILER-DEFUN 'TEST T NIL NIL))
 (SB-IMPL::%DEFUN 'TEST
                  (SB-INT:NAMED-LAMBDA TEST
                      NIL
                    (BLOCK TEST (RETURN-FROM :TEST 0)))))



;;;;;;;;;;;;;;;;;
;; ruby の return, next, break

;; ruby のブロックからの `break` は、lambda を呼び出す関数の外側に block を置く.
;;  ... 全部に置くか?
(block hoge (reduce (lambda (cur item)
                      (if (= item 100) (return-from hoge 50) cur))
                    '(1 2 100 3)))
  #=> 50

;; Ruby の `next`: 単にクロージャから抜けるには, lambda の内側に block を置く
;; `DEFUN` マクロの展開と同じ考え方.
(reduce (lambda (cur item)
          (block hage
            (if (= item 100) (return-from hage 50) cur)))
        '(1 2 100 3))

;; `return`: 外側の関数から抜けるには (return-from 関数名) でよい
;; 

