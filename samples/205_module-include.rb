# -*- coding: utf-8 -*-

# Ruby はメソッド検索順序がだいぶ混乱している。

%{
C.include M1 をコメントアウトした場合  -> M1 のほうが左側 (先に検索).
M1       C
  ＼   ／
     E

C.include M1 をイキにした場合, 菱形継承になる.
       -> ●●この場合は,「経路に同じクラスが二つ」エラーにすべき.
  M1
  |  ＼
  |     C
  |  ／
  E
}

# Ruby の module は, 名前空間ではなく, インタフェイス. インスタンス化できないだ
# けで, 機能は class とほぼ同じ。
module M1
  # module M1 が class Eでincludeされて, class C から include されない場合,
  # こちらが先に検索される.
  def m(x)
    puts "M1#m"
    p self.class  #=> E
    super 1    # C#m を呼び出す!  
               # superclassの同名メソッドではなく, call-next-method の動作.
  end
end

class C
  # この include をコメントアウトすると, M1#m -> C#m
  # イキにすると C#m
  include M1
  
  # module M1 とclass Cは基底・派生関係がないが, self = E instance なので,
  # 呼び出される
  def m(x)
    puts "C#m"
  end
end


# M1 が E と Cの間に入る. module のほうが検索順序が先.
class E < C
  include M1
end

E.new.m 1  #=> E, C#m

%{
(defclass M1 () ())
(defmethod m ((x M1)) (print "M1 method"))
(defclass C () ())  
(defmethod m ((x C)) (print "C method"))
(defclass E (M1 C) ())  ;; C のスーパークラスに M1入れると, エラーになる.
                        ;; -> 経路に同じクラスが二つあってはいけない. Good.
;;(setq x (make-instance 'E))   ;; トップレベルでは警告.
(defglobal x (make-instance 'E))
X
* (m x)

菱形継承できるはず
(defclass B () ())
(defmethod m ((x B)) (print "B method"))
(defclass C (B) ())
(defmethod m ((x C))  ;; これが呼び出される.
    (print "C method")
    (call-next-method))  ;; D に渡す.
(defclass D (B) ())  
(defmethod m ((x D)) (print "D method"))
(defclass E (C D) ())  
(defglobal x (make-instance 'E))

* (m x)
"C method" 
"D method" 
}


###### λ式との関係 .. super はレキシカルに superclass に bind する.

class P
  def m(x)
    "P#m: #{x}"
  end
end

class S < P
  def m(x)
    # Crystal は `lambda` がない。'->' を使え.
    return -> () {
      super 10    # class S の superclassを探す
    }
  end
end

class L
  def m(x)
    proc = S.new.m 1
    p proc.call # ここで呼び出す. #=> P#m:10
  end
end

L.new.m 100

