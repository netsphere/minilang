

x = ->(z ) {
  puts "lambda1"
  yield "foo"   # Invalid yield (SyntaxError)
                # Crystal: Error: can't use `yield` outside a method
}

x.call(0) do |ss| 
  p ss 
end
