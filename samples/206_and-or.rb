
# and と or は関数ではない。

%{
Common Lisp も同じ. これらはマクロで, if 式に展開される.
<pre>
* <kbd>(macroexpand '(and a b c))</kbd>
(IF (IF A
        B)
    C)
</pre>
nil か末尾の値そのものを返す. 戻り値は true/false ではない。a が偽なら b は評価しない.

<pre>
* <kbd>(macroexpand '(or a b c))</kbd>
(LET ((#:G370 A))
  (IF #:G370
      #:G370
      (LET ((#:G371 B))
        (IF #:G371
            #:G371
            C))))
</pre>
どれかが真なら, 以降は評価せずに, その値 (true/falseではない) を返す。全部が偽のときはnil.
}

class C
  # Error: can't use Object as the type of an instance variable yet, use a more specific type
  #@foo : Object
  #@foo : Int32?
  
  def foo
    p "C#foo"
    return @foo
  end

  def foo=(x)
    p "C#foo="
    @foo = x
  end
end

# ||= の左辺、右辺の評価回数
# obj.foo || obj.foo = new_val と評価される.
o = C.new
o.foo ||= 20  #=> C#foo, C#foo=
o.foo ||= 30  #=> C#foo   .. C#foo= は呼び出されない.

