# -*- coding: utf-8 -*-

# "/"はメソッド名として使える

class F
  # defの直後の"/"はメソッド名
  def /(hoge)
    puts hoge
  end
end

# "."の直後もメソッド名
F.new./ 1

