
def foo(x, y) end

# Crystal では次のように書く。個数と型を固定するため. ruby では通らない.
#foo(*{1, 2})

# Crystal: Error: argument to splat must be a tuple, not Array(Int32)
foo(*[1, 2])

# これも通る。メソッド実引数に限らない。
h = {a: 1, b: 2, **{x: 10, y:11}, c:3}
p h
