
# 大きな整数

x = 2 ** 100
p x, x.class  #=> 1267650600228229401496703205376, Integer

# 割り算

y = 5 / 3
p y  #=> 1!

=begin
* <kbd>(/ 3 5)</kbd>
3/5                              ;; 分数になる
* <kbd>(* 3/5 165/25)</kbd>
99/25                            ;; 自動的に約分される
=end

# 有理数
require "rational"

p 5 / 3           #=> 除算の挙動は変わらない.
p Rational(1, 3)  #=> (1/3)
p Rational(1, 3) * 3  #=> (1/1) 自動的に約分される.

# 複素数
require "complex"

x = Complex(Rational(3, 5), Rational(7, 10))  
y = Complex(Rational(5, 8), Rational(10, 13))  
p x * y   #=> ((-17/104)+(187/208)*i)
p x / y   #=> ((1976/2125)-(52/2125)*i)
p x ** 2  #=> ((-13/100)+(21/25)*i)



=begin
* (defglobal x #C(3/5 7/10))
X
* (defglobal y #C(5/8 10/13))
Y
* (* x y)
#C(-17/104 187/208)
* (/ x y)
#C(1976/2125 -52/2125)
* (expt x 2)
#C(-13/100 21/25)
=end


# Ruby はこのページの「4. 除数と同符号の剰余」を採用. 商を floor 床で計算する。
# http://www.gem.hi-ho.ne.jp/joachim/floorandceiling/remainder.html
#   Common Lisp の mod も商を floor で出すので同じ。

# 他方, Numeric#remainder() は割られる数の符号で余りを返す。商は truncate() -- 0の方向に丸める. イケてない.
#   Common Lisp の rem も商を truncate で出すので同じ。そこも真似したか・・

p 5.divmod(2)   #=> [2, 1]
p 5.divmod(-2)  #=> [-3, -1]
p (-5).divmod(2)  #=> [-3, 1]   割る数bが正の場合, 余りrは 0 ≦ r < b が実用上も良い.
p (-5).divmod(-2) #=> [2, -1]
