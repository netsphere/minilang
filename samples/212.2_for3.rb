
class X
  def each &block
    block.call(1)
  end
end

o = X.new()
for j in o #do  `do` 禁止. ブロック引数と区別がつかない
  p j  #=> 1
end
