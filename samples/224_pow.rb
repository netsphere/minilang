
# Ruby: undefined method `-' for nil:NilClass (NoMethodError)
# Crystal: Error: undefined method '-' for Tuple()
#p - 2 ** 2

# Ruby -4
# Crystal 4   マジか!!
p -2 ** 2

%{
Ruby の演算子の優先順位 (`**` の近辺): `+@` と `-@` が分かれていておかしい.
   []
   +@  !  ~
   **
   -@
   *  /  %
Crystal は単項をまとめて `**` より上に持ってきている。一貫している。

Python の優先順位:   べき乗が単項より高いのが一般的に見える.
   x[index]   x(args...)  x.attribute
   await x
   **
   +x  -x  ~x
   *   @   /   //   %

べき乗が単項より下 (弱い) のもある:
   JavaScript, Bash, OCaml, Elixir (v1.13以降). ※Bash はべき乗とか不要やろ?
   メジャーな JavaScript がこちらの流派だが、ほかはマイナー.

JavaScript は次の構文になっている:
`UpdateExpression` で単項演算子を skip するようになっている。これなら分かる.
 
ExponentiationExpression[Yield] :
    UnaryExpression[?Yield]
    UpdateExpression[?Yield] ** ExponentiationExpression[?Yield]
}
