
# 正規表現

x = /[A-Z]+/
print("x = ", x, "\n")   #=> (?-mix:[A-Z]+)

# 一致
y = x =~ "A"
print "y = ", y, ", class = ", y.class, "\n"  #=> 0, Integer
print $~, " ", $~.class, "\n"  # Ruby:    "A", MatchData
                               # Crystal: Regex::MatchData("A") Regex::MatchData

# 条件式に渡せる. 0 が真であることを利用. Common Lisp も 0 は真.
if y ; print("regex matched. OK\n") end

# Crystal: Error: unexpected token: "then"   .. if の後ろに then を置けない
if x =~ "あ" ;    print("internal error!!")
             else print("unmatched. ok\n") end

if x =~ "aaABCzz" 
  print("matched. ok\n");
else
  print("internal error.\n");
end

# 例外を投げる
begin
  x =~ 1    # Ruby: 例外発生.
            # Crystal: 例外にならない
  p "no exception"
#rescue ex : Exception   # Crystal: 変数として定数名は不可
rescue TypeError => ex
  print ex, " OK\n"   #=> no implicit conversion of Integer into String
end

