
# 実引数の "&" はブロック扱いだが, do ブロックとは挙動が異なる!

# 無茶苦茶すぎる!
#        return                   next                break
#        -------------------      ---------------     -------
# proc   外側の関数から抜ける     ｸﾛｰｼﾞｬから抜ける     LocalJumpError
#        生成した場所以外だとLocalJumpError
# lambda ｸﾛｰｼﾞｬから抜ける          ｸﾛｰｼﾞｬから抜ける     ｸﾛｰｼﾞｬから抜ける!
#                                 Crystal: error!     Crystal: error!
# block  外側の関数から抜ける     ｸﾛｰｼﾞｬから抜ける     呼出した関数から抜ける
def test_break
  [1, 2].each do |x|
    if x == 1; break end
    print x # 何も表示されない
  end
  [1, 2].each &(-> (x ) {
                  if x == 1; return end # ｸﾛｰｼﾞｬからの脱出.
                  print "lambda x = ", x, "\n"  #=> 2が表示される!
                } )

  [1, 2].each &(Proc.new do |x|
                  if x == 1; next end # `break` は error: break from proc-closure (LocalJumpError)
                  #return  # test_break() から抜ける
                  print "proc x = ", x, "\n"
                end)
  print "3"
end
test_break

