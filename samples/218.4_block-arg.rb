
# "&" のブロック実引数の挙動

p ((1..10).map do |i| i.to_s end)

# "&" 実引数で proc を渡す
to_s_proc = ->(i) { i.to_s }
p (1..10).map &to_s_proc

# これが異質
# Crystal: Error: unterminated call
# これはエラーにするのがよい。[[incompat]]
p (1..10).map(& :to_s) # & の後ろに空白があってもよい

########################
# 2. これを
p (1..10).map {|i| i * 2}

# こうしたい。が、これはエラー
#(1..10).map(&:*(2))

# 2 * x にしてよいなら、こう書ける. この例では短くならないが.
p (1..10).map(&2.method(:*))

# Method#curry
# Proc#curry





# inject(init) の引数 init を省略すると, 1番目と2番目の要素で呼び出す。
p ((1..10).inject() do |sum, i| sum + i end)

# 引数は2以上でもよい.
p (1..10).inject( &(->sum, i {sum + i}))


add = -> x, y { x + y }
add_2 = add.curry[2]   # curry(2) は不可. 惜しい. 
p add_2.call(5)


