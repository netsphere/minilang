# -*- coding:utf-8 -*-

# 自由変数は, レキシカルに外の変数 (not オブジェクト) に bind されること.

x = 10
z = nil

fn = -> () {
  y = 20  # これは lambda 内のローカル変数. 弱点: 字面から見分けがつかない.
  z = 30
  return x + 5
}

p fn.call   #=> ruby: 15
#p y  # undefined local variable or method `y' for main:Object (NameError)
p z
x = 20
p fn.call   #=> ruby: 25 .. オブジェクトではなく, 変数 `x` を参照する.
  
