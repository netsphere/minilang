# -*- coding: utf-8 -*-

# for文のテスト: break, next

for i in 1..6 
  x = 2
  print i, " "
  # breakで抜ける
  if i > 4 then break
  elsif i == 2 
    print("(i..2) ")
  end
end
# for文から抜けても変数は有効 => 新たなスコープが導入される訳ではない
print("\ni,x = ", i, ",", x, "\n")

for i in 1..10
  i = i + 2 # ループ内で変数を操作してもよい. が、効果は永続しない. Rubyも同じ
  print(i, " ")
end
print("\n")

# nextで先頭に戻る
for i in 1..10
  if i < 5; next end
  print(" i.." + i.to_s);
end
print("\n");

