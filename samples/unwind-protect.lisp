
;; ensure 節で return した場合、戻り値はどちらか?

(defun exception-raiser ()
  (print "exception-raiser()")
  (/ 3 1))

; body 部は文並び
(defun foo ()
  (print "foo()")
  (unwind-protect
;;   (progn       ; 一つしか書けないので, progn が必要.
       (handler-bind
           ((division-by-zero (lambda (ex)
                                (format t "Got error: ~a~%" ex)))
            (warning (lambda (ex)
                       (format t "Got warning: ~a~%" ex)
                       (return-from foo 100))))
         (print 1)
         (print 2)
         (exception-raiser)
         (return-from foo 300)  ; こちらは劣後
         )
    (print "cleanup 1")
    (print "cleanup 2")
    (return-from foo -200)  ; こっちの戻り値が採用される
    ))  


;; 記事には handler-bind は連鎖するとあるが、本当?
;;   -> エラーと warning の場合で挙動が違う.
;;            -> warning のときは, 補足「されなくても」そこで終了.
;;      エラーの場合, 補足したとしても(!), 順次上へ回っていき、デバッガに落ちる
;;      これを止めるには, return-from, go, throw しないといけない.
(defun bar ()
  (print "bar() enter!")
  (handler-bind
      ((error  (lambda (ex)
                 (format t "Got ERROR: ~a~%" ex)
                 (return-from bar)  ; これでデバッガに上がるのを止める
                        ))
       (warning (lambda (ex)
                 (format t "Got warning: ~a~%" ex)
                 ;(return-from bar)  ; なくてもそこで終了.
                        ))
       )
    (print 1)
    (print 2)
    (warn "わーん!")   ; ここに戻る!
    (error "oops")
    )
  (print "bar() here!")   ; なので、ここは実行されない
  )

;;(foo)

(bar)


;;
;; handler-bind もマクロになっている
* (macroexpand '(handler-bind ((error (lambda (ex) (format t "Got Error" ex)))) (print 1)))
(FLET ((#:H0 (EX)
         (FORMAT T "Got Error" EX)))
  (DECLARE (SB-INT:TRULY-DYNAMIC-EXTENT (FUNCTION #:H0)))
  (SB-INT:DX-LET ((SB-KERNEL:*HANDLER-CLUSTERS*
                   (CONS
                    (LIST
                     (CONS
                      (LOAD-TIME-VALUE
                       (SB-KERNEL:FIND-CLASSOID-CELL 'ERROR :CREATE T) T)
                      (THE (SB-KERNEL:FUNCTION-DESIGNATOR (CONDITION))
                           #'#:H0)))
                    SB-KERNEL:*HANDLER-CLUSTERS*)))
    (PROGN (PRINT 1))))
T
