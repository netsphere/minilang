
class T
  def foo
    if !true; x = 1 end
    # 字面上初期化されていても、実際に初期化されていなければ例外か?
    begin
      p x   # ruby: 通る. 未初期化は例外ではない.
               # Crystal: 同じく通る。
    rescue NameError 
    #rescue ex : Exception 
      print "do not come here\n"
    end
  end
end

T.new().foo()

# 未定義のローカル変数は例外発生
#x = 1
begin
  print(x) # ruby: 実行時に例外
           # Crystal: コンパイル時Error: undefined local variable or method 'x' for top-level
rescue NameError
#rescue ex : Exception
  print "exception2 ok\n"
end
