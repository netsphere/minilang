
# ハッシュの新しい書き方の許容範囲

h = { :sym => "hoge" }

# 空白は必須ではない
h = { a:"hage" }

# ruby: これは syntax error
#h = { @a: "foo" }

# ruby: 警告のみ. warning: key :a is duplicated and overwritten
# -> エラーにしたほうがよい.
h = {a:1, a:2}

# ruby: これは通る. 定数ではなく, ただのシンボル
h = { CONST: "hoge" }
p( h[:CONST] )
p h[:CONST] 

