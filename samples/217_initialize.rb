
# new() クラスメソッドは暗黙に initialize() を呼び出す

class C
  def initialize
    raise "hoge"
  end
end

C.new  #=> hoge (RuntimeError)

