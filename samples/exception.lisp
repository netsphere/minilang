
(defun foo (num)
    (print "enter `foo`")
    ;; マイナスのときはエラーを投げる
    (if (> 0 num)
        ;; (error) は simple-error を投げる
        ;(error "oops!! ~d~%" num)
        (warn "oops!! ~d~%" num)  ; simple-warning を投げる
        )
    (print "exit `foo`")
)
  
(handler-case
  (progn
    (format t "Calling foo")
    (foo -30))
  ; キャッチする型を書く. `t` だとすべてを受け取る.
  ; キャッチできなかった場合は, デバッガに落ちる。OK
  (warning (ex)
    (print "Catched error!")
    (print ex)
    ))
    


;; t は頂点のクラス
;;   * <kbd>(sb-mop:class-precedence-list (find-class 'simple-error))</kbd>
;;   (#<SB-PCL::CONDITION-CLASS COMMON-LISP:SIMPLE-ERROR>
;;   #<SB-PCL::CONDITION-CLASS COMMON-LISP:SIMPLE-CONDITION>
;;   #<SB-PCL::CONDITION-CLASS COMMON-LISP:ERROR>
;;   #<SB-PCL::CONDITION-CLASS COMMON-LISP:SERIOUS-CONDITION>
;;   #<SB-PCL::CONDITION-CLASS COMMON-LISP:CONDITION>
;;   #<SB-PCL::SLOT-CLASS SB-PCL::SLOT-OBJECT>
;;   #<SB-PCL:SYSTEM-CLASS COMMON-LISP:T>)


; Predefined Condition Types
; condition 
;    simple-condition 
;    serious-condition 
;        error 
;            simple-error 
;            以下略
;    warning 
;        simple-warning
;        以下略
