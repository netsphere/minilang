
# Crystal には `for` loop がない。おっと!

# マクロで「それっぽく」はできる。が同じにはできない。
# See https://crystal-lang.org/reference/1.8/crystal_for_rubyists/index.html#for-loops
=begin
macro for(expr)
  {{expr.args.first.args.first}}.each do |{{expr.name.id}}|
    {{expr.args.first.block.body}}
  end
end
=end

for i in (1..2) #do  `do` 禁止 [incompat]
  p i
end

