# -*- coding: utf-8 -*-

# 特異クラス (singleton class) は, クラスかそうでないかで挙動が違う

class C
  #class << self
  def self.m
    "C"
  end
  #end
end

class D < C
  #class << self
  # コメントアウトすると, "C"が表示される
  def self.m
    "D"
  end
  #end
end

p C.m  #=> "C"
p D.m  #=> "D"

printf "C = %x, C.class = %x, C.singleton_class = %x, C.singleton_class.class = %s\n", 
       C.__id__, C.class.__id__, C.singleton_class.__id__, C.singleton_class.class
printf "D = %x, D.class = %x, D.singleton_class = %x\n", 
       D.__id__, D.class.__id__, D.singleton_class.__id__
printf "D.superclass = %x, D.singleton_class.superclass = %x\n",
       D.superclass.__id__,
       D.singleton_class.superclass.__id__

%{
# Dsingleton にインスタンスメソッドが定義されていない場合, Csingletonのそれが
# 検索される
Object class
  ↑
C class -> Csingleton -> Class class
  ↑            ↑
D class -> Dsingleton -> Class class
}


#########################################################################
# オブジェクト (クラス以外) の特異メソッド object-specialized method

c = C.new
d = D.new

#class << c
# Crystal はクラスしか特異メソッドを作れない Error: def receiver can only be a Type or self
def c.foo
  "c.foo"
end
#end

#class << d
# コメントアウトすると, NoMethodErrorになる
def d.foo
  "d.foo"
end
#end

p d.foo

printf "c = %x, c.class = %x, c.singleton_class = %s, c.singleton_class.class = %s\n", 
       c.__id__, c.class.__id__, c.singleton_class.__id__, c.singleton_class.class
printf "d = %x, d.class = %x, d.singleton_class = %s\n", 
       d.__id__, d.class.__id__, d.singleton_class.__id__
printf "D.singleton_class = %x, d.singleton_class.superclass = %s, d.class.superclass = %s\n", 
       D.singleton_class.__id__,
       d.singleton_class.superclass,  # ruby: `D` おやおや?
       d.class.superclass


%{
c object  -> c_singleton  -> C class
                  x             ^
          *superclassではない*   |
                  x             |
d object  -> d_singleton  -> D class
}

