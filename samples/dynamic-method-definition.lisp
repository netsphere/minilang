
;; 動的なメソッド定義はしてよいか

;; 全然、問題ないし。
(if t
    (defmethod hoge (x) ()))


;; メソッド定義の入れ子はよいか?

;; これも問題ない。hoge を呼び出すと hage を定義する。動的すぎる!
(defmethod hoge (x)
  (defmethod hage (y) ()))


