# -*- coding: utf-8 -*-

# メソッド定義ではなく, メソッド呼出しのたびに, optional parameterは評価される

def a; p "a"; return [] end

def m( x = a() )
  x << 1
end

p m  #=> [1]
p m  #=> [1]  新しい配列が生成される

%{
(defun m (&optional (x "STRING"))
   (return-from m (reverse x)) )
(defun n (&optional (x "STRING"))
   (nreverse x) ;; 破壊的に変更 -> リテラルが変更される!
   x)
(m "HOGE" )
(m)
(m)
}
