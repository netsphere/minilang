
# ガウス整数

c = Complex(3, 2)
d = Complex(5, 6)
p c  #=> (3+2i)

# Ruby の Integer#/ は, Complex と整合していない。
# Common Lisp のように有理数を返すほうがよい。

p c + d  #=> (8+8i)  real 同士, imag 同士を足す
p c - d  #=> (-2-4i)
p c * d  #=> (3+28i)            (3*5 - 2*6) + (3*6 + 5*2)i
p c / d  #=> ((27/61)-(8/61)*i)   3*5 + 2*6      5*2 - 3*6
         #                       ----------- +   ---------- i 
         #                        5^2 + 6^2       5^2 + 6^2
p c.divmod(d)  # undefined method `divmod' for (3+2i):Complex (NoMethodError)
               # Numeric#divmod をサブクラスで無効化している。

