
r = 1..5   # 1以上5以下
s = 1...5  # 1以上5未満
t = (1..)  # 1以上. 括弧が必要.
u = ..5    # 5以下. こっちは括弧がなくてもよい.

puts r, s, t, u
r.step() do |x| print x, " " end; print "\n"
s.step() do |x| print x, " " end; print "\n"
t.step() do |x|
  print x, " "; if x >= 10; break end
end; print "\n"

#u.step() do |x|  # undefined method `<=' for nil:NilClass (NoMethodError)
#  print x, " "
#end
p u.cover? 3  #=> true
