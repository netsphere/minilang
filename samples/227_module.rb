
# モジュールの挙動
# モジュールは名前空間ではない。

module M_A
  def m_of_A; end
end
module M_B; end

class One
  include M_A
end

class Two
  include M_B
end

class Three < Two
  include M_A
end

one = One.new
p one.is_a?(M_A)  #=> true
p one.is_a?(M_B)  #=> false


## extend ###########################################################

module SomeSize
  def size; 3 end
end

class Items
  extend SomeSize
end

p Items.size  #=> 3

class D
  # extend は次と同じ。
  # Crystal: Error: expecting token 'CONST', not '<<'
  #   -> おとなしく extend を使え.
  #class << self
  #  include A
  #end
  extend M_A

  # これは Crystal でも OK
  def D.hoge; end
  def self.hage; end
end

D.m_of_A


## 特異メソッド object-specialized method ##################################

class C end

obj1 = C.new
obj2 = C.new
SClass = obj2.singleton_class

# Crystal: Error: SClass is not a class, it's a constant
#          ※クラスの再定義が禁止されているわけではない。
%{
class SClass
  def hoge
    p "This is a singleton method!"
  end
end
}

# Crystal: Error: def receiver can only be a Type or self
#          クラス以外に対して特異メソッドは作れない
def obj2.hoge
end

obj2.hoge
obj1.hoge  # undefined method `hoge' for #<C:0x00007fe379ccc478> (NoMethodError)

