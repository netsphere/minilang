
// $ gcc -Wall -Wextra calendar.cc `pkg-config --libs icu-io icu-i18n` -lstdc++
// $ ./a.out 
// 15時00分01秒 日本標準時
// 6時00分01秒 グリニッジ標準時

#include <unicode/calendar.h>
#include <unicode/datefmt.h>
#include <cassert>
#include <iostream>
#include <string>

using namespace std;
using namespace icu;

int main() 
{
  TimeZone* tz = TimeZone::createTimeZone("Asia/Tokyo");
  assert( tz );

  UErrorCode err = U_ZERO_ERROR;
  Calendar* cal = Calendar::createInstance( *tz, err );
  assert( U_SUCCESS(err) );

  cal->set(2012, 12, 21, 15, 0);

  DateFormat* fmt = DateFormat::createTimeInstance( DateFormat::kFull );
  assert( fmt );

  FieldPosition p;
  UnicodeString s;
  fmt->format(*cal, s, p);
  string stds;
  s.toUTF8String(stds);
  cout << stds << "\n";

  const TimeZone* gmt = TimeZone::getGMT();
  cal->setTimeZone( *gmt );

  s = ""; stds = "";
  fmt->format(*cal, s, p);
  s.toUTF8String(stds);
  cout << stds << "\n";

  delete tz;
  delete cal;
  // delete gmt;
  delete fmt;

  return 0;
}
