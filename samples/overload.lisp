
;; Common Lisp は overloading が禁止。仮引数が「合同」congruent でなければならない。

(defclass C () ())
(defclass D (C) ())

;; `defgeneric` がある場合に限り、「個数」の検査が行われる
(defgeneric display (what x &key y z)
  (:documentation "Returns string that represents the object"))

(defgeneric hoge (x &rest z)
  (:documentation "逆にすると"))

;; 1. 必須パラメータの個数が同じでなければならない。
;; 2. optional パラメータの個数も同じでなければならない。増えても減ってもダメ.
;;    最大を定めるからか?
;;    必須パラメータと optional の入替えも不可。それぞれの個数でチェック
;;    &optional と &rest の入替えも不可。
;;       &optional と &key の入替えも不可.
;; 3. &rest の有無も単独でチェック. &key の有無も単独でチェック。
;; 4. ただし, &rest の代わりに &key はOK!  ええ??
;;           &key の代わりに &rest も OK.
;; 仕様はこちら;
;;   7.6.4 Congruent Lambda-lists for all Methods of a Generic Function
;;   http://www.lispworks.com/documentation/lw70/CLHS/Body/07_fd.htm

(defmethod display ((x C) y &rest z)
  (print z))  ;; => (:Y 2 :Z 3)   なるほど!

(setq obj (make-instance 'C))
(display obj 10 :y 2 :z 3)

(defmethod hoge ((x C) &key aa bb)
  (print aa)    ;; => nil
  (print bb))   ;; => 6

(setq obj2 (make-instance 'D))
(hoge obj2 3 4 :bb 6)  ;; 3 4 :bb 6 部分が奇数だとエラー. なるほど

