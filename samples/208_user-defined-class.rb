
# ユーザ定義クラス
class F #< String
  # Ruby はここに普通の文が書け, ローカル変数も定義できる。
  #  -> クラスの「ローカル変数」はナンセンス。クラス変数を使え.
  @@x = 1 + 2
  # Crystal では、コンパイル時には初期化されていないので、print などは
  # ここには書けない.
  #print "self = ", self, ", class = ", self.class, "\n"   #=> F, class = Class
  #print @@x, "\n"  #=> 3

  
  def m; print("F#m!!\n") end

  def n(x, y)
    p self    #=> #<F:0x00007f81a529e6a8>
    #p F.send :local_variables   #=> [:x, :y]  クラスのローカル変数は取れない!
    p F.class_variables         #=> [:@@x]
    print "@@x + x + y = ", @@x + x + y, "\n"
  end 

  def f(x)
    print "F#f: x = ", x.inspect, "\n"   #=> nil
  end 

  def g(x, y)
    # 戻り値は nil になる
  end

  def h 
    f g(1, 2)
  end
end

o = F.new()
o.m()        #=> "F#m!!"
o.n 5, 6     #=> @@x + x + y = 14
o.h          #=> F#f: x = nil

