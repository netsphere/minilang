

(defclass C () ())

;; &rest は &key の「前」に置かないといけない.
(defmethod display ((x C) y &rest z &key aa bb &allow-other-keys)
  (print x)
  (print y)
  (print aa)
  (print bb)
  (print z))

(setq obj2 (make-instance 'C))

;; invalid keyword arguments: :Z, :A (valid keys are :BB, :AA).
;;    -> キーワード引数のほうは、仮引数のシンボル以外は不可
;;       「その他」がrest に入るわけではない。
;; &allow-other-keys を付けると, rest に入る。
;;     (:Z 5 :A 1 :AA 100)   :aa も入ってくることに注意!
(display obj2 3 :z 5 :a 1 :aa 100 )

;; keyword argument not a symbol: 4.
;;    -> &rest に 4 が入って、:aa からが &key というわけではない
;;(display obj2 3 4 :aa 100 ) 

;; &rest と &key が重なっている
;; &rest に全部が入ったうえで、&key として再解釈
(display obj2 3 :aa 100 ) ;; z = (:AA 100) えーっっ!



;; &rest は、&key の有無にかかわらず、同じ挙動
(defmethod hoge ((x C) y &rest z )
  (print x)
  (print y)
  (print z))

(setq obj3 (make-instance 'C))

(hoge obj3 3 4 :aa 100 ) ;; z = (4 :AA 100)



;; &optional と &key が両立するか?
(defmethod foo ((x C) y &optional (z 42) &key aa)
  (print z)
  (print aa) )

(foo obj3 3 4 :aa 100)

;; こちらはエラー. optional を埋めたうえで key に進む
;; optional 仮引数のデフォルト値があっても、それで補ってくれるわけではない。
(foo obj3 3 :aa 100)  ; => keyword argument not a symbol: 100.
