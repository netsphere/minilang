
# ローカル変数

s = "hoge"

def f
  x = {} #of Symbol => NamedTuple(x: Hash(Symbol, Int32))
  # 空白がなくてもOK. x: x と解釈する (x :x ではない)
  y = {x:x}
  #y = {x: {:x => 1}}
  x[:y] = y   # 循環する
  puts y[:x]
end
f

# このようにすれば, Crystal でも相互参照できる。
class X
  #@y : Y?
  def y=(y) @y = y end   # 属性代入
end

class Y
  #@x : X?
  #def x=(x) @x = x end
  attr_accessor :x
end

x = X.new; y = Y.new
x.y = y; y.x = x    # 属性代入はメソッド呼出し
p x, y
