
class Foo
  def [](x, y, &block)
    if block_given?   
      block.call 30
    end
  end
end

obj = Foo.new
obj[1, 2]
# Crystal はここでエラー
p(obj[1, 2] do |x| p x end)   # obj.[](1, 2, &block)

ary = [obj]
ary[0][2, 3] do |x| p x end # {|x| p "hoge" } ブロック引数を二回重ねられない
                                            # `do` でも同じ.

# ブロックの後ろにいきなりメソッド呼出しを付けられる。primary になっている
p(ary[0][2, 3] do |x| p x end.to_s)

