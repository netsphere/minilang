
;; スーパクラスからサブクラスのメソッドを呼び出せるか?

;; これで警告が消える.
(defgeneric bar (what param)
  (:documentation "Must override.") )

(defclass M1 () ())
;; 警告: undefined function: COMMON-LISP-USER::BAR
;;   -> 通る
(defmethod foo ((self M1)) (print "M1#foo") (bar self 100))

(defclass C (M1) ())
;; ここをコメントアウトすると error: There is no applicable method for the generic function
(defmethod bar ((self C) (x integer)) (print "C#bar") (print (* x 2)))

(defglobal obj (make-instance 'C))
(foo obj)
