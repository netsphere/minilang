
# Ruby の for文は変数を隠さない

x = 10
for x in 1..3
  print x, " "
end
p ""
p x #=> 3

# なので、インスタンス変数を書いてもよい.
for @v in 1..2
  print "@v = ", @v, " "
end
