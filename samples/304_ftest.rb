# ファイルの読み込み
fp = File.open("ftest.crb", "r");
print fp, "\n"  #=> #<FileIO:...>
fp2 = File.open("ftest.result", "w");
print("fp2.." + fp2.path + "\n");

while (str = fp.gets()) != nil # EOFのときは nil を返す
  fp2.puts(str)
end

fp.close()
fp2.close()
