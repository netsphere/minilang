# -*- mode:ruby -*-

# ラムダ式, クロージャ
# Proc.new (proc) と lambda (->) とで挙動が違う。

p lambda{}.lambda?   #=> true
p (->() {}).lambda?  #=> true
p proc{}.lambda?     #=> false
#p Proc.new {x:1}.lambda? # これは syntax error. 空白があっても block
p Proc.new {}.lambda? #=> false

# 仮引数が shadowing するか?
def test_shadow
  x = 10
  c = -> (x ) {   # shadow する
    x + 2
  }
  print( "lambda ret = ", c.call(3), "\n" )    #=> 5

  c = Proc.new do |x|  # shadow する
    x + 20
  end
  print( "proc ret = ", c.call(3), "\n" )    #=> 23
  print "outer x = ", x, "\n"  #=> 10
end
test_shadow

# 引数の扱い: 無茶苦茶だ..
# https://docs.ruby-lang.org/ja/latest/method/Proc/i/lambda=3f.html
def test_arg
  c = -> (x , y ) { print "x = #{x}, y = #{y}\n" }
  begin
    c.call(1 )   # Crystal: 実引数の個数違いはコンパイルエラー
  rescue ArgumentError => ex
    print "lambda: arg#2 missing: ", ex.message, "\n"
  end
  d = Proc.new do |x, y| print "x = #{x}, y = #{y}\n" end
  d.call(1 )  # Ruby: エラーにならない. これはアカン...
              # Crystal: コンパイルエラー. よい.
end
test_arg

