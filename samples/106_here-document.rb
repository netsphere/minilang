
# heredoc
# Crystal: `<<-` のみをサポート. 終端をインデントしてよい.

# Ruby: 終端行の先頭空白は、文字列の内容にならない。
#       "  hoge foo bar\n"
# Crystal: 各行の先頭空白 (終端行の識別子の位置まで), 終端行の先頭空白と *その
#          直前の改行文字* は、文字列の内容にならない.
#          "hoge foo bar"
p <<-EOS, 2
  hoge foo bar
  EOS

# Ruby: `<<` の後ろは識別子 *ではない*. "?" は付けてはならない.
# Crystal: 同様. 小文字始まりはOK.
print <<-ident, <<-hoge
  Here is string
ident
  Here is string, too.
hoge

# Ruby: 複数の文字列は合体される。
#   => 文字列の直後は AFTER_EXPR に遷移。ここでも文字列を受け付ける
# Crystal: Error: unexpected token: "DELIMITER_START"
p "hoge" + "fuga" + "hage"

p((<<-EOS)
あいうえお
EOS
       .bytesize)   #=> Ruby: 16

# Ruby: syntax error, unexpected ==, expecting end-of-input
#       全部の二項演算子の前に改行を入れていいわけではない. "." だけ特別扱い.
#p 10
#   == 10  

x = 10
p "x = #{x}"
