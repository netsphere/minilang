
# Ruby, Crystal とも, モジュールでクラス変数を定義できる

module M
  @@cvar = 100
end

class C
  include M

  def hoge
    p @@cvar
  end
end

C.new.hoge #=> 100

