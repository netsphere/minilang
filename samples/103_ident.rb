
# Unicode の識別子を作れる

あいうえお = 10
p あいうえお #=> 10

CONST = 100
p CONST

# Crystal: Error: $global_variables are not supported, use @@class_variables instead
# minilang: グローバル変数はサポートしない。

#$global = 200
#p $global


class C
  @@class_var = 1000
  p @@class_var   #=> 1000
end
