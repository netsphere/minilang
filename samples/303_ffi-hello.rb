
require "rubygems"
gem "ffi"
require "ffi"

module LibC
  extend FFI::Library

  ffi_lib 'c'

  # int puts(const char*)
  attach_function :puts, [:string], :int  # 文字列からポインタへの変換あり.
end

LibC.puts "Hello!"
