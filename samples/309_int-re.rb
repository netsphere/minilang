
# これは ReDoS になる。
re = /^([1-9](_?[0-9]+)*|0+)$/

# 最初と最後の括弧が必要
#re = /^([1-9][0-9]*(_[0-9]+)*|0+)$/

p "1_" =~ re     #=> nil
p "1000" =~ re   #=> 0
p "000" =~ re    #=> 0

# ruby: 長考で返ってこない。ダメ!
# crystal: Unhandled exception: Regex match error: match limit exceeded (Regex::Error) good!
p ("11" + "0" * 31 + "\x00") =~ re    #=> nil
