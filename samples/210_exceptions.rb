
# 例外のテスト

class C
  def f
    raise "foo"    # overload: RuntimeErrorのメッセージになる.
  end	  
end

class Fuga < Exception; end
  
begin
  x = 1 + 2
  C.new.f
  print x     # not reach
rescue RuntimeError => e
#rescue e : RuntimeError
  print "RuntimeError exception caught!\n"    # (1)
  print e, "\n"              #=> foo
rescue Fuga
  print "fuga!"   # not reach
ensure
  print "here is ensure\n"   # (2)
end

# Ruby: メソッド名は大文字で始めてもよい.
# Crystal: メソッド名は大文字で始めてはならない. 構文エラー
def myError(s)
  return s ? 1234 : ArgumentError.new("hoge")
end

# Ruby: RuntimeError < StandardError
#     rescue 節で型を省略したときは StandardError (Exception ではない) を捕捉
#     raise で型を省略したときは RuntimeError. チグハグ.
# Crystal: RuntimeError< Exception
#     rescue 節で型を省略したときは, 例外すべて (Exception かそのサブクラス) を捕捉.
begin
  raise myError("hoge")
rescue => ex
#rescue ex
  # Ruby: TypeError < StandardError   なので、捕捉される
  print "StandardError caught: ", ex.inspect
      #=> #<TypeError: exception class/object expected>
end


# いろいろ省略
begin ensure end
#begin end
begin rescue; end
