
# 大文字始まりのメソッド名は不可。


# Crystal は大文字始まりのメソッドはエラー:
# Error: unexpected token: "("
def x() end

class C
  Y = 50
  #@X : Int32
  # Ruby OK
  attr_accessor :X
  # Crystal: マクロとして実装。`def` に展開され、大文字始まりでエラー. むむ
  #property :X
  
  def initialize
    @X = 100
    #Y = 50   ruby: error: dynamic constant assignment
  end

  #def x=(x); @X = x end
  #def x; @X end
end

obj = C.new
p obj.X
obj.X = 200
p obj.X
p obj.Y

