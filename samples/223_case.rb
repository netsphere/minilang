
# case 文いろいろ

## オブジェクトの値
# fall through しないし、させる方法もない。
n = 2
case n
when 1
  puts "one"
when "2"
  puts "two"
when 2
  puts "number 2"
else
  puts "other"
end

# case は式文
p case n
  when 2
    "two"
  end

## case 直後を省略した場合, 単に式も書ける  -- Crystal も OK
a = -1.0 / 0.0 #0.0 #1 / 0.0
p case # ここを省略
  when a < 0   # これにマッチ
    "minus"
  when a > 0
    "plus"
  when a == 0
    "zero"
  else
    a
  end

    
## 型も書ける. `is_a?` で判定する
class C1 end
class C2 < C1; end
class C3 < C2; end
obj = C2.new
p case obj
  when String
    "String"
  when C3
    "C3"
  when C1  # これにマッチ
    "C1"
  end


## `case in` 式文   -- Crystal は未サポート

#user = {role:"admin", name:"hoge"}
#case user
#in role: "admin", name:
#  puts "Hello, mighty #{name}!"
#else
#  puts "I don't know you well yet"
#end


