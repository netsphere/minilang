
def foo
  l1 = ->() {
    l2 = 100  # ruby: これはλ式内のローカル変数の定義. 
  }
  #p l2.inspect  # undefined local variable or method `l2' for main:Object (NameError)
  l2 = 10 if l2  # ruby: ここで外側のローカル変数の定義, しかし l2 = nil なので
                 #       代入は行われない
  p l2.inspect  #=> "nil"
end

foo
