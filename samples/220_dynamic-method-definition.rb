# -*- coding:utf-8 -*-

# Ruby: 関数定義を入れ子にできる。
#       実行時の分岐で、定義する/しないができる。

class C
  def self.newmethod
    p "C.newmethod"
    
    # Crystal: Error: can't define def inside def
    def m; p "m" end    #=> ruby: こっちはOK!
    
    #class D; end  #=> [ruby] error: class definition in method body
  end

  def foo
    # foo() が呼び出されたときに、定義される
    def bar
    end
  end

  if 1
    def hage()
    end
  end
end

class D
  def f
    C.newmethod
  end
end

D.new.f

obj = C.new
obj.foo()
obj.bar()   # ruby: foo() をコメントアウトすると、こちらもエラーになる

obj.hage()

C.new.m  # 入れ子にしても、直接 C のメソッドになる。
#C::D.new
