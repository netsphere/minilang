
;; 'resumable exception' semantics
;; => Algebraic Effects として車輪の再発明されようとしている

;; ほとんどのプログラミング言語には、例外からの再開はない。
;; R が withRestarts(), invokeRestart() を持つ。CommonLisp と一緒のセマンティク
;; ス。
;; 例外が発生したブロック自体を再実行する Ruby はかなり独自。

;; See https://lisp-journey.gitlab.io/blog/error-and-condition-handling/

(defun division-restarter (num)
  (format t "Enter division-restarter")
  ;; Macro `RESTART-CASE`: block, restart-bind などに分解される.
  ;; 例外が発生したときの選択肢を複数、書ける.
  (restart-case
    (progn
      (format t "Enter restartable-form")
      (/ 3 num))
    ;; それぞれの先頭は case-name
    (return-nil () nil)
    (divide-by-one () (/ 3 1))  ;; これは (restart-case expression) の再実行で
                                ;; はない. 例外に代えて, 式の値としてこの値を返す.
    ))

(defun division-and-bind ()
  ;; (handler-bind) は先にエラー処理リストを書く.
  (handler-bind
    ((error (lambda (cond)
        (format t "Got error: ~a~%" cond)
        (format t "and will divide by 1~&")
        (invoke-restart 'divide-by-one)))
        )
    (division-restarter 0))
    )

(division-and-bind)

