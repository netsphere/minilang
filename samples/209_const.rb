# -*- coding: utf-8 -*-

class C
  puts "class C enter"

  def f
    #CONST = 1     # コンパイル時エラー: "dynamic constant assignment"
    #class CC; end # コンパイル時エラー: "class definition in method body"
  end
  CONST_AT_CLASS = 1

  # module が混乱している。クラスの中にモジュールを書ける.
  module M
    class CC
      def g; CONST_CC end 
    end
  end

  # ruby: 下の行をコメントアウトすると 4 が表示される。
  # superclassではなく、outer class に向かって探索
  M::CC::CONST_CC = 3  # ok

  # 順に外側に向かって検索される
  CONST_CC = "In C const"
end
CONST_CC = "Top level const"

class D < C
  CONST_AT_CLASS = 2   # hidingできる. overrideではない
end

D.new.f
p D::CONST_AT_CLASS  #=> 2
p C::CONST_AT_CLASS  #=> 1

p C::M::CC.new.g        #=> 3. class Cのコメント参照.
