# -*- coding: utf-8 -*-


module M
  class C  # ここはbacktraceでは表示されない
    X = lambda do |xx| 
      p xx, Y.call() end   # ここは表示される

    Y = lambda do 
          Thread.current.backtrace
        end

    def fn   # ここと
      X.call "hoge" # ここが表示される
    end
  end
end


M::C.new.fn
