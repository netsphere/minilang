# -*- coding:utf-8 -*-

require "io"


# DatagramSocketIO, StreamSocketIO のスーパークラス.
class BasicSocket < IO

  # ストリームソケットでは, サーバに接続する.
  # データグラムソケットでは, デフォルトの送信先を設定する. ここで送信先を設定していても
  # 送信のつど, 送信先を変えることができる.
  #
  # example:
  #     addrs = Socket.getaddrinfo "example.com", "80", AF_UNSPEC, 
  #                                SOCK_STREAM,
  #                                nil, AI_NUMERICSERV
  #     sock = nil
  #     addrs.each do |addr|
  #       sock = Socket.new addr.afamily, addr.socktype, addr.protocol
  #       begin
  #         sock.connect addr.addr
  #       rescue SystemCallError
  #         next
  #       end
  #       break # connected
  #     end
  # 
  # @param [Sockaddr] sockaddr  相手方のアドレス
  # @return   self を返す.
  def connect sockaddr
    raise # TODO: impl.
    self
  end


  # ソケットをローカルアドレスに bind する.
  # ストリームソケットでは, accept() で接続を受け付けられるようにする.
  # データグラムソケットでは, バケットを read() で受信するために bind する.
  # @param [Sockaddr] sockaddr   ローカルアドレス
  def bind sockaddr
    raise # TODO: impl.
  end


  # @!method close
  #     ソケットを閉じる


  # @!method send bytes, flags
  #     データグラムソケットでは, connect() を呼ぶと, send() で送信できる
  #     flags を指定しない時は, write() と同じ
  #     @param [ByteArray] bytes  送信するバイト列
  #     @param [Integer] flags    ソケットのフラグ


  # @!method write bytes
  #     ストリームに書き出す.
  #     データグラムソケットでは, connect() を呼ぶと, write() で送信できる.
  def write bytes
    raise TypeError if !bytes.is_a?(ByteArray)
    send bytes, 0
  end


  # max_lengthバイトだけ, ストリームから読み込む
  # データグラムソケットでは, bind() を呼び出すと, read() で受信できる.
  def read max_length
    raise TypeError if !max_length.is_a?(Integer)
    recv max_length, 0
  end


  # @!method recv max_length, flags
  #     flags を指定しないときは read() と同じ.


  # ソケットオプションを取得する
  def getsockopt level, optname
    raise # TODO: impl.
  end


  def setsockopt level, optname, optval
    raise # TODO: impl.
    self
  end

    
  # ソケットに bind されたローカルアドレスを返す. getsockname(2) のラッパ.
  # @return [Sockaddr] ソケットのローカルアドレス
  def getsockname
    raise # TODO: impl.
  end


  # @param [Sockaddr] dest_sockaddr  送信先のアドレス. ストリームソケットでは
  #     指定してはならない.
  def sendmsg message, flags = 0, dest_sockaddr = nil
    raise # TODO: impl.
  end


  def recvmsg
  end

end # class BasicSocket

