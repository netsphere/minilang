# -*- coding:utf-8 -*-

require "numeric"

# 複素数
class Complex < Numeric
  # 実部
  attr_reader :real

  # 虚部
  attr_reader :imag

  class << self
    def polar r, theta = 0
      raise # TODO:impl.
    end

    # 新しい複素数オブジェクトを生成する
    # @param [Numeric] real 実部
    # @param [Numeric] imag 虚部
    def rect real, imag = 0
      if !real.is_a?(Numeric) || real.is_a?(Complex)
        raise TypeError, "'real' must be a real number" 
      end
      if !imag.is_a?(Numeric) || imag.is_a?(Complex)
        raise TypeError, "'imag' must be a real number"
      end

      x = Complex.new
      x.instance_variable_set "@real", real
      x.instance_variable_set "@imag", imag
      x
    end
  end # class << self

end
