# -*- coding: utf-8 -*-


#require "basic_socket"


class SocketError < StandardError
end


class Socket

  class << self

    # @!method getaddrinfo nodename, servname, afamily = nil, socktype = nil, 
    #                      protocol = nil, flags = nil
    #     DNSで名前解決し, ホスト名からIPアドレスの組を得る
    #     RFC3493: Basic Socket Interface Extensions for IPv6 (Obsoletes 
    #     RFC2553)
    #     @return [Addrinfo]   検索結果の配列
    #         getaddrinfo(3) がエラーを返した場合は SocketError を投げる


    # @return [String]   ノード名, ポート名の2要素の配列
    def getnameinfo sockaddr, flags = 0
      raise # TODO: impl.
    end


    # Socketインスタンスではなく, サブクラスのインスタンスを生成する
    # connect() ていなくても close() しなければならないので, この時点でIOを生成する
    # socket(2) に対応.
    def open afamily, socktype, protocol = 0
      # UNIXドメインかつデータグラムという組み合わせも可能なのに, ruby 1.9には対応する
      # クラスがない
      case socktype
      when SOCK_STREAM
        return ::Socket::StreamSocketIO.new afamily, protocol
      when SOCK_DGRAM
        return ::Socket::DatagramSocketIO.new afamily, protocol
      else
        raise ArgumentError, "not supported socktype"
      end
    end


    def socketpair
      raise # TODO: impl.
    end


    # Sockaddrを生成して返す. IPv4
    def sockaddr_in port, host
      raise TypeError if !port.is_a?(Integer)
      raise TypeError if !host.is_a?(String)

      Socket::SockaddrIn.new port, host
    end

    
    # Sockaddrを生成して返す. UNIXドメインソケットアドレス
    def sockaddr_un path
      raise TypeError if !path.is_a?(String)

      Socket::SockaddrUn.new path
    end


    # Sockaddrを生成して返す. IPv6
    def sockaddr_in6 port, flowinfo, addr, scope_id
      raise TypeError if !port.is_a?(Integer)
      raise TypeError if !flowinfo.is_a?(Integer)
      raise TypeError if !addr.is_a?(String)
      raise TypeError if !scope_id.is_a?(Integer)

      Socket::SockaddrIn6.new port, flowinfo, addr, scope_id
    end

  end # << self


=begin
  # @return Socket
  def initialize afamily, socktype, protocol = 0
    ..........
  end
=end

end # class Socket


