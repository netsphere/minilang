﻿// -*- coding:utf-8-with-signature; mode:c++; tab-width:4; -*-

●●このファイルはビルド対象ではない


/*
  minilang -- a minilang interpreter.

  Copyright (c) 2011-2013 HORIKAWA Hisashi. All rights reserved.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// class Socket

#include "support.h"
#include "execute.h"
#ifdef _WIN32
......
#else
  #include <unistd.h> // close()
  #include <sys/types.h>
  #include <sys/socket.h>
  #include <netdb.h>
  #include "string.h" // memset()

  typedef int SOCKET;
  typedef struct addrinfo ADDRINFOEX;
  #define closesocket close
  #define INVALID_SOCKET (-1)
#endif // _WIN32

using namespace std;


struct CRB_SocketIO: public CRB_Object
{
    /** socket() したものは, connect() などしていなくても close() しなければならない */
    SOCKET sock_fd;

    CRB_SocketIO( CRB_Class* k ): 
        CRB_Object(k), /*socket(nullptr),*/ sock_fd(INVALID_SOCKET)
    {
    }

    ~CRB_SocketIO() {
        if ( sock_fd != INVALID_SOCKET ) {
            closesocket(sock_fd);
            sock_fd = INVALID_SOCKET;
            // socket->sock_fd = INVALID_SOCKET;
        }
    }
    
};


struct CRB_Sockaddr: public CRB_Object
{
    struct sockaddr_storage sockaddr;
    int sockaddr_len;

    CRB_Sockaddr( CRB_Class* k ): CRB_Object(k) {
        memset( sockaddr, 0, sizeof(sockaddr) );
    }
};


// これでは複数のインタプリタが作れない.
// static CRB_Class* class_sockaddr_in = nullptr;
// static CRB_Class* class_sockaddr_in6 = nullptr;


///////////////////////////////////////////////////////////////////////////
// class Socket

/**  
 * Socket.getaddrinfo
 *     RFC3493: Basic Socket Interface Extensions for IPv6 (Obsoletes RFC2553)
 */
static EvalResult socket_s_getaddrinfo( CRB_Thread* thread,
                                        LocalEnvironment& env,
                                        CRB_Value& receiver,
                                        const ScriptLocation& loc )
{
    CRB_Interpreter* const interp = thread->interp;

    ADDRINFOEX hints;
    memset( &hints, 0, sizeof(hints) );

    if ( crb_object_isa(interp, env["nodename"], interp->class_string) ) {
        return crb_raise_type_error( thread, env, loc, env["nodename"],
                                     "nodename", "String" );
    }
    UnicodeString* nodename = 
              static_cast<CRB_String*>(env["nodename"].u.object_value)->value;

    if ( crb_object_isa(interp, env["servname"], interp->class_string) ) {
        return crb_raise_type_error( thread, env, loc, env["servname"],
                                     "servname", "String" );
    }
    UnicodeString* servname = 
              static_cast<CRB_String*>(env["servname"].u.object_value)->value;
        
    if ( env["afamily"].type() != CRB_NIL_VALUE ) {
        if ( !crb_object_isa(interp, env["afamily"], interp->class_integer) ) {
            return crb_raise_type_error( thread, env, loc, env["afamily"],
                                     "afamily", "Integer" );
        }
        hints.ai_family = mpz_get_si( env["afamily"].u.int_value );
    }
    else
        hints.ai_family = AF_UNSPEC;

    if ( env["socktype"].type() != CRB_NIL_VALUE ) {
        if ( !crb_object_isa(interp, env["socktype"], interp->class_integer) ) {
            return crb_raise_type_error( thread, env, loc, env["socktype"],
                                     "socktype", "Integer" );
        }
        hints.ai_socktype = mpz_get_si( env["socktype"].u.int_value );
    }
    else
        hints.ai_socktype = 0;

    if ( env["protocol"].type() != CRB_NIL_VALUE ) {
        if ( !crb_object_isa(interp, env["protocol"], interp->class_integer) ) {
            return crb_raise_type_error( thread, env, loc, env["protocol"],
                                     "protocol", "Integer" );
        }
        hints.ai_protocol = mpz_get_si( env["protocol"].u.int_value );
    }
    else
        hints.ai_protocol = 0;

    if ( env["flags"].type() != CRB_NIL_VALUE ) {
        if ( !crb_object_isa(interp, env["flags"], interp->class_integer) ) {
            return crb_raise_type_error( thread, env, loc, env["flags"],
                                     "flags", "Integer" );
        }
        hints.ai_flags = mpz_get_si( env["flags"].u.int_value );
    }
    else 
        hints.ai_flags = AI_V4MAPPED | AI_ADDRCONFIG; // glibc. POSIXでは 0

    // サーバ側の時は, AI_PASSIVE を指定する. 戻り値のsocket address で bind() する
    // AI_PASSIVEを指定して, かつ hostname = nullptr にすると, 
    // INADDR_ANY (IPv4) or IN6ADDR_ANY_INIT (IPv6) 相当になる.
    // hints.ai_flags = AI_PASSIVE;

    ADDRINFOEX* result = nullptr;
#ifdef _WIN32
    int r = ::GetAddrInfoEX( hostname,
                             service,
                             NS_ALL,
                             nullptr,
                             &hints,
                             &result,
                             nullptr, // timeout
                             nullptr, // 非同期にする場合に指定
                             nullptr, nullptr );
#else
    string hostname, service;
    nodename->toUTF8String(hostname);
    servname->toUTF8String(service);
    int r = getaddrinfo( hostname.c_str(), service.c_str(), &hints, &result );
#endif // _WIN32
    if ( r != 0 ) {
        CRB_Value err(CRB_INT_VALUE);
        mpz_set_si( err.u.int_value, r ); // 戻り値がエラーコード
        return crb_runtime_error( thread, env, SYSTEM_CALL_ERROR, loc,
                                  _("getaddrinfo() failed"), {err} );
    }

    const ADDRINFOEX* ptr;
    for ( ptr = result; ptr != nullptr; ptr = ptr->ai_next ) {
        CRB_Value ao( new CRB_Object( 
                      crb_interp_find_class(interp, "::Socket::Addrinfo")) );

        assert( ptr->ai_family == AF_INET || ptr->ai_family == AF_INET6 );
        CRB_Sockaddr* addr = new CRB_Sockaddr( ptr->ai_family == AF_INET ? 
        crb_object_assign_ivar( ao, "addr", ptr->ai_sockaddr );
        crb_object_assign_ivar( ao, "afamily", ptr->ai_family );
        crb_object_assign_ivar( ao, "socktype", ptr->ai_socktype );
        crb_object_assign_ivar( ao, "protocol", ptr->ai_protocol );

        EvalResult r = crb_method_call( thread, env, ao, "initialize", &args, 
                                        loc, false );
        if ( r.type == EXCEPTION_EVAL_RESULT ) {
            delete ao.u.object_value;
            return r;
        }
        if ( ptr->ai_canonname ) {
            // 内部でreferされない
            crb_object_assign_ivar("canonname", ptr->ai_canonname );
        }
    }

#ifdef _WIN32
    ::FreeAddrInfoEx( result );
#else
    freeaddrinfo( result );
#endif // _WIN32
}


///////////////////////////////////////////////////////////////////////////
// class Socket::Sockaddr

static EvalResult sockaddr_s_parse( CRB_Thread* thread,
                                    LocalEnvironment& env,
                                    CRB_Value& receiver,
                                    const ScriptLocation& loc )
{
    if ( crb_object_isa(
    CRB_ByteArary* bytes = env["bytes"];
}


///////////////////////////////////////////////////////////////////////////
// class Socket::SocketIO

/** SocketIO.new */
static EvalResult socketio_s_new( CRB_Thread* thread,
                                  LocalEnvironment& env,
                                  CRB_Value& receiver,
                                  const ScriptLocation& loc )
{
    return crb_class_create_instance<CRB_SocketIO>( thread, receiver, 
                          {env["afamily"], env["socktype"], env["protocol"]},
                          nullptr, loc );
}


static EvalResult socketio_initialize( CRB_Thread* thread,
                                       LocalEnvironment& env, 
                                       CRB_Value& receiver,
                                       const ScriptLocation& loc )
{
    CRB_SocketIO* self = static_cast<CRB_SocketIO*>(receiver.u.object_value);

    self->sock_fd = ::socket( env["afamily"], env["socktype"], env["protocol"] );
    if ( sock->sock_fd < 0 ) {
        .......error;
    }

    return EvalResult( CRB_NIL_VALUE );
}


/** Socket#connect  クライアントとして接続 */
static EvalResult socket_connect( CRB_Thread* thread,
                                        CRB_Value& receiver,
                                        CRB_ValueList& args,
                                        const ScriptLocation& loc )
{
    CRB_Socket* sock = static_cast<CRB_Socket*>(receiver.u.object_value);

    int r = ::connect( sock->sock_fd, sockaddr, sockaddr_len );
    if ( r != 0 ) {
        .......error;
    }

    CRB_SocketIO* sio = new CRB_SocketIO( interp-> class_socket_io );
    

    return EvalResult( CRB_NIL_VALUE );
}


void class_socket_add_methods( CRB_Thread* thread, LocalEnvironment& env )
{
#ifdef _WIN32
    WSADATA wsadata;
    int r = WSAStartup( MAKEWORD(2, 2), &wsadata );
    if ( r != 0 ) {
        abort();
    }
#endif // _WIN32

    ///////////////////////////////////////////////////////////////////////
    // class Socket

    CRB_Class* class_socket = nullptr;
    crb_interp_create_class( thread, env, "::Socket", 
                             interp->class_object,
                             ScriptLocation(), &class_socket );
    assert( class_socket );
    CRB_Class* socket_s = crb_object_create_singleton_class( interp, 
                                                             class_socket );

    CRB_add_native_function( interp, socket_s, "getaddrinfo", 
                             {"nodename", "servname", "?afamily", "?socktype", 
                                     "?protocol", "?flags"},
                             socket_s_getaddrinfo );


    ///////////////////////////////////////////////////////////////////////
    // class Socket::Sockaddr

    CRB_Class* class_sockaddr = nullptr;
    crb_interp_create_class( thread, env, "::Socket::Sockaddr",
                             interp->class_object,
                             ScriptLocation(), &class_sockaddr );
    assert( class_sockaddr );

    CRB_add_native_function( interp, 
                             crb_object_create_singleton_class(interp, 
                                                               class_sockaddr),
                             "parse", {"bytes"},
                             sockaddr_s_parse );

                             
    ///////////////////////////////////////////////////////////////////////
    // class Socket::SocketIO

    CRB_Class* class_socket_io = nullptr;
    crb_interp_create_class( thread, env, "::BasicSocket", 
                             interp->class_io,
                             ScriptLocation(), &class_socket_io );
    CRB_Class* socketio_s = crb_object_create_singleton_class( interp, 
                                                             class_socket_io );

    CRB_add_native_function( interp, socketio_s, "new", 
                             {"afamily", "socktype", "protocol"},
                             socketio_s_new );

    CRB_add_native_function( interp, class_socket_io, "initialize", 
                             {"afamily", "socktype", "protocol"}, 
                             socketio_initialize );

    CRB_add_native_function( interp, class_socket_io, "close", { }, 
                             socketio_close );

    CRB_add_native_function( interp, class_socket_io, 
                             "send", {"bytes", "flags"}, 
                             socketio_write );

    CRB_add_native_function( interp, class_socket_io, "recv", 
                             {"max_length", "flags"}, 
                             socketio_read );

    ///////////////////////////////////////////////////////////////////////
    // class Socket::StreamSocketIO

    CRB_Class* klass = nullptr;
    crb_interp_create_class( thread, env, "::Socket::StreamSocketIO", 
                             class_socket_io,
                             ScriptLocation(), &klass );
    ..........;
}


// Local variables:
// tab-width: 4
// c-basic-offset: 4
// indent-tabs-mode: nil
// End:
