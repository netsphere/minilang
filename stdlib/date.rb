# -*- coding:utf-8 -*-

require "comparable"
require "time/duration"

# 日付クラス. 時刻の h:m:s = 0 とは意味が異なる
# いかなるタイムゾーンでも, 現地時刻で0:00 - 23:59を意味する. 
#   => ある日付は, 時刻でいえば48時間ぐらいを含みうる
class Date
  include Comparable

  attr_reader :year, :mon, :day

  # 日付に加算する
  # @param [Integer, Time::Duration] other    加算する日数.
  # @return [Date] new Date object promoted 'other' days.
  def + other
    raise TypeError if !other.is_a?(Integer) && !other.is_a?(Time::Duration)

    raise # TODO: impl.
  end

  
  def - other
    raise TypeError if !other.is_a?(Date)

    raise # TODO: impl.
  end
end
