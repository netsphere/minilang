# -*- coding:utf-8 -*-

require "numeric"


# 有理数
class Rational < Numeric
  # 分母
  attr_reader :denominator

  # 分子
  attr_reader :numerator

  def initialize nume, deno = 1
    @denominator = deno
    @numerator = nume
  end

end # class Rational

