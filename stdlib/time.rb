# -*- coding: utf-8 -*-

require "comparable"
require "time/duration"

# time with timezone [incompat]
# 時刻クラス. 他のプログラミング言語/ライブラリでは Timestamp とか Date と呼ばれるもの
# 昔からグレゴリオ暦だと仮定
# Time objest has a real time internally. And, zone determines the appearance time.
# APIはマイクロ秒単位。
class Time
  include Comparable

  attr_reader :year, :mon, :day, :hour, :min, :sec, :usec

  # timezone
  attr_reader :zone

  # 別名.
  # def mday; day end


  class << self
    def utc year, mon, day, hour, min, sec = 0, usec = 0
      return local(year, mon, day, hour, min, sec, usec, :zone => "UTC")
    end

    # 時刻オブジェクトを生成する。zoneを省略した場合、default timezone を使用する
    # @param [Hash] options  オプションを指定.
    # @option options [String] :zone    timezone [incompat]
    def local year, mon, day, hour, min, sec, usec = 0, options = {}
      # gettimeofday() is deprecated. マイクロ秒 (1/10^6秒) 単位
      # But, clock_gettime() -- ナノ秒 (1/10^9秒) 単位 -- isn't implemented in MacOS.
      include <time.h>
      timespec tv;
      clock_gettime( CLOCK_REALTIME, &tv );
      .......
    end

    # if zone is omitted, use default timezone.
    def now zone = nil
      ........
    end
  end # class << self


  # return new Time object promoted 'other' seconds.
  def + other
    raise TypeError if !other.is_a?(Integer) && !other.is_a?(Time::Duration)
    .........
  end


  # @return [Time::Duration] 時刻の差 (秒単位)
  def - other
    raise TypeError if !other.is_a?(Time)
    ......
  end


  # 時刻同士を比較する
  # @param [Time] other  比較対象
  def <=> other
    raise TypeError if !other.is_a?(Time)
    .........
  end


  def getutc
    return getlocal("UTC")
  end

  # create new Time object with 'zone' timezone
  def getlocal zone = nil
    ......
  end


  def utc?
    return zone == "UTC"
  end


  def zone= new_zone
    # TODO: error check
    ...........
  end

  def utc
    self.zone = "UTC"
  end


  # for compat.
  def localtime new_zone = nil
    self.zone = new_zone ? new_zone : default_timezone
  end
end

