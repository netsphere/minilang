
// internal header.

#ifndef PRIVATE_DBG_H_INCLUDED
#define PRIVATE_DBG_H_INCLUDED

#include <stdio.h>
#include "DBG.h"

#ifdef __cplusplus
extern "C" {
#endif

struct DBG_Controller_tag {
    FILE        *debug_write_fp;
    int         current_debug_level;
};

typedef DBG_Controller_tag* DBG_Controller;

#ifdef __cplusplus
}
#endif

#endif /* PRIVATE_DBG_H_INCLUDED */
