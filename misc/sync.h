﻿// -*- coding:utf-8-with-signature; mode:c++; tab-width:4; -*-

/*
  minilang -- a minilang interpreter.

  Copyright (C) 2011 HORIKAWA Hisashi. All rights reserved.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef SYNC_H__
#define SYNC_H__ 1


#ifdef _WIN32
.......
#else
  #define _XOPEN_SOURCE 700
  #define _POSIX_C_SOURCE 200809L  
  #undef _REENTRANT
  #include <pthread.h>
#endif // _WIN32
#include "noncopyable.h"


/** 
 * 再入可能なクリティカルセクション 
 *     See http://www.tsoftware.jp/nptl/
 */
class Sync: private noncopyable
{
#ifdef _WIN32
    CRITICAL_SECTION cs;
#else
    pthread_mutex_t mutex;
#endif // _WIN32

public:

#ifdef _WIN32
    Sync() 
    {
#ifdef NDEBUG
        // See http://msdn.microsoft.com/en-us/library/windows/desktop/ms686908(v=vs.85).aspx
        BOOL r = ::InitializeCriticalSectionEx( &cs, 1500, 
                                             CRITICAL_SECTION_NO_DEBUG_INFO );
#else
        BOOL r = ::InitializeCriticalSectionEx( &cs, 1500, 0 );
#endif // NDEBUG

        if (!r) {
#ifndef NDEBUG
            printf( "InitializeCriticalSectionEx() failed: %d\n", 
                    GetLastError() );
#endif // !NDEBUG
            abort();
        }
    }

    ~Sync() {
        DeleteCriticalSection(&cs);
    }

    void lock() {
        EnterCriticalSection(&cs);
    }

    void unlock() {
        LeaveCriticalSection(&cs);
    }

#else // _WIN32
    Sync() 
    {
        pthread_mutexattr_t attr;
        pthread_mutexattr_init(&attr);
        pthread_mutexattr_settype( &attr, PTHREAD_MUTEX_RECURSIVE );
        
        pthread_mutex_init( &mutex, &attr );
    }

    ~Sync() {
        pthread_mutex_destroy(&mutex);
    }

    void lock() {
        pthread_mutex_lock(&mutex);
    }

    void unlock() {
        pthread_mutex_unlock(&mutex);
    }
#endif // _WIN32

};


#endif


// Local variables:
// tab-width: 4
// c-basic-offset: 4
// indent-tabs-mode: nil
// End:
