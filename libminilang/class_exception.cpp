﻿// -*- coding:utf-8-with-signature; mode:c++; tab-width:4; -*-

/*
  minilang -- a minilang interpreter.

  Copyright (c) 2011-2013 HORIKAWA Hisashi. All rights reserved.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// 例外クラス

#include "support.h"
#include "execute.h"


struct LocalJumpError: public CRB_Object
{
    LocalJumpError( CRB_Class* klass ): 
        CRB_Object(klass), exit_value(CRB_NIL_VALUE) /*, reason(CRB_NIL_VALUE)*/ { 
    }

    ~LocalJumpError() {
        crb_object_release( &exit_value );
        // crb_object_release( &reason );
    }

    CRB_Value exit_value;

    // インスタンス変数に
    // CRB_Value reason;
};


//////////////////////////////////////////////////////////////////
// Exception

/** prelude でも例外が起こる可能性があるので, ネイティブで定義する */
static EvalResult exception_initialize( CRB_Thread* thread,
                                        LocalEnvironment& env,
                                        CRB_Value& receiver,
                                        const ScriptLocation& loc )
{
    CRB_Interpreter* const interp = thread->interp;
    CRB_Object* const self = receiver.u.object_value;

    if ( env["message"].type() != CRB_NIL_VALUE &&
         !crb_object_isa(interp, env["message"], interp->class_string ) ) {
        crb_raise_type_error( thread, env, loc,
                              env["message"], "message", "String" );
    }

    if ( env["message"].type() == CRB_NIL_VALUE ) {
        crb_object_assign_ivar( self, "message", 
                            crb_string_new(interp, new icu::UnicodeString()) );
    }
    else {
        crb_object_assign_ivar( self, "message", 
                                crb_object_refer(env["message"]) );
    }

    return EvalResult( CRB_NIL_VALUE );
}


//////////////////////////////////////////////////////////////////
// LocalJumpError

static EvalResult local_jump_error_s_new( CRB_Thread* thread,
                                          LocalEnvironment& env,
                                          CRB_Value& receiver,
                                          const ScriptLocation& loc )
{
    return crb_class_create_instance<LocalJumpError>( thread, env, receiver, 
                                            {env["message"], env["reason"]},
                                            nullptr, loc );
}


//////////////////////////////////////////////////////////////////

void class_exception_add_methods( CRB_Thread* thread, LocalEnvironment& env )
{
    CRB_Interpreter* const interp = thread->interp;

    ///////////////////////////////////////////////////////////////////
    // class Exception

    crb_interp_create_class( thread, env,
                             CRB_EXCEPTION_CLASS,
                             interp->class_object,
                             ScriptLocation(), &interp->class_exception );

    CRB_add_native_function( interp, 
                             interp->class_exception,
                             "initialize", {"?message"},
                             exception_initialize );

    ///////////////////////////////////////////////////////////////////

    // rescueでクラスを省略した場合に捕捉される. 実行時エラーの基底クラス.
    // 名前がおかしい
    crb_interp_create_class( thread, env,  
                             CRB_STANDARD_ERROR,
                             interp->class_exception, ScriptLocation(), 
                             &interp->class_standard_error );

    crb_interp_create_class( thread, env, ARGUMENT_ERROR, 
                             interp->class_standard_error, 
                             ScriptLocation(), nullptr );
    crb_interp_create_class( thread, env, REGEXP_ERROR, 
                             interp->class_standard_error, 
                             ScriptLocation(), nullptr );
    crb_interp_create_class( thread, env, TYPE_ERROR, 
                             interp->class_standard_error, 
                             ScriptLocation(), nullptr );

    // raise でクラスを省略した場合に投げられる. その他の実行時エラー.
    crb_interp_create_class( thread, env,  
                             CRB_RUNTIME_ERROR, 
                             interp->class_standard_error, ScriptLocation(), 
                             &interp->class_runtime_error );

    crb_interp_create_class( thread, env, SYSTEM_CALL_ERROR, 
                             interp->class_standard_error, 
                             ScriptLocation(), nullptr );
    crb_interp_create_class( thread, env, STOP_ITERATION, 
                             interp->class_standard_error,
                             ScriptLocation(), nullptr );

    // CRB_Class* script_err = crb_interp_create_class( interp, CRB_SCRIPT_ERROR,
    // exception );
    crb_interp_create_class( thread, env, CRB_LOAD_ERROR, 
                             interp->class_standard_error,
                             ScriptLocation(), nullptr );
    crb_interp_create_class( thread, env, NO_METHOD_ERROR, 
                             interp->class_standard_error,
                             ScriptLocation(), nullptr );


    //////////////////////////////////////////////////////////////
    // LocalJumpError

    crb_interp_create_class( thread, env, 
                             LOCAL_JUMP_ERROR, 
                             interp->class_standard_error,
                             ScriptLocation(), 
                             &interp->class_local_jump_error );

    CRB_add_native_function( interp, 
                             crb_object_create_singleton_class(interp, interp->class_local_jump_error),
                             "new", {"?message", "reason"},
                             local_jump_error_s_new );

/*
    CRB_add_native_function( interp, interp->class_local_jump_error, "initialize", 
                             {"?message"},
                             local_jump_error_initialize );
*/

    //////////////////////////////////////////////////////////////
    
    crb_interp_create_class( thread, env, CRB_NAME_ERROR, 
                             interp->class_standard_error, 
                             ScriptLocation(), nullptr );
    
    // [incompat] RubyではStandardErrorではなく ScriptError のサブクラス.
    crb_interp_create_class( thread, env, NOT_IMPLEMENTED_ERROR, 
                             interp->class_standard_error, 
                             ScriptLocation(), nullptr );

    // [incompat]
    crb_interp_create_class( thread, env, FUNCTION_MULTIPLE_DEFINE_ERR, 
                             interp->class_standard_error,
                             ScriptLocation(), nullptr );
}



// Local variables:
// tab-width: 4
// c-basic-offset: 4
// indent-tabs-mode: nil
// End:
