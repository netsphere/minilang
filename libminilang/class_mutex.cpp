﻿// -*- coding:utf-8-with-signature; mode:c++; tab-width:4; -*-

/*
  minilang -- a minilang interpreter.

  Copyright (c) 2011-2013 HORIKAWA Hisashi. All rights reserved.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


// クリティカルセクション

#include "support.h"
#include "sync.h"
#include "execute.h"

using namespace std;


struct CRB_Mutex: public CRB_Object
{
    Sync sync;

    CRB_Mutex( CRB_Class* klass ): CRB_Object(klass) { }
};


static EvalResult mutex_s_new( CRB_Thread* thread,
                               LocalEnvironment& env,
                               CRB_Value& receiver,
                               const ScriptLocation& loc )
{
    return crb_class_create_instance<CRB_Mutex>( thread, env, receiver, { },
                                                 nullptr, loc );
}


/*
static EvalResult mutex_initialize( CRB_Thread* thread,
                                    LocalEnvironment& env,
                                    CRB_Value& receiver,
                                    const ScriptLocation& loc )
{
#ifdef _WIN32
    mutex = ::CreateMutexEx( nullptr, nullptr, 0, 0 );
    .......;
#else
    ..........;
#endif // _WIN32
}
*/


static EvalResult mutex_lock( CRB_Thread* thread,
                              LocalEnvironment& env,
                              CRB_Value& receiver,
                              const ScriptLocation& loc )
{
    CRB_Mutex* self = static_cast<CRB_Mutex*>(receiver.u.object_value);
    self->sync.lock();

    return EvalResult( CRB_NIL_VALUE );
}


static EvalResult mutex_unlock( CRB_Thread* thread,
                                LocalEnvironment& env,
                                CRB_Value& receiver,
                                const ScriptLocation& loc )
{
    CRB_Mutex* self = static_cast<CRB_Mutex*>(receiver.u.object_value);
    self->sync.unlock();

    return EvalResult( CRB_NIL_VALUE );
}


static EvalResult mutex_synchronize( CRB_Thread* thread,
                                     LocalEnvironment& env,
                                     CRB_Value& receiver,
                                     const ScriptLocation& loc )
{
    // CRB_Interpreter* const interp = thread->interp;
    CRB_Mutex* self = static_cast<CRB_Mutex*>(receiver.u.object_value);

/* #callを持っていれば何でもいい
    if ( !crb_object_isa(interp, env["block"], interp->class_proc) ) {
        return crb_raise_type_error( thread, env, loc, env["block"],
                                     "block", "Proc" );
    }
*/
    self->sync.lock();
    EvalResult result = crb_method_call( thread, env, env["block"], "call",
                                         nullptr, //nullptr,
                                         loc, false );
    self->sync.unlock();

    return result;
}


void class_mutex_add_methods( CRB_Thread* thread, LocalEnvironment& env )
{
    CRB_Interpreter* const interp = thread->interp;

    CRB_Class* klass;
    crb_interp_create_class( thread, env, "::Mutex",
                             interp->class_object, ScriptLocation(), &klass );
    assert( klass );

    // singleton methods
    CRB_add_native_function( interp,
                             crb_object_create_singleton_class(interp, klass),
                             "new", { },
                             mutex_s_new );

    // instance methods
/*
    CRB_add_native_function( interp, klass, "initialize", { },
                             mutex_initialize );
*/
    CRB_add_native_function( interp, klass, "lock", { }, mutex_lock );
    CRB_add_native_function( interp, klass, "unlock", { }, mutex_unlock );
    CRB_add_native_function( interp, klass, "synchronize", {"&block"},
                             mutex_synchronize );
}


// Local variables:
// tab-width: 4
// c-basic-offset: 4
// indent-tabs-mode: nil
// End:
