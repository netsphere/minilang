﻿// -*- coding:utf-8-with-signature; mode:c++; tab-width:4; -*-

/*
  minilang -- a minilang interpreter.

  Copyright (c) 2011-2013 HORIKAWA Hisashi. All rights reserved.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#if 0
struct CRB_IO: public CRB_Object
{
    // インスタンス変数へ移動
    // bool binary_mode;

    /** 
     * modeがテキストモードで,
     *     かつext_encが指定されているとき => 読み込み時に変換
     *     ext_encが指定されていないとき => UTF-8と仮定して変換
     * modeがバイナリモード    => ByteString を返す
     */
    UnicodeString ext_enc;

    /** int_enc が指定されているとき, read() などは ByteString を返す */
    // UnicodeString int_enc;

    CRB_IO( CRB_Class* klass ): CRB_Object(klass) { 
    }

    virtual ~CRB_IO()
    { }
};
#endif // 0




// Local variables:
// tab-width: 4
// c-basic-offset: 4
// indent-tabs-mode: nil
// End:
