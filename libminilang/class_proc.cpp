﻿// -*- coding:utf-8-with-signature; mode:c++; tab-width:4; -*-

/*
  minilang -- a minilang interpreter.

  Copyright (c) 2011-2013 HORIKAWA Hisashi. All rights reserved.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "support.h"
#include "CRB.h"
#include "DBG.h"
#include "execute.h"
#include "eval.h"
#include "ast.h"
#include <cstring>
#include <cassert>
#include <unicode/ustdio.h>

using namespace std;
using namespace icu;


CRB_Proc::CRB_Proc( CRB_Interpreter* interp ):
    CRB_Object(interp->class_proc), outer_env(nullptr) {
    // printf("%s ctor\n", __func__); // DEBUG
}


/**
 * Proc.new {...}
 * 実引数の評価の時点ですでにクロージャを作っているので, どうしてくれよう？
 */
static EvalResult proc_s_new( CRB_Thread* thread,
                              LocalEnvironment& env,
                              CRB_Value& receiver,
                              const ScriptLocation& loc )
{
    CRB_Interpreter* const interp = thread->interp;

    if ( !crb_object_isa(interp, env["block"], interp->class_proc) ) {
        return crb_raise_type_error( thread, env, loc, env["block"],
                                     "block", "Proc" );
    }

    return EvalResult( crb_object_refer(env["block"]) );
}


/*
static EvalResult proc_initialize( CRB_Thread* thread,
                                    CRB_Value& receiver,
                                    CRB_ValueList& args,
                                    const ScriptLocation& loc )
{
    ............;
}
*/


// Proc#call(*args, &block)
static EvalResult class_proc_call( CRB_Thread* thread,
                                   LocalEnvironment& env_in_call,
                                   CRB_Value& receiver,
                                   const ScriptLocation& loc )
{
    printf("%s enter.\n", __func__); // DEBUG

    CRB_Proc* self = static_cast<CRB_Proc*>(receiver.u.object_value);

    thread->stack_trace.push_back( CRB_Caller(&env_in_call, loc) );

    // 起動のたびに環境を作る。これでRubyと同じ。
    // "self"は, クロージャを作った場所でのself. outer_env のをそのまま使えばOK
    LocalEnvironment* local_env = new LocalEnvironment( self->outer_env );

    // super() 用には, レキシカルな外側のメソッドをそのまま用いる
    local_env->klass = self->outer_env->klass;
    local_env->method_name = self->outer_env->method_name;
    local_env->node = self->expr;

    CRB_Array* args = static_cast<CRB_Array*>(env_in_call["args"].u.object_value);
    EvalResult argresult = env_set_vars_by_param_and_args( thread,
                                                       *local_env,
                                                       self->expr->param_list,
                                                       args->list,
                                                       &env_in_call["block"],
                                                       loc );
    if ( argresult.type == EXCEPTION_EVAL_RESULT ) {
        thread->stack_trace.pop_back();
        local_env->release();
        return argresult;
    }
    assert( argresult.type == NORMAL_EVAL_RESULT );
    crb_object_release( &argresult.value );

#if 0
    VariableMap::iterator kk = local_env->variables.begin();
    UFILE* out = u_finit(stdout, nullptr, nullptr );

    for ( ; kk != local_env->variables.end(); kk++) {
        u_fprintf( out, "%S:%d (ref_count=%d) ",
             const_cast<UnicodeString&>(kk->first).getTerminatedBuffer(),
                   kk->second.type(),
                   kk->second.type() == CRB_OBJECT_VALUE ?
                   	     kk->second.u.object_value->ref_count : 0);
    }
    printf("\n");

    u_fclose(out);
#endif // !NDEBUG

    // next はクロージャからの単なる脱出,
    // return はレキシカルな*外側の*メソッドからの脱出. クラス/モジュール定義の中だとエラーに
    // する
    EvalResult result = crb_execute_statement_list( thread, *local_env,
                             self->expr->block,
                             //CAN_RETURN_MASK | CAN_NEXT_MASK | CAN_BREAK_MASK,
                             true );

    thread->stack_trace.pop_back();
    local_env->release();

    switch ( result.type ) {
    case EXCEPTION_EVAL_RESULT:
    case RETURN_EVAL_RESULT:  // return は突き抜ける
    case BREAK_EVAL_RESULT:   // break は突き抜ける
        return result;

    case NEXT_EVAL_RESULT:
        result.type = NORMAL_EVAL_RESULT;
        break;
    case NORMAL_EVAL_RESULT:
        break;
    }

/* // crb_call_function() と重複
    // for tail call
    while ( result.value.type() == CRB_TAIL_CALL_VALUE ) {
        EvalResult x = crb_method_call( thread, env_in_call,
                              result.value.u.tail_call->receiver,
                              result.value.u.tail_call->method_name,
                              &result.value.u.tail_call->arg_values,
                              &result.value.u.tail_call->block_arg,
                              result.value.u.tail_call->caller_loc );
        // crb_value_list_free_objects( &result.value.u.tail_call->arg_values );
        crb_object_release( &result.value );
        result = x;
    }
*/

    return result;
}


void class_proc_add_methods( CRB_Thread* thread, LocalEnvironment& env )
{
    CRB_Interpreter* const interp = thread->interp;

    crb_interp_create_class( thread, env, "::Proc",
                             interp->class_object, ScriptLocation(),
                             &interp->class_proc );

    // singleton methods
    CRB_add_native_function( interp,
               crb_object_create_singleton_class(interp, interp->class_proc),
               "new", {"&block"},
               proc_s_new );

    // instance methods
/*
    CRB_add_native_function( interp, klass, "initialize", {"&block"},
                             proc_initialize );
*/
    CRB_add_native_function( interp, interp->class_proc, "call",
                             {"*args", "&block"},
                             class_proc_call );
}



// Local variables:
// tab-width: 4
// c-basic-offset: 4
// indent-tabs-mode: nil
// End:
