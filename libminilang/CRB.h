﻿// -*- coding:utf-8-with-signature; mode:c++; tab-width:4; -*-
// SPDX-License-Identifier: LGPL-3.0-or-later
/*
  minilang -- A small programming language, minilang interpreter.
  Copyright (c) 2011-2013, 2023 Netsphere Laboratories, Hisashi Horikawa.
  All rights reserved.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef PUBLIC_CRB_H_INCLUDED
#define PUBLIC_CRB_H_INCLUDED

// 公開インターフェイス. すべて接頭辞CRB_を持つ.

#ifdef _WIN32
.......
#else
  #define _XOPEN_SOURCE 700
  #define _POSIX_C_SOURCE 200809L
  #undef _REENTRANT
  #undef _GNU_SOURCE
  #include <pthread.h>
#endif // _WIN32

#define GC_THREADS 1
#include <gc/gc_cpp.h>

#include <vector>
#include "ptr_map.h"
#include <unicode/unistr.h>
#include <gmp.h>
#include <cassert>
#include "noncopyable.h"


struct CRB_Class;
struct CRB_Interpreter;
struct CRB_Value;


typedef std::map<icu::UnicodeString, CRB_Value> VariableMap;

void crb_varmap_free_objects( VariableMap* list );

/**
 * 組み込みクラス ::Object
 * gc から派生させるとデストラクタは呼び出されない。
 * See http://www.nminoru.jp/~nminoru/programming/boehm-gc3.html
 *
 * TODO: 循環参照以外で, まずメモリリークをすべて潰す. その後でgcを有効にする
 */
struct CRB_Object: /*public gc_cleanup,*/ private noncopyable
{
    /** 参照カウンタ. atomicに加減算すること */
    int32_t ref_count;

    /** クラス. 参照するだけ. 所有しない. */
    CRB_Class* m_class;

    /**
     * 特異クラス.
     * Rubyではオブジェクトに特異メソッド (object-specialized method) を付けら
     * れる. クラスにはクラス変数がある.
     *  - オブジェクトのクラスにサブクラスとして特異クラスを追加する.
     *  - クラス変数は, 特異クラスではなく, 別の方法で実装する.
     **/
    CRB_Class* singleton_class;

    /** インスタンス変数. 先頭の "@" は省略する */
    VariableMap ivars;

    /** create_instance 用 */
    CRB_Object( CRB_Class* klass ):
        ref_count(1), m_class(klass), singleton_class(nullptr) { }

    virtual ~CRB_Object();
};


struct Function;
struct CRB_Module;

/** メソッド名 => メソッドの実体 */
typedef ptr_map<icu::UnicodeString, Function*> CRB_MethodMap;


// Class と Module の共通クラス.
// インスタンス変数を定義する
struct CRB_ClassDescription: public CRB_Object
{
    /**
     * モジュール名. 名前は本来は変数側だが、名前空間を兼ねるため, こちらで持つ.
     * "C::D" の形. 先頭の"::"は省略する
     */
    icu::UnicodeString name;

    /** インスタンスメソッド. */
    CRB_MethodMap methods;

    /** 参照するだけ.
        ruby 1.9ではModuleが保有. class を include することはできない. */
    std::vector<CRB_Module*> included_modules;

    // クラスメソッドはない。Ruby はクラスもオブジェクトなので、クラスオブジェ
    // クトのクラス (metaclass) にインスタンスメソッドを生やす.

    // クラス変数は、クラスのクラス (metaclass) のインスタンス変数ではなく、
    // モジュールインスタンスが持つ。MRO (Method Resolution Order) で辿る.
    VariableMap classvars;

    CRB_ClassDescription( CRB_Class* klass ): CRB_Object(klass) { }

    CRB_ClassDescription( CRB_Class* klass, const icu::UnicodeString& mod_name):
                CRB_Object(klass) {
        name = mod_name.startsWith("::") ? icu::UnicodeString(mod_name, 2) :
                                           mod_name;
    }

    virtual ~CRB_ClassDescription();
};

// Ruby: - メソッドを定義できる. 名前空間ではなく, インタフェイスと考える.
//       - `is_a?()` もクラスと同様に可能.
//       - インスタンス変数も定義できる!  -> インタフェイスですらない。
struct CRB_Module: public CRB_ClassDescription
{
    /** create_instance用 */
    CRB_Module( CRB_Class* klass ): CRB_ClassDescription(klass) { }

    /**
     * @param mod_name  フルパスのモジュール名. 呼び出す側がフルパスにすること
     */
    CRB_Module( CRB_Class* k, const icu::UnicodeString& mod_name ):
        CRB_ClassDescription(k, mod_name) {
    }

    virtual ~CRB_Module() { }
};


#define CRB_RANGE_CLASS      "::Range"
#define CRB_REGEXP_CLASS     "::Regexp"


struct EvalResult;
struct CRB_Thread;
struct LocalEnvironment;
struct ScriptLocation;

/** 新たにクラスを生成して, 定数として登録する */
EvalResult crb_interp_create_class( CRB_Thread* thread,
                                    LocalEnvironment& env,
                                    const icu::UnicodeString& class_name,
                                    CRB_Class* const superclass,
                                    const ScriptLocation& loc,
                                    CRB_Class** newclass );

/** 新たにモジュールを生成して, 定数として登録する */
EvalResult crb_interp_create_module( CRB_Thread* thread,
                                      LocalEnvironment& env,
                                      const icu::UnicodeString& mod_name,
                                      const ScriptLocation& loc,
                                      CRB_Module** newmodule );

/** 組み込みクラス, またはフルパスでクラスを検索する */
CRB_Class* crb_interp_find_class( CRB_Interpreter* interp,
                                  const icu::UnicodeString& class_name );


/**
 * 組み込みクラス `::Class`
 * [[incompat]] モジュールのサブクラスは止める. Smalltalk は Behavior クラスがある
 */
struct CRB_Class: public CRB_ClassDescription
{
    /**
     * スーパークラス. 参照するだけ。解放しない.
     * BasicObject では nullptr
     */
    CRB_Class* superclass;

    /** create_instance用 */
    CRB_Class( CRB_Class* klass ): CRB_ClassDescription( klass ) { }

protected:
    /**
     * construct_type_system() で使う.
     * 通常はこのコンストラクタではなく crb_interp_create_class() を呼出すこと。
     * @param klass 通常は interp->class_class を与えること.
     *              クラスは `Class` クラスのインスタンス.
     */
    CRB_Class( CRB_Class* klass,
               const icu::UnicodeString& class_name,
               CRB_Class* super );
public:
    virtual ~CRB_Class() {
        // printf("free class: %s\n", name_.c_str() ); // DEBUG
    }

    // 最初の型システムを生成.
    friend void construct_type_system( CRB_Interpreter* interp );

    // クラスを生成する
    friend EvalResult crb_interp_create_class( CRB_Thread* thread,
                                LocalEnvironment& env,
                                const icu::UnicodeString& class_name,
                                CRB_Class* const superclass,
                                const ScriptLocation& loc,
                                CRB_Class** newclass );

    // 特異クラスを生成する
    friend CRB_Class* crb_object_create_singleton_class( CRB_Interpreter* interp,
                                                     CRB_Object* v );
};


/** 組み込みクラス ::String */
struct CRB_String: CRB_Object
{
    /** 所有する */
    icu::UnicodeString* value;

    // bool is_literal;

    /** create_instance 用 */
    CRB_String( CRB_Class* klass ):
        CRB_Object(klass), value(nullptr) /*, is_literal(true)*/ { }

    CRB_String( CRB_Interpreter* interp, icu::UnicodeString* s );

    ~CRB_String() {
        // printf("%s dtor\n", __func__); // DEBUG
        //if (!is_literal) {
            // MEM_free(value);
        delete value;
        // }
    }

    std::string std_string() const {
        std::string result;
        value->toUTF8String(result);
        return result;
    }
};


/** 埋め込み型かオブジェクト型か */
enum CRB_ValueType
{
    CRB_BOOLEAN_VALUE = 1,
    CRB_INT_VALUE,
    CRB_DOUBLE_VALUE,  // Float class
    CRB_OBJECT_VALUE,
    CRB_NIL_VALUE,
    CRB_SYMBOL_VALUE,

    CRB_TAIL_CALL_VALUE,

    // 未初期化
    //undefined_value,

    //CRB_RETURN_VALUE,      これは冗長では?
};


struct CRB_TailCall;
//struct CRB_ReturnValue;


/** オブジェクトの値 */
struct CRB_Value
{
private:
    /** int_value とそれ以外の種類を変更してはいけない. */
    CRB_ValueType type_;

public:
    union {
        // 大きさが小さくてimmutableなオブジェクトは埋め込む
        bool boolean_value;

        // 32bit環境では96bit, 64bit環境では128bit.
        mpz_t int_value;

        /** 8バイト. mpz_t より短いので, こちらも埋め込む */
        double double_value;

        /** オブジェクトは複数の変数から共有されることもある */
        CRB_Object* object_value;

        uint32_t symbol_value;

        CRB_TailCall* tail_call;
        //CRB_ReturnValue* return_value;
    } u;

    CRB_Value( const CRB_ValueType& type ): type_(type) {
        if ( type_ == CRB_INT_VALUE )
            mpz_init( u.int_value );
    }

    CRB_Value( const CRB_Value& v ): type_(v.type_) {
        if ( v.type_ == CRB_INT_VALUE )
            mpz_init_set(u.int_value, v.u.int_value);
        else
            u = v.u;
    }

    CRB_Value( CRB_Object* obj ): type_(CRB_OBJECT_VALUE) {
        assert( obj );
        // referしない
        u.object_value = obj;
    }

    ~CRB_Value() {
        if ( type_ == CRB_INT_VALUE )
            mpz_clear( u.int_value );
    }

    CRB_Value& operator = ( const CRB_Value& v ) {
        if ( this != &v ) {
            if ( type_ == CRB_INT_VALUE ) {
                if ( v.type_ == CRB_INT_VALUE ) {
                    mpz_set( u.int_value, v.u.int_value );
                    return *this;
                }
                else
                    mpz_clear( u.int_value );
            }

            type_ = v.type_;
            if ( v.type_ == CRB_INT_VALUE )
                mpz_init_set( u.int_value, v.u.int_value );
            else
                u = v.u;
        }
        return *this;
    }

    CRB_Value& operator = ( CRB_Object* obj ) {
        assert( obj );
        if (type_ == CRB_OBJECT_VALUE && u.object_value == obj )
            return *this;

        if ( type_ == CRB_INT_VALUE )
            mpz_clear( u.int_value );

        type_ = CRB_OBJECT_VALUE;
        u.object_value = obj;
        return *this;
    }

    // 2 stepの場合
    void set_type( CRB_ValueType newtype ) {
        if ( type_ == CRB_INT_VALUE ) {
            if ( newtype != CRB_INT_VALUE )
                mpz_clear( u.int_value );
        }
        else  {
            if ( newtype == CRB_INT_VALUE )
                mpz_init( u.int_value );
        }

        type_ = newtype;
    }

    const CRB_ValueType& type() const { return type_; }

    /** 内容ではなく, アドレスでの同一性 */
    bool operator < ( const CRB_Value& other ) const {
        if (type_ != other.type_)
            return type_ < other.type_;
        else {
            switch (type_) {
            case CRB_BOOLEAN_VALUE:
                return u.boolean_value < other.u.boolean_value;
            case CRB_INT_VALUE:
                return mpz_cmp(u.int_value, other.u.int_value) < 0;
            case CRB_DOUBLE_VALUE:
                return u.double_value < other.u.double_value;
            case CRB_OBJECT_VALUE:
                return u.object_value < other.u.object_value;
            case CRB_NIL_VALUE:
                return false; // すべて同じ
            case CRB_SYMBOL_VALUE:
                return u.symbol_value < other.u.symbol_value;

            case CRB_TAIL_CALL_VALUE:
            //case CRB_RETURN_VALUE:
                abort(); // あり得ない
            }
        }
        return false;
    }
};


/** 汎用の値リスト. 各要素を自動で解放しない */
typedef std::vector<CRB_Value> CRB_ValueList;

// 実引数を格納する. *各要素を自動解放しない*
struct CRB_ArgValues {
    // splat を展開して格納する
    CRB_ValueList normal;

    VariableMap keys;

    CRB_Value block_arg;

    CRB_ArgValues(): block_arg(CRB_NIL_VALUE) { }
/*
    virtual ~CRB_ArgValues() {
        // 不要?
        crb_value_list_free_objects(&normal);
        crb_varmap_free_objects(&keys);
        crb_object_release(&block_arg);
    }
*/
};


/** 各要素を release して、要素を詰める。リスト自体は解放しない。 */
void crb_value_list_free_objects(CRB_ValueList* value_list);

void crb_value_list_free_objects(CRB_ArgValues* value_list);


/////////////////////////////////////////////////////////////////////////

enum EvalResultType {
    NORMAL_EVAL_RESULT = 1,
    RETURN_EVAL_RESULT,  // The value of Special Operator `return-from`
    //BREAK_EVAL_RESULT,
    //NEXT_EVAL_RESULT,
    EXCEPTION_EVAL_RESULT,
};


/** 文/式の戻り値. */
struct EvalResult
{
    /** 更新することがある */
    EvalResultType type;

    CRB_Value value;

    // RETURN_EVAL_RESULT の場合に設定
    icu::UnicodeString return_tag;

    EvalResult( const CRB_ValueType& vtype ):
        type(NORMAL_EVAL_RESULT), value(vtype) { }

    EvalResult( const CRB_Value& v ): type(NORMAL_EVAL_RESULT), value(v) { }
};


enum MethodDefinitionType {
    SCRIPT_FUNCTION = 1,
    NATIVE_FUNCTION_DEFINITION,
};


struct ScriptLocation;
struct CRB_Thread;

typedef EvalResult (*CRB_NativeFunctionProc)( CRB_Thread* thread,
                                              struct LocalEnvironment& env,
                                              CRB_Value& receiver,
                                              const ScriptLocation& loc );

/** インスタンスメソッドを追加する */
void CRB_add_native_function( CRB_Interpreter* interp, CRB_Module* klass,
                              const icu::UnicodeString& method_name,
                              const std::vector<icu::UnicodeString>& params,
                              CRB_NativeFunctionProc proc );


/**
 * (実行時の) 関数
 * ● TODO: 名前を変える
 */
struct Function
{
    MethodDefinitionType type;

    /**
     * 最低限必要な実引数の数.
     *   func(x) と func(x, y = 1) はいずれも arity = 1
     */
    int arity;

    union {
        /** 参照するだけ */
        const struct LambdaExprNode* ast_node;

        struct {
            // icu::UnicodeString* name;   // ポインタではないオブジェクトは使えない
            const struct ParameterList* param_list;
            CRB_NativeFunctionProc proc;
        } native_f;
    } u;

    ~Function();
};


/** インタプリタを生成 */
//CRB_Interpreter* CRB_create_interpreter();

/** スクリプトを読み込む */
EvalResult CRB_interp_load_file( CRB_Thread* thread, LocalEnvironment& env,
                                 const icu::UnicodeString& filename,
                                 const ScriptLocation& loc );

bool CRB_interp_load_string( CRB_Interpreter* interp, const char* string );

EvalResult CRB_interp_require_file( CRB_Thread* thread, LocalEnvironment& env,
                                    const icu::UnicodeString& filename,
                                    const ScriptLocation& loc );

bool CRB_interp_run( CRB_Interpreter* interp, const char* filename );

// TODO: コンパイルしたASTのダンプ

// void CRB_interpret(CRB_Interpreter* interpreter);

//void CRB_dispose_interpreter(CRB_Interpreter* interpreter);


#endif /* PUBLIC_CRB_H_INCLUDED */


// Local variables:
// tab-width: 4
// c-basic-offset: 4
// indent-tabs-mode: nil
// End:
