﻿// -*- coding:utf-8-with-signature; mode:c++; tab-width:4; -*-
// SPDX-License-Identifier: LGPL-3.0-or-later
/*
  minilang -- A small programming language, minilang interpreter.
  Copyright (c) 2011-2013, 2023 Netsphere Laboratories, Hisashi Horikawa.
  All rights reserved.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

// 環境, 変数リスト. eval.cpp から呼び出される.


#include "CRB.h"
#include "support.h"
#include "DBG.h"
#include "../libparser/ast.h"
#include "execute.h"
#include "eval.h"
#include <cmath>
#include <cstring>
#include <cassert>
#include <unicode/ustdio.h>
#include <unicode/numfmt.h>
#include <unicode/ustream.h>
#include <algorithm> // find_if
#include <iostream> // cout, cerr

using namespace std;
using namespace icu;


Function::~Function()
{
    if ( type == NATIVE_FUNCTION_DEFINITION ) {
        // delete u.native_f.name;
        delete u.native_f.param_list;
    }
}


/**
 * 実行時エラーを投げる.
 * @param opt_args   要素を*解放する* (method_callなどと違う).
 */
EvalResult crb_runtime_error( CRB_Thread* thread,
                              LocalEnvironment& env,
                              const UnicodeString& error_class_name,
                              const ScriptLocation& loc,
                              const UnicodeString& message,
                              const CRB_ValueList& opt_args )
{
    CRB_Interpreter* interp = thread->interp;

    CRB_Class* err_class = crb_interp_find_class(interp, error_class_name);
    if ( !err_class ) {
        UFILE* err = u_finit( stderr, nullptr, nullptr );
        u_fprintf( err, "error class missing: %S\n",
                   const_cast<UnicodeString&>(error_class_name).getTerminatedBuffer() );
        u_fclose( err );
        abort(); // TODO:
    }

    UErrorCode error = U_ZERO_ERROR;
    NumberFormat* fmt = NumberFormat::createInstance(error);
    UnicodeString output;
    fmt->format( loc.line_number, output );
    output += ":";
    fmt->format( loc.column_number, output );
    output += ": " + message + " (" + err_class->name + ")";
    delete fmt;

#ifndef NDEBUG
    // エラーコンソールに出力.
    cerr << output << "\n";
    // UFILE* err = u_finit(stderr, nullptr, nullptr );
/*
    u_fprintf( err, "%3d:%d: %S (%S)\n", loc.line_number, loc.column_number,
	     buf, const_cast<UnicodeString&>(error_class_name).getTerminatedBuffer() );
    u_fclose(err);
*/
#endif // !NDEBUG

    CRB_ArgValues args;
    args.normal.push_back(CRB_Value(crb_string_new(interp, new UnicodeString(output))));
    CRB_ValueList::const_iterator i = opt_args.cbegin();
    for ( ; i != opt_args.cend(); i++ )
        args.normal.push_back(*i);

    CRB_Value receiver(err_class);
    EvalResult r = crb_method_call( thread, env,
                                    receiver, "new",
                                    &args, //nullptr,
                                    loc, NOT_LAST_STMT );
    crb_value_list_free_objects( &args );

    assert( r.type == NORMAL_EVAL_RESULT || r.type == EXCEPTION_EVAL_RESULT );
    r.type = EXCEPTION_EVAL_RESULT;

    return r;
}


EvalResult crb_raise_type_error( CRB_Thread* thread,
                                 LocalEnvironment& env,
                                 const ScriptLocation& loc,
                                 const CRB_Value& object,
                                 const icu::UnicodeString& object_name,
                                 const icu::UnicodeString& expect_type_name )
{
    return crb_runtime_error( thread, env, TYPE_ERROR, loc,
               object_name + _(" is expected a ") + expect_type_name +
               ", but " + crb_object_get_class(thread->interp, object)->name );
}


/** 各要素を release して、要素を詰める。リストオブジェクト自体は解放しない. */
void crb_value_list_free_objects(CRB_ValueList* value_list)
{
    if ( value_list ) {
        CRB_ValueList::iterator it;
        for (it = value_list->begin(); it != value_list->end(); it++)
            crb_object_release(&*it);
        value_list->clear();
    }
}

/** 各要素を release して、要素を詰める。リストオブジェクト自体は解放しない. */
void crb_value_list_free_objects(CRB_ArgValues* value_list)
{
    if ( value_list ) {
        crb_value_list_free_objects(&value_list->normal);
        crb_varmap_free_objects(&value_list->keys);
        crb_object_release(&value_list->block_arg);
    }
}

// 各要素を release して, 要素を詰める。
void crb_varmap_free_objects(VariableMap* list)
{
    if (list) {
        VariableMap::iterator i;
        for ( i = list->begin(); i != list->end(); ++i )
            crb_object_release( &i->second );
        list->clear();
    }
}


/**
 * メソッド・クロージャの呼び出しのために, ローカル変数を設定する
 * 実引数は, refer して local_env に追加する。
 *
 * @param params    仮引数. nullptrのことがある
 * @param args      実引数. この関数内で解放しない
 *
 * @return 例外 or nil
 */
EvalResult env_set_vars_by_param_and_args( CRB_Thread* thread,
                                           LocalEnvironment& local_env,
                                           const ParameterList* params,
                                           CRB_ArgValues& args,
                                           //CRB_Value* block_arg,
                                           const ScriptLocation& caller_loc )
{
    assert( thread );
    // assert( local_env );

    if ( params ) {
        CRB_ValueList::const_iterator args_it = args.normal.cbegin();

        // 1. 必須パラメータ
        StringList::const_iterator i1 = params->params.cbegin();
        while ( i1 != params->params.cend() ) {
            if ( args_it != args.normal.cend() ) {
                // 実引数があるとき
                // TODO: 型検査
                crb_object_refer( local_env.add_local_var(*(*i1), *args_it) );
            }
            else {  // 実引数がない
                return crb_runtime_error( thread, local_env,
                                              ARGUMENT_ERROR,
                                              caller_loc,
                                              _("Normal argument too few") );
            }
            ++i1; ++args_it;
        }

        // 2. オプション引数
        ptr_list<OptionalParamNode*>::const_iterator i2 =
                                params->optional_params.cbegin();
        while ( i2 != params->optional_params.cend()) {
            if ( args_it != args.normal.cend() ) {
                // 実引数があるとき
                // TODO: 型検査
                crb_object_refer( local_env.add_local_var(*(*i2)->var_name,
                                                           *args_it) );
            }
            else {  // 実引数がない
                assert( (*i2)->default_value );
                EvalResult r = eval_expr_node( thread, local_env,
                                        (*i2)->default_value, NOT_LAST_STMT);
                if ( r.type == EXCEPTION_EVAL_RESULT )
                    return r;
                assert( r.type == NORMAL_EVAL_RESULT );

                local_env.add_local_var( *(*i2)->var_name, r.value );
/*
                else {
                    return crb_runtime_error( thread, local_env,
                                              ARGUMENT_ERROR,
                                              caller_loc,
                                              _("Argument too few") );
                } */
            }
            ++i2; ++args_it;
        }

        // 3. Rest パラメータ
        if ( params->rest_param && args_it != args.normal.cend() ) {
            CRB_Array* ary = new CRB_Array( thread->interp->class_array );
            for ( ; args_it != args.normal.end(); ++args_it ) {
                ary->list.push_back( *args_it );
                crb_object_refer( ary->list.back() );
            }
            local_env.add_local_var( *params->rest_param, ary );
        }

        // normal 実引数のほうが多い
        if ( args_it != args.normal.cend() ) {
            return crb_runtime_error( thread, local_env, ARGUMENT_ERROR,
                                      caller_loc,
                                  _("Argument too many") );
        }

        // 4. key パラメータ
        // ●● TODO: impl.
        //          key 実引数の個数チェック
    }

    // 5. block パラメータ. 仮引数がないときは暗黙のブロック引数 `__block__`
    if ( params && params->block_param ) {
        local_env.add_local_var( *params->block_param,
                                 crb_object_refer(args.block_arg) );
    }
    else {
        local_env.add_local_var( "__block__",
                                          crb_object_refer(args.block_arg) );
    }

    return EvalResult( CRB_NIL_VALUE );
}


/**
 * crb_method_call() と eval_super_expr() の下請け.
 * @return    最後に評価した文の値. tail-call のことがある
 * @param args     実引数. nullptr でもよい. この関数内で解放しない
 */
static EvalResult crb_call_function( CRB_Thread* thread,
                              LocalEnvironment& caller_env,
                              CRB_Value& receiver,  // この関数内では解放しない
                              const MethodSearchGraph& graph,
                              const UnicodeString& method_name,
                                     CRB_ArgValues* args,
                                     //CRB_Value* block_arg,
                              const ScriptLocation& caller_loc )
{
    // グラフから, 前から順にメソッドを探す
    const CRB_ClassDescription* defined_class = nullptr;
    Function* func = nullptr;
    //Function* method_missing = nullptr;   #method_missing は発見しにくいバグの温床。削除 [[incompat]]

    MethodSearchGraph::const_iterator k;
    for ( k = graph.begin(); k != graph.end(); k++ ) {
        func = crb_module_find_method( *(*k), method_name );
        if ( func ) {
            defined_class = *k;
            break;
        }

        // 見つからなかったときに備え, method_missing も検索しておく
/*        if ( !method_missing ) {
            method_missing = crb_module_find_method( *(*k), "method_missing" );
            defined_class = *k;
          } */
    }

    CRB_ArgValues* call_args;
    CRB_ArgValues dummy_args;

    if ( func )
        call_args = args ? args : &dummy_args;
    else {
/*        if ( method_missing ) {
            dummy_args.push_back(
                        crb_interp_get_symbol(thread->interp, method_name) );
            if ( args ) {
                CRB_ValueList::const_iterator i;
                for ( i = args->cbegin(); i != args->cend(); i++ )
                    dummy_args.push_back(*i);
            }
            func = method_missing;
            call_args = &dummy_args;
        }
        else { */
            return crb_runtime_error( thread, caller_env,
              NO_METHOD_ERROR, caller_loc,
              UnicodeString(_("Method not found: ")) +
              "class " + crb_object_get_class(thread->interp, receiver)->name +
              " method " + method_name );
//        }
    }

    thread->stack_trace.push_back( CRB_Caller(&caller_env, caller_loc) );

    // ラムダ式と異なり, 外側のローカル変数にはアクセスできない
    LocalEnvironment* local_env = new LocalEnvironment( nullptr );
    local_env->klass = defined_class;
    local_env->method_name = method_name;
    local_env->node = func->u.ast_node;

    // 局所変数を解放するときに receiverまで解放してしまわないように, refer する
    local_env->add_local_var( "self", crb_object_refer(receiver) );

    // 内部でreferする
    EvalResult argresult = env_set_vars_by_param_and_args( thread,
                                        *local_env,
                                        (func->type == SCRIPT_FUNCTION ?
                                               func->u.ast_node->param_list :
                                               func->u.native_f.param_list),
                                        *call_args,
                                                           //block_arg,
                                        caller_loc );
    if ( argresult.type == EXCEPTION_EVAL_RESULT ) {
        thread->stack_trace.pop_back();
        local_env->release();
        return argresult;
    }
    assert( argresult.type == NORMAL_EVAL_RESULT );

    EvalResult result( CRB_NIL_VALUE );

    switch (func->type) {
    case SCRIPT_FUNCTION:
        // thread->stack_trace.push_back( func->u.ast_node );
        result = crb_execute_statement_list( thread, *local_env,
                                             func->u.ast_node->block,
                                             MAYBE_LAST_STMT );
        break;
    case NATIVE_FUNCTION_DEFINITION:
        // thread->stack_trace.push_back( nullptr );
        result = func->u.native_f.proc( thread, *local_env, receiver,
                                        caller_loc );
        break;
    }

    thread->stack_trace.pop_back();
    local_env->release();
    // crb_value_list_free_objects( &args );

    switch ( result.type ) {
    case EXCEPTION_EVAL_RESULT:
        return result;

    case RETURN_EVAL_RESULT:
        {
            //assert( result.value.type() == CRB_RETURN_VALUE );
            // λ式の中からの場合, 突き抜ける場合がある
/*      CRB_ReturnValue* jump_err = result.value.u.return_value;
            if ( func->u.ast_node == jump_err->return_from ) {
                // result.value はそのまま生かす
                result.type = NORMAL_EVAL_RESULT;
                result.value = jump_err->exit_value;
                delete jump_err;
            }
*/
            result.return_tag = "";
            return result;  // tail-call のことがある
        }
        break;
/*    case NEXT_EVAL_RESULT:
        abort(); // あり得ない
    case BREAK_EVAL_RESULT:
        {
            //assert( result.value.type() == CRB_RETURN_VALUE );
            // λ式の中から上がってくる場合, 突き抜けることがある
            CRB_ReturnValue* jump_err = result.value.u.return_value;
            if ( caller_env.node == jump_err->return_from ) {
                result.type = NORMAL_EVAL_RESULT;
                crb_object_release( &result.value );
                result.value.set_type( CRB_NIL_VALUE );
            }

            return result;
        }
        break;   */
    case NORMAL_EVAL_RESULT:
        break;
    }
    assert( result.type == NORMAL_EVAL_RESULT );

    return result;
}


// note: 特異クラスは、クラスから派生させることにした。
//       単に (特異クラス || クラス) を起点に検索するだけでよい。
static EvalResult method_call_sub( CRB_Thread* thread,
                                   LocalEnvironment& env,
                                   CRB_Value& receiver, // メソッド内部で解放しない
                                   const UnicodeString& method_name,
                                   CRB_ArgValues* args,
                                   //CRB_Value* block_arg,
                                   const ScriptLocation& caller_loc )
{
    CRB_Interpreter* const interp = thread->interp;

    // メソッドを検索する
    // singleton class がある場合は, まずそれ (とそのスーパークラス) を探索する
    MethodSearchGraph graph;
    if ( receiver.type() == CRB_OBJECT_VALUE &&
                                 receiver.u.object_value->singleton_class ) {
        crb_create_method_search_graph( &graph,
                                   receiver.u.object_value->singleton_class );
    }
    else {
        crb_create_method_search_graph( &graph,
                                crb_object_get_class(interp, receiver) );
    }

    return crb_call_function( thread, env, receiver, graph,
                              method_name,
                              args, //block_arg,
                              caller_loc );
}


EvalResult force_eval( CRB_Thread* thread, LocalEnvironment& env,
                              EvalResult result )
{
    while ( (result.type == NORMAL_EVAL_RESULT &&
                               result.value.type() == CRB_TAIL_CALL_VALUE) ||
            result.type == RETURN_EVAL_RESULT /*||
                         result.type == BREAK_EVAL_RESULT */ ) {
        if ( result.value.type() == CRB_TAIL_CALL_VALUE ) {
            EvalResult x = method_call_sub( thread, env,
                                        result.value.u.tail_call->receiver,
                                        result.value.u.tail_call->method_name,
                                        result.value.u.tail_call->arguments,
                                            //&result.value.u.tail_call->block_arg,
                                        result.value.u.tail_call->caller_loc );

            crb_object_release( &result.value );
            result = x;
        }
        else { // return or break. 突き抜けかたが変わることがある => 1段階で評価する
            /*            CRB_ReturnValue* jump_err = result.value.u.return_value;
            if ( jump_err->exit_value.type() != CRB_TAIL_CALL_VALUE )
                break;

            EvalResult x = method_call_sub( thread, env,
                                jump_err->exit_value.u.tail_call->receiver,
                                jump_err->exit_value.u.tail_call->method_name,
                                jump_err->exit_value.u.tail_call->arguments,
                                &jump_err->exit_value.u.tail_call->block_arg,
                                jump_err->exit_value.u.tail_call->caller_loc );
            if ( x.type == EXCEPTION_EVAL_RESULT ) {
                crb_object_release( &result.value );
                return x;
            }
            else if ( x.type == NORMAL_EVAL_RESULT ) {
                assert( x.value.type() != CRB_TAIL_CALL_VALUE );

                crb_object_release( &jump_err->exit_value );
                jump_err->exit_value = x.value;
                break;
            }
            else { // other return/break
                crb_object_release( &result.value );
                result = x;
                } */
            result.type = NORMAL_EVAL_RESULT;
        }
    }

    return result;
}


/**
 * メソッドを呼び出す
 * @param args        実引数リスト. nullptr でもよい. この関数の内部で解放*しない*
 * @param block_arg   ブロック引数. nullptr でもよい. この関数の内部で解放*しない*
 * @param caller_loc  呼び出した側のメソッド名の場所
 */
EvalResult crb_method_call( CRB_Thread* thread,
                            LocalEnvironment& env,
                            CRB_Value& receiver, // メソッド内部で解放しない
                            const UnicodeString& method_name,
                            CRB_ArgValues* args,
                            //CRB_Value* block_arg,
                            const ScriptLocation& caller_loc,
                            MaybeLastStmt tail_call )
{
    if ( tail_call ) {
        // 末尾再帰
        cout << "create tail-call: " << method_name << "\n";  // DEBUG

        CRB_TailCall* t = new CRB_TailCall();
        t->receiver = crb_object_refer(receiver);
        t->method_name = method_name;
        if ( args ) {
            t->arguments = new CRB_ArgValues();
            for_each( args->normal.cbegin(), args->normal.cend(),
                      [&](const CRB_Value& v) {
                          t->arguments->normal.push_back(v);
                          crb_object_refer( t->arguments->normal.back() );
                      } );
            //if ( args->block_arg )
            t->arguments->block_arg = crb_object_refer( args->block_arg );
        }
        t->caller_loc = caller_loc;

        EvalResult result( CRB_TAIL_CALL_VALUE );
        result.value.u.tail_call = t;
        return result;
    }

    EvalResult result = method_call_sub( thread, env, receiver,
                                         method_name,
                                         args, //block_arg,
                                         caller_loc );

    return tail_call ? result : force_eval( thread, env, result );
}


/**
 * 戻り値の型を強制するメソッド呼び出し. 暗黙の型変換などに使う.
 * method_name メソッドで klass に型変換を試みる.
 *
 * @return      method_name メソッドの戻り値
 */
EvalResult crb_object_coerce( CRB_Thread* thread,
                            LocalEnvironment& env,
                            CRB_Value& object,
                            const UnicodeString& method_name,
                            CRB_Class* klass,
                            const ScriptLocation& loc )
{
    assert( klass );

    EvalResult v = crb_method_call( thread, env, object, method_name,
                                    nullptr, //nullptr,
                                    loc, NOT_LAST_STMT );
    if ( v.type == EXCEPTION_EVAL_RESULT )
        return v;
    assert( v.type == NORMAL_EVAL_RESULT );

    if ( !crb_object_isa(thread->interp, v.value, klass) ) {
        crb_object_release( &v.value );
        return crb_raise_type_error( thread, env, loc, v.value,
                                     method_name, klass->name );
    }

    return v;
}


EvalResult crb_call_super( CRB_Thread* thread,
                           LocalEnvironment& env,
                           CRB_Value& receiver,
                           const CRB_Class* klass,
                           const UnicodeString& method_name,
                           CRB_ArgValues* args,
                           //CRB_Value* block_arg,
                           const ScriptLocation& caller_loc,
                           MaybeLastStmt tail_call )
{
    assert( klass );

    MethodSearchGraph graph;
    crb_create_method_search_graph( &graph, klass->superclass );

    EvalResult result = crb_call_function( thread, env, receiver, graph,
                                           method_name,
                                           args, //block_arg,
                                           caller_loc );

    return tail_call ? result : force_eval(thread, env, result);
}


//////////////////////////////////////////////////////////////////////////
// LocalEnvironment

/** ローカル変数を探す. クロージャの場合, 外に向かって検索する
 * @return 変数の値へのポインタを返す. ●●マルチスレッドで大丈夫?
 */
CRB_Value* LocalEnvironment::search_local_var( const UnicodeString& name ) const
{
    const LocalEnvironment* env = this;

    for ( ; env; env = env->outer_env ) {
        VariableMap::const_iterator it = env->variables.find( name );
        if ( it != env->variables.end() )
            return const_cast<CRB_Value*>(&it->second);
        else
            return nullptr;
    }

    return nullptr;
}


/**
 * ローカル変数を追加する. shadowing できるように, ここでは outer は見ない
 * この関数の内部では参照カウンタを加算しない. 必要があるときは呼出し側でreferすること.
 */
CRB_Value& LocalEnvironment::add_local_var( const UnicodeString& identifier,
                                      const CRB_Value& value )
{
#ifndef NDEBUG
    UFILE* err = u_finit(stderr, nullptr, nullptr);
    u_fprintf( err, "%s: called: %p %S\n", __func__,
               this,
               const_cast<UnicodeString&>(identifier).getTerminatedBuffer() );
    u_fclose(err);
#endif // !NDEBUG

    pair<VariableMap::iterator, bool> r =
              variables.insert( pair<VariableMap::key_type, CRB_Value>(identifier, value) );
    if ( !r.second ) {
        abort(); // TODO: error処理
    }

    return r.first->second;
}






// Local variables:
// tab-width: 4
// c-basic-offset: 4
// indent-tabs-mode: nil
// End:
