﻿// -*- coding:utf-8-with-signature; mode:c++; tab-width:4; -*-
// SPDX-License-Identifier: LGPL-3.0-or-later
/*
  minilang -- A small programming language, minilang interpreter.
  Copyright (c) 2011-2013, 2023 Netsphere Laboratories, Hisashi Horikawa.
  All rights reserved.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef PUBLIC_CRB_DEV_H_INCLUDED
#define PUBLIC_CRB_DEV_H_INCLUDED

// #include "support.h"
#include "CRB.h"
#include "ptr_map.h"
#include "ptr_list.h"
#include "ptr_vector.h"
#include <string>
#include <unicode/unistr.h>
#include <vector>
#include <map>
#include <cassert>
#include "../libparser/ast.h"
#include "sync.h"
#include "eval.h"


enum BinaryOp {
  ADD_OP,
  SUB_OP,
  MUL_OP,
  DIV_OP,
  MOD_OP,
};


/** 参照カウンタを加算する */
CRB_Value& crb_object_refer( CRB_Value& v );
CRB_Object* crb_object_refer( CRB_Object* v );

void crb_object_release(CRB_Value* v);

CRB_Class* crb_object_get_class( CRB_Interpreter* interp, const CRB_Value& v );

bool crb_object_isa( CRB_Interpreter* interp, const CRB_Value& v,
                     const CRB_ClassDescription* klass );

CRB_Class* crb_object_create_singleton_class( CRB_Interpreter* interp,
                                           CRB_Object* v );


struct LocalEnvironment;

bool crb_interp_search_const( CRB_Interpreter* interp,
                              LocalEnvironment& env,
                              const icu::UnicodeString& const_name,
                              CRB_Value* out );

/**
 * 定数を代入する。内部でreferしない。
 * 同名の定数があった場合 (再代入のとき) は, already_defined に設定し, falseを返す
 * @param const_name フルパスでなければならない. "::" で始まっていること.
 */
EvalResult crb_interp_assign_const( CRB_Thread* thread,
                                    LocalEnvironment& env,
                                    const icu::UnicodeString& const_name,
                                    const CRB_Value& value,
                                    const ScriptLocation& loc,
                                    CRB_Value* already_defined );

/** 現在の名前空間だけから定数をフルパスにする */
EvalResult crb_interp_resolve_const_name( CRB_Thread* thread,
                                          LocalEnvironment& env,
                                          const icu::UnicodeString& name,
                                          const ScriptLocation& loc,
                                          icu::UnicodeString* resolved_name );

CRB_Value crb_interp_get_symbol( CRB_Interpreter* interp,
                                 const icu::UnicodeString& symbol_name );

icu::UnicodeString crb_interp_symbol_to_str( CRB_Interpreter* interp,
                                             uint32_t symid );


///////////////////////////////////////////////////////////////////////
// LocalEnvironment

struct LocalEnvironment;

EvalResult env_set_vars_by_param_and_args( CRB_Thread* thread,
                                           LocalEnvironment& local_env,
                                           const ParameterList* params,
                                           CRB_ArgValues& args,
                                           //CRB_Value* block_arg,
                                           const ScriptLocation& caller_loc );

class RefCount
{
protected:
    /**
     * 参照カウンタ. スレッドローカル.
     * 環境が保有するオブジェクトはスレッド間で共有されることがあることに注意.
     */
    // これに `mutable` キーワードを付けるのも一法。オブジェクトをコピーすると
    // きに参照カウンタを加算するなど。
    // See https://yohhoy.hatenadiary.jp/entry/20130803/p1
    int32_t m_ref_count;

public:
    RefCount() : m_ref_count(1) { }
protected:
    virtual ~RefCount() { }

public:
    virtual RefCount* add_ref() {
#ifdef _WIN32
        InterlockedIncrement(&m_ref_count);
#else
        // legacy 関数. __atomic_add_fetch() が後継。ただ, どのメモリオーダを使
        // うのがいいか? `__ATOMIC_RELAXED` でもよい?
        __sync_add_and_fetch(&m_ref_count, 1);
#endif
        return this;
    }

    virtual int32_t release() = 0;

private:
    RefCount(const RefCount& ); // copy 不可
    RefCount& operator = (const RefCount& ); // 代入不可
};


/** 局所変数リスト */
struct LocalEnvironment: public RefCount
{
private:
    /**
     * レキシカルな外側の環境. クロージャを生成したときに設定する。
     * 関数, クラスでは nullptr
     */
    LocalEnvironment* const outer_env;

public:
    /** ローカル変数 */
    VariableMap variables;

    /** 名前空間. toplevel を扱うために, クラス名と別に設定する. トップレベルは "" */
    // icu::UnicodeString const ns_name;

    /** メソッドを定義しているモジュール. super() で使う. self のクラスとは一致しない */
    const CRB_ClassDescription* klass;
    icu::UnicodeString method_name;

    /** この環境を導入した場所. return/breakで使う. backtrace でも */
    const struct AST_Node* node;

public:
    LocalEnvironment( LocalEnvironment* outer ):
        outer_env(outer), klass(nullptr) {
        // assert( ns_name.startsWith("::") );
    }

    virtual ~LocalEnvironment() {
        crb_varmap_free_objects(&variables);

#if 0 // DEBUG
        printf("%s: ", __func__);
        it = variables.begin();
        for ( ; it != variables.end(); it++)
            printf("%s:%d:%d ", it->first.c_str(), it->second->value.type(),
                   it->second->value.type() == CRB_OBJECT_VALUE ?
                   	     it->second->value.u.object_value->ref_count : 0);
        printf("\n");
#endif // DEBUG
    }

    virtual LocalEnvironment* add_ref() {
        RefCount::add_ref();
        if ( outer_env ) // ブロックのネストに対応
            outer_env->add_ref();   // ●● TODO: 自由変数があるときだけでは?
        return this;
    }

    virtual int32_t release() {
#ifdef _WIN32
        int32_t r = InterlockedDecrement(&m_ref_count);
#else
        int32_t r = __sync_sub_and_fetch(&m_ref_count, 1);
#endif
        // マイナスになりうることに注意
        if ( !r ) {
            if ( outer_env )
                outer_env->release();
            delete this;
        }
        return r;
    }

    /** 外に向かって検索する */
    CRB_Value* search_local_var( const icu::UnicodeString& name ) const;

    /** こちらは outer は見ない. value の参照カウンタも加算しない */
    CRB_Value& add_local_var( const icu::UnicodeString& name,
                              const CRB_Value& value );

    CRB_Value& operator [] (const icu::UnicodeString& name) const {
        CRB_Value* v = search_local_var(name);
        assert(v);
        return *v;
    }
};


/** 末尾再帰 */
struct CRB_TailCall
{
    CRB_Value receiver;

    icu::UnicodeString method_name;

    CRB_ArgValues* arguments;

    //CRB_Value block_arg;

    ScriptLocation caller_loc;

    CRB_TailCall(): receiver(CRB_NIL_VALUE),
                    arguments(nullptr) //, block_arg(CRB_NIL_VALUE)
    { }

    ~CRB_TailCall() {
        crb_object_release( &receiver );
        if ( arguments ) {
            crb_value_list_free_objects( arguments );
            delete arguments;
        }
        //crb_object_release( &block_arg );
    }
};

EvalResult force_eval( CRB_Thread* thread, LocalEnvironment& env,
                       EvalResult result );

#if 0
struct CRB_ReturnValue
{
    /** 参照するだけ */
    const AST_Node* return_from;

    /** 所有する */
    CRB_Value exit_value;

    CRB_ReturnValue(): exit_value(CRB_NIL_VALUE) { }

    ~CRB_ReturnValue() {
        crb_object_release( &exit_value );
    }
};
#endif // 0


//////////////////////////////////////////////////////////////////////
// 例外クラス名

#define CRB_EXCEPTION_CLASS "::Exception"

#define CRB_STANDARD_ERROR "::StandardError"

/** その他の実行時エラー */
#define CRB_RUNTIME_ERROR  "::RuntimeError"

/** 引数の数が合っていない. 型が違うのはTypeError */
#define  ARGUMENT_ERROR      "::ArgumentError"

/** errnoにエラーコードを設定 */
#define  SYSTEM_CALL_ERROR   "::SystemCallError"

/** 正規表現のコンパイルに失敗 */
#define  REGEXP_ERROR        "::RegexpError"

// #define CRB_SCRIPT_ERROR "::ScriptError"

/** require(), load() に失敗 */
#define CRB_LOAD_ERROR   "::LoadError"

/** メソッドが見つからない */
#define  NO_METHOD_ERROR     "::NoMethodError"

/** 型が想定と違う */
#define  TYPE_ERROR          "::TypeError"

/** 未定義の変数を参照. クラス名が今ひとつ */
#define  CRB_NAME_ERROR          "::NameError"

/** ループの中でないのにbreak文 */
#define  LOCAL_JUMP_ERROR    "::LocalJumpError"

/** 同じ名前のメソッドはエラー [incompat] */
#define  FUNCTION_MULTIPLE_DEFINE_ERR  "::MethodMultipleDefinedError"

/** 未実装 */
#define  NOT_IMPLEMENTED_ERROR   "::NotImplementedError"

/** リストの終わり. next() で終端のときに投げられる */
#define STOP_ITERATION "::StopIteration"

#define ZERO_DIVISION_ERROR   "::ZeroDivisionError"


EvalResult crb_method_call( CRB_Thread* thread,
                            LocalEnvironment& env,
                            CRB_Value& receiver,
                            const icu::UnicodeString& method_name,
                            CRB_ArgValues* args,
                            //CRB_Value* block_arg,
                            const ScriptLocation& loc,
                            MaybeLastStmt tail_call );

EvalResult crb_object_coerce( CRB_Thread* thread,
                            LocalEnvironment& env,
                            CRB_Value& object,
                            const icu::UnicodeString& method_name,
                            CRB_Class* klass,
                            const ScriptLocation& loc );

typedef std::vector<const CRB_ClassDescription*> MethodSearchGraph;

// クラス優先順位リスト class-precedence-list.
void crb_create_method_search_graph( MethodSearchGraph* graph,
                                     const CRB_Class* klass );

EvalResult crb_call_super( CRB_Thread* thread,
                           LocalEnvironment& caller_env,
                           CRB_Value& receiver,  // この関数内では解放しない
                           const CRB_Class* klass,
                           const icu::UnicodeString& method_name,
                           CRB_ArgValues* args,
                           //CRB_Value* block_arg,
                           const ScriptLocation& caller_loc,
                           MaybeLastStmt tail_call );


///////////////////////////////////////////////////////////////////////
// CRB_Thread

struct CRB_Caller
{
#if 0
    /** とりあえずクラスとメソッド名で記録してみる */
    CRB_Module* defined_class;

    icu::UnicodeString method_name;
#endif // 0

    /** 呼び出し元の環境 */
    const struct LocalEnvironment* env;

    ScriptLocation caller_loc;

    CRB_Caller( const LocalEnvironment* env_, const ScriptLocation& loc ):
        env(env_), caller_loc(loc) { }
};


struct CRB_Thread: public CRB_Object
{
    // 所有しない
    CRB_Interpreter* interp;

    std::vector<CRB_Caller> stack_trace;
    // EvalResult result;

    /** 先頭の"$"は省略する */
    VariableMap special_vars;

    // LocalEnvironment* toplevel;

#ifdef _WIN32
    ..........;
#else
    pthread_t tid;
#endif // _WIN32

    CRB_Thread( CRB_Class* klass );

    ~CRB_Thread() {
        // delete toplevel;
        crb_varmap_free_objects(&special_vars);
    }
};


// EvalResult crb_thread_run( CRB_Thread* cur_thread, StatementList* block );


/////////////////////////////////////////////////////////////////////////////
// CRB_Interpreter

/** インタプリタ */
struct CRB_Interpreter: private noncopyable
{
    /** TODO: 定数などそれぞれに分割? */
    Sync interp_sync;

    /** 定数. キーは"F::G" の形. 登録するときは, 先頭の"::" は省略する. */
    VariableMap const_vars;

    /** シンボル. 先頭の":" は省略する. */
    std::map<icu::UnicodeString, uint32_t> symbols;

    /** 読み込み済みのスクリプト. ruby での '$"' or $LOADED_FEATURES */
    ptr_map<icu::UnicodeString, ParsedScript*> loaded_files;

    /** ruby での '$:' or $LOAD_PATH */
    std::vector<icu::UnicodeString> load_path;

    CRB_Thread* main_thread;

    LocalEnvironment* toplevel;

    /**
     * crb_interp_find_class() を毎回呼び出さなくてもいいように, キャッシュする
     * すべて参照するだけ.
     */
    // この 6 クラスがすべての基本.
    CRB_Class* class_basic_object; // root
    CRB_Class* class_object;    // < mod Kernel < BasicObject
    CRB_Class* class_class_description;
    CRB_Class* class_module;
    CRB_Class* class_class;     // ruby: < Module   明らかに設計の失敗. 変える
    CRB_Class* class_thread;

    // バイト列. ruby の String
    CRB_Class* class_byte_array;
    CRB_Class* class_enumerator; // < mod Enumerable < Object
    // Unicode の rope [[incompat]]
    CRB_Class* class_string;    // < mod Comparable < Object
    // String::BreakIterator < Enumerator

    // Smalltalk では String のサブクラス.
    //   frozen_string_literal: true なら, Smalltalk と同じとしてよいのでは?
    //   See Rubyのシンボルをなくせるか考えてみた（翻訳）
    //       https://techracho.bpsinc.jp/hachi8833/2018_01_10/50578
    // CL: System Class `SYMBOL`: パッケージに所属する.
    //     名前と, (値 or 関数) フィールドを持つ。変数ではないが変数っぽい.
    //     (setf (symbol-value 'a) 1)    => 1 という値を設定する
    //     -> まったく別物.
    // String のサブクラスにする [[incompat]]
    CRB_Class* class_symbol;

    CRB_Class* class_numeric;   // < mod Comparable < Object
    CRB_Class* class_io;
    CRB_Class* class_file_io;   // FileIO < IO

    // クロージャ
    CRB_Class* class_proc;

    // ruby: Booleanクラスはなく, TrueClass/FalseClass がある.
    //       (1 == true) は偽. true は比較には使えない.
    //       nil, false 以外はすべて真. true は代表値であって, 唯一の真ではない.
    // CL: 偽は nil (= ()). nil のクラスは null. それ以外は真。t は すべての
    //     superclass
    // Smalltalk: 真は true だけ, 偽は false だけ.
    //            なので, `True` クラス, `False` クラスの意味がある。
    //     Smalltalk-80: `True`, `False` と, 共通の Boolean クラスがある.
    //     Smalltalk-76: true, false, nil は Object のインスタンス.
    //     Smalltalk-72: true はシンボルオブジェクト, false は falseclass.
    // -> false は NilClass でよく, true は BasicObject のインスタンスにする
    //CRB_Class* class_boolean;

    // ruby: Fixnum, Bignum に分かれていた。ruby 2.4 で Integer に一本化
    // 型推論のため、型を分けるのは不味い.
    CRB_Class* class_integer;
    CRB_Class* class_float;

    // ruby: nil == nil は真, false == false も真, but nil == false は偽!
    // nil と false は区別せず、一つのクラスにする [[incompat]].
    // TODO: FalseClass は NilClass の別名にする.
    CRB_Class* class_nilclass;

    CRB_Class* class_hash;
    CRB_Class* class_array;
    CRB_Class* class_regexp;
    CRB_Class* class_match_data;
    CRB_Class* class_encoding;

    CRB_Class* class_exception;
    CRB_Class* class_standard_error;
    CRB_Class* class_runtime_error;
    CRB_Class* class_local_jump_error;

    CRB_Interpreter(): toplevel(nullptr), class_class(nullptr) { }

    ~CRB_Interpreter();

    /** 型システムを構築したり, prelude を読み込む */
    void setup_environment();
};


EvalResult crb_raise_type_error( CRB_Thread* thread,
                                 LocalEnvironment& env,
                                 const ScriptLocation& loc,
                                 const CRB_Value& object,
                                 const icu::UnicodeString& object_name,
                                 const icu::UnicodeString& expect_type_name );


/**
 * 指定のC++構造体の, インスタンスを生成する.
 * CRB_Object C++クラスのサブクラスを作るときは, 必ずこの関数でminilangインスタンスを生成
 * すること
 * @param receiver Classクラスまたはそのサブクラス
 */
template <typename Klass>
EvalResult crb_class_create_instance( CRB_Thread* thread,
                                      LocalEnvironment& env,
                                      CRB_Value& receiver,
                                      const CRB_ArgValues& args,
                                      //CRB_Value* block_arg,
                                      const ScriptLocation& loc )
{
    CRB_Interpreter* const interp = thread->interp;

    if ( !crb_object_isa(interp, receiver, interp->class_class) ) {
        return crb_raise_type_error( thread, env, loc, receiver,
                                     "receiver", "Class" );
    }
    CRB_Value obj = new Klass(
                           static_cast<CRB_Class*>(receiver.u.object_value) );

    // method_call で release されないように.
    // CRB_ValueList::iterator i;
    // for ( i = args.begin(); i != args.end(); i++ )
    // crb_object_refer(*i);

    EvalResult r = crb_method_call( thread, env, obj, "initialize",
                                    &args, //block_arg,
                                    loc, NOT_LAST_STMT );
    if ( r.type == EXCEPTION_EVAL_RESULT ) {
        delete obj.u.object_value;
        return r;
    }
    assert( r.type == NORMAL_EVAL_RESULT );

    // 成功したときは initialize() の結果は捨てる
    crb_object_release( &r.value );
    r.value = obj;

    return r;
}


///////////////////////////////////////////////////////////////////////
// CRB_Proc

struct LambdaExprNode;

/** 組み込みクラス ::Proc. クロージャ */
struct CRB_Proc: public CRB_Object
{
    /** ブロック本体. 参照するだけ */
    const LambdaExprNode* expr;

    /** λ式の場所の, 外側の環境 */
    LocalEnvironment* outer_env;

    CRB_Proc( CRB_Class* klass ): CRB_Object( klass ), outer_env(nullptr) { }

    CRB_Proc( CRB_Interpreter* interp );

    ~CRB_Proc() {
        // printf("%s dtor\n", __func__); // DEBUG
        if ( outer_env ) {
            outer_env->release();
            outer_env = nullptr;
        }
    }
};


///////////////////////////////////////////////////////////////////////
// CRB_Array

struct CRB_Array: public CRB_Object
{
    CRB_ValueList list;

    CRB_Array( CRB_Class* klass ): CRB_Object( klass ) { }

    //CRB_Array( CRB_Interpreter* interp );

    ~CRB_Array() {
        crb_value_list_free_objects( &list );
    }
};


///////////////////////////////////////////////////////////////////////
// CRB_Hash

struct CRB_Hash: public CRB_Object
{
    typedef std::map<CRB_Value, CRB_Value> Pairs;
    Pairs pairs;

    CRB_Hash( CRB_Class* klass ): CRB_Object( klass ) { }

    //CRB_Hash( CRB_Interpreter* interp ): CRB_Object(

    ~CRB_Hash() {
        Pairs::iterator i;
        for ( i = pairs.begin(); i != pairs.end(); i++ ) {
            crb_object_release( const_cast<CRB_Value*>(&i->first) );
            crb_object_release( &i->second );
        }
    }
};


/** 単にクラスのインスタンスメソッドを検索する */
Function* crb_module_find_method( const CRB_ClassDescription& klass,
                                 const icu::UnicodeString& method_name );

/**
 * インスタンス変数に代入する.
 * 新たに設定するオブジェクトは, 内部でrefer*しない*.
 * すでに設定されていたオブジェクトは, 新たに設定するオブジェクトと同一でない限り, releaseする
 */
bool crb_object_assign_ivar( CRB_Object* obj,
                             const icu::UnicodeString& ivar_name,
                             const CRB_Value& value );

// インスタンス変数を得る.
CRB_Value* crb_object_find_ivar( const CRB_Value& obj,
                                 const icu::UnicodeString& ivar_name );

// クラス変数を得る.
CRB_Value* crb_object_find_classvar( const CRB_Value& obj,
                                     const icu::UnicodeString& classvar );


/** CRB_Stringインスタンスを生成して返す. 内容の文字列は複製しない */
inline CRB_String* crb_string_new( CRB_Interpreter* interp,
                                   icu::UnicodeString* str )
{
    assert( interp );
    assert( str );
    return new CRB_String( interp, str );
}


EvalResult crb_runtime_error( CRB_Thread* thread,
                              LocalEnvironment& env,
                              const icu::UnicodeString& error_class_name,
                              const ScriptLocation& loc,
                              const icu::UnicodeString& message,
                              const CRB_ValueList& opt_args = CRB_ValueList() );


#endif /* PUBLIC_CRB_DEV_H_INCLUDED */



// Local variables:
// tab-width: 4
// c-basic-offset: 4
// indent-tabs-mode: nil
// End:
