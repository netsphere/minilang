﻿// -*- coding:utf-8-with-signature; mode:c++; tab-width:4; -*-

/*
  minilang -- a minilang interpreter.

  Copyright (c) 2011-2013 HORIKAWA Hisashi. All rights reserved.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// class IO

#include "support.h"
#include "execute.h"
#include "class_io.h"

using namespace std;


void class_io_add_methods( CRB_Thread* thread, LocalEnvironment& env )
{
    printf("%s: enter.\n", __func__); // DEBUG
    
    CRB_Interpreter* const interp = thread->interp;

    crb_interp_create_class( thread, env, "::IO",
                             interp->class_object, ScriptLocation(), 
                             &interp->class_io );
/*  
    CRB_add_native_function( interp, interp->class_io, 
                             "gets", {"?eol", "?limit"},
                             io_gets );
*/
}



// Local variables:
// tab-width: 4
// c-basic-offset: 4
// indent-tabs-mode: nil
// End:
