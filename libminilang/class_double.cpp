﻿// -*- coding:utf-8-with-signature; mode:c++; tab-width:4; -*-

/*
  minilang -- a minilang interpreter.

  Copyright (c) 2011-2013 HORIKAWA Hisashi. All rights reserved.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "support.h"
#include "CRB.h"
#include "DBG.h"
#include "execute.h"
#include <unicode/numfmt.h>
#include <cmath>

using namespace std;
using namespace icu;


static EvalResult double_to_i( CRB_Thread* thread,
                               LocalEnvironment& env,
                               CRB_Value& receiver,
                               const ScriptLocation& loc )
{
    EvalResult result( CRB_INT_VALUE );
    mpz_set_si( result.value.u.int_value, receiver.u.double_value );

    return result;
}


/** class_int.ccからも呼び出される */
EvalResult class_double_binary_op( CRB_Thread* thread,
                                   LocalEnvironment& env,
                                   CRB_Value& receiver,
                                   BinaryOp binary_op,
                                   const ScriptLocation& loc )
{
    // CRB_Interpreter* const interp = thread->interp;
    double right;

    if ( env["other"].type() == CRB_DOUBLE_VALUE)
        right = env["other"].u.double_value;
    else if ( env["other"].type() == CRB_INT_VALUE)
        right = mpz_get_d( env["other"].u.int_value);
    else {
        return crb_raise_type_error( thread, env, loc, env["other"],
                                     "other", "Integer/Float" );
    }

    EvalResult result( CRB_DOUBLE_VALUE );

    switch (binary_op) {
    case ADD_OP:
        result.value.u.double_value = receiver.u.double_value + right;
        break;
    case SUB_OP:
        result.value.u.double_value = receiver.u.double_value - right;
        break;
    case MUL_OP:
        result.value.u.double_value = receiver.u.double_value * right;
        break;
    case DIV_OP:
        // [incompat] ruby: 1 / 0.0 => Infinity, 0 / 0.0 => NaN
        if ( !right ) {
            return crb_runtime_error( thread, env, ZERO_DIVISION_ERROR, loc,
                                      _("divided by 0") );
        }
        result.value.u.double_value = receiver.u.double_value / right;
        break;
    case MOD_OP:
        // [incompat] ruby: 1.0 % 0.0 => NaN, 0.0 % 0.0 => NaN
        if ( !right ) {
            return crb_runtime_error( thread, env, ZERO_DIVISION_ERROR, loc,
                                      _("divided by 0") );
        }
        result.value.u.double_value = receiver.u.double_value - 
                                      floor(receiver.u.double_value / right);
        break;
    default:
        abort(); // あり得ない
    }

    return result;
}


static EvalResult class_double_add( CRB_Thread* interp, 
                                    LocalEnvironment& env,
                                    CRB_Value& receiver,
                                    const ScriptLocation& loc)
{
    return class_double_binary_op( interp, env, receiver, ADD_OP, loc);
}


static EvalResult class_double_sub( CRB_Thread* interp, 
                                    LocalEnvironment& env,
                                    CRB_Value& receiver,
                                    const ScriptLocation& loc)
{
    return class_double_binary_op( interp, env, receiver, SUB_OP, loc);
}


static EvalResult class_double_mul( CRB_Thread* interp, 
                                    LocalEnvironment& env,
                                    CRB_Value& receiver,
                                    const ScriptLocation& loc)
{
    return class_double_binary_op( interp, env, receiver, MUL_OP, loc);
}


static EvalResult class_double_div( CRB_Thread* interp, 
                                    LocalEnvironment& env,
                                    CRB_Value& receiver,
                                    const ScriptLocation& loc)
{
    return class_double_binary_op( interp, env, receiver, DIV_OP, loc);
}


static EvalResult class_double_op_mod( CRB_Thread* interp, 
                                       LocalEnvironment& env,
                                       CRB_Value& receiver,
                                       const ScriptLocation& loc)
{
    return class_double_binary_op( interp, env, receiver, MOD_OP, loc );
}


/** 単項マイナス */
static EvalResult class_double_neg( CRB_Thread* thread, 
                                    LocalEnvironment& env,
                                    CRB_Value& receiver, 
                                    const ScriptLocation& loc )
{
    EvalResult result( CRB_DOUBLE_VALUE );
    result.value.u.double_value = -receiver.u.double_value;

    return result;
}


/** Integer からも呼び出される */
EvalResult class_double_op_compare( CRB_Thread* thread, 
                                    LocalEnvironment& env,
                                    CRB_Value& receiver, 
                                    const ScriptLocation& loc )
{
    // CRB_Interpreter* const interp = thread->interp;
    double right;

    if ( env["other"].type() == CRB_DOUBLE_VALUE )
        right = env["other"].u.double_value;
    else if ( env["other"].type() == CRB_INT_VALUE )
        right = mpz_get_d( env["other"].u.int_value );
    else {
        return crb_raise_type_error( thread, env, loc, env["other"],
                                     "other", "Integer/Float" );
    }

    EvalResult result( CRB_INT_VALUE );
    // result.type = NORMAL_EVAL_RESULT;

    if ( receiver.u.double_value > right ) 
        mpz_set_si( result.value.u.int_value, +1 );
    else if ( receiver.u.double_value < right ) 
        mpz_set_si( result.value.u.int_value, -1 );
    else
        mpz_set_si( result.value.u.int_value, 0 );

    return result;
}


static EvalResult class_double_to_s( CRB_Thread* thread,
                                     LocalEnvironment& env,
                                     CRB_Value& receiver, 
                                     const ScriptLocation& loc )
{
    UErrorCode error = U_ZERO_ERROR;
    NumberFormat* fmt = NumberFormat::createInstance(error);
    UnicodeString output;
    fmt->format( receiver.u.double_value, output );
    delete fmt;

    return EvalResult( crb_string_new(thread->interp, 
                                                new UnicodeString(output)) );
}


void class_float_add_methods( CRB_Thread* thread, LocalEnvironment& env )
{
    CRB_Interpreter* const interp = thread->interp;

    crb_interp_create_class( thread, env, 
                             "::Numeric",
                             interp->class_object, ScriptLocation(), 
                             &interp->class_numeric );

    crb_interp_create_class( thread, env, 
                             "::Float",
                             interp->class_numeric, ScriptLocation(),
                             &interp->class_float );

    CRB_add_native_function(interp, interp->class_float, "+", {"other"}, class_double_add );
    CRB_add_native_function(interp, interp->class_float, "-", {"other"}, class_double_sub );
    CRB_add_native_function(interp, interp->class_float, "*", {"other"}, class_double_mul );
    CRB_add_native_function(interp, interp->class_float, "/", {"other"}, class_double_div );
    CRB_add_native_function( interp, interp->class_float, "%", {"other"}, class_double_op_mod );
    CRB_add_native_function(interp, interp->class_float, "-@", { }, class_double_neg);

    CRB_add_native_function( interp, interp->class_float, "<=>", {"other"}, class_double_op_compare );
    CRB_add_native_function( interp, interp->class_float, "to_s", { }, class_double_to_s);
    CRB_add_native_function( interp, interp->class_float, "to_i", { }, double_to_i );
}



// Local variables:
// tab-width: 4
// c-basic-offset: 4
// indent-tabs-mode: nil
// End:
