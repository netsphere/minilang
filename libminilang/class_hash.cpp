﻿// -*- coding:utf-8-with-signature; mode:c++; tab-width:4; -*-

/*
  minilang -- a minilang interpreter.

  Copyright (c) 2011-2013 HORIKAWA Hisashi. All rights reserved.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// class Hash

#include "support.h"
#include <cstdio>
#include <cstring>
#include "DBG.h"
#include "execute.h"
#include <cassert>
#include <unicode/ustdio.h>
#include <map>

using namespace std;
using namespace icu;


/*
CRB_Hash::CRB_Hash( CRB_Interpreter* interp ): 
    CRB_Object( interp->class_hash ) 
{
}
*/


struct CRB_HashIterator: public CRB_Object
{
    CRB_Hash::Pairs::iterator it;

    CRB_HashIterator( CRB_Class* klass ): CRB_Object(klass) { }
};


//////////////////////////////////////////////////////////////////////////////
// class Hash

static EvalResult hash_s_new( CRB_Thread* thread,
                               LocalEnvironment& env,
                               CRB_Value& receiver,
                               const ScriptLocation& loc )
{
    return crb_class_create_instance<CRB_Hash>( thread, env, receiver, { }, 
                                                 nullptr, loc );
}


//////////////////////////////////////////////////////////////////////////////
// class Hash::Iterator

static EvalResult hash_i_s_new( CRB_Thread* thread,
                               LocalEnvironment& env,
                               CRB_Value& receiver,
                               const ScriptLocation& loc )
{
    EvalResult result = crb_class_create_instance<CRB_HashIterator>( 
                     thread, env, receiver, {env["hash"], env["method"]}, 
                     nullptr, loc );
    if ( result.type == EXCEPTION_EVAL_RESULT )
        return result;
    assert( result.type == NORMAL_EVAL_RESULT );

    static_cast<CRB_HashIterator*>(result.value.u.object_value)->it = static_cast<CRB_Hash*>(env["hash"].u.object_value)->pairs.begin();

    return result;
}


static EvalResult hash_i_has_next( CRB_Thread* thread,
                               LocalEnvironment& env,
                               CRB_Value& receiver,
                               const ScriptLocation& loc )
{
    // CRB_Interpreter* const interp = thread->interp;
    CRB_HashIterator* const self = static_cast<CRB_HashIterator*>(receiver.u.object_value);

    CRB_Hash* hash = static_cast<CRB_Hash*>(crb_object_find_ivar(receiver, "hash")->u.object_value);

    EvalResult result( CRB_BOOLEAN_VALUE );
    result.value.u.boolean_value = self->it != hash->pairs.end();

    return result;
}


static EvalResult hash_i_next( CRB_Thread* thread,
                               LocalEnvironment& env,
                               CRB_Value& receiver,
                               const ScriptLocation& loc )
{
    CRB_Interpreter* const interp = thread->interp;
    CRB_HashIterator* const self = static_cast<CRB_HashIterator*>(receiver.u.object_value);

    CRB_Hash* hash = static_cast<CRB_Hash*>(crb_object_find_ivar(receiver, "hash")->u.object_value);

    if ( self->it == hash->pairs.end() ) {
        return crb_runtime_error( thread, env, STOP_ITERATION, loc,
                                  _("already reach the end") );
    }

    CRB_Array* ary = new CRB_Array( interp->class_array );
    ary->list.push_back( self->it->first );
    crb_object_refer( ary->list.back() );
    ary->list.push_back( self->it->second );
    crb_object_refer( ary->list.back() );

    self->it++;

    return EvalResult( ary );
}


void class_hash_add_methods( CRB_Thread* thread, LocalEnvironment& env )
{
    CRB_Interpreter* const interp = thread->interp;

    crb_interp_create_class( thread, env, "::Hash",
                             interp->class_object,
                             ScriptLocation(), &interp->class_hash );

    // singleton methods
    CRB_add_native_function( interp, 
                crb_object_create_singleton_class(interp, interp->class_hash), 
                "new", { }, hash_s_new );

    /////////////////////////////////////////////////////////////////////
    // Hash::Iterator

    CRB_Class* klass = nullptr;
    crb_interp_create_class( thread, env, "::Hash::Iterator",
                             interp->class_object,
                             ScriptLocation(), &klass );

    // singleton methods
    CRB_add_native_function( interp, 
                crb_object_create_singleton_class(interp, klass), 
                "new", {"hash", "method"}, hash_i_s_new );
    
    // instance methods
    CRB_add_native_function( interp, klass, "has_next?", { }, hash_i_has_next );
    CRB_add_native_function( interp, klass, "next", { }, hash_i_next );
}



// Local variables:
// tab-width: 4
// c-basic-offset: 4
// indent-tabs-mode: nil
// End:
