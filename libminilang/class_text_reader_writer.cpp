﻿// -*- coding:utf-8-with-signature; mode:c++; tab-width:4; -*-

/*
  minilang -- a minilang interpreter.

  Copyright (c) 2011-2013 HORIKAWA Hisashi. All rights reserved.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// class TextReaderWriter

#include "support.h"
#include "execute.h"
#include "class_io.h"

using namespace std;


#define LINE_BUF_SIZE 1000

#if 0
/**
 * IO#gets - 1行読み込む。改行文字は削除しない。ただし、CRLFはLFに変換する。
 * @return EOFの場合はnilを返す
 */
static EvalResult text_reader_writer_gets( CRB_Thread* thread,
                           LocalEnvironment& env,
                           CRB_Value& receiver,
                           const ScriptLocation& loc )
{
    CRB_Interpreter* const interp = thread->interp;

    CRB_Value* io = crb_object_find_ivar(receiver, "io");
    CRB_Value* ext_enc = crb_object_find_ivar(receiver, "external_encoding");

    // FILE *fp;
    char buf[LINE_BUF_SIZE]; // 固定長で問題ない
    char* ret_buf = nullptr;
    int ret_len = 0;
    CRB_Interpreter* interp = thread->interp;

/*
    fp = static_cast<CRB_FileIO*>(receiver.u.object_value)->fp;
    if (!fp) {
        return crb_runtime_error( thread, ARGUMENT_ERROR, loc,
                                                     "not opened file" );
    }
*/

    while (true) {
        CRB_ValueList args = { ... };
        EvalResult r = crb_method_call( thread, env, receiver, "read",
                                        &args, //nullptr,
                                        loc, false );
        if ( r.type == EXCEPTION_EVAL_RESULT )
            return r;
        assert( r.type == NORMAL_EVAL_RESULT );
        ....型チェック. ByteStringでなければならない

/*
        if ( !fgets(buf, LINE_BUF_SIZE, fp) ) {
            if ( errno ) {
                free(ret_buf);
                return crb_runtime_error( thread, SYSTEM_CALL_ERROR, loc,
				         "system call error %d", errno );
            }
            break;
        }
*/

        int new_len;
        new_len = ret_len + strlen(buf);
        ret_buf = (char*) realloc(ret_buf, new_len + 1);
        strcpy(ret_buf + ret_len, buf);
        ret_len = new_len;

        if (ret_buf[ret_len - 1] == '\n') {
            if (ret_len >= 2 && ret_buf[ret_len - 2] == '\r') {
                ret_buf[ret_len - 2] = '\n';
                ret_buf[ret_len - 1] = '\0';
            }
            break;
        }
    }

    EvalResult result( CRB_OBJECT_VALUE );

    if ( ret_buf ) {
        result.value = crb_string_new( interp, new UnicodeString(ret_buf) );
        free(ret_buf);
        ret_buf = nullptr;
    }
    else {
        printf("ret_len = 0!!!\n"); // DEBUG
        result.value = crb_string_new( interp, new UnicodeString("") );
        // printf("%p '%s'\n", result.value.u.object_value,
        // static_cast<CRB_String*>(result.value.u.object_value)->value );
    }

#if 0 // DEBUG
    if (result.value.u.object_value) {
        printf("fgets() -> %s\n", static_cast<CRB_String*>(result.value.u.object_value)->value ); // DEBUG
    }
#endif

    return result;
}
#endif // 0


void class_text_reader_writer_add_methods( CRB_Thread* thread,
                                           LocalEnvironment& env )
{
    CRB_Interpreter* const interp = thread->interp;

    CRB_Class* klass = nullptr;
    crb_interp_create_class( thread, env, "::TextReaderWriter",
                             interp->class_object, ScriptLocation(),
                             &klass );
/*
    CRB_add_native_function( interp, klass,
                             "gets", {"?eol", "?limit"},
                             text_reader_writer_gets );
*/
}



// Local variables:
// tab-width: 4
// c-basic-offset: 4
// indent-tabs-mode: nil
// End:
