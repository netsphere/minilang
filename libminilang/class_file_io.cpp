﻿// -*- coding:utf-8-with-signature; mode:c++; tab-width:4; -*-

/*
  minilang -- a minilang interpreter.

  Copyright (c) 2011-2013 HORIKAWA Hisashi. All rights reserved.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// FileIO
// RubyのFileクラスは詰め込みすぎ. Fileと FileIOに分割する

#define _FILE_OFFSET_BITS 64 // 内部で __USE_FILE_OFFSET64 をセットさせる
// _LARGEFILE64_SOURCE は古いので, 定義しない
#include "support.h"
#include "CRB.h"
#include "execute.h"
#include <cstring>
#include <cassert>
#include <cerrno>
#include <string>
#include <unicode/ustdio.h>
#include "class_byte_array.h"
#ifdef _WIN32
  .....
#else
  #include <linux/fiemap.h>
  #include <sys/ioctl.h>
  #include <unistd.h> // write()
  #include <fcntl.h> // open()
  #include <sys/mman.h>
  #include <linux/fs.h>
  #include <sys/stat.h> // fstat()
#endif // !_WIN32

using namespace std;
using namespace icu;


struct CRB_FileIO: public CRB_Object
{
    FILE* fp;

    UnicodeString mode;

    uint32_t perm;

    /** create_instance 用 */
    CRB_FileIO( CRB_Class* klass ): CRB_Object( klass ), fp(nullptr) { }

    ~CRB_FileIO() {
        if (fp) {
            fclose(fp);
            fp = nullptr;
        }
    }
};


#if 0
/** 
 * File.open path, mode = "r", perm = 0o666 -> FileIO
 * ファイルを開く
 * ファイルが見つからなかった等の場合は、例外を投げる
 * [incompat] Fileインスタンスではなく, FileIO / TextReaderWriter インスタンスを返す
 */
static EvalResult class_file_s_open( CRB_Thread* thread,  
                                     LocalEnvironment& env,
                                     CRB_Value& receiver,
                                     const ScriptLocation& loc)
{
    CRB_Interpreter* interp = thread->interp;

    if ( !crb_object_isa(interp, env["path"], interp->class_string) ) {
        if ( crb_object_isa(interp, env["path"], interp->class_integer) ) {
            // [incompat]
            return crb_runtime_error(thread, env, CRB_RUNTIME_ERROR, loc,
                                _("file descriptor as path not supported") );
        }
        return crb_raise_type_error( thread, env, loc, env["path"],
                                     "path", "String" );
    }
    if ( env["mode"].type() != CRB_NIL_VALUE && 
             !crb_object_isa(interp, env["mode"], interp->class_string) ) {
        return crb_raise_type_error( thread, env, loc, env["mode"],
                                     "mode", "String" );
    }

    CRB_FileIO* fio = new CRB_FileIO( interp->class_file_io );

    if ( env["mode"].type() == CRB_NIL_VALUE ) {
        fio->mode = "r";
        fio->ext_enc = "UTF-8"; // TODO: ロケールの文字コード
    }
    else {
        CRB_String* mode = static_cast<CRB_String*>(env["mode"].u.object_value);
        int idx = mode->value->indexOf(":");
        if ( idx < 0 ) {
            fio->mode = *mode->value;
            fio->ext_enc = "UTF-8"; // TODO: ロケールの文字コード
        }
        else {
            if ( idx == 0 )
                fio->mode = "r";
            else 
                fio->mode = UnicodeString( *mode->value, 0, idx );

            int idx2 = mode->value->indexOf(":", idx + 1);
            if ( idx2 < 0 ) {
                // ext_enc のみ
                fio->ext_enc = UnicodeString( *mode->value, idx + 1 );
            }
            else {
                if ( idx2 == idx + 1 )       
                    fio->ext_enc = "UTF-8"; // TODO: ロケールの文字コード
                else 
                    fio->ext_enc = UnicodeString( *mode->value, 
                                              idx + 1, idx2 - idx - 1 );

                UnicodeString int_enc = UnicodeString( *mode->value, idx2 + 1 );
                if ( int_enc != "" && int_enc != "UTF-16" ) {
                    delete fio;
                    return crb_runtime_error( thread, env, ARGUMENT_ERROR, loc,
                                     _("Internal encoding is not supported") );
                }
            }
        }
    }
#endif // 0


//////////////////////////////////////////////////////////////////////////
// FileIO

/** FileIO.new */
static EvalResult fileio_s_new( CRB_Thread* thread,
                                LocalEnvironment& env,
                                CRB_Value& receiver,
                                const ScriptLocation& loc )
{
    return crb_class_create_instance<CRB_FileIO>( thread, env, receiver, 
                              {env["path"], env["mode"], env["perm"]},
                              nullptr, loc );
}


static EvalResult fileio_initialize( CRB_Thread* thread,
                                     LocalEnvironment& env,
                                     CRB_Value& receiver,
                                     const ScriptLocation& loc )
{
    CRB_Interpreter* const interp = thread->interp;
    CRB_FileIO* self = static_cast<CRB_FileIO*>(receiver.u.object_value);

    if ( !crb_object_isa(interp, env["path"], interp->class_string) ) {
        return crb_raise_type_error( thread, env, loc, env["path"],
                                     "path", "String" );
    }
    if ( !crb_object_isa(interp, env["mode"], interp->class_string) ) {
        return crb_raise_type_error( thread, env, loc, env["mode"],
                                     "mode", "String" );
    }

    self->mode = *static_cast<CRB_String*>(env["mode"].u.object_value)->value;

    string m;
    self->mode.toUTF8String(m);

    self->fp = fopen( 
            static_cast<CRB_String*>(env["path"].u.object_value)->std_string().c_str(),
            m.c_str() );

    if ( !self->fp ) {
        CRB_Value no(CRB_INT_VALUE);
        mpz_set_si(no.u.int_value, errno);
        return crb_runtime_error( thread, env, SYSTEM_CALL_ERROR, loc, 
                                  _("File open error"), {no} );
    }

    return EvalResult( CRB_NIL_VALUE );
}


static EvalResult fileio_close( CRB_Thread* thread,
                                LocalEnvironment& env,
                                CRB_Value& receiver,
                                const ScriptLocation& loc )
{
    // CRB_Interpreter* const interp = thread->interp;
    CRB_FileIO* self = static_cast<CRB_FileIO*>(receiver.u.object_value);

    fclose( self->fp );
    self->fp = nullptr;

    return EvalResult( crb_object_refer(receiver) );
}


/**
 * FileIO#write str -> Integer
 */
static EvalResult fileio_write( CRB_Thread* thread,
                                LocalEnvironment& env,
                                CRB_Value& receiver,  // 今のところrelease不要
                                const ScriptLocation& loc )
{
    CRB_Interpreter* interp = thread->interp;
    CRB_FileIO* self = static_cast<CRB_FileIO*>(receiver.u.object_value);

    if ( !crb_object_isa(interp, env["bytes"], interp->class_byte_array) ) {
        return crb_raise_type_error( thread, env, loc, env["bytes"],
                                     "bytes", "ByteArray" );
    }
    CRB_ByteArray* bytes = 
                     static_cast<CRB_ByteArray*>(env["bytes"].u.object_value);
/*
    UFILE* out = u_finit( self->fp, nullptr, nullptr );

    CRB_Value& arg = env["str"];
    // CRB_Class* str_class = crb_interp_find_class(interp, CRB_STRING_CLASS);

    .........外部エンコーディングを参照すること
    if ( !crb_object_isa(interp, arg, interp->class_string) ) {
        // 暗黙の型変換
        EvalResult v = crb_type_change( thread, env, arg, "to_s", str_class, 
                                            loc );
        if (v.type == EXCEPTION_EVAL_RESULT) {
            u_fclose(out);
            return v;
        }
        assert( v.type == NORMAL_EVAL_RESULT );

        u_fprintf( out, "%S", 
                       static_cast<CRB_String*>(v.value.u.object_value)->
                                                value->getTerminatedBuffer() );

        crb_object_release(&v.value);
    }
    else {
        u_fprintf( out, "%S", 
                       static_cast<CRB_String*>(arg.u.object_value)->
                                               value->getTerminatedBuffer() );
    }
    u_fclose(out);
*/
    
    if ( !self->fp ) {
        return crb_runtime_error( thread, env, CRB_RUNTIME_ERROR, loc,
                                  _("file not open.") );
    }

    intptr_t r = fwrite( bytes->ptr, bytes->size, 1, self->fp );
    if ( r < bytes->size ) {
        CRB_Value err(CRB_INT_VALUE);
        mpz_set_si(err.u.int_value, errno);
        return crb_runtime_error( thread, env, SYSTEM_CALL_ERROR, loc,
                                  _("write error"), {err} );
    }

    EvalResult result( CRB_INT_VALUE );
    mpz_set_si( result.value.u.int_value, r );

    return result;
}


static EvalResult fileio_read( CRB_Thread* thread,
                               LocalEnvironment& env,
                               CRB_Value& receiver,  // 今のところrelease不要
                               const ScriptLocation& loc )
{
    CRB_Interpreter* interp = thread->interp;
    CRB_FileIO* self = static_cast<CRB_FileIO*>(receiver.u.object_value);

    if ( !crb_object_isa(interp, env["max_length"], interp->class_integer) ) {
        return crb_raise_type_error( thread, env, loc, env["max_length"],
                                     "max_length", "Integer" );
    }
    if ( mpz_get_si(env["max_length"].u.int_value) < 0 ) {
        return crb_runtime_error(thread, env, ARGUMENT_ERROR, loc, 
                     UnicodeString(_("max_length must not be negative")) );
    }

    if ( !self->fp ) {
        return crb_runtime_error( thread, env, CRB_RUNTIME_ERROR, loc,
                                  _("file not open.") );
    }

    CRB_ByteArray* bytes = new CRB_ByteArray( interp->class_byte_array );

    bytes->capacity = mpz_get_si(env["max_length"].u.int_value);
    bytes->ptr = (unsigned char*) malloc( bytes->capacity );
    assert( bytes->ptr );
    bytes->own = true;

    bytes->size = fread( bytes->ptr, bytes->capacity, 1, self->fp );
    if ( !bytes->size ) { 
        if ( ferror(self->fp) ) { 
            CRB_Value err(CRB_INT_VALUE);
            mpz_set_si(err.u.int_value, errno);
            delete bytes;
            return crb_runtime_error( thread, env, SYSTEM_CALL_ERROR, loc, 
                                  _("File open error"), {err} );
        }
        else if ( feof(self->fp) ) {
            delete bytes;
            return EvalResult(CRB_NIL_VALUE);
        }
    }

    return EvalResult( bytes );
}


/** FileUtils.copy */
static EvalResult file_utils_s_copy( CRB_Thread* thread,
                                LocalEnvironment& env,
                                CRB_Value& receiver,  // 今のところrelease不要
                                const ScriptLocation& loc )
{
    CRB_Interpreter* interp = thread->interp;

    if ( !crb_object_isa(interp, env["src_file"], interp->class_string) ) {
        return crb_raise_type_error( thread, env, loc, env["src_file"],
                                     "src_file", "String" );
    }
    if ( !crb_object_isa(interp, env["dest_file"], interp->class_string) ) {
        return crb_raise_type_error( thread, env, loc, env["dest_file"],
                                     "dest_file", "String" );
    }

    UnicodeString* src_file = static_cast<CRB_String*>(env["src_file"].u.object_value)->value;
    UnicodeString* dest_file = static_cast<CRB_String*>(env["dest_file"].u.object_value)->value;

#ifdef _WIN32
    // 代替ストリームがあるので, Win32 APIを使うこと
    // http://msdn.microsoft.com/en-us/library/windows/desktop/aa363852%28v=vs.85%29.aspx
    ::CopyFileEx( existing_file_name, new_file_name, nullptr,
                  nullptr, false, 0 );

#else
    // See http://www.nminoru.jp/~nminoru/programming/sparse_file.html
    // cpコマンドのオプション
    //   --sparse=auto/always
    //   --link コピーする代わりにハードリンクを作る

    string s;
    CRB_Value err(CRB_INT_VALUE);

    src_file->toUTF8String(s);

    int rfd = open( s.c_str(), O_RDONLY );
    if ( rfd < 0 ) {
        mpz_set_si(err.u.int_value, errno);
        return crb_runtime_error(thread, env, SYSTEM_CALL_ERROR, loc, 
                                 _("open for src_file failed"), {err} );
    }

    struct stat statinfo;
    if ( fstat(rfd, &statinfo) < 0 ) {
        mpz_set_si(err.u.int_value, errno);
        close(rfd);
        return crb_runtime_error(thread, env, SYSTEM_CALL_ERROR, loc, 
                                 _("fstat for src_file failed"), {err} );
    }

    // FS_IOC_FIEMAP は, <linux/fs.h> で定義されている
    int SIZ = sizeof(offsetof(struct fiemap, fm_extents[0])) + 
              sizeof(struct fiemap_extent) * 100;
    struct fiemap* src_fiemap = (struct fiemap*) malloc( SIZ );
    memset( src_fiemap, 0, SIZ );

    dest_file->toUTF8String(s);
    // すでにファイルが存在するときは上書き
    int wfd = open( s.c_str(), O_CREAT | O_WRONLY | O_TRUNC );
    if ( wfd < 0 ) {
        mpz_set_si(err.u.int_value, errno);
        close(rfd);
        free(src_fiemap);
        return crb_runtime_error(thread, env, SYSTEM_CALL_ERROR, loc, 
                                 _("open for src_file failed"), {err} );
    }        

    off_t start = 0;
    while ( start < statinfo.st_size ) {
        src_fiemap->fm_start = start; // 後ろへずらしていく
        src_fiemap->fm_length = statinfo.st_size;
        src_fiemap->fm_flags = FIEMAP_FLAG_SYNC;
        src_fiemap->fm_extent_count = 100;

        int r = ioctl( rfd, FS_IOC_FIEMAP, src_fiemap );
        if ( r < 0 ) {
            mpz_set_si( err.u.int_value, errno );
            close(rfd);
            close(wfd);
            free(src_fiemap);
            return crb_runtime_error(thread, env, SYSTEM_CALL_ERROR, loc, 
                                 _("ioctl failed"), {err} );
        }

        for ( unsigned i = 0; i < src_fiemap->fm_mapped_extents; i++ ) {
            struct fiemap_extent* extent = &src_fiemap->fm_extents[i];

            void* map = mmap( nullptr, extent->fe_length, 
                              PROT_READ, MAP_SHARED, rfd, 
                              extent->fe_logical );
            assert( map != MAP_FAILED );

            lseek( wfd, extent->fe_logical, SEEK_SET );
            write( wfd, map, extent->fe_length );

            munmap( map, extent->fe_length );

            start = extent->fe_logical + extent->fe_length;
        }
    }

    close(rfd);
    close(wfd);
    free( src_fiemap );
#endif // _WIN32

    return EvalResult(CRB_NIL_VALUE);
}


void class_file_add_methods( CRB_Thread* thread, LocalEnvironment& env )
{
    CRB_Interpreter* const interp = thread->interp;

    ///////////////////////////////////////////////////////////////////////
    // class File
    CRB_Class* file = nullptr;
    crb_interp_create_class( thread, env, "::File", 
                             interp->class_object,
                             ScriptLocation(), &file );

    // CRB_Class* file_s = crb_object_create_singleton_class( interp, file );

    ///////////////////////////////////////////////////////////////////////
    // class FileIO
    // FileとFileIOを分ける [incompat]
    crb_interp_create_class( thread, env, "::File::FileIO", 
                             interp->class_io, ScriptLocation(), 
                             &interp->class_file_io );

    CRB_Class* fileio_s = crb_object_create_singleton_class( interp, 
                                                      interp->class_file_io );

    CRB_add_native_function( interp, fileio_s, "new", {"path", "mode", "perm"},
                             fileio_s_new );

    CRB_add_native_function( interp, interp->class_file_io, "initialize", 
                             {"path", "mode", "perm"}, fileio_initialize );

    CRB_add_native_function( interp, interp->class_file_io, "close", { }, 
                             fileio_close );

    CRB_add_native_function( interp, interp->class_file_io, "write", {"bytes"}, 
                             fileio_write );
    CRB_add_native_function( interp, interp->class_file_io, "read", 
                             {"max_length"}, 
                             fileio_read );

    ///////////////////////////////////////////////////////////////////////
    // module FileUtils

    CRB_Module* klass = nullptr;
    crb_interp_create_module( thread, env, "::FileUtils",
                              ScriptLocation(),
                              &klass );

    CRB_add_native_function( interp, 
                             crb_object_create_singleton_class(interp, klass),
                             "copy", 
                             {"src_file", "dest_file", "?options"},
                             file_utils_s_copy );
}



// Local variables:
// tab-width: 4
// c-basic-offset: 4
// indent-tabs-mode: nil
// End:
