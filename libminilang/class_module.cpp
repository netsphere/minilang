﻿// -*- coding:utf-8-with-signature; mode:c++; tab-width:4; -*-

/*
  minilang -- a minilang interpreter.

  Copyright (c) 2011-2013 HORIKAWA Hisashi. All rights reserved.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// class module

#include "support.h"
#include "CRB.h"
#include "DBG.h"
#include "execute.h"
#include <assert.h>
#include "ast.h"

using namespace std;
using namespace icu;


/*
Ruby は class Module に #attr_accessor, #attr_reader, #attr_writer が定義されて
おり、モジュールがインスタンス変数を持つ。マジか...

Class が Module のサブクラスなのは、設計の誤り。継承関係にないものを継承させる,
いつもの悪い癖。

Smalltalk の継承関係は、次のようになる。
  Object
    Behavior   `superclass`, `subclasses` 今風に言えばデフォルト実装付きインタフェイス
      ClassDescription    `instanceVariables`, `organization` を持つ
        Class
そうすると, `ClassDescription` から Class, Module を派生させるのがよさそう。
*/

/////////////////////////////////////////////////////////////////////////
// class abstract ClassDescription

void class_class_description_add_methods( CRB_Thread* thread,
                                          LocalEnvironment& env )
{
    CRB_Interpreter* const interp = thread->interp;

    // interpreter.cpp の先頭で登録
    //crb_interp_create_class( thread, env, "::ClassDescription",
    //                         interp->class_object, ScriptLocation(),
    //                         &interp->class_class_description );
}

/** 単にクラスのインスタンスメソッドを検索する.  */
Function* crb_module_find_method( const CRB_ClassDescription& klass,
                                   const UnicodeString& method_name )
{
    CRB_MethodMap::const_iterator i = klass.methods.find(method_name);
    if ( i == klass.methods.cend() )
        return nullptr;
    else
        return i->second;
}


/** インスタンスメソッドを追加する */
void CRB_add_native_function( CRB_Interpreter* interp,
                              CRB_ClassDescription* klass,
                              const UnicodeString& method_name,
                              const vector<UnicodeString>& param_names,
                              CRB_NativeFunctionProc proc )
{
    assert(klass);

    ParameterList* param_list = new ParameterList();

    vector<UnicodeString>::const_iterator i;
    for ( i = param_names.begin(); i != param_names.end(); i++ ) {
        if ( i->startsWith("?") ) {
            OptionalParamNode* n = new OptionalParamNode(
                        new UnicodeString(*i, 1),
                        new LiteralExpression( NULL_EXPRESSION, YYLTYPE(), nullptr ));
            if ( n->var_name->startsWith("&") == 0 )
                abort(); // "?&..."は不可   TODO: エラーメッセージ
            param_list->optional_params.push_back(n);
        }
        else if ( i->startsWith("&") )
            param_list->block_param = new UnicodeString(*i, 1);
        else if ( i->startsWith("*") )
            param_list->rest_param = new UnicodeString(*i, 1);
        else {
            //ParameterNode* n = new ParameterNode();
            //n->var_name = new UnicodeString(*i);
            param_list->params.push_back(new UnicodeString(*i));
        }
    }

    Function* fd = new Function();
    fd->type = NATIVE_FUNCTION_DEFINITION;
    // fd->u.native_f.name = new UnicodeString(method_name);
    fd->u.native_f.param_list = param_list;
    fd->u.native_f.proc = proc;

    pair<CRB_MethodMap::iterator, bool> r =
        klass->methods.insert(pair<CRB_MethodMap::key_type, Function*>(method_name, fd));
    if ( !r.second ) {
        assert(0); // TODO: error処理
    }
}

CRB_ClassDescription::~CRB_ClassDescription() {
    crb_varmap_free_objects(&classvars);
}


////////////////////////////////////////////////////////////////////////////
// class Module

/**
 * Module.new
 * 無名モジュールを作る
 */
static EvalResult module_s_new( CRB_Thread* thread,
                                LocalEnvironment& env,
                                CRB_Value& receiver,
                                const ScriptLocation& loc )
{
    return crb_class_create_instance<CRB_Module>( thread, env, receiver, { },
                                                  nullptr, loc );
}


/*
static EvalResult module_initialize( CRB_Thread* thread,
                                     LocalEnvironment& env,
                                     CRB_Value& receiver,
                                     const ScriptLocation& loc )
{
    return EvalResult( CRB_NIL_VALUE );
}
*/


/** Module#to_s */
static EvalResult class_module_to_s( CRB_Thread* thread,
                                     LocalEnvironment& env,
                                     CRB_Value& receiver,
                                     const ScriptLocation& loc )
{
    return EvalResult( crb_string_new(thread->interp,
                                      new UnicodeString(static_cast<CRB_Class*>(receiver.u.object_value)->name)) );
}


/**
 * レシーバのインスタンスメソッドを定義する.
 * Module#define_method name, method
 */
static EvalResult module_define_method( CRB_Thread* thread,
                                        LocalEnvironment& env,
                                        CRB_Value& receiver,
                                        const ScriptLocation& loc )
{
    CRB_Interpreter* const interp = thread->interp;

    if ( !crb_object_isa(interp, env["name"], interp->class_symbol) ) {
        return crb_raise_type_error( thread, env, loc, env["name"],
                                     "name", "Symbol" );
    }
    if ( !crb_object_isa(interp, env["method"], interp->class_proc) ) {
        return crb_raise_type_error( thread, env, loc, env["method"],
                                     "method", "Proc" );
    }

    CRB_Module* self = static_cast<CRB_Module*>(receiver.u.object_value);

    // proc そのものではなく, lambda式のみ参照する. proc は解放されて構わない
    Function* func = new Function();
    func->type = SCRIPT_FUNCTION;
    func->u.ast_node =
                  static_cast<CRB_Proc*>(env["method"].u.object_value)->expr;

    UnicodeString name = crb_interp_symbol_to_str( interp,
                                                   env["name"].u.symbol_value);
    pair<CRB_MethodMap::iterator, bool> r =
        self->methods.insert(
                      pair<CRB_MethodMap::key_type, Function*>(name, func) );
    if ( !r.second ) {
        delete func;
        return crb_runtime_error( thread, env, FUNCTION_MULTIPLE_DEFINE_ERR,
                             loc,
                             UnicodeString(_("Method multiple define: ")) +
                             name );
    }

    return EvalResult( CRB_NIL_VALUE );
}


void class_module_add_methods( CRB_Thread* thread, LocalEnvironment& env )
{
    CRB_Interpreter* const interp = thread->interp;
    CRB_Class* klass = interp->class_module;
    assert(klass);

    // singleton methods
    CRB_add_native_function( interp,
                             crb_object_create_singleton_class(interp, klass),
                             "new", { },
                             module_s_new );

    // instance methods
/*
    CRB_add_native_function( interp, klass, "initialize", { },
                             module_initialize );
*/
    CRB_add_native_function(interp, klass, "to_s", { }, class_module_to_s );
    CRB_add_native_function( interp, klass,
                             "define_method", {"name", "method"},
                             module_define_method );
}



// Local variables:
// tab-width: 4
// c-basic-offset: 4
// indent-tabs-mode: nil
// End:
