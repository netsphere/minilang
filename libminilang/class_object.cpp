﻿// -*- coding:utf-8-with-signature; mode:c++; tab-width:4; -*-

/*
  minilang -- a minilang interpreter.

  Copyright (c) 2011-2013 HORIKAWA Hisashi. All rights reserved.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// class Object

#include "support.h"
#include <stdlib.h>  // UNIX: realpath()
#include <stdio.h>
#include <string.h>
#include "DBG.h"
#include "execute.h"
#include <assert.h>
#include <unicode/ustdio.h>
#include <sys/stat.h>
#include "parser.h"
#include "eval.h"
#include <set>

using namespace std;
using namespace icu;


/** 参照カウンタを加算する */
CRB_Value& crb_object_refer( CRB_Value& v )
{
    if ( v.type() == CRB_OBJECT_VALUE ) {
        assert( dynamic_cast<CRB_Object*>(v.u.object_value) );

#ifdef _WIN32
        _InterlockedIncrement(&v.u.object_value->ref_count);
#else
        __sync_add_and_fetch(&v.u.object_value->ref_count, +1);
#endif
    }

    return v;
}


/** 参照カウンタを加算する */
CRB_Object* crb_object_refer( CRB_Object* v )
{
    if ( v ) {
#ifdef _WIN32
        _InterlockedIncrement(&v->ref_count);
#else
        __sync_add_and_fetch(&v->ref_count, +1);
#endif
    }

    return v;
}


/** オブジェクトを解放 */
void crb_object_release( CRB_Value* v )
{
    assert( v );

    if ( v->type() == CRB_OBJECT_VALUE ) {
        CRB_Object* obj = v->u.object_value;
        if (!obj)
            return;
        assert( dynamic_cast<CRB_Object*>(obj) );

#ifdef _WIN32
        int32_t r = _InterlockedExchangeAdd(&obj->ref_count, -1);
#else
        int32_t r = __sync_fetch_and_add(&obj->ref_count, -1);
#endif

        DBG_assert( r >= 1, ("obj->ref_count..%d\n", r - 1) );
        if ( r == 1 ) {
            delete obj;
            v->u.object_value = nullptr;
        }
    }
    else if ( v->type() == CRB_TAIL_CALL_VALUE ) {
        delete v->u.tail_call;
        v->u.tail_call = nullptr;
    }

    // CRB_INT_VALUEの場合はデストラクタでクリアする。ここでは対処不要。
    // mpz_clear( v->u.int_value );
}


/**
 * インスタンス変数に代入する. 新たに設定するオブジェクトは, 内部でrefer*しない*.
 * すでに設定されていたオブジェクトは, 新たに設定するオブジェクトと同一でない限り, releaseする
 * @param ivar_name   インスタンス変数名. 先頭の"@"は省略すること
 * @return 同一オブジェクトだったので, 何もしていないとき false
 */
bool crb_object_assign_ivar( CRB_Object* obj,
                             const UnicodeString& ivar_name,
                             const CRB_Value& value )
{
    assert( !ivar_name.startsWith("@") );

    VariableMap::iterator i = obj->ivars.find(ivar_name);
    if ( i != obj->ivars.end() ) {
        if ( i->second < value || value < i->second ) {
            CRB_Value tmp = i->second;
            i->second = value;
            // crb_object_refer( i->second );
            crb_object_release( &tmp );
            return true;
        }
        else
            return false;
    }
    else {
        obj->ivars.insert( pair<UnicodeString, CRB_Value>(ivar_name, value) );
        // crb_object_refer( r.first->second );
        return true;
    }
}


/**
 * インスタンス変数を検索する. それぞれのオブジェクトが所有する
 * @param ivar_name   先頭の"@"は省略すること
 * @return 見つかったときはその値へのポインタ. refer しない。見つからなかったときは nullptr.
 */
CRB_Value* crb_object_find_ivar( const CRB_Value& v,
                                 const UnicodeString& ivar_name )
{
    assert( !ivar_name.startsWith("@") );

    if ( v.type() == CRB_OBJECT_VALUE ) {
        CRB_Object* obj = static_cast<CRB_Object*>(v.u.object_value);
        VariableMap::iterator i = obj->ivars.find(ivar_name);
        if ( i == obj->ivars.end() )
            return nullptr;
        return &i->second;
    }

    return nullptr;
}

/**
 * クラス変数を検索する.
 * @param classvar   先頭の "@@" は省略すること
 * @return 見つかったときはその値へのポインタ. refer しない。見つからなかったときは nullptr.
 */
CRB_Value* crb_object_find_classvar( const CRB_Value& v,
                                 const UnicodeString& classvar )
{
    assert( !classvar.startsWith("@@") );

    if ( v.type() == CRB_OBJECT_VALUE && v.is? Class ) {
        CRB_Object* obj = static_cast<CRB_Object*>(v.u.object_value);
        VariableMap::iterator i = obj->ivars.find(ivar_name);
        if ( i == obj->ivars.end() )
            return nullptr;
        return &i->second;
    }
    else {
        // まずクラスを得る.
        ●●
            必要があれば、特異クラスをここで作る
    }

    return nullptr;
}

/**
 * 内部利用: 値のクラスを得る.
 * @return 値のクラス
 * 特異クラス (singleton class) は無視する.
 */
CRB_Class* crb_object_get_class( CRB_Interpreter* interp, const CRB_Value& v )
{
    assert(interp);
    CRB_Class* r = nullptr;

    switch ( v.type() ) {
    case CRB_BOOLEAN_VALUE:
        r = interp->class_boolean; ●●これは通らない
        break;
    case CRB_INT_VALUE:
        r = interp->class_integer;
        break;
    case CRB_DOUBLE_VALUE:
        r = interp->class_float;
        break;
    case CRB_OBJECT_VALUE:
        assert( v.u.object_value->m_class );
        r = v.u.object_value->m_class;
        break;
    case CRB_NIL_VALUE:
        r = interp->class_nilclass;
        break;
    case CRB_SYMBOL_VALUE:
        r = interp->class_symbol;
        break;

    case CRB_TAIL_CALL_VALUE:
        abort(); // あり得ない
    }

    DBG_assert( r, ("v->type = %d\n", v.type()) );
    return r;
}


/* ruby: クラスに特異クラスを作るときは、その特異クラス (metaclass) のスーパクラスは、元のクラスのスーパクラスの特異クラスになる。
        obj_c <- class C  <-  metaC
                    ↑           ↑
        obj_d <- class D  <-  metaD
  例えば, IO.popen() を File.popen() としても呼び出せる.

Smalltalk: それぞれのクラスについて, `Metaclass` クラスから派生した無名クラスがクラスのクラスになる。
CL: 基本は `standard-class` メタクラスが各クラスのクラスになる。クラス変数は別の仕組みで実装。
  `standard-class` から派生させた自作のメタクラスを
    (defclass my-class (superclasses) (slots) (:metaclass class-name))
  で指定できる。
--> いずれにしても、メタクラスは元のメタクラスから派生させる形。Ruby は何由来か?
*/

/**
 * 特異クラスを生成する. 特異クラスがすでにある場合は、メソッド名に反して, 既存
 * の特異クラスを返す。
 * @param v オブジェクト.
 */
CRB_Class* crb_object_create_singleton_class( CRB_Interpreter* interp,
                                              CRB_Object* v )
{
    assert( interp );
    assert( v );

    if ( v->singleton_class )
        return v->singleton_class;

    char buf[100];
    sprintf(buf, "#<%p:singleton>", v); // 一意

    //CRB_Class* vv = dynamic_cast<CRB_Class*>(v);
    //if ( vv ) {
    // クラスの場合, metaclass は `Class` クラスのサブクラスにする. [[incompat]]
    // クラス以外のオブジェクトの場合も, 元のクラスのサブクラスにする.
    v->singleton_class = new CRB_Class( interp->class_class, buf,
                                            v->m_class ); // superclass
        //crb_object_create_singleton_class(interp, vv->superclass) );
    //}
    //else {
    //    v->singleton_class = new CRB_Class( interp->class_class, buf,
    //                                        v->m_class );
    //}

    return v->singleton_class;
}


/** オブジェクトがどのクラスのインスタンスか. 多重継承なので, モジュールでも可 */
bool crb_object_isa( CRB_Interpreter* interp, const CRB_Value& v,
                     const CRB_ClassDescription* klass )
{
    assert( interp );
    assert( klass );

    const CRB_Class* vclass = crb_object_get_class( interp, v );
    assert(vclass);

    for ( ; vclass; vclass = vclass->superclass ) {
        if (vclass == klass)
            return true;

        // モジュールも
        vector<CRB_Module*>::const_iterator it;
        for ( it = vclass->included_modules.begin();
                               it != vclass->included_modules.end(); it++ ) {
            if ( *it == klass )
                return true;
        }
    }

    return false;
}


//////////////////////////////////////////////////////////////////////////
// class Object

CRB_Object::~CRB_Object()
{
    if ( singleton_class ) {
#if 0
        UFILE* out = u_finit(stdout, nullptr, nullptr);
        u_fprintf( out, "free singleton class: %S\n", const_cast<UnicodeString&>(klass->name).getTerminatedBuffer() );
        u_fclose(out);

        if (klass->ref_count != 1) {
            printf("ref_count must be 1, but %d\n", klass->ref_count);
        }
#endif // !NDEBUG

        delete singleton_class;
        singleton_class = nullptr;
    }

    crb_varmap_free_objects(&ivars);
}


/**
 * #class メソッド
 */
static EvalResult class_object_class( CRB_Thread* thread,
                                      LocalEnvironment& env,
                                      CRB_Value& receiver,
                                      const ScriptLocation& loc )
{
    return EvalResult( crb_object_refer(
                          crb_object_get_class(thread->interp, receiver)) );
}


#if 0
/**
 * singleton class用の名前。
 * to_sはオーバーライドされることがある
 */
static EvalResult class_object_inspect( CRB_Thread* thread,
                                        LocalEnvironment& env,
                                        CRB_Value& receiver,
                                        const ScriptLocation& loc )
{
    UnicodeString r = UnicodeString("#<") + receiver.u.object_value->klass->name;
    UChar buf[100];
    u_sprintf(buf, "%p>", receiver.u.object_value );

    return EvalResult( crb_string_new(thread->interp,
                                               new UnicodeString(r + buf)) );
}
#endif // 0


/*
 * Object#is_a? メソッド
 * rubyでは引数としてモジュールも可. 多重継承なので, 合わせる.
 */
static EvalResult class_object_isa( CRB_Thread* thread,
                                    LocalEnvironment& env,
                                    CRB_Value& receiver,
                                    const ScriptLocation& loc )
{
    CRB_Interpreter* const interp = thread->interp;

    //CRB_Class* mod_class = crb_interp_find_class(interp, CRB_MODULE_CLASS);

    if ( !crb_object_isa(interp, env["klass"], interp->class_module) ) {
        return crb_raise_type_error( thread, env, loc, env["klass"],
                                     "klass", "Module" );
    }

    EvalResult result( CRB_BOOLEAN_VALUE );
    result.value.u.boolean_value = crb_object_isa(interp, receiver,
                      static_cast<CRB_Module*>(env["klass"].u.object_value) );

    return result;
}


static EvalResult object_instance_variable_get( CRB_Thread* thread,
                                    LocalEnvironment& env,
                                    CRB_Value& receiver,
                                    const ScriptLocation& loc )
{
    CRB_Interpreter* const interp = thread->interp;

    UnicodeString var_name;
    if ( crb_object_isa(interp, env["var"], interp->class_symbol) )
        var_name = crb_interp_symbol_to_str(interp, env["var"].u.symbol_value);
    else if ( crb_object_isa(interp, env["var"], interp->class_string) )
        var_name = *static_cast<CRB_String*>(env["var"].u.object_value)->value;
    else {
        return crb_raise_type_error( thread, env, loc, env["var"],
                                     "var", "Symbol/String" );
    }

    if ( !var_name.startsWith("@") ) {
        return crb_runtime_error( thread, env, ARGUMENT_ERROR, loc,
                                  _("Argument must start '@'") );
    }

    CRB_Value* vp = crb_object_find_ivar(receiver, UnicodeString(var_name, 2) );
    if ( !vp ) {
        // ruby ではnil. [incompat]
        return crb_runtime_error( thread, env, CRB_NAME_ERROR, loc,
                                  _("ivar not found") );
    }

    return crb_object_refer(*vp);
}


static EvalResult object_instance_variable_set( CRB_Thread* thread,
                                    LocalEnvironment& env,
                                    CRB_Value& receiver,
                                    const ScriptLocation& loc )
{
    CRB_Interpreter* const interp = thread->interp;

    UnicodeString var_name;
    if ( crb_object_isa(interp, env["var"], interp->class_symbol) )
        var_name = crb_interp_symbol_to_str(interp, env["var"].u.symbol_value);
    else if ( crb_object_isa(interp, env["var"], interp->class_string) )
        var_name = *static_cast<CRB_String*>(env["var"].u.object_value)->value;
    else {
        return crb_raise_type_error( thread, env, loc, env["var"],
                                     "var", "Symbol/String" );
    }

    if ( !var_name.startsWith("@") ) {
        return crb_runtime_error( thread, env, ARGUMENT_ERROR, loc,
                                  _("Argument must start '@'") );
    }

    if ( receiver.type() != CRB_OBJECT_VALUE ) {
        return crb_runtime_error( thread, env, TYPE_ERROR, loc,
                                  _("Argument is primitive type.") );
    }

    bool r = crb_object_assign_ivar( receiver.u.object_value,
                            UnicodeString(var_name, 2), env["value"] );
    if ( r )
        crb_object_refer(env["value"]);

    return EvalResult( crb_object_refer(env["self"]) );
}


static EvalResult object_instance_variables( CRB_Thread* thread,
                                             LocalEnvironment& env,
                                             CRB_Value& receiver,
                                             const ScriptLocation& loc )
{
    CRB_Interpreter* const interp = thread->interp;
    CRB_Object* self = static_cast<CRB_Object*>(receiver.u.object_value);

    CRB_Array* ary = new CRB_Array( interp->class_array );

    VariableMap::iterator i = self->ivars.begin();
    for ( ; i != self->ivars.end(); i++ ) {
        ary->list.push_back(
                crb_interp_get_symbol(interp, UnicodeString("@") + i->first) );
    }

    return CRB_Value( ary );
}


// Object#methods - そのオブジェクトに対して呼び出せるメソッドの一覧
static EvalResult object_methods( CRB_Thread* thread,
                                  LocalEnvironment& env,
                                  CRB_Value& receiver,
                                  const ScriptLocation& loc )
{
    CRB_Interpreter* const interp = thread->interp;
    CRB_Object* const self = static_cast<CRB_Object*>(receiver.u.object_value);

    // メソッドを検索する
    // singleton class がある場合は, まずそれ (とそのスーパークラス) を探索する
    MethodSearchGraph graph;
    if ( receiver.type() == CRB_OBJECT_VALUE && self->singleton_class )
        crb_create_method_search_graph( &graph, self->singleton_class );
    else {
        crb_create_method_search_graph( &graph,
                                crb_object_get_class(interp, receiver) );
    }

    set<UnicodeString> list;

    vector<const CRB_Module*>::const_iterator k;
    for ( k = graph.begin(); k != graph.end(); k++ ) {
        CRB_MethodMap::const_iterator m;
        for ( m = (*k)->methods.cbegin(); m != (*k)->methods.cend(); m++ )
            list.insert( m->first );
    }

    CRB_Array* ary = new CRB_Array( interp->class_array );

    set<UnicodeString>::const_iterator i;
    for ( i = list.begin(); i != list.end(); i++ )
        ary->list.push_back( crb_interp_get_symbol(interp, *i) );

    return CRB_Value( ary );
}


/**
 * 3つの書き方がある
 *   raise "エラーメッセージ"       RuntimeErrorを投げる
 *   raise クラス名, ...           クラス.new(...) で生成されたオブジェクトを投げる
 *                                クラスは例外クラスでなくともok
 *   raise                        rescue節から例外の再送出. TODO: impl.
 */
static EvalResult class_object_raise( CRB_Thread* thread,
                                      LocalEnvironment& env,
                                      CRB_Value& receiver,
                                      const ScriptLocation& loc )
{
    CRB_Interpreter* const interp = thread->interp;

    CRB_Value klass( CRB_OBJECT_VALUE );
    CRB_ArgValues exception_args;

    if ( env["type_or_msg"].type() == CRB_NIL_VALUE ) {
        abort(); // 例外の再送出. TODO: impl.
    }
    else if ( crb_object_isa(interp, env["type_or_msg"],
                                                   interp->class_string) &&
              env["message"].type() == CRB_NIL_VALUE ) {
        // 1引数
        klass.u.object_value = interp->class_runtime_error; // その他エラー
        exception_args.push_back( env["type_or_msg"] );
    }
    else if ( crb_object_isa(interp, env["type_or_msg"],
                                                     interp->class_class) ) {
        // 2引数
        klass = env["type_or_msg"];
        exception_args.push_back( env["message"] );
    }
    else {
        return crb_raise_type_error( thread, env, loc, env["type_or_msg"],
                                     "type_or_msg", "String/Class" );
    }

    EvalResult r = crb_method_call( thread, env,
                                    klass, "new", &exception_args, //nullptr,
                                    loc, false );
    if (r.type == EXCEPTION_EVAL_RESULT)
        return r;
    assert( r.type == NORMAL_EVAL_RESULT );
    if ( !crb_object_isa(interp, r.value, interp->class_exception) ) {
        EvalResult result = crb_raise_type_error( thread, env, loc,
                                       r.value, "result of new", "Exception" );
        crb_object_release( &r.value );
        return result;
    }

    // TODO: これでは不味い. 直接取り出すこと.
    EvalResult bt = crb_method_call( thread, env,
                                     env["self"], "backtrace", nullptr, //nullptr,
                                     loc, false );
    assert( bt.type == NORMAL_EVAL_RESULT );
    crb_object_assign_ivar( r.value.u.object_value, "backtrace", bt.value );

    r.type = EXCEPTION_EVAL_RESULT;
    return r;
}


static EvalResult class_object_require( CRB_Thread* thread,
                                        LocalEnvironment& env,
                                        CRB_Value& receiver,
                                        const ScriptLocation& loc )
{
    CRB_Interpreter* const interp = thread->interp;

    if ( !crb_object_isa(interp, env["filename"], interp->class_string) ) {
        return crb_raise_type_error( thread, env, loc, env["filename"],
                                     "filename", "String" );
    }
    CRB_String* req_path =
                   static_cast<CRB_String*>(env["filename"].u.object_value);

    return CRB_interp_require_file( thread, env, *req_path->value, loc );
}


/**
 * backtraceを得る
 * See http://jp.rubyist.net/magazine/?0031-BackTrace
 */
static EvalResult kernel_backtrace( CRB_Thread* thread,
                                    LocalEnvironment& env,
                                    CRB_Value& receiver,
                                    const ScriptLocation& loc )
{
    CRB_Interpreter* const interp = thread->interp;

    CRB_Array* ary = new CRB_Array( interp->class_array );

    vector<CRB_Caller>::iterator i = thread->stack_trace.begin();
    for ( ; i != thread->stack_trace.end(); i++ ) {
        const LocalEnvironment* caller_env = i->env;

        CRB_Value filename = crb_string_new(interp,
                          new UnicodeString("<filename>") ); // TODO: filename

        CRB_Value lineno( CRB_INT_VALUE );
        mpz_set_si( lineno.u.int_value, i->caller_loc.line_number );

        CRB_Value funcname( CRB_OBJECT_VALUE );
        if ( caller_env->node->m_type == LAMBDA_EXPR ) {
            funcname.u.object_value = crb_string_new( interp,
                                   new UnicodeString("<nameless function>") );
        }
        else {
            assert( caller_env->node->m_type == METHOD_DEFINITION );
            funcname.u.object_value = crb_string_new( interp,
                                  new UnicodeString(caller_env->method_name) );
        }

        CRB_Array* tri = new CRB_Array( interp->class_array );
        tri->list.push_back( filename );
        tri->list.push_back( lineno );
        tri->list.push_back( funcname );

        ary->list.push_back( tri );
    }

    return EvalResult( ary );
}


void class_object_add_methods( CRB_Thread* thread, LocalEnvironment& env )
{
    CRB_Interpreter* const interp = thread->interp;
    CRB_Class* klass = interp->class_object;
    assert(klass);

    // singleton methods

    // instance methods
    CRB_add_native_function(interp, klass, "class", { }, class_object_class);

    CRB_add_native_function( interp, klass, "is_a?", {"klass"},
                             class_object_isa );

    CRB_add_native_function( interp, klass, "methods", { },
                             object_methods );

    CRB_add_native_function( interp, klass, "instance_variable_get", {"var"},
                             object_instance_variable_get );

    CRB_add_native_function( interp, klass, "instance_variable_set",
                             {"var", "value"},
                             object_instance_variable_set );

    CRB_add_native_function( interp, klass, "instance_variables",
                             { },
                             object_instance_variables );


    /////////////////////////////////////////////////////////////////////////
    // 関数呼び出しスタイル用

    CRB_add_native_function( interp, klass,
                             "raise", {"?type_or_msg", "?message"},
                             class_object_raise );

    CRB_add_native_function( interp, klass, "require", {"filename"},
                             class_object_require );

    CRB_add_native_function( interp, klass, "caller", {"?level_from", "?size"},
                             kernel_backtrace );
}



// Local variables:
// tab-width: 4
// c-basic-offset: 4
// indent-tabs-mode: nil
// End:
