﻿// -*- coding:utf-8-with-signature; mode:c++; tab-width:4; -*-
// SPDX-License-Identifier: LGPL-3.0-or-later
/*
  minilang -- A small programming language, minilang interpreter.
  Copyright (c) 2011-2013, 2023 Netsphere Laboratories, Hisashi Horikawa.
  All rights reserved.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "support.h"
#include "execute.h"

using namespace std;


static EvalResult symbol_op_compare( CRB_Thread* thread,
                                     LocalEnvironment& env,
                                     CRB_Value& receiver,
                                     const ScriptLocation& loc )
{
    CRB_Interpreter* const interp = thread->interp;

    if ( !crb_object_isa(interp, env["other"], interp->class_symbol) ) {
        return crb_raise_type_error( thread, env, loc, env["other"],
                                     "other", "Symbol" );
    }

    EvalResult result( CRB_INT_VALUE );
    mpz_set_si( result.value.u.int_value,
                receiver.u.symbol_value - env["other"].u.symbol_value );

    return result;
}


static EvalResult symbol_to_s( CRB_Thread* thread,
                               LocalEnvironment& env,
                               CRB_Value& receiver,
                               const ScriptLocation& loc )
{
    CRB_Interpreter* const interp = thread->interp;

    return EvalResult( crb_string_new(interp,
              new icu::UnicodeString(crb_interp_symbol_to_str(interp,
                                             receiver.u.symbol_value))) );
}


void class_symbol_add_methods( CRB_Thread* thread, LocalEnvironment& env )
{
    CRB_Interpreter* const interp = thread->interp;

    // String のサブクラスにする [[incompat]]
    crb_interp_create_class( thread, env, "::Symbol",
                             interp->class_string, ScriptLocation(),
                             &interp->class_symbol );

    // hashのkeyにするため, == だけでなく, <=> が必要.
    CRB_add_native_function( interp, interp->class_symbol, "<=>", {"other"},
                             symbol_op_compare );

    CRB_add_native_function( interp, interp->class_symbol, "to_s", { }, symbol_to_s );
}





// Local variables:
// tab-width: 4
// c-basic-offset: 4
// indent-tabs-mode: nil
// End:
