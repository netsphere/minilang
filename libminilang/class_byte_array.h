﻿// -*- coding:utf-8-with-signature; mode:c++; tab-width:4; -*-
// SPDX-License-Identifier: LGPL-3.0-or-later
/*
  minilang -- A small programming language, minilang interpreter.
  Copyright (c) 2011-2013, 2023 Netsphere Laboratories, Hisashi Horikawa.
  All rights reserved.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "execute.h"
#include <stdint.h>


struct CRB_ByteArray: public CRB_Object
{
    /** mallocで確保すること */
    unsigned char* ptr;

    intptr_t capacity;

    /** 実データ長 */
    intptr_t size;

    bool own;

    CRB_ByteArray( CRB_Class* klass ): CRB_Object( klass ),
                            ptr(nullptr), capacity(0), size(0), own(false) { }

    ~CRB_ByteArray() {
        if (own) {
            free(ptr);
            ptr = nullptr;
            capacity = size = 0;
        }
    }
};


// Local variables:
// tab-width: 4
// c-basic-offset: 4
// indent-tabs-mode: nil
// End:
