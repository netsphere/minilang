﻿// -*- coding:utf-8-with-signature; mode:c++; tab-width:4; -*-

/*
  minilang -- a minilang interpreter.

  Copyright (c) 2011-2013 HORIKAWA Hisashi. All rights reserved.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


// class BasicObject

#include "support.h"
#include "execute.h"

using namespace std;


/** BasicObject#__id__ */
static EvalResult basic_object___id__( CRB_Thread* thread,
                                       LocalEnvironment& env,
                                       CRB_Value& receiver,
                                       const ScriptLocation& loc )
{
    EvalResult result( CRB_INT_VALUE );
    mpz_set_ui( result.value.u.int_value, (uintptr_t) receiver.u.object_value );

    return result;
}


// BasicObject#__send__(name, *args, &block)
static EvalResult basic_object___send__( CRB_Thread* thread,
                                         LocalEnvironment& env,
                                         CRB_Value& receiver,
                                         const ScriptLocation& loc )
{
    CRB_Interpreter* const interp = thread->interp;

    if ( !crb_object_isa(interp, env["name"], interp->class_symbol) ) {
        return crb_raise_type_error( thread, env, loc, env["name"],
                                     "name", "Symbol" );
    }

    icu::UnicodeString method_name = crb_interp_symbol_to_str( interp,
                                                 env["name"].u.symbol_value );

    CRB_Array* args = static_cast<CRB_Array*>(env["args"].u.object_value);

    CRB_ArgValues method_args;
    CRB_ValueList::const_iterator i = args->list.cbegin() + 1;
    for ( ; i != args->list.cend(); i++ )
        method_args.push_back(*i);  // refer不要

    return crb_method_call( thread, env, receiver, method_name,
                            &method_args, //nullptr,
                            loc, true );
}


void class_basic_object_add_methods( CRB_Thread* thread, LocalEnvironment& env )
{
    CRB_Interpreter* const interp = thread->interp;
    // construct_type_system() 内で生成済み
    CRB_Class* klass = interp->class_basic_object;
    assert(klass);

    // singleton methods
/*
    CRB_add_native_function( interp,
                             crb_object_create_singleton_class(interp, klass),
                             "new", { },
                             basic_object_s_new );
*/

    // instance methods
    CRB_add_native_function( interp, klass, "__id__", { },
                             basic_object___id__ );
    CRB_add_native_function( interp, klass,
                             "__send__", {"name", "*args", "&block"},
                             basic_object___send__ );
}




// Local variables:
// tab-width: 4
// c-basic-offset: 4
// indent-tabs-mode: nil
// End:
