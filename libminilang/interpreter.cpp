﻿// -*- coding:utf-8-with-signature; mode:c++; tab-width:4; -*-

/*
  minilang -- a minilang interpreter.

  Copyright (c) 2011-2013 HORIKAWA Hisashi. All rights reserved.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// compileとexecuteの統合

#ifdef _WIN32
.....
#else
  #define _XOPEN_SOURCE 700
  #define _POSIX_C_SOURCE 200809L  // new version realpath()
  #undef _REENTRANT
  #undef _GNU_SOURCE
#endif // _WIN32

#include "support.h"
#include "CRB.h"
#include "../libparser/parser.h"
#include "execute.h"
#include "eval.h"
#include <string.h>
#include <unicode/uclean.h>
#include <unicode/ustdio.h>
#include <assert.h>
#include <algorithm>
#include <sys/stat.h> // stat()

using namespace std;
using namespace icu;


static void set_class_const_name(CRB_Interpreter* interp, const char* klass, CRB_Class* value)
{
    CRB_Value v( CRB_OBJECT_VALUE );
    v.u.object_value = value;
    interp->const_vars.insert( VariableMap::value_type(klass, v) );
}

/** 最小限の型システムを構築する */
void construct_type_system( CRB_Interpreter* interp )
{
    // `Class` クラスを作る前なので, いったん仮で各クラスを作る.
    // BasicObject に superclass はない. Ruby 1.9 も同じ
    interp->class_basic_object = new CRB_Class( nullptr, "::BasicObject",
                                                nullptr );
    interp->class_object = new CRB_Class( nullptr, "::Object",
                                          interp->class_basic_object );

    interp->class_class_description =
                        new CRB_Class( nullptr, "::ClassDescription",
                                       interp->class_object );
    interp->class_module = new CRB_Class( nullptr, "::Module",
                                          interp->class_class_description );
    interp->class_class = new CRB_Class( nullptr, "::Class",
                                         interp->class_class_description );

    // 各クラスのクラスを埋める.
    interp->class_basic_object->m_class = interp->class_class;
    interp->class_object->m_class = interp->class_class;
    interp->class_class_description->m_class = interp->class_class;
    interp->class_module->m_class = interp->class_class;
    // `Class` クラスの class は `Class` 自身.
    interp->class_class->m_class = interp->class_class;

    set_class_const_name(interp, "BasicObject", interp->class_basic_object);
    set_class_const_name(interp, "Object", interp->class_object);
    set_class_const_name(interp, "ClassDescription", interp->class_class_description);
    set_class_const_name(interp, "Module", interp->class_module);
    set_class_const_name(interp, "Class", interp->class_class);

    // Thread も最初からないといけない
    interp->class_thread = new CRB_Class( interp->class_class,
                                          "::Thread",
                                          interp->class_object );
    set_class_const_name(interp, "Thread", interp->class_thread);
}


// 組み込みメソッド
extern void class_array_add_methods( CRB_Thread* thread, LocalEnvironment& env );
extern void class_basic_object_add_methods( CRB_Thread* thread, LocalEnvironment& env );
extern void class_boolean_add_methods(CRB_Thread* thread, LocalEnvironment& env);
extern void class_byte_array_add_methods( CRB_Thread* thread, LocalEnvironment& env );
extern void class_class_add_methods(CRB_Thread* thread, LocalEnvironment& env);
extern void class_encoding_add_methods( CRB_Thread* thread, LocalEnvironment& env );
extern void class_float_add_methods(CRB_Thread* thread, LocalEnvironment& env);
extern void class_exception_add_methods(CRB_Thread* thread, LocalEnvironment& env);
extern void class_file_add_methods(CRB_Thread* thread, LocalEnvironment& env);
extern void class_hash_add_methods(CRB_Thread* thread, LocalEnvironment& env);
extern void class_integer_add_methods(CRB_Thread* thread, LocalEnvironment& env);
extern void class_io_add_methods( CRB_Thread* thread, LocalEnvironment& env );
extern void class_module_add_methods(CRB_Thread* thread, LocalEnvironment& env);
extern void class_nilclass_add_methods(CRB_Thread* thread, LocalEnvironment& env);
extern void class_object_add_methods(CRB_Thread* thread, LocalEnvironment& env);
extern void class_proc_add_methods(CRB_Thread* thread, LocalEnvironment& env);
extern void class_range_add_methods(CRB_Thread* thread, LocalEnvironment& env);
extern void class_regexp_add_methods(CRB_Thread* thread, LocalEnvironment& env);
extern void class_string_add_methods(CRB_Thread* thread, LocalEnvironment& env);
extern void class_symbol_add_methods( CRB_Thread* thread, LocalEnvironment& env );
extern void class_text_reader_writer_add_methods( CRB_Thread* thread, LocalEnvironment& env );
extern void class_thread_add_methods(CRB_Thread* thread, LocalEnvironment& env);
extern void mod_comparable_add_methods(CRB_Thread* thread, LocalEnvironment& env);
extern void mod_enumerable_add_methods(CRB_Thread* thread, LocalEnvironment& env);


#ifndef NDEBUG
static void dump_class_list( CRB_Interpreter* interp )
{
    CRB_Class* klass = interp->class_class;
    UFILE* err = u_finit(stderr, nullptr, nullptr );

    VariableMap::const_iterator i;
    for ( i = interp->const_vars.begin(); i != interp->const_vars.end(); i++ ) {
        if ( crb_object_isa(interp, i->second, klass) ) {
            u_fprintf( err, "%S\n",
                 const_cast<UnicodeString&>(i->first).getTerminatedBuffer() );
        }
    }
    u_fclose(err);
}
#endif // !NDEBUG


void CRB_Interpreter::setup_environment()
{
    printf("%s: enter\n", __func__); // DEBUG

    // CRB_Interpreter* interp = this;

    // 型 (オブジェクト) システムの構築
    construct_type_system( this );

    main_thread = new CRB_Thread( class_thread );
    main_thread->interp = this;
#ifdef _WIN32
    ..........;
#else
    main_thread->tid = pthread_self();
#endif // _WIN32

    toplevel = new LocalEnvironment( nullptr );

    // "main" オブジェクトは, クラス (or module) でなければならない.
    // 名前が "" な特別なクラスをself にする。
    // self は 独立した toplevel [incompat]
    toplevel->add_local_var( "self", new CRB_Module(class_module, "") );

    CRB_Thread* thread = main_thread;
    LocalEnvironment* env = toplevel;
    assert( env );

    printf("1\n"); // DEBUG

    // 組み込みクラスのメソッド. 必ずスーパークラスのほうから呼び出すこと
    class_exception_add_methods( thread, *env );
    printf("9\n"); // DEBUG

    class_array_add_methods( thread, *env );
    printf("2\n"); // DEBUG
    class_basic_object_add_methods( thread, *env );
    printf("3\n"); // DEBUG
    class_boolean_add_methods( thread, *env );
    printf("4\n"); // DEBUG
    class_byte_array_add_methods( thread, *env );
    printf("5\n"); // DEBUG
    class_class_add_methods( thread, *env );
    printf("6\n"); // DEBUG
    class_encoding_add_methods( thread, *env );
    printf("7\n"); // DEBUG
    class_float_add_methods( thread, *env );   // Numeric, Float
    printf("8\n"); // DEBUG
    class_io_add_methods(thread, *env);
    printf("10\n"); // DEBUG
    class_file_add_methods(thread, *env);   // FileIO < IO
    printf("11\n"); // DEBUG
    class_hash_add_methods( thread, *env );
    printf("12\n"); // DEBUG
    class_integer_add_methods(thread, *env);
    printf("14\n"); // DEBUG
    class_module_add_methods(thread, *env);
    printf("15\n"); // DEBUG
    class_nilclass_add_methods(thread, *env);
    printf("16\n"); // DEBUG
    class_object_add_methods(thread, *env);
    printf("17\n"); // DEBUG
    class_proc_add_methods(thread, *env);
    printf("18\n"); // DEBUG
    class_range_add_methods(thread, *env);
    printf("19\n"); // DEBUG
    class_regexp_add_methods(thread, *env);
    printf("20\n"); // DEBUG
    class_string_add_methods(thread, *env);
    printf("21\n"); // DEBUG
    class_symbol_add_methods( thread, *env );
    printf("22\n"); // DEBUG
    class_text_reader_writer_add_methods( thread, *env );
    printf("23\n"); // DEBUG
    class_thread_add_methods(thread, *env);
    printf("24\n"); // DEBUG
    mod_comparable_add_methods( thread, *env );
    printf("25\n"); // DEBUG
    mod_enumerable_add_methods( thread, *env );

    EvalResult r = CRB_interp_require_file( thread, *env, "prelude",
                                            ScriptLocation() );
    crb_object_release( &r.value );

#if 0 // ndef NDEBUG
    dump_class_list( interp );
#endif

    printf("%s: exit\n", __func__); // DEBUG
}


/**
 * スクリプトをコンパイルし, 実行する. ライブラリを検索したりしない.
 * @param filename スクリプトの実ファイル名.
 */
EvalResult CRB_interp_load_file( CRB_Thread* thread, LocalEnvironment& env,
                                 const UnicodeString& filename,
                                 const ScriptLocation& loc )
{
    printf("%s: enter.\n", __func__); // DEBUG

    CRB_Interpreter* const interp = thread->interp;

    ptr_map<UnicodeString, ParsedScript*>::iterator i;
    for ( i = interp->loaded_files.begin(); i != interp->loaded_files.end();
          i++ ) {
        if ( filename == i->first )
            return EvalResult( CRB_NIL_VALUE ); // 読み込み済み
    }

    Parser parser;
    if ( !parser.compile_file(filename) ) {
        printf("compile error!!\n"); // DEBUG

        // delete parser;
        return crb_runtime_error( thread, env, CRB_LOAD_ERROR, loc,
                                  _("compiling error") );
    }

    ParsedScript* ps = parser.detach_parsed_script();
    interp->loaded_files.insert( make_pair(filename, ps) );

    // eval.cc
    return force_eval(thread, env,
                      crb_execute_statement_list( thread,
                                       *interp->toplevel,
                                       ps->stmt_list,
                                                  NOT_LAST_STMT));
}


bool CRB_interp_load_string( CRB_Interpreter* interp, const char* string )
{
    abort(); // TODO: impl.
    return false;
}


/**
 * ファイル名から".."などを取り去る. 大文字・小文字を区別しない環境では, 小文字にする
 * "/"や"." で終わるなど, 明らかにディレクトリのときは, "/" で終わるパス名を返す
 * @return エラーの場合, ""
 */
static UnicodeString clean_path( const UnicodeString& filename )
{
    vector<UnicodeString> parts;
    int idx = 0;

    if ( filename.startsWith("/") ) {
        parts.push_back("/");
        idx = 1;
    }

    while ( idx < filename.length() ) {
        int p = filename.indexOf("/", idx);
        UnicodeString part = p >= 0 ?
                                 UnicodeString(filename, idx, p - idx + 1) :
                                 UnicodeString(filename, idx);

        if ( part == "../" || part == ".." ) {
            if ( parts.size() >= 2 ||
                               (parts.size() == 1 && parts.front() != "/") )
                parts.pop_back();
            else
                return "";
        }
        else if ( part == "./" || part == "." ) {
            // 無視
        }
        else if ( part == "/" )
            return "";  // "...//..."
        else {
#ifdef _WIN32
            parts.push_back( tolower(part) );
#else
            parts.push_back( part );
#endif // _WIN32
        }

        if ( p < 0 )
            break;
        idx = p + 1;
    }

    UnicodeString result;
    for_each( parts.begin(), parts.end(),
              [&](const UnicodeString& part) { result += part; } );

    return result;
}


/** ファイルをライブラリから検索し, load する */
EvalResult CRB_interp_require_file( CRB_Thread* thread, LocalEnvironment& env,
                                    const UnicodeString& filename,
                                    const ScriptLocation& loc )
{
    CRB_Interpreter* const interp = thread->interp;

    UnicodeString req_path = clean_path(filename);
    if ( req_path.endsWith(".rb") || req_path.endsWith(".so") )
        req_path = UnicodeString(req_path, 0, req_path.length() - 3); // for compat.

    if ( req_path == "" || req_path[req_path.length() - 1] == '/' ) {
        return crb_runtime_error( thread, env, ARGUMENT_ERROR, loc,
                                  UnicodeString(_("Pathname error")) );
    }

    struct stat buf;
    char* fullpath = nullptr;

#ifndef NDEBUG
    UFILE* err = u_finit(stderr, nullptr, nullptr);
    u_fprintf(err, "%s: %S\n", __func__, req_path.getTerminatedBuffer());
    u_fclose(err);
#endif // !NDEBUG

    if ( req_path.startsWith("/") ) {
#ifdef _WIN32
        ....;
#else
        string r;
        req_path.toUTF8String(r);
        printf("%s: %s\n", __func__, r.c_str()); // DEBUG
        fullpath = ::realpath( (r + ".rb").c_str(), nullptr );
#endif // _WIN32

        if ( ::stat(fullpath, &buf) < 0 ) {
            free(fullpath);
            return crb_runtime_error( thread, env, CRB_LOAD_ERROR, loc,
                                      UnicodeString(_("File not found: ")) +
                                      req_path );
        }
    }
    else {
        bool found = false;
        vector<UnicodeString>::const_iterator i = interp->load_path.cbegin();
        for ( ; i != interp->load_path.cend(); i++ ) {
            string r;
            (*i + "/" + req_path).toUTF8String(r);

            fullpath = ::realpath( (r + ".rb").c_str(), nullptr );
            if ( !fullpath )
                continue; // 見つからなかったりアクセス不可
            if ( !stat(fullpath, &buf) ) {
                found = true;
                break;
            }

            free(fullpath);
        }
        if ( !found ) {
            // TODO: require 元のファイルの場所 (カレントディレクトリではなく) を起点として
            // ファイルを探し直す

            return crb_runtime_error( thread, env, CRB_LOAD_ERROR, loc,
                                      UnicodeString(_("File not found: ")) +
                                      req_path );
        }
    }

    EvalResult result = CRB_interp_load_file( thread, env,
                                              UnicodeString(fullpath),
                                              loc );
    free(fullpath);

    return result;
}


// Entry point
bool CRB_interp_run( CRB_Interpreter* interp, const char* filename )
{
    printf("%s: enter.\n", __func__); // DEBUG
    assert( interp->main_thread );
    assert( interp->toplevel );

    EvalResult result = CRB_interp_load_file( interp->main_thread,
                                              *interp->toplevel,
                                              filename,
                                              ScriptLocation() );
    printf("result.type = %d\n", result.type); // DEBUG
    switch (result.type) {
    case EXCEPTION_EVAL_RESULT:
        printf("uncaught exception!\n");
        abort(); // DEBUG
        crb_object_release(&result.value);
        break;
    case NORMAL_EVAL_RESULT:
    case RETURN_EVAL_RESULT:
        crb_object_release(&result.value);
        break;
/*    case BREAK_EVAL_RESULT:
    case NEXT_EVAL_RESULT:
        printf("Unexpected break or continue!!\n");
        break;
    default:
        abort(); */
    }

    return true;
}


/**
 * クラス (特異クラスではない) を生成してインタプリタに登録する.
 * 同名のクラスがすでにある場合は, newclass にその既存のクラスを設定し, falseを返す
 * @param class_name  フルパスのクラス名. "::" で始まること.
 * @param newclass [out] 新しく生成したクラスへのポインタを返す.
 */
EvalResult crb_interp_create_class( CRB_Thread* thread,
                                    LocalEnvironment& env,
                                    const UnicodeString& class_name,
                                    CRB_Class* const superclass,
                                    const ScriptLocation& loc,
                                    CRB_Class** newclass )
{
    assert( thread );
    assert( superclass );
    assert( class_name.startsWith("::") );

    CRB_Interpreter* const interp = thread->interp;

    // クラスは `Class` クラスのインスタンス
    assert( interp->class_class );
    CRB_Class* k = new CRB_Class( interp->class_class, class_name, superclass );

    // メソッド検索を簡単にするため, 常に特異クラス (singleton class) を持たせる
    //     -> やっぱり、必要なときのみ生成するようにする.
    // crb_object_create_singleton_class( interp, k );

    // interp->interp_sync.lock();

    CRB_Value already_defined( CRB_NIL_VALUE );
    EvalResult result = crb_interp_assign_const( thread, env,
                                                 class_name,
                                                 k,
                                                 loc,
                                                 &already_defined );
    if ( result.type == EXCEPTION_EVAL_RESULT ) {
        delete k;
        return result;
    }
    assert( result.type == NORMAL_EVAL_RESULT &&
                                  result.value.type() == CRB_BOOLEAN_VALUE );

    if ( !result.value.u.boolean_value ) {
        // すでに登録されていた
        printf("%s: already defined.\n", __func__); // DEBUG
        delete k;

        if ( !crb_object_isa(interp, already_defined, interp->class_class) ) {
            return crb_raise_type_error( thread, env, loc, already_defined,
                                         "class-name", "Class" );
        }
        if (newclass)
            *newclass = static_cast<CRB_Class*>(already_defined.u.object_value);
    }
    else {
        if (newclass)
            *newclass = k;
    }

    return result;
}


/**
 * モジュールを生成してインタプリタに登録する.
 * モジュールがすでにあった場合は, そのモジュールを返す
 * @param mod_name  フルパスのモジュール名. "::"で始まること.
 */
EvalResult crb_interp_create_module( CRB_Thread* thread,
                                     LocalEnvironment& env,
                                     const UnicodeString& mod_name,
                                     const ScriptLocation& loc,
                                     CRB_Module** newmodule )
{
    assert( thread );
    // assert( newmodule );

    CRB_Interpreter* const interp = thread->interp;

    // モジュールは Module クラスのインスタンス
    CRB_Module* k = new CRB_Module( interp->class_module, mod_name );

    // pair<VariableMap::iterator, bool> r;

    CRB_Value already_defined( CRB_NIL_VALUE );
    EvalResult result = crb_interp_assign_const( thread, env,
                                                 mod_name,
                                                 k,
                                                 loc,
                                                 &already_defined );
    if ( result.type == EXCEPTION_EVAL_RESULT ) {
        delete k;
        return result;
    }
    assert( result.type == NORMAL_EVAL_RESULT &&
                                  result.value.type() == CRB_BOOLEAN_VALUE );

    if ( !result.value.u.boolean_value ) {
        // すでに登録されていた
        delete k;

        // ここがcrb_interp_create_class() と違う
        if ( !crb_object_isa(interp, already_defined, interp->class_module) ) {
            return crb_raise_type_error( thread, env, loc, already_defined,
                                         "module-name", "Module" );
        }
        if (newmodule)
            *newmodule = static_cast<CRB_Module*>(already_defined.u.object_value);
    }
    else {
        if (newmodule)
            *newmodule = k;
    }

    return result;
}


/**
 * 組み込みクラス, またはフルパスでクラスを検索する。"::"で始まっていない場合は、エラー.
 */
CRB_Class* crb_interp_find_class( CRB_Interpreter* interp,
                                  const UnicodeString& class_name )
{
    if ( !class_name.startsWith("::") ) {
        abort();
    }

    interp->interp_sync.lock();

    // 定数を探して, それがクラスかどうか
    VariableMap::iterator i;
    i = interp->const_vars.find( UnicodeString(class_name, 2) );
    if ( i == interp->const_vars.end() ) {
        interp->interp_sync.unlock();
        return nullptr;
    }
    CRB_Value vp = i->second;

    interp->interp_sync.unlock();

    if ( vp.type() != CRB_OBJECT_VALUE )
        return nullptr;

    return dynamic_cast<CRB_Class*>(vp.u.object_value);
}


/** 現在の名前空間だけから定数をフルパスにする */
EvalResult crb_interp_resolve_const_name( CRB_Thread* thread,
                                          LocalEnvironment& env,
                                          const UnicodeString& name,
                                          const ScriptLocation& loc,
                                          UnicodeString* resolved_name )
{
    assert( thread );
    // assert( env );
    assert( resolved_name );

    CRB_Interpreter* const interp = thread->interp;

    CRB_Value& self = env["self"];
    // assert( self );

    // class, moduleでしか代入できない。class/module定義文と同じ
    if ( !crb_object_isa(interp, self, interp->class_module) ) {
        return crb_runtime_error( thread, env, CRB_RUNTIME_ERROR, loc,
                            _("const-assign must be in a class or module.") );
    }
    CRB_Module* outer_mod = static_cast<CRB_Module*>(self.u.object_value);

    // UnicodeString const_name;
    if ( name.startsWith("::") )
        *resolved_name = name;
    else {
        // assignでは outerを見ない。
        if ( outer_mod->name == "" )
            *resolved_name = UnicodeString("::") + name; // toplevelの場合
        else {
            *resolved_name = UnicodeString("::") + outer_mod->name +
                             "::" + name;
        }
    }

    return EvalResult( CRB_NIL_VALUE );
}


/**
 * 定数を代入する。内部でreferしない。すでに変数が存在したとき (再代入のとき)
 * は, 代入しない
 * @param const_name フルパスでなければならない. "::" で始まっていること.
 * @return 同名の定数があった場合 (再代入のとき) は, already_defined に設定し, falseを返す
 */
EvalResult crb_interp_assign_const( CRB_Thread* thread,
                                    LocalEnvironment& env,
                                    const UnicodeString& const_name,
                                    const CRB_Value& value,
                                    const ScriptLocation& loc,
                                    CRB_Value* already_defined )
{
    // 同名のが2回来たら止まるのでは?  -> 当たり!
    string t;
    const_name.toUTF8String(t);
    printf("%s: param const_name = %s\n", __func__, t.c_str()); // DEBUG

    assert( thread );
    assert( const_name.startsWith("::") );

    CRB_Interpreter* const interp = thread->interp;

/*
    UnicodeString const_name;
    EvalResult r = crb_interp_resolve_const_name( interp, env, name,
                                                  &const_name );
    if ( r.type == EXCEPTION_EVAL_RESULT )
        return r;
*/

    interp->interp_sync.lock();

    // すべての part が moduleか確認しておく
    int idx = 0;
    // bool exist;
    while ( (idx = const_name.indexOf("::", idx + 2)) >= 0 ) {
        UnicodeString partname = UnicodeString(const_name, 2, idx - 2);
        // CRB_Value part(CRB_OBJECT_VALUE);
        // exist = crb_interp_search_const( interp, env, partname, &part );
        VariableMap::iterator it = interp->const_vars.find(partname);
        if ( it == interp->const_vars.end() ) {
            interp->interp_sync.unlock();
            return crb_runtime_error( thread, env, CRB_NAME_ERROR, loc,
                             UnicodeString(_("const name not found: ")) +
                             partname );
        }
        if ( !crb_object_isa(interp, it->second, interp->class_module) ) {
            interp->interp_sync.unlock();
            return crb_raise_type_error( thread, env, loc, it->second,
                                         partname, "Module/Class" );
        }
    }

    // 変数を追加. すでに登録されていたときは, 追加されない
    pair<VariableMap::iterator, bool> r =
        interp->const_vars.insert(
               VariableMap::value_type(UnicodeString(const_name, 2), value) );
    if ( !r.second ) {
        // 登録済み
        if ( already_defined )
            *already_defined = (r.first)->second;
    }

    interp->interp_sync.unlock();

    EvalResult result( CRB_BOOLEAN_VALUE );
    result.value.u.boolean_value = r.second;

    return result;
}


/**
 * 定数を検索する. Rubyの定数は, インスタンス定数と名前空間が合わさったようなもの.
 * const_name が"::" で始まっていない場合,
 *     (1) スーパークラスに向かって検索する
 *     (2) 順番に外側 (名前空間) に向かって, 検索する
 *
 * @param out 見つかった定数. nullptr でも可.
 * @return 見つかった場合, true
 */
bool crb_interp_search_const( CRB_Interpreter* interp,
                              LocalEnvironment& env,
                              const UnicodeString& const_name,
                              CRB_Value* out )
{
    assert( interp );
    // assert( env );
    assert( out );

    VariableMap::iterator it;

    if ( const_name.startsWith("::") ) {
        interp->interp_sync.lock();
        it = interp->const_vars.find( UnicodeString(const_name, 2) );
        if ( it == interp->const_vars.end() ) {
            interp->interp_sync.unlock();
            return false;
        }

        *out = it->second;
        interp->interp_sync.unlock();

        return true;
    }

    // スーパークラスに向かって検索する
    CRB_Class* klass = crb_object_get_class( interp, env["self"] );
    UnicodeString ns_name = klass->name;

    interp->interp_sync.lock();

    if ( ns_name != "" ) {
        for ( ; klass; klass = klass->superclass ) {
            it = interp->const_vars.find( klass->name + "::" + const_name );
            if ( it != interp->const_vars.end() ) {
                *out = it->second;
                interp->interp_sync.unlock();
                return true;
            }
        }

        // 今度は外に向かって検索する
        int idx;
        while ( (idx = ns_name.lastIndexOf("::")) >= 0 ) {
            ns_name = UnicodeString(ns_name, 0, idx);
            it = interp->const_vars.find( ns_name + "::" + const_name );
            if ( it != interp->const_vars.end() ) {
                *out = it->second;
                interp->interp_sync.unlock();
                return true;
            }
        }
    }

    // トップレベル
    it = interp->const_vars.find( const_name );
    if ( it != interp->const_vars.end() ) {
        *out = it->second;
        interp->interp_sync.unlock();
        return true;
    }

    interp->interp_sync.unlock();
    return false;
}


/**
 * シンボルIDを得る.
 * @param symbol_name シンボル名. 先頭の":"は付けない.
 */
CRB_Value crb_interp_get_symbol( CRB_Interpreter* interp,
                                 const UnicodeString& symbol_name )
{
    assert( symbol_name[0] != ':' );

    uint32_t x;

    map<UnicodeString, uint32_t>::iterator i;
    i = interp->symbols.find( symbol_name );
    if ( i != interp->symbols.end() )
        x = i->second;
    else {
        // 追加
        x = interp->symbols.size() + 1;
        interp->symbols.insert( pair<UnicodeString, uint32_t>(symbol_name, x) );
    }

    CRB_Value result( CRB_SYMBOL_VALUE );
    result.u.symbol_value = x;
    return result;
}


UnicodeString crb_interp_symbol_to_str( CRB_Interpreter* interp,
                                        uint32_t symid )
{
    // ものすごく効率が悪い. TODO: 書き直し

    map<UnicodeString, uint32_t>::const_iterator i;
    for (i = interp->symbols.begin(); i != interp->symbols.end(); i++) {
        if ( i->second == symid )
            return i->first;
    }

    abort(); // ありえない
    return "";
}


CRB_Interpreter::~CRB_Interpreter()
{
    crb_varmap_free_objects(&const_vars);

    delete main_thread;
    delete toplevel;

    Parser::cleanup();
}



// Local variables:
// tab-width: 4
// c-basic-offset: 4
// indent-tabs-mode: nil
// End:
