﻿// -*- coding:utf-8-with-signature; mode:c++; tab-width:4; -*-
// SPDX-License-Identifier: LGPL-3.0-or-later
/*
  minilang -- A small programming language, minilang interpreter.
  Copyright (c) 2011-2013, 2023 Netsphere Laboratories, Hisashi Horikawa.
  All rights reserved.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

// module Comparable
// tautology になるため, native で定義する

#include "support.h"
#include "execute.h"

using namespace std;


/** `<=>` メソッドを呼び出して, 比較する */
static EvalResult compare( CRB_Thread* thread,
                           LocalEnvironment& env,
                           CRB_Value& receiver, const CRB_ValueList& args,
                           const ScriptLocation& loc )
{
    EvalResult r = crb_method_call( thread, env, receiver, "<=>",
                                    &args, nullptr, loc, false );
    if ( r.type == EXCEPTION_EVAL_RESULT )
        return r;
    assert( r.type == NORMAL_EVAL_RESULT );

    if ( !crb_object_isa(thread->interp, r.value,
                                           thread->interp->class_integer) ) {
        EvalResult result = crb_raise_type_error( thread, env, loc, r.value,
                                     "the value of method '<=>'", "Integer" );
        crb_object_release( &r.value );
        return result;
    }

    return r;
}


template <typename T>
static EvalResult binary_op(
           CRB_Thread* thread, LocalEnvironment& env,
           CRB_Value& receiver, const CRB_Value& arg,
           const ScriptLocation& loc, const T& op )
{
    EvalResult c = compare( thread, env, receiver, {arg}, loc );
    if ( c.type == EXCEPTION_EVAL_RESULT )
        return c;
    assert( c.type == NORMAL_EVAL_RESULT );

    EvalResult result( CRB_BOOLEAN_VALUE );

    if ( op(mpz_cmp_si(c.value.u.int_value, 0)) ) {
        crb_object_release( &c.value );
        result.value.u.boolean_value = true;
    }
    else {
        crb_object_release( &c.value );
        result.value.u.boolean_value = false;
    }

    return result;
}


static EvalResult comparable_op_eq(
                       CRB_Thread* interp, LocalEnvironment& env,
                       CRB_Value& receiver,
                       const ScriptLocation& loc )
{
    return binary_op( interp, env, receiver, env["other"], loc,
                      [](int x) { return x == 0; } );
}


static EvalResult comparable_op_ne(
                      CRB_Thread* interp, LocalEnvironment& env,
                      CRB_Value& receiver,
                      const ScriptLocation& loc )
{
    return binary_op( interp, env, receiver, env["other"], loc,
                      [](int x) { return x != 0; } );
}


static EvalResult comparable_op_gt(
                       CRB_Thread* interp, LocalEnvironment& env,
                       CRB_Value& receiver,
                       const ScriptLocation& loc)
{
    return binary_op( interp, env, receiver, env["other"], loc,
                      [](int x) { return x > 0; } );
}


static EvalResult comparable_op_ge(
        CRB_Thread* interp, LocalEnvironment& env,
        CRB_Value& receiver,
        const ScriptLocation& loc)
{
    return binary_op( interp, env, receiver, env["other"], loc,
                      [](int x) { return x >= 0; } );
}


static EvalResult comparable_op_lt(
        CRB_Thread* interp, LocalEnvironment& env,
        CRB_Value& receiver,
        const ScriptLocation& loc)
{
    return binary_op( interp, env, receiver, env["other"], loc,
                      [](int x) { return x < 0; } );
}


static EvalResult comparable_op_le(
        CRB_Thread* interp, LocalEnvironment& env,
        CRB_Value& receiver,
        const ScriptLocation& loc)
{
    return binary_op( interp, env, receiver, env["other"], loc,
                      [](int x) { return x <= 0; } );
}


void mod_comparable_add_methods( CRB_Thread* thread, LocalEnvironment& env )
{
    CRB_Interpreter* const interp = thread->interp;

    CRB_Module* mod = nullptr;
    crb_interp_create_module( thread, env, "::Comparable",
                              ScriptLocation(), &mod );
    assert( mod );

    CRB_add_native_function( interp, mod, "==", {"other"}, comparable_op_eq );

    // "!=" もメソッド. ruby 1.9も同じ
    CRB_add_native_function( interp, mod, "!=", {"other"}, comparable_op_ne );
    CRB_add_native_function( interp, mod, ">", {"other"}, comparable_op_gt );
    CRB_add_native_function( interp, mod, ">=", {"other"}, comparable_op_ge );
    CRB_add_native_function( interp, mod, "<", {"other"}, comparable_op_lt );
    CRB_add_native_function( interp, mod, "<=", {"other"}, comparable_op_le );
}


// Local variables:
// tab-width: 4
// c-basic-offset: 4
// indent-tabs-mode: nil
// End:
