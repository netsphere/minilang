﻿// -*- coding:utf-8-with-signature; mode:c++; tab-width:4; -*-

/*
  minilang -- a minilang interpreter.

  Copyright (c) 2011-2013 HORIKAWA Hisashi. All rights reserved.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "support.h"
#ifdef _WIN32
.....;
#else
  #define _XOPEN_SOURCE 700
  #define _POSIX_C_SOURCE 200809L
  #undef _REENTRANT
  #undef _GNU_SOURCE
  #include <pthread.h>
#endif // _WIN32
#include "execute.h"
#include "eval.h"
#include <map>

using namespace std;


struct ThreadArguments 
{
#ifdef _WIN32
    .....;
#else
    pthread_t tid;
#endif // _WIN32

    CRB_Value thread;

    CRB_ValueList args;

    /** 参照するだけ */
    const LambdaExprNode* block;

    ScriptLocation loc;

    ThreadArguments(): thread(CRB_NIL_VALUE) { }

    ~ThreadArguments() {
        crb_value_list_free_objects( &args );
        crb_object_release( &thread );
    }        
};


static ptr_map<pthread_t, ThreadArguments*> threads;


#if 0
/** トップレベル */
EvalResult crb_thread_run( CRB_Thread* cur_thread, StatementList* block )
{
    assert( cur_thread );
    assert( cur_thread->toplevel );
    assert( block );

    // cur_thread->toplevel = new LocalEnvironment( nullptr );
    

    EvalResult result = crb_execute_statement_list( cur_thread, 
                                            *cur_thread->toplevel,
                                            block, RETURN_FORCE_EVAL, false );

    // delete cur_thread->toplevel;
    // cur_thread->toplevel = nullptr;

    return result;
}
#endif // 0


/** スレッドの起点 */
static void* crb_thread_eval_node( void* arg )
{
    ThreadArguments* thread_args = static_cast<ThreadArguments*>(arg);
    thread_args->tid = pthread_self();

    threads.insert( pair<pthread_t, ThreadArguments*>(thread_args->tid, thread_args) );

    // 関数を呼び出す
    CRB_Thread* cur_thread = static_cast<CRB_Thread*>(thread_args->thread.u.object_value);
    EvalResult result = crb_execute_statement_list( cur_thread,
                                                 *cur_thread->interp->toplevel,
                                                 thread_args->block->block,
                                                 RETURN_FORCE_EVAL, false );

    // TODO: スレッドへの結果の書き戻し

    threads.erase( thread_args->tid );
    return nullptr;
}


////////////////////////////////////////////////////////////////////////
// class Thread

CRB_Thread::CRB_Thread( CRB_Class* klass ):
    CRB_Object(klass), interp(nullptr), tid(0) 
{ 
    assert( klass );
}


/**
 * Thread.start *args do |*args| ... end
 * スレッドを生成、開始し、終了を待たずに生成したスレッドインスタンスを返す
 */
static EvalResult class_thread_s_start( CRB_Thread* thread,
                                        LocalEnvironment& env,
                                        CRB_Value& receiver,
                                        const ScriptLocation& loc )
{
    CRB_Interpreter* const interp = thread->interp;

    if ( !crb_object_isa(interp, env["block"], interp->class_proc) ) {
        return crb_raise_type_error( thread, env, loc, env["block"],
                                     "block", "Proc" );
    }

    CRB_Thread* new_thread = new CRB_Thread( interp->class_thread );
    new_thread->interp = interp;

    ThreadArguments* thread_args = new ThreadArguments();
    thread_args->thread = new_thread;
    thread_args->loc = loc;

    // proc 自体は捨てる
    thread_args->block = static_cast<CRB_Proc*>(env["block"].u.object_value)->expr;

    CRB_Array* args = static_cast<CRB_Array*>(env["args"].u.object_value);

    // 先に refer していく
    CRB_ValueList::iterator i;
    for ( i = args->list.begin(); i != args->list.end(); i++ ) {
        if ( (i + 1) == args->list.end() )
            break;
        thread_args->args.push_back( crb_object_refer(*i) );
    }

    crb_object_refer( thread_args->thread );

    // 生成したスレッドが一瞬で終わる可能性に注意
    int r = pthread_create( &new_thread->tid, nullptr, &crb_thread_eval_node, 
                            thread_args );
    if (r) {
        delete thread_args;
        delete new_thread;
        return crb_runtime_error( thread, env, CRB_RUNTIME_ERROR, loc, 
                                  _("couldnot create thread") );
    }
    // thread_args->tid = thread_args->tid;

    return EvalResult( new_thread );
}


static EvalResult thread_s_new( CRB_Thread* thread,
                                LocalEnvironment& env,
                                CRB_Value& receiver,  // クラスがレシーバになる
                                const ScriptLocation& loc )
{
    EvalResult result = crb_class_create_instance<CRB_Thread>( 
                             thread, env, receiver, { }, nullptr, loc );
    if ( result.type == EXCEPTION_EVAL_RESULT )
        return result;
    assert( result.type == NORMAL_EVAL_RESULT );

    CRB_Thread* self = static_cast<CRB_Thread*>(result.value.u.object_value);
    self->interp = thread->interp;

    return result;
}


/*
static EvalResult thread_initialize( CRB_Thread* thread,
                                     LocalEnvironment& env,
                                     CRB_Value& receiver, 
                                     const ScriptLocation& loc )
{
    return EvalResult( CRB_NIL_VALUE );
}
*/


/** [incompat] デタッチする */
static EvalResult thread_detach( CRB_Thread* thread,
                                 LocalEnvironment& env,
                                 CRB_Value& receiver, 
                                 const ScriptLocation& loc )
{
    CRB_Thread* thr = static_cast<CRB_Thread*>(receiver.u.object_value);

    int r = pthread_detach( thr->tid );
    if ( r ) {
        CRB_Value err(CRB_INT_VALUE);
        mpz_set_si(err.u.int_value, r);
        return crb_runtime_error( thread, env, SYSTEM_CALL_ERROR, loc,
                                  _("system call error"), {err} );
    }

    return EvalResult( CRB_NIL_VALUE );
}


void class_thread_add_methods( CRB_Thread* thread, LocalEnvironment& env )
{
    printf("%s: enter.\n", __func__); // DEBUG
    
    CRB_Interpreter* const interp = thread->interp;
    // construct_type_system() で生成済み.
    assert(interp->class_thread);

    // singleton methods
    CRB_Class* thread_s = crb_object_create_singleton_class( interp, 
                                                       interp->class_thread );

    CRB_add_native_function( interp, thread_s, "start", {"*args", "&block"},
                             class_thread_s_start );

    // [incompat] Rubyでは new() は start() と同じ
    CRB_add_native_function( interp, thread_s, "new", { }, thread_s_new );

    // instance methods
/*
    CRB_add_native_function( interp, klass, "initialize", { }, 
                             thread_initialize );
*/
    // CRB_add_native_function( interp, klass, "run", { }, thread_run );
    CRB_add_native_function( interp, interp->class_thread, "detach", { }, 
                             thread_detach );

/*
    CRB_add_native_function( interp, klass, "backtrace", { }, 
                                                          thread_backtrace );
*/
}




// Local variables:
// tab-width: 4
// c-basic-offset: 4
// indent-tabs-mode: nil
// End:
