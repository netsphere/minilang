﻿// -*- coding:utf-8-with-signature; mode:c++; tab-width:4; -*-
// SPDX-License-Identifier: LGPL-3.0-or-later
/*
  minilang -- A small programming language, minilang interpreter.
  Copyright (c) 2011-2013, 2023 Netsphere Laboratories, Hisashi Horikawa.
  All rights reserved.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

// ASTを評価するための関数など

#ifndef EVAL_H__
#define EVAL_H__ 1

#include "../libparser/ast.h"


/** return_mask */
//#define CAN_RETURN_MASK 1
//#define CAN_NEXT_MASK   2
//#define CAN_BREAK_MASK  4

enum MaybeLastStmt {
    // 最後の文ではない。return の場合はトランポリン可
    NOT_LAST_STMT = 0,

    // 最後の文だろう。通常の文と, もちろん return もトランポリン.
    MAYBE_LAST_STMT = 1,

    // return 文であっても、トランポリン化しない。
    //RETURN_FORCE_EVAL = 8    こうじゃない。呼出し先では適宜 tail call してよい。結果を受け取ったほうで force_eval しないといけない。
} ;

// eval.cpp
EvalResult eval_expr_node( CRB_Thread* thread, LocalEnvironment& env,
                        const Expression* expr,
                        MaybeLastStmt is_last_stmt );

EvalResult crb_execute_statement_list( CRB_Thread* thread,
                        LocalEnvironment& env, const StatementList* list,
                        MaybeLastStmt maybe_last_stmt );


#endif // EVAL_H__

// Local variables:
// tab-width: 4
// c-basic-offset: 4
// indent-tabs-mode: nil
// End:
