﻿// -*- coding:utf-8-with-signature; mode:c++; tab-width:4; -*-

/*
  minilang -- a minilang interpreter.

  Copyright (c) 2011-2013 HORIKAWA Hisashi. All rights reserved.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "support.h"
#include "CRB.h"
#include "execute.h"
#include <unicode/numfmt.h>

using namespace std;
using namespace icu;


extern EvalResult class_double_binary_op( CRB_Thread* thread,
                                          LocalEnvironment& env,
                                          CRB_Value& receiver,
                                          BinaryOp binary_op,
                                          const ScriptLocation& loc );

extern EvalResult class_double_op_compare( CRB_Thread* thread, 
                                           LocalEnvironment& env,
                                           CRB_Value& receiver, 
                                           const ScriptLocation& loc );


/** Integer#to_f */
static EvalResult class_int_to_f( CRB_Thread* thread,
                                  LocalEnvironment& env,
                                  CRB_Value& receiver, 
                                  const ScriptLocation& loc )
{
    EvalResult result( CRB_DOUBLE_VALUE );
    result.value.u.double_value = mpz_get_d(receiver.u.int_value);

    return result;
}


static EvalResult class_int_to_s( CRB_Thread* thread,
                                  LocalEnvironment& env,
                                  CRB_Value& receiver, 
                                  const ScriptLocation& loc )
{
    CRB_Interpreter* interp = thread->interp;

    char* t = mpz_get_str(nullptr, 10, receiver.u.int_value);
    EvalResult result( crb_string_new(interp, new UnicodeString(t)) );
    free(t);

    return result;
}


static EvalResult class_int_binary_op(
                           CRB_Thread* thread,
                           LocalEnvironment& env,
                           CRB_Value& receiver,   // このメソッド内では解放しない
                           BinaryOp binary_op,
                           const ScriptLocation& loc )
{
    // CRB_Interpreter* const interp = thread->interp;

    if ( env["other"].type() != CRB_INT_VALUE) {
        if ( env["other"].type() == CRB_DOUBLE_VALUE) {
            // doubleに揃える
            CRB_Value d( CRB_DOUBLE_VALUE );
            d.u.double_value = mpz_get_d(receiver.u.int_value);

            EvalResult dblresult = class_double_binary_op( thread, env, 
                                                    d, binary_op, loc );
            crb_object_release( &d );
            return dblresult;
        }
        else {
            return crb_raise_type_error( thread, env, loc, env["other"],
                                     "other", "Integer/Float" );
        }
    }
    
    EvalResult result( CRB_NIL_VALUE );
    // result.type = NORMAL_EVAL_RESULT;

    switch (binary_op) {
    case ADD_OP:
        result.value.set_type( CRB_INT_VALUE );
        mpz_add( result.value.u.int_value, 
                 receiver.u.int_value, env["other"].u.int_value );
        break;
    case SUB_OP:
        result.value.set_type( CRB_INT_VALUE );
        mpz_sub( result.value.u.int_value, 
                 receiver.u.int_value, env["other"].u.int_value );
        break;
    case MUL_OP:
        result.value.set_type( CRB_INT_VALUE );
        mpz_mul( result.value.u.int_value,
                 receiver.u.int_value, env["other"].u.int_value );
        break;
    case DIV_OP:
        if ( !mpz_cmp_si(env["other"].u.int_value, 0) ) {
            return crb_runtime_error( thread, env, ZERO_DIVISION_ERROR, loc,
                                      _("divided by 0") );
        }

        // Rubyは整除. 今さら変えられない.
        // Ruby 1.8: 商はマイナス無限大の方向へ丸める. 余りは割る数と同じ符号.
        result.value.set_type( CRB_INT_VALUE );
        {
            // 商は0の方向へ丸める [incompat]
            mpz_div( result.value.u.int_value, 
                      receiver.u.int_value, env["other"].u.int_value );
/*
            mpf_t t1, t2, t3;
            mpf_inits(t1, t2, t3);
            mpf_set_z(t1, receiver.u.int_value);
            mpf_set_z(t2, env["other"].u.int_value);
            mpf_div(t3, t1, t2);
            result.value.u.double_value = mpf_get_d(t3);
            mpf_clears(t1, t2, t3);
*/
        }
        break;
    case MOD_OP:
        if ( !mpz_cmp_si(env["other"].u.int_value, 0) ) {
            return crb_runtime_error( thread, env, ZERO_DIVISION_ERROR, loc,
                                      _("divided by 0") );
        }

        result.value.set_type( CRB_INT_VALUE );
        // 余りは割られる数と同じ符号 [incompat]
        mpz_mod( result.value.u.int_value, 
                 receiver.u.int_value, env["other"].u.int_value );
        break;
    default:
        abort(); // あり得ない
    }

    return result;
}


static EvalResult class_int_add(
        CRB_Thread* thread, LocalEnvironment& env, 
        CRB_Value& receiver, 
        const ScriptLocation& loc)
{
    return class_int_binary_op(thread, env, receiver, ADD_OP, loc);
}


static EvalResult class_int_sub(
        CRB_Thread* thread, LocalEnvironment& env,
        CRB_Value& receiver, 
        const ScriptLocation& loc)
{
    return class_int_binary_op(thread, env, receiver, SUB_OP, loc);
}


static EvalResult class_int_mul(
        CRB_Thread* thread, LocalEnvironment& env,
        CRB_Value& receiver, 
        const ScriptLocation& loc)
{
    return class_int_binary_op(thread, env, receiver, MUL_OP, loc);
}


static EvalResult class_int_div(
        CRB_Thread* thread, LocalEnvironment& env,
        CRB_Value& receiver, 
        const ScriptLocation& loc)
{ 
    return class_int_binary_op(thread, env, receiver, DIV_OP, loc); 
}


static EvalResult class_int_mod(
        CRB_Thread* thread, LocalEnvironment& env,
        CRB_Value& receiver, 
        const ScriptLocation& loc)
{
    return class_int_binary_op(thread, env, receiver, MOD_OP, loc);
}


/** 単項マイナス */
static EvalResult class_int_neg( CRB_Thread* thread, 
                                 LocalEnvironment& env,
                                 CRB_Value& receiver, 
                                 const ScriptLocation& loc )
{
    EvalResult result( CRB_INT_VALUE );
    mpz_neg( result.value.u.int_value, receiver.u.int_value );;

    return result;
}


// Integer#<=>
static EvalResult int_op_compare( CRB_Thread* thread,
                                  LocalEnvironment& env,
                                  CRB_Value& receiver,
                                  const ScriptLocation& loc )
{
    // CRB_Interpreter* const interp = thread->interp;

    if ( env["other"].type() != CRB_INT_VALUE) {
        if ( env["other"].type() == CRB_DOUBLE_VALUE) {
            // doubleに揃える
            CRB_Value d( CRB_DOUBLE_VALUE );
            d.u.double_value = mpz_get_d(receiver.u.int_value);

            EvalResult dblresult = class_double_op_compare( thread, env, 
                                                            d, loc );
            crb_object_release( &d );
            return dblresult;
        }
        else {
            return crb_raise_type_error( thread, env, loc, env["other"],
                                     "other", "Integer/Float" );
        }
    }

    EvalResult result( CRB_INT_VALUE );
    mpz_set_si( result.value.u.int_value,
                     mpz_cmp(receiver.u.int_value, env["other"].u.int_value) );

    return result;
}


void class_integer_add_methods( CRB_Thread* thread, LocalEnvironment& env )
{
    CRB_Interpreter* const interp = thread->interp;

    crb_interp_create_class( thread, env, "::Integer",
                             interp->class_numeric, ScriptLocation(),  
                             &interp->class_integer );

    CRB_add_native_function(interp, interp->class_integer, "+", {"other"}, class_int_add);
    CRB_add_native_function(interp, interp->class_integer, "-", {"other"}, class_int_sub);
    CRB_add_native_function(interp, interp->class_integer, "*", {"other"}, class_int_mul);
    CRB_add_native_function(interp, interp->class_integer, "/", {"other"}, class_int_div);
    CRB_add_native_function(interp, interp->class_integer, "%", {"other"}, class_int_mod);
    CRB_add_native_function(interp, interp->class_integer, "-@", { }, class_int_neg);

    CRB_add_native_function( interp, interp->class_integer, "<=>", {"other"}, int_op_compare );
    CRB_add_native_function(interp, interp->class_integer, "to_s", { }, class_int_to_s);
    CRB_add_native_function(interp, interp->class_integer, "to_f", { }, class_int_to_f);
}



// Local variables:
// tab-width: 4
// c-basic-offset: 4
// indent-tabs-mode: nil
// End:
