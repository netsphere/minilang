﻿// -*- coding:utf-8-with-signature; mode:c++; tab-width:4; -*-
// SPDX-License-Identifier: LGPL-3.0-or-later
/*
  minilang -- A small programming language, minilang interpreter.
  Copyright (c) 2011-2013, 2023 Netsphere Laboratories, Hisashi Horikawa.
  All rights reserved.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "support.h"
#include <math.h>
#include <string.h>
#include "DBG.h"
#include "../libparser/ast.h"
#include "execute.h"
#include "eval.h"
#include "ptr_list.h"
#include <assert.h>
#include <unicode/unistr.h>  // UnicodeString
//#include <unicode/regex.h>   // RegexMatcher
#include <unicode/ustdio.h>
#ifndef NDEBUG
  #include <iostream>
  #include <unicode/ustream.h>
#endif // !NDEBUG

using namespace std;
using namespace icu;


// boolean リテラル
static EvalResult eval_boolean_expression( const LiteralExpression* expr )
{
    assert( expr->m_type == BOOLEAN_LITERAL );

    EvalResult result(CRB_BOOLEAN_VALUE);
    result.value.u.boolean_value = expr->u.boolean_value;

    return result;
}


// int リテラル
static EvalResult eval_int_expression( const LiteralExpression* expr )
{
    assert( expr->m_type == INT_LITERAL );

    EvalResult result( CRB_INT_VALUE );
    mpz_set( result.value.u.int_value, expr->u.int_value );

    return result;
}


// 浮動小数点数リテラル
static EvalResult eval_double_expression( const LiteralExpression* expr )
{
    assert( expr->m_type == FLOAT_LITERAL );

    EvalResult result( CRB_DOUBLE_VALUE );
    result.value.u.double_value = expr->u.double_value;

    return result;
}


/** 文字列式. 式を埋め込める */
static EvalResult eval_string_expression( CRB_Thread* thread,
                                          LocalEnvironment& env,
                                          const StringExpr* expr,
                                          MaybeLastStmt /*can_tail_call*/ )
{
    CRB_Interpreter* interp = thread->interp;

    UnicodeString str;

    ptr_vector<icu::UnicodeString*>::const_iterator i = expr->literals.cbegin();
    ptr_vector<Expression*>::const_iterator j = expr->embeds.cbegin();

    for ( ; j != expr->embeds.cend(); i++, j++ ) {
        EvalResult r = eval_expr_node( thread, env, *j, NOT_LAST_STMT);
        if ( r.type == EXCEPTION_EVAL_RESULT )
            return r;
        assert( r.type == NORMAL_EVAL_RESULT );

        if ( crb_object_isa(interp, r.value, interp->class_string) ) {
            str += *(*i) +
                   *static_cast<CRB_String*>(r.value.u.object_value)->value;
        }
        else {
            // 暗黙の型変換
            EvalResult v = crb_object_coerce( thread, env, r.value, "to_s",
                                            interp->class_string,
                                            (*j)->m_loc );
            if ( v.type == EXCEPTION_EVAL_RESULT ) {
                crb_object_release( &r.value );
                return v;
            }
            assert( v.type == NORMAL_EVAL_RESULT );

            str += *(*i) +
                   *static_cast<CRB_String*>(v.value.u.object_value)->value;
            crb_object_release( &v.value );
        }

        crb_object_release( &r.value );
    }
    str += *(*i);  // literalのほうが一つだけ多い

    return EvalResult( crb_string_new(interp, new UnicodeString(str)) );
}


/** ナル値リテラル: `nil`
 * CL: System Class `null` の唯一の値が `nil` */
static EvalResult eval_nil_expression()
{
    return EvalResult( CRB_NIL_VALUE );
}


/**
 * 右辺値の変数参照, または引数のないメソッド呼出し.
 * この関数の内部で referする.
 */
static EvalResult eval_variable_ref( CRB_Thread* thread,
                                     LocalEnvironment& env,
                                     const VariableRef* expr,
                                     MaybeLastStmt is_last_stmt )
{
    CRB_Value* vp = nullptr;

    CRB_Value dmy(CRB_SYMBOL_VALUE);

    switch (expr->var_type) {
    case LOCAL_VAR:
        vp = env.search_local_var( *expr->name );
        if ( !vp ) {
            // ruby ではローカル変数の参照と引数のないメソッド呼び出しが区別できない
            //   => 代入なしにローカル変数が使われていないかのエラーチェックも
            //      実行時にしかできない
            // selfを必須にすれば意味上はクリアになるが, 今さら変更できない
            // [incompat] rubyでは nil を返す
            //            Crystal は代入なしの参照はエラー。こちらに合わせる
            return crb_method_call( thread, env, env["self"], *expr->name,
                                    nullptr, //nullptr,
                                    expr->m_loc, is_last_stmt );
        }
        break;
    case CONST_VAR:
        {
            bool r = crb_interp_search_const( thread->interp, env,
                                              *expr->name, &dmy );
            if ( r )
                vp = &dmy;
        }
        break;
    case INSTANCE_VAR:
        {
            CRB_Value* self = env.search_local_var("self");
            vp = crb_object_find_ivar( *self, *expr->name );
        }
        break;
    case CLASS_VAR:
        {
            CRB_Value* self = env.search_local_var("self");
            vp = crb_object_find_classvar( *self, *expr->name );
        }
        break;
    case SYMBOL:
        dmy = crb_interp_get_symbol( thread->interp, *expr->name );
        vp = &dmy;
        break;
    }

    if ( !vp ) {
        return crb_runtime_error(thread, env, CRB_NAME_ERROR, expr->m_loc,
                     UnicodeString(_("Variable Not found: ")) +  *expr->name );
    }

    // ここで refer することで、一時オブジェクトを問答無用でリリースできる
    return EvalResult( crb_object_refer(*vp) );
}


// 右辺値
static EvalResult eval_object_slot_ref( CRB_Thread* thread,
                                     LocalEnvironment& env,
                                     const ObjectSlotRef* expr,
                                     MaybeLastStmt is_last_stmt )
{
    abort(); // TODO: impl.
}

/**
 * for, assign の下請け. rescue節でも使う
 * 代入に成功した場合、この関数の内部で valueを 1回だけ refer する
 * @param name   変数名. インスタンス変数の場合, 先頭の"@"を省略した名前.
 *               クラス変数も同様に, 先頭の "@@" を省略すること。
 */
static EvalResult assign_variable( CRB_Thread* thread,
                                   LocalEnvironment& env,
                                   const VariableType& var_type,
                                   const UnicodeString& name,
                                   const CRB_Value& value,
                                   const ScriptLocation& loc )
{
    assert( thread );
    // CRB_Interpreter* interp = thread->interp;

    switch (var_type) {
    case LOCAL_VAR:
        {
            if ( name == "self" ) {
                return crb_runtime_error( thread, env, CRB_RUNTIME_ERROR, loc,
                                          _("cannot assign to 'self'") );
            }
            // outer も検索する
            CRB_Value* left = env.search_local_var( name );
            if (left) {
                if (left->type() == CRB_OBJECT_VALUE &&
                    left->type() == value.type() && left->u.object_value == value.u.object_value) {
                    // Crystal: Error: expression has no effect
                    return crb_runtime_error(thread, env, CRB_RUNTIME_ERROR, loc,
                                             _("assign the same object"));
                }

                // TODO: 型検査. 共通のスーパークラスが Object のときにエラーにする
                crb_object_release(left);
                *left = value;
            }
            else
                env.add_local_var( name, value ); // ここではreferされない
        }
        break;

    case CONST_VAR:
        {
            UnicodeString const_name;
            EvalResult s = crb_interp_resolve_const_name( thread, env, name,
                                                          loc,
                                                          &const_name );
            if ( s.type == EXCEPTION_EVAL_RESULT ) return s;

            s = crb_interp_assign_const( thread, env, const_name, value,
                                                            loc, nullptr );
            if ( s.type == EXCEPTION_EVAL_RESULT ) return s;
            // 再代入は禁止 [incompat]
            if ( s.type == NORMAL_EVAL_RESULT && !s.value.u.boolean_value ) {
                return crb_runtime_error( thread, env, CRB_RUNTIME_ERROR, loc,
                                      _("cannot assign constant multiply.") );
            }
        }
        break;

    case INSTANCE_VAR:
        {
            assert( !name.startsWith("@") );
            CRB_Value* self = env.search_local_var( "self" );
            CRB_Value* left = crb_object_find_ivar(*self, name);
            if ( left ) {
                if (left->type() == CRB_OBJECT_VALUE &&
                    left->type() == value.type() && left->u.object_value == value.u.object_value) {
                    return crb_runtime_error(thread, env, CRB_RUNTIME_ERROR, loc,
                                             _("assign the same object"));
                }

                // TODO: 型検査
                crb_object_release(left);
                *left = value;
            }
            else {
                if ( self->type() != CRB_OBJECT_VALUE ) {
                    return crb_raise_type_error( thread, env, loc,
                                        *self, "self",
                                        "non-primitive type"); // expected
                }
                // 内部でreferされない
                crb_object_assign_ivar( self->u.object_value, name, value );
                // return EvalResult( value ); // refer済みのため
            }
        }
        break;

    case CLASS_VAR:
  /* CL: class variable = "class-allocated slot"
* (defclass Foo () ((bar :allocation :class )))
#<STANDARD-CLASS COMMON-LISP-USER::FOO>
* (setq x (make-instance 'Foo))
* (setq y (make-instance 'Foo))
* (setf (slot-value x 'bar) 100)
* (slot-value y 'bar)     ;; クラス内で共有される
100
  */
        {
            assert( !name.startsWith("@@") );
            CRB_Value* self = env.search_local_var("self");
            assert(self);
            CRB_Value* left = crb_object_find_classvar(*self, name);
            assert(left);

            if (left->type() == CRB_OBJECT_VALUE &&
                left->type() == value.type() && left->u.object_value == value.u.object_value) {
                return crb_runtime_error(thread, env, CRB_RUNTIME_ERROR, loc,
                                             _("assign the same object"));
            }

            crb_object_release(left);
            *left = value;
        }
        break;

    case SYMBOL:
        abort(); // あり得ない
    }

    EvalResult result( value );
    crb_object_refer( result.value );

    return result;
}


/**
 * 代入式. lefthand = operand
 * @param expr
 *     lefthand 変数の場合と object.foo の場合 (foo= メソッド呼出し), primary[x] ([]= メソッド呼出し) がある
 * Note. ローカル変数への代入の場合は, 末尾再帰できる
 */
static EvalResult eval_assign_expression( CRB_Thread* thread,
                                          LocalEnvironment& env,
                                          const AssignExpression* expr,
                                          MaybeLastStmt can_tail_call )
{
    assert( expr );
    assert( expr->lefthand );

    // 末尾再帰だけ, 先に場合分けしておく
    if ( can_tail_call && expr->lefthand->m_type == VARIABLE_REF) {
        // 直下のみ検索する. 外側への代入工程がない。
        if ( expr->lefthand->var_type == LOCAL_VAR &&
             env.variables.find(*expr->lefthand->name) != env.variables.end()) {
            // 単に値を返せばよい
            return eval_expr_node(thread, env, expr->operand, MAYBE_LAST_STMT);
        }
    }

    EvalResult assigned(CRB_NIL_VALUE);

    // まず右辺
    EvalResult value = eval_expr_node(thread, env, expr->operand, NOT_LAST_STMT);
    if (value.type == EXCEPTION_EVAL_RESULT)
        return value;
    DBG_assert( value.type == NORMAL_EVAL_RESULT,
                ("expected normal, but %d\n", value.type) );

    if (expr->lefthand->m_type == VARIABLE_REF) {
        // 成功したら内部でreferされる
        assigned = assign_variable( thread, env,
                                           expr->lefthand->var_type,
                                           *expr->lefthand->name,
                                           value.value,
                                           expr->m_loc );
        if ( assigned.type == EXCEPTION_EVAL_RESULT )
            crb_object_release(&value.value);
    }
    else {
        // 常にメソッド呼出しに変換する. obj.foo なら `foo=` メソッド.
        assert(expr->lefthand->m_type == OBJECT_SLOT_REF);

        ObjectSlotRef* left = static_cast<ObjectSlotRef*>(expr->lefthand);
        EvalResult obj_r = eval_expr_node(thread, env, left->object,
                                        NOT_LAST_STMT);
        if (obj_r.type == EXCEPTION_EVAL_RESULT) {
            crb_object_release(&value.value);
            return obj_r;
        }

        // 右辺を実引数として渡す
        // ● TODO: `[]=` の場合はカッコ内も実引数として追加
        CRB_ArgValues method_args;
        method_args.normal.push_back(value.value);
        assigned = crb_method_call(thread, env, obj_r.value, *left->name + "=",
                                   &method_args, // NULL,
                                   expr->m_loc, can_tail_call );
        crb_object_release(&obj_r.value);
    }

    return assigned;
}


/**
 * 真かどうか. 次の場合以外が真:
 *   (1) false. (2) nil. <s>(3) true? が偽 [incompat]</s>
 * ruby, CL とも, Boolean 型はない。System Class `T`, System Class `NULL`
 * -> bool 型への型強制 type coercion ではない.
 */
static EvalResult crb_val_is_true( CRB_Thread* thread,
                                   LocalEnvironment& env,
                                   const CRB_Value& value, // この関数内で解放しない
                                   const ScriptLocation& loc )
{
    EvalResult result( CRB_BOOLEAN_VALUE );

    if ( value.type() == CRB_NIL_VALUE ) {
        result.value.u.boolean_value = false;
        return result;
    }
    else if ( value.type() == CRB_BOOLEAN_VALUE ) {
        result.value = value;
        return result;
    }

    result.value.u.boolean_value = true;

    // 通常のオブジェクト. 挙動を変更できるように, true? を呼び出す
    //result = crb_object_coerce( thread, env, value, "true?",
    //                          thread->interp->class_boolean,
    //                          loc );
    //if ( result.type == EXCEPTION_EVAL_RESULT )
    //    return result;  // true? 内で例外発生
    //assert( result.type == NORMAL_EVAL_RESULT );

    return result;
}


/** 論理AND/OR. 右辺は遅延評価 */
static EvalResult eval_binary_expression( CRB_Thread* thread,
                                          LocalEnvironment& env,
                                          const LogicalOpExpr* expr,
                                          MaybeLastStmt can_tail_call )
{
    // 戻り値として使うことがある
    EvalResult left_val = eval_expr_node(thread, env, expr->left, NOT_LAST_STMT);
    if (left_val.type == EXCEPTION_EVAL_RESULT)
        return left_val;
    assert( left_val.type == NORMAL_EVAL_RESULT );

    EvalResult istrue = crb_val_is_true( thread, env, left_val.value,
                                                            expr->left->m_loc );
    if ( istrue.type == EXCEPTION_EVAL_RESULT ) {
        crb_object_release( &left_val.value );
        return istrue;
    }
    assert( istrue.type == NORMAL_EVAL_RESULT );
    assert( istrue.value.type() == CRB_BOOLEAN_VALUE );

    bool is_left_true = istrue.value.u.boolean_value;
    crb_object_release( &istrue.value );

    switch (expr->m_type) {
    case LOGICAL_AND_EXPRESSION:
        if (!is_left_true)
            return left_val;
        break;
    case LOGICAL_OR_EXPRESSION:
        if (is_left_true)
            return left_val;
        break;
    default:
        abort(); // internal error
    }

    crb_object_release(&left_val.value);

    // "&&", "||" の右辺は末尾再帰可能
    // 単に右辺を返す
    return eval_expr_node( thread, env, expr->right, can_tail_call );
}


/** ラムダ式からクロージャを生成 */
static EvalResult eval_lambda_expr( CRB_Thread* thread,
                                    LocalEnvironment& env,
                                    const LambdaExprNode* expr,
                                    MaybeLastStmt can_tail_call )
{
    // 自由変数 と self のキャプチャ
    // Rubyではラムダ式の外の変数への代入ができるように*変数*を共有する。
    CRB_Proc* closure = new CRB_Proc(thread->interp);
    closure->expr = expr;
    closure->outer_env = &env;
    closure->outer_env->add_ref();

    return EvalResult( closure );
}


/**
 * 通常の実引数.
 * 例外が発生したときは, この関数内で arg_values の要素を解放する
 */
static EvalResult eval_argument_list( CRB_Thread* thread,
                                      LocalEnvironment& env,
                                      const ArgumentList* args,
                                      CRB_ValueList* arg_values )
{
    assert( args );
    assert( arg_values );

    CRB_Interpreter* const interp = thread->interp;

    ArgumentList::const_iterator it = args->cbegin();
    for ( ; it != args->cend(); ++it ) {
        EvalResult a = eval_expr_node( thread, env, (*it)->expr, NOT_LAST_STMT);
        if (a.type == EXCEPTION_EVAL_RESULT) {
            crb_value_list_free_objects( arg_values );
            return a;
        }
        assert( a.type == NORMAL_EVAL_RESULT );

        if ( (*it)->type == NORMAL_ARG )
            arg_values->push_back(a.value);
        else if ( (*it)->type == SPLAT_ARG ) {
            // 暗黙の型変換
            EvalResult aa = crb_object_coerce( thread, env, a.value,
                                                    "to_a",
                                                    interp->class_array,
                                                    (*it)->expr->m_loc );
            if ( aa.type == EXCEPTION_EVAL_RESULT ) {
                crb_value_list_free_objects(arg_values);
                return aa;
            }
            assert( aa.type == NORMAL_EVAL_RESULT );
            CRB_Array* ary = static_cast<CRB_Array*>(aa.value.u.object_value);

            CRB_ValueList::iterator vi = ary->list.begin();
            for ( ; vi != ary->list.end(); vi++ )
                arg_values->push_back( crb_object_refer(*vi) );
        }
    }

    return EvalResult(CRB_NIL_VALUE);
}


/**
 * メソッド呼び出し (クロージャの呼び出し、末尾呼び出し含む.) の実引数の評価
 * 通常の実引数, キーワード実引数, ブロック実引数
 * @param args        評価すべき実引数の AST ノード.
 * @param arg_values  評価した実引数の値を格納するリスト
 *                    例外が発生したときは, この関数内で各要素を解放する
 * @return 実引数の評価中に例外が発生したとき, 例外オブジェクト
 *         エラーのないとき nil value
 */
static EvalResult eval_method_call_arguments( CRB_Thread* thread,
                                              LocalEnvironment& env,
                                              const MethodArgument* args,
                                              CRB_ArgValues* arg_values )
{
    assert( args );
    assert( arg_values );

    CRB_Interpreter* interp = thread->interp;
    EvalResult ret(CRB_NIL_VALUE);

    if ( args->normal ) { // 通常の実引数
        ret = eval_argument_list(thread, env, args->normal, &arg_values->normal);
        if (ret.type == EXCEPTION_EVAL_RESULT)
            return ret;
        crb_object_release( &ret.value );
    }

    // ここでは optional params (default value) の評価は行わない。
    // -> 呼出し先の環境を作ってから評価する

    if ( args->key ) {  // キーワード引数
        for (AssociationList::const_iterator j = args->key->begin();
             j != args->key->end(); ++j) {
            const KeyValueNode* kv = *j;
            ret = eval_expr_node(thread, env, kv->key, NOT_LAST_STMT); // foo("x".to_sym => 2) のように左辺も式
            if (ret.type == EXCEPTION_EVAL_RESULT) {
                crb_value_list_free_objects(&arg_values->normal);
                crb_varmap_free_objects(&arg_values->keys);
                return ret;
            }
            crb_object_release( &ret.value );

            ret = eval_expr_node(thread, env, kv->value, NOT_LAST_STMT);
            if (ret.type == EXCEPTION_EVAL_RESULT) {
                crb_value_list_free_objects(&arg_values->normal);
                crb_varmap_free_objects(&arg_values->keys);
                return ret;
            }
            crb_object_release( &ret.value );
        }
    }

    if ( args->block_arg ) {
        // 単に式を評価して、結果が proc オブジェクトになることを確認する.
        // シンボルの場合は関数を引き出す. CL も同じ.
        ret = eval_expr_node( thread, env, args->block_arg, NOT_LAST_STMT );
        if (ret.type == EXCEPTION_EVAL_RESULT) {
            crb_value_list_free_objects(&arg_values->normal);
            crb_varmap_free_objects(&arg_values->keys);
            return ret;
        }

        if (ret.value.type() == CRB_OBJECT_VALUE &&
            crb_object_isa(interp, ret.value, crb_interp_find_class(interp, "::Proc"))) {
            arg_values->block_arg = ret.value;
        }
        else if (ret.value.type() == CRB_SYMBOL_VALUE) {
            // CL: シンボルを渡してもよい
            //    (find-if 'stringp '(1 2 "a" 3))  ; 少し遅い
            //    (find-if #'stringp '(1 2 "a" 3)) ; 少し速い
            // ● TODO: impl.
        }
        else {
            crb_value_list_free_objects(&arg_values->normal);
            crb_varmap_free_objects(&arg_values->keys);
            crb_object_release( &ret.value );
            return crb_raise_type_error(thread, env,
                                        ScriptLocation(), // TODO: impl.
                                        ret.value,
                                        "block_arg",
                                        "must be a Proc or Symbol.");
        }
    }

    return EvalResult( CRB_NIL_VALUE );
}


/**
 * メソッド呼び出し or yield の下請け.
 * @param receiver   レシーバ. この関数内で*解放する*
 */
static EvalResult eval_function_call( CRB_Thread* thread,
                                      LocalEnvironment& env,
                                      CRB_Value& receiver,
                                      const UnicodeString& method_name,
                                      const FunctionCallNode* func,
                                      MaybeLastStmt can_tail_call )
{
    assert(func);

    CRB_ArgValues arg_val_list;
    // 引数を先に評価 (正格)
    EvalResult block_val = eval_method_call_arguments( thread, env,
                                                       func->args,
                                                       &arg_val_list );
    if ( block_val.type == EXCEPTION_EVAL_RESULT ) {
        // arg_val_list は, eval_method_call_arguments() 内で解放ずみ
        crb_object_release( &receiver );
        return block_val;
    }

    EvalResult result = crb_method_call( thread, env, receiver,
                                         method_name,
                                         &arg_val_list, //&block_val.value,
                                         func->m_loc,
                                         can_tail_call );

    crb_object_release( &receiver );
    crb_value_list_free_objects( &arg_val_list );
    crb_object_release( &block_val.value );

    return result;
}


/** メソッド呼び出し式の評価 */
static EvalResult eval_method_call_expr( CRB_Thread* thread,
                                         LocalEnvironment& env,
                                         const MethodCallNode* expr,
                                         MaybeLastStmt can_tail_call )
{
    assert( expr->receiver );
    EvalResult receiver_val = eval_expr_node( thread, env, expr->receiver,
                                              NOT_LAST_STMT );
    if (receiver_val.type == EXCEPTION_EVAL_RESULT)
        return receiver_val;
    assert( receiver_val.type == NORMAL_EVAL_RESULT );

    return eval_function_call( thread, env, receiver_val.value,
                               *expr->method_name,
                               expr,
                               can_tail_call );
}


/**
 * if, while文の条件式 (下請け).
 *
 * @return 例外または真偽値オブジェクト
 */
static EvalResult eval_cond_expr( CRB_Thread* thread,
                                  LocalEnvironment& env,
                                  const Expression* cond_expr )
{
    EvalResult cond = eval_expr_node( thread, env, cond_expr, NOT_LAST_STMT );
    if (cond.type == EXCEPTION_EVAL_RESULT)
        return cond;
    assert(cond.type == NORMAL_EVAL_RESULT);

    EvalResult istrue = crb_val_is_true( thread, env, cond.value,
                                                           cond_expr->m_loc );
    crb_object_release(&cond.value); // もう不要
    if ( istrue.type == EXCEPTION_EVAL_RESULT )
        return istrue;

    assert( istrue.type == NORMAL_EVAL_RESULT );
    assert( istrue.value.type() == CRB_BOOLEAN_VALUE );

    return istrue;
}


static EvalResult eval_unary_not_expr( CRB_Thread* thread,
                                       LocalEnvironment& env,
                                       const UnaryExpr* expr,
                                       MaybeLastStmt )
{
    assert( expr->m_type == UNARY_NOT_EXPR );

    EvalResult v = eval_cond_expr( thread, env, expr->operand );
    if (v.type == EXCEPTION_EVAL_RESULT)
        return v;
    assert( v.type == NORMAL_EVAL_RESULT );
    assert( v.value.type() == CRB_BOOLEAN_VALUE );

    EvalResult result( CRB_BOOLEAN_VALUE );
    result.value.u.boolean_value = !v.value.u.boolean_value;

    crb_object_release(&v.value);
    return result;
}


/** if式 */
static EvalResult eval_if_expr( CRB_Thread* thread,
                                LocalEnvironment& env,
                                const IfStatement* ifs,
                                MaybeLastStmt is_last_stmt )
{
    CondList::iterator it;
    for ( it = ifs->cond_list->begin(); it != ifs->cond_list->end(); it++ ) {
        EvalResult cond = eval_cond_expr( thread, env, (*it)->condition );
        if (cond.type == EXCEPTION_EVAL_RESULT)
            return cond;
        assert( cond.type == NORMAL_EVAL_RESULT );
        assert( cond.value.type() == CRB_BOOLEAN_VALUE );

        if ( cond.value.u.boolean_value ) {
            crb_object_release( &cond.value );
            return crb_execute_statement_list( thread, env, (*it)->block,
                                               is_last_stmt );
        }
        crb_object_release( &cond.value );
    }

    // else節. else節がないときは nil を返す. (Common Lispと同じ)
    return crb_execute_statement_list( thread, env, ifs->else_block,
                                       is_last_stmt );
}


// 暗黙に `===` 演算子を呼び出す。ruby と同じ
static EvalResult eval_case_expr( CRB_Thread* thread,
                                  LocalEnvironment& env,
                                  const CaseExpr* expr,
                                  MaybeLastStmt is_last_stmt )
{
    EvalResult cond = eval_expr_node(thread, env, expr->condition,
                                     NOT_LAST_STMT);
    if ( cond.type == EXCEPTION_EVAL_RESULT )
        return cond;
    assert( cond.type == NORMAL_EVAL_RESULT );

    WhenClauseList::const_iterator i;
    for ( i = expr->when_list->cbegin(); i != expr->when_list->cend(); i++ ) {
        ArgumentList::const_iterator j;
        for ( j = (*i)->patterns->cbegin(); j != (*i)->patterns->cend(); j++ ) {
            assert( (*j)->type == NORMAL_ARG );
            EvalResult patv = eval_expr_node( thread, env, (*j)->expr,
                                              NOT_LAST_STMT);
            if ( patv.type == EXCEPTION_EVAL_RESULT ) {
                crb_object_release( &cond.value );
                return patv;
            }
            assert( patv.type == NORMAL_EVAL_RESULT );

            // "match()" は String#match() が Regexp#=~ の別名になっていて不味い
            CRB_ArgValues arg_list;
            arg_list.normal.push_back(cond.value);
            EvalResult match = crb_method_call( thread, env, patv.value,
                                                "===",
                                                &arg_list, //nullptr,
                                                (*j)->expr->m_loc,
                                                NOT_LAST_STMT );
            crb_object_release( &patv.value );

            if ( match.type == EXCEPTION_EVAL_RESULT ) {
                crb_object_release( &cond.value );
                return match;
            }
            assert( match.type == NORMAL_EVAL_RESULT );

            // ==() の戻り値は boolean でなくてもいい
            EvalResult istrue = crb_val_is_true( thread, env, match.value,
                                                   (*j)->expr->m_loc );
            if ( istrue.type == EXCEPTION_EVAL_RESULT ) {
                crb_object_release( &cond.value );
                crb_object_release( &match.value );
                return istrue;
            }
            assert( istrue.type == NORMAL_EVAL_RESULT );
            assert( istrue.value.type() == CRB_BOOLEAN_VALUE );

            if ( istrue.value.u.boolean_value ) {
                crb_object_release( &istrue.value );
                crb_object_release( &cond.value );
                crb_object_release( &match.value );
                return crb_execute_statement_list( thread, env, (*i)->block,
                                                   is_last_stmt );
            }
            crb_object_release( &istrue.value );
            crb_object_release( &match.value );
        }
    }

    crb_object_release( &cond.value );

    if ( expr->else_block ) {
        return crb_execute_statement_list( thread, env, expr->else_block,
                                           is_last_stmt );
    }

    // どれにマッチせず, else節もないときはnil
    return EvalResult( CRB_NIL_VALUE );
}


static EvalResult execute_while_statement( CRB_Thread* thread,
                                           LocalEnvironment& env,
                                           const WhileStatement* ws,
                                           MaybeLastStmt can_tail_call )
{
    assert(ws);

    for (;;) {
        EvalResult cond = eval_cond_expr( thread, env, ws->condition );
        if (cond.type == EXCEPTION_EVAL_RESULT)
            return cond;
        assert( cond.type == NORMAL_EVAL_RESULT );
        assert( cond.value.type() == CRB_BOOLEAN_VALUE );

        if (!cond.value.u.boolean_value) {
            crb_object_release(&cond.value);
            break;
        }
        crb_object_release(&cond.value);

        // 脱出条件判定があるので、return文以外、末尾再帰の最適化はできない。
        // return文はtailcallでよい.
        EvalResult result = crb_execute_statement_list( thread, env, ws->block,
                                //return_mask | CAN_BREAK_MASK | CAN_NEXT_MASK,
                                NOT_LAST_STMT );

        switch ( result.type )
        {
        case EXCEPTION_EVAL_RESULT:
            return result;
        case RETURN_EVAL_RESULT:
            // 値を持って脱出.
            // `break` を確認する. (`next` は内側で処理済み.)
            if (result.return_tag == ws->block_tag) {
                crb_object_release( &result.value );
                return EvalResult( CRB_NIL_VALUE );
            }
            return result;
        case NORMAL_EVAL_RESULT:
            // empty
            break;
        }

        crb_object_release( &result.value );
    }

    return EvalResult( CRB_NIL_VALUE );
}


/** for文
ruby: オブジェクトの `each` メソッドを呼び出す
このようにせず, 1回 `to_enum` を噛ませて, 外部イテレータにする。
*/
static EvalResult execute_for_statement( CRB_Thread* thread,
                                         LocalEnvironment& env,
                                         const ForStatement* fors,
                                         MaybeLastStmt can_tail_call )
{
    EvalResult range_val = eval_expr_node( thread, env, fors->range,
                                           NOT_LAST_STMT);
    if (range_val.type == EXCEPTION_EVAL_RESULT)
        return range_val;
    assert(range_val.type == NORMAL_EVAL_RESULT);

    // 外部イテレータ (Enumerator クラス).
    // ruby: `Object#enum_for()`, `to_enum()` で class Enumerator のインスタンスを得る.
    EvalResult enums = crb_method_call( thread, env,
                                        range_val.value, "to_enum",
                                        nullptr, //nullptr,
                                        fors->range->m_loc,
                                        NOT_LAST_STMT );
    crb_object_release(&range_val.value);
    if (enums.type == EXCEPTION_EVAL_RESULT)
        return enums;
    assert(enums.type == NORMAL_EVAL_RESULT);

    while (true) {
/*
        // [incompat] `has_next?` を要求  -> これはよくない。読まないと分からないことも多い
        EvalResult has_next = crb_object_coerce( thread, env,
                                               enums.value, "has_next?",
                                               thread->interp->class_boolean,
                                               fors->range->m_loc );
        if ( has_next.type == EXCEPTION_EVAL_RESULT ) {
            crb_object_release( &enums.value );
            return has_next;
        }
        assert( has_next.type == NORMAL_EVAL_RESULT );

        if ( !has_next.value.u.boolean_value ) {
            crb_object_release( &has_next.value );
            break;
        }
        crb_object_release( &has_next.value );
*/
        // 値を得る
        EvalResult next_val = crb_method_call( thread, env,
                                        enums.value, "next",
                                        nullptr, //nullptr,
                                        fors->range->m_loc, NOT_LAST_STMT);
        if (next_val.type == EXCEPTION_EVAL_RESULT) {
            // StopIteration 例外ならOK
            if (crb_object_isa(interp, next_val.value, STOP_ITERATION)) {
                ●OK!;
            }
            crb_object_release(&enums.value);
            return next_val;
        }
        assert(next_val.type == NORMAL_EVAL_RESULT);

        EvalResult assigned = assign_variable( thread, env,
                                  fors->var_ref->var_type,
                                  *fors->var_ref->name,
                                  next_val.value,   // 成功したら内部でreferされる
                                  fors->range->m_loc );
        if (assigned.type == EXCEPTION_EVAL_RESULT) {
            crb_object_release(&enums.value);
            crb_object_release(&next_val.value);
            return assigned;
        }

        // ローカル環境の解放でnext_valが解放されるように, release しておく.
        crb_object_release(&next_val.value);

        // ブロックの実行. return文以外、末尾再帰の最適化はできない。
        EvalResult result = crb_execute_statement_list( thread, env,
                               fors->block,
                               return_mask | CAN_BREAK_MASK | CAN_NEXT_MASK,
                               false );
        switch ( result.type )
        {
        case EXCEPTION_EVAL_RESULT:
            // 値を持って脱出
            crb_object_release(&enums.value);
            return result;
        case RETURN_EVAL_RESULT:
            // 値を持って脱出
            crb_object_release(&enums.value);
            // `break` を確認。`next` は内側で確認ずみ。
            if (result.return_tag == fors->block_tag) {
                crb_object_release( &result.value );
                return EvalResult( CRB_NIL_VALUE );
            }
            return result;
        case NORMAL_EVAL_RESULT:
            // empty
            break;
        }

        crb_object_release( &result.value );
    }
    crb_object_release(&enums.value);

    // for変数は解放しない

    return EvalResult( CRB_NIL_VALUE );
}


/** return 文, next 文, break 文. いつでも末尾再帰 */
static EvalResult execute_return_statement( CRB_Thread* thread,
                                            LocalEnvironment& env,
                                            const ReturnStatement* rets,
                                            MaybeLastStmt /*can_tail_call*/ )
{
    assert( thread );
    //assert( rets->m_type == RETURN_STATEMENT );

    CRB_Interpreter* const interp = thread->interp;

    if ( !(return_mask & CAN_RETURN_MASK) ) {
        return crb_runtime_error( thread, env, LOCAL_JUMP_ERROR, rets->m_loc,
                                  _("unexpected return"),
                                  {crb_interp_get_symbol(interp, "return")} );
    }

    // レキシカルに外側を見て, メソッド内か
    // TODO: コンパイル時に検査する
    const AST_Node* method_def = nullptr;
    for ( const AST_Node* node = rets->parent; node; node = node->parent ) {
        if ( node->m_type == METHOD_DEFINITION ) {
            method_def = node;
            break; // ok
        }
        else if ( node->m_type == CLASS_DEFINITION ||
                                          node->m_type == MODULE_DEFINITION ) {
            return crb_runtime_error( thread, env, LOCAL_JUMP_ERROR, rets->m_loc,
                                  _("Return not in method"),
                                  {crb_interp_get_symbol(interp, "return")} );
        }
        // λ式は突き抜ける
    }
    if ( !method_def ) {
        return crb_runtime_error( thread, env, LOCAL_JUMP_ERROR, rets->m_loc,
                                  _("Return not in method"),
                                  {crb_interp_get_symbol(interp, "return")} );
    }

    // 直上
    bool found = false;
    if ( env.node == method_def )
        found = true;
    else {
        // スタックを遡って, 脱出可能か確認. λ式のなかのreturn文は, レキシカルなメソッドを抜ける
        vector<CRB_Caller>::iterator i = thread->stack_trace.end();
        do {
            --i;
            if ( i->env->node->m_type == CLASS_DEFINITION ||
                                   i->env->node->m_type == MODULE_DEFINITION ) {
                return crb_runtime_error( thread, env, LOCAL_JUMP_ERROR,
                             rets->m_loc,
                             _("Return cannot break class/module definition"),
                             {crb_interp_get_symbol(interp, "return")} );
            }

            if ( i->env->node == method_def ) {
                found = true;
                break;
            }
        } while ( i != thread->stack_trace.begin() );
    }

    if ( !found ) {
        return crb_runtime_error( thread, env, LOCAL_JUMP_ERROR, rets->m_loc,
                                  _("Return out of method"),
                                  {crb_interp_get_symbol(interp, "return")} );
    }

    // 戻り値を投げてOK
    CRB_ReturnValue* jump_error = new CRB_ReturnValue();
    jump_error->return_from = method_def;

    if ( rets->return_value ) {
        EvalResult v = eval_expr_node( thread, env, rets->return_value,
                                       MAYBE_LAST_STMT); // いつでも末尾再帰
        if ( v.type == EXCEPTION_EVAL_RESULT ) {
            delete jump_error;
            return v;
        }

        jump_error->exit_value = v.value;
    }
    else {
        // 引数なしのreturn/nextはvoidを返す
        jump_error->exit_value.set_type( CRB_NIL_VALUE );
    }

    EvalResult result( CRB_RETURN_VALUE );
    result.type = RETURN_EVAL_RESULT;
    result.value.u.return_value = jump_error;

    return result;
}


/**
 * クロージャの中で break した場合, レキシカルなすぐ外側まで脱出する (スタックを何段階も上がる)
 */
/*
static EvalResult execute_break_statement( CRB_Thread* thread,
                                           LocalEnvironment& env,
                                           const ReturnStatement* statement,
                                           MaybeLastStmt can_tail_call )
{
    assert( thread );
    assert( statement->m_type == BREAK_STATEMENT );

    CRB_Interpreter* const interp = thread->interp;

    if ( !(return_mask & CAN_BREAK_MASK) ) {
        return crb_runtime_error( thread, env, LOCAL_JUMP_ERROR, statement->m_loc,
                                  _("unexpected break"),
                                  {crb_interp_get_symbol(interp, "break")} );
    }

    // レキシカルに外側を見る
    // TODO: コンパイル時に検査する
    const AST_Node* return_from = nullptr;
    const AST_Node* node;
    for ( node = statement->parent; node; node = node->parent ) {
        if ( node->m_type == LAMBDA_EXPR || node->m_type == WHILE_STATEMENT ||
                                             node->m_type == FOR_STATEMENT ) {
            return_from = node;
            break;
        }
        else if ( node->m_type == METHOD_DEFINITION ||
                       node->m_type == CLASS_DEFINITION ||
                       node->m_type == MODULE_DEFINITION ) {
            return crb_runtime_error( thread, env, LOCAL_JUMP_ERROR,
                                   statement->m_loc,
                                   _("Not allow break statement"),
                                   {crb_interp_get_symbol(interp, "break")} );
        }
    }
    if ( !return_from ) {
        return crb_runtime_error( thread, env, LOCAL_JUMP_ERROR,
                                  statement->m_loc,
                                  _("Not allow break statement"),
                                  {crb_interp_get_symbol(interp, "break")} );
    }

    if ( return_from->m_type == WHILE_STATEMENT ||
                                    return_from->m_type == FOR_STATEMENT ) {
        return EvalResult( CRB_NIL_VALUE );
    }

    assert( return_from->m_type == LAMBDA_EXPR );
    // λ式のレキシカルなすぐ外側の環境を得る
    for ( node = node->parent; node; node = node->parent ) {
        if ( node->m_type == LAMBDA_EXPR ||
                       node->m_type == METHOD_DEFINITION ||
                       node->m_type == CLASS_DEFINITION ||
                       node->m_type == MODULE_DEFINITION ) {
            break;
        }
    }

    // スタックを辿って確認.
    vector<CRB_Caller>::iterator i = thread->stack_trace.end();
    bool found = false;
    do {
        --i;
        if ( i->env->node->m_type == METHOD_DEFINITION ||
                            i->env->node->m_type == CLASS_DEFINITION ||
                            i->env->node->m_type == MODULE_DEFINITION ) {
            return crb_runtime_error( thread, env, LOCAL_JUMP_ERROR,
                                   statement->m_loc,
                                   _("Break cannot jump from here"),
                                   {crb_interp_get_symbol(interp, "break")} );
        }

        if ( i->env->node == node ) {
            found = true;
            break;
        }
    } while ( i != thread->stack_trace.begin() );

    if ( !found ) {
        return crb_runtime_error( thread, env, LOCAL_JUMP_ERROR,
                                  statement->m_loc,
                                  _("Break cannot jump from here"),
                                  {crb_interp_get_symbol(interp, "break")} );
    }

    // 脱出できる
    CRB_ReturnValue* jump_error = new CRB_ReturnValue();
    jump_error->return_from = node; // return_from ではない
    jump_error->exit_value = CRB_Value(CRB_NIL_VALUE);

    EvalResult result( CRB_RETURN_VALUE );
    result.type = BREAK_EVAL_RESULT;
    result.value.u.return_value = jump_error;

    return result;
}
*/


/** 1段階だけ脱出する */
/*
static EvalResult execute_next_statement( CRB_Thread* thread,
                                          LocalEnvironment& env,
                                          const ReturnStatement* rets,
                                          MaybeLastStmt can_tail_call )
{
    assert( rets->m_type == NEXT_STATEMENT );

    CRB_Interpreter* const interp = thread->interp;

    if ( !(return_mask & CAN_NEXT_MASK) ) {
        return crb_runtime_error( thread, env, LOCAL_JUMP_ERROR, rets->m_loc,
                                  _("unexpected next"),
                                  {crb_interp_get_symbol(interp, "next")} );
    }

    // 引数は取らない [incompat]
    EvalResult result( CRB_NIL_VALUE );
    result.type = NEXT_EVAL_RESULT;

    return result;
}
*/


/** メソッド定義の評価
 *     def method     通常の関数 / メソッド
 *     def ClassName.foo, def self.foo   クラスメソッド.
 *                    クラスオブジェクトのクラスのインスタンスメソッド
 *     def other.foo  特異メソッド. 無名クラスを作る.
 */
static EvalResult exec_method_definition( CRB_Thread* thread,
                                          LocalEnvironment& env,
                                          const MethodDefinition* ast_node,
                                          MaybeLastStmt can_tail_call )
{
    Function* fd;
    CRB_Interpreter* interp = thread->interp;

    CRB_Module* klass;
    if (ast_node->specializer) {
        // クラスメソッド or 特異メソッド
        EvalResult objr = eval_variable_ref(thread, env, ast_node->specializer, false);
        crb_object_release( &objr.value );
        klass = crb_object_create_singleton_class(interp, objr.value.u.object_value );
    }
    else {
        CRB_Value* receiver = env.search_local_var("self");
        assert( receiver );
        if ( !crb_object_isa(interp, *receiver, interp->class_module) ) {
            // メソッド内でのメソッド定義禁止.
            // Rubyではselfがクラスでないときは
            // 定義されるメソッドは、selfのクラスのインスタンスメソッドになる [incompat]
            return crb_runtime_error( thread, env, CRB_RUNTIME_ERROR,
                         ast_node->m_loc,
                         _("method definition must be in a class or module.") );
        }
        klass = static_cast<CRB_Module*>(receiver->u.object_value);

        if ( klass->name == "" ) {
            // toplevel の場合, 特別に, Object class に定義する
            klass = thread->interp->class_object;
        }
    }

    fd = new Function();
    fd->type = SCRIPT_FUNCTION;
    fd->u.ast_node = ast_node;

    pair<CRB_MethodMap::iterator, bool> r =
            klass->methods.insert(CRB_MethodMap::value_type(*ast_node->method_name, fd));
    if ( !r.second ) {
        // 重複
        delete fd;
        return crb_runtime_error( thread, env, FUNCTION_MULTIPLE_DEFINE_ERR,
                                ast_node->m_loc,
                                "Method multiple define: " + *ast_node->method_name );
    }

    return EvalResult( CRB_NIL_VALUE );
}


/**
 * class_definition() or module_definition() の下請け
 * @param klass 定義しようとする新しいクラス/モジュール
 */
static EvalResult module_or_class_body( CRB_Thread* thread,
                                        LocalEnvironment& env,
                                        const ModuleDefinitionNode* ast_node,
                                        CRB_Module* klass )
{
    assert( ast_node );
    assert( ast_node->block );  // しかし一つも文がないかもしれない

    thread->stack_trace.push_back( CRB_Caller(&env, ast_node->m_loc) );

    // 局所環境を作る. 外側のローカル変数へのアクセス不可.
    LocalEnvironment* local_env = new LocalEnvironment( nullptr );
    local_env->klass = nullptr;
    local_env->method_name = "";
    local_env->node = ast_node;

    // 局所変数の解放のときにクラスを解放してしまわないように, referしてから追加.
    CRB_Value klassv = klass;
    crb_object_refer( klassv );
    local_env->add_local_var( "self", klassv );

    // class定義内でのreturn文/next/break は不可。
    EvalResult result = crb_execute_statement_list( thread, *local_env,
                                                    ast_node->block,
                                                    0, false );
    thread->stack_trace.pop_back();
    local_env->release();

    if (result.type == EXCEPTION_EVAL_RESULT)
        return result;
    assert( result.type == NORMAL_EVAL_RESULT );

    crb_object_release(&result.value);
    return EvalResult( CRB_NIL_VALUE ); // [incompat] Rubyではクラスへの参照
}


/** クラス定義と似ている. */
static EvalResult exec_module_definition( CRB_Thread* thread,
                                          LocalEnvironment& env,
                                          const ModuleDefinitionNode* ast_node,
                                          MaybeLastStmt can_tail_call )
{
    CRB_Interpreter* const interp = thread->interp;

    UnicodeString mod_name;
    EvalResult r = crb_interp_resolve_const_name( thread, env, *ast_node->name,
                                                  ast_node->m_loc,
                                                  &mod_name );
    if ( r.type == EXCEPTION_EVAL_RESULT )
        return r;

    // 再定義かどうか? 外に向かって検索する必要なし
    CRB_Value classv( CRB_NIL_VALUE );
    CRB_Module* klass = nullptr;
    bool exist = crb_interp_search_const( interp, env, mod_name, &classv );
    if ( exist ) {
        // 再定義のとき. ここがクラス定義と違う
        if ( !crb_object_isa(interp, classv, interp->class_module) ) {
            return crb_raise_type_error( thread, env, ast_node->m_loc, classv,
                                         mod_name, "Module" );
        }
        klass = static_cast<CRB_Module*>(classv.u.object_value);
    }
    else {
        // 初回のモジュール定義
        r = crb_interp_create_module( thread, env, mod_name, ast_node->m_loc,
                                      &klass );
        if ( r.type == EXCEPTION_EVAL_RESULT )
            return r;
    }

    return module_or_class_body( thread, env, ast_node, klass );
}


/** クラス定義の評価 */
static EvalResult exec_class_definition( CRB_Thread* thread,
                                         LocalEnvironment& env,
                                         const ClassDefinitionNode* ast_node,
                                         MaybeLastStmt can_tail_call )
{
    assert( thread );

    CRB_Interpreter* const interp = thread->interp;

    /*    if ( !ast_node->name ) {
        // singleton class
        assert( ast_node->expr );
        EvalResult r = eval_expr_node( thread, env, ast_node->expr,
                                          0, false );
        if ( r.type == EXCEPTION_EVAL_RESULT )
            return r;

        if ( r.value.type() != CRB_OBJECT_VALUE ) {
            return crb_runtime_error( thread, env, TYPE_ERROR, ast_node->m_loc,
                  _("cannot create a singleton class for primitive object."));
        }
        CRB_Class* klass = crb_object_create_singleton_class( interp,
                                                      r.value.u.object_value );

        return module_or_class_body( thread, env, ast_node, klass );
    } */

    UnicodeString class_name;
    EvalResult r = crb_interp_resolve_const_name( thread, env, *ast_node->name,
                                                  ast_node->m_loc,
                                                  &class_name );
    if ( r.type == EXCEPTION_EVAL_RESULT )
        return r;

    CRB_Class* superclass = nullptr;
    bool exist;
    if ( ast_node->superclass ) {
        // 外に向かって検索
        CRB_Value sv(CRB_OBJECT_VALUE);
        exist = crb_interp_search_const( interp, env,
                                         *ast_node->superclass, &sv );
        if ( !exist ) {
            return crb_runtime_error( thread, env, CRB_NAME_ERROR,
                                      ast_node->m_loc,
                                      UnicodeString(_("missing superclass ")) +
                                      *ast_node->superclass );
        }
        else if ( !crb_object_isa(interp, sv, interp->class_class) ) {
            return crb_raise_type_error( thread, env, ast_node->m_loc, sv,
                                         "superclass", "Class" );
        }

        superclass = static_cast<CRB_Class*>(sv.u.object_value);
    }

    CRB_Value classv( CRB_OBJECT_VALUE );
    CRB_Class* klass = nullptr;
    exist = crb_interp_search_const( interp, env, class_name, &classv );
    if ( exist ) {
        // 再定義のとき
        if ( !crb_object_isa(interp, classv, interp->class_class) ) {
            return crb_raise_type_error( thread, env, ast_node->m_loc, classv,
                                         "classname", "Class" );
        }
        klass = static_cast<CRB_Class*>(classv.u.object_value);

        // スーパークラスの変更はできない
        if ( superclass && klass->superclass != superclass ) {
            return crb_runtime_error( thread, env, TYPE_ERROR, ast_node->m_loc,
                 UnicodeString(_("superclass cannot be changed for class ")) +
                 *ast_node->name );
        }
    }
    else {
        // 初回のクラス定義のとき
        if ( !superclass )
            superclass = interp->class_object;

        // インスタンスの生成
        r = crb_interp_create_class( thread, env, class_name, superclass,
                                     ast_node->m_loc, &klass );
        if ( r.type == EXCEPTION_EVAL_RESULT )
            return r;
    }

    return module_or_class_body( thread, env, ast_node, klass );
}


/** begin ... rescue ... ensure ... end */
static EvalResult exec_begin_rescue_stmt( CRB_Thread* thread,
                                          LocalEnvironment& env,
                                          const BeginRescueStmt* stmt,
                                          MaybeLastStmt can_tail_call )
{
    CRB_Interpreter* const interp = thread->interp;

    // 例外発生時に rescue 節を実行するため、末尾再帰の最適化はできない。
    EvalResult result = crb_execute_statement_list( thread, env,
                                      stmt->try_block,
                                      return_mask | RETURN_FORCE_EVAL, false );

    // begin節で例外発生
    if (result.type == EXCEPTION_EVAL_RESULT) {
        ptr_list<RescueBlock*>::iterator it = stmt->rescue_list->begin();
        for ( ; it != stmt->rescue_list->end(); it++) {
            CRB_Class* klass;
            if ( !(*it)->class_name ) {
                // 省略した場合, RuntimeErrorではない
                klass = interp->class_standard_error;
                assert(klass);
            }
            else {
                // rescue 例外クラス名
                CRB_Value error_class(CRB_NIL_VALUE);
                bool exist = crb_interp_search_const( interp, env,
                                                 *(*it)->class_name,
                                                 &error_class );
                if ( !exist ) {
                    crb_object_release(&result.value);
                    result = crb_runtime_error( thread, env, CRB_NAME_ERROR,
                              (*it)->m_loc,
                              "uninitialized constant " + *(*it)->class_name );
                    break; // 必ずensure節実行
                }
                else if ( !crb_object_isa(interp, error_class,
                                                    interp->class_class) ) {
                    crb_object_release(&result.value);
                    result = crb_raise_type_error( thread, env, (*it)->m_loc,
                                                   error_class,
                                                   "error-class", "Class" );
                    break; // 必ずensure節実行
                }
                klass = static_cast<CRB_Class*>(error_class.u.object_value);
            }

            // match するか確認
            if ( crb_object_isa(interp, result.value, klass) ) {
                if ( (*it)->var_ref ) {
                    assign_variable( thread, env, (*it)->var_ref->var_type,
                                     *(*it)->var_ref->name,
                                     result.value, (*it)->m_loc );
                    crb_object_release( &result.value ); // 2回のrefer不要
                }
                else
                    crb_object_release(&result.value);

                // ensureを実行するため、ここでも末尾再帰の最適化はできない。
                result = crb_execute_statement_list( thread, env, (*it)->block,
                                              return_mask | RETURN_FORCE_EVAL,
                                              false );
                break;
            }
        }
    }

    return result;
}


// ensure 節
// body 部のほうの結果を返すことに注意.
static EvalResult eval_unwind_protect_node( CRB_Thread* thread,
                                            LocalEnvironment& env,
                                            const UnwindProtectNode* expr,
                                            MaybeLastStmt can_tail_call )
{
    // 末尾再帰の最適化ができない
    EvalResult result = crb_execute_statement_list( thread, env, expr->block,
                                           true, false );
    if ( !expr->cleanup_block )
        return result;

    // ensure節の値は捨てるため、return文以外の、末尾再帰の最適化できない
    EvalResult x = crb_execute_statement_list( thread, env,
                                expr->cleanup_block, true, false );
    if (x.type == EXCEPTION_EVAL_RESULT) {
        crb_object_release(&result.value);  // 正常系のほうを捨てる
        return x;
    }
    else if (x.type == RETURN_EVAL_RESULT) {
        // Common Lisp, Ruby とも, ensure 節で return すると, そちらを採用
        crb_object_release(&result.value);
        return x;
    }

    crb_object_release(&x.value); // ensure節の値は単に捨てる
    return result;
}


/** [...] */
static EvalResult eval_array_ctor_expr( CRB_Thread* thread,
                                        LocalEnvironment& env,
                                        const ArrayExpr* expr,
                                        MaybeLastStmt can_tail_call )
{
    CRB_Array* ary = new CRB_Array( thread->interp->class_array );

    if ( expr->list ) {
        EvalResult r = eval_argument_list( thread, env, expr->list,
                                           &ary->list );
        if ( r.type == EXCEPTION_EVAL_RESULT ) {
            delete ary;
            return r;
        }
        assert( r.type == NORMAL_EVAL_RESULT );
        crb_object_release( &r.value );
    }

    return EvalResult( ary );
}


static EvalResult eval_hash_ctor_expr( CRB_Thread* thread,
                                       LocalEnvironment& env,
                                       const HashExpr* expr,
                                       MaybeLastStmt /*can_tail_call*/ )
{
    CRB_Hash* h = new CRB_Hash( thread->interp->class_hash );

    AssociationList::iterator i;
    for ( i = expr->list->begin(); i != expr->list->end(); i++ ) {
        EvalResult k = eval_expr_node( thread, env, (*i)->key, NOT_LAST_STMT);
        if ( k.type == EXCEPTION_EVAL_RESULT ) {
            delete h;
            return k;
        }

        EvalResult v = eval_expr_node( thread, env, (*i)->value, NOT_LAST_STMT);
        if ( v.type == EXCEPTION_EVAL_RESULT ) {
            crb_object_release( &k.value );
            delete h;
            return v;
        }

        h->pairs.insert( pair<CRB_Value, CRB_Value>(k.value, v.value) );
    }

    return EvalResult( h );
}


/** yield arg1, ... */
static EvalResult eval_yield_expr( CRB_Thread* thread,
                                   LocalEnvironment& env,
                                   const FunctionCallNode* expr,
                                   MaybeLastStmt can_tail_call )
{
    CRB_Interpreter* const interp = thread->interp;

    CRB_Value* receiver = env.search_local_var("__block__");
    if ( !receiver || receiver->type() == CRB_NIL_VALUE ) {
        return crb_runtime_error(thread, env, LOCAL_JUMP_ERROR, expr->m_loc,
                                 _("No block given"),
                                 {crb_interp_get_symbol(interp, "yield")} );
    }
/* call() さえあれば, 型は問わない
    if ( !crb_object_isa(thread->interp, *receiver,
                                               thread->interp->class_proc) ) {
        return crb_runtime_error( thread, env, TYPE_ERROR, expr->loc,
                                  "Type mismatch.", nullptr );
    }
*/

    // eval_function_call() 内で解放させない
    crb_object_refer(*receiver);
    return eval_function_call( thread, env, *receiver, "call", expr,
                               can_tail_call );
}


/**
 * super arg1, ...
 * [incompat] super() と super を区別しない  ●TODO: 区別すること. parse.yy のなかで細工が必要。
 */
static EvalResult eval_super_expr( CRB_Thread* thread,
                                   LocalEnvironment& env,
                                   const FunctionCallNode* expr,
                                   MaybeLastStmt can_tail_call )
{
    // クラス定義の中ではエラーにする. トップレベルでもエラーにする
    // TODO: コンパイル時検査
    const AST_Node* node;
    bool found = false;
    for ( node = expr->parent; node; node = node->parent ) {
        if ( node->m_type == METHOD_DEFINITION ) {
            found = true;
            break; // OK
        }
        else if ( node->m_type == CLASS_DEFINITION ||
                                          node->m_type == MODULE_DEFINITION ) {
            return crb_runtime_error( thread, env, NO_METHOD_ERROR,
                                      expr->m_loc,
                                      _("super() must be in method") );
        }
        // λ式は突き抜ける
    }
    if ( !found ) {
        return crb_runtime_error( thread, env, NO_METHOD_ERROR, expr->m_loc,
                                  _("super() must be in method") );
    }

    // モジュールのメソッド内は不可
    if ( !dynamic_cast<const CRB_Class*>(env.klass) ) {
        return crb_runtime_error( thread, env, NO_METHOD_ERROR, expr->m_loc,
                                  UnicodeString(_("No superclass of ")) +
                                  env.klass->name );
    }
    const CRB_Class* klass = static_cast<const CRB_Class*>( env.klass );

    CRB_ArgValues arg_values;
    EvalResult block_val = eval_method_call_arguments( thread, env, expr,
                                                       &arg_values );
    if ( block_val.type == EXCEPTION_EVAL_RESULT )
        return block_val;

    EvalResult result = crb_call_super( thread, env, env["self"],
                                        klass,
                                        env.method_name,
                                        &arg_values, //&block_val.value,
                                        expr->m_loc,
                                        can_tail_call );
    crb_value_list_free_objects( &arg_values );
    crb_object_release( &block_val.value );

    return result;
}


#define DISPATCH(_Node, _Func, _Type) \
    case _Node:                       \
        result = _Func(thread, env, static_cast<const _Type*>(expr), maybe_last_stmt); \
        break;

/**
 * 文 (式文を含む.) を一つ実行 (評価) する
 * @param env              現在の環境.
 * @param maybe_last_stmt  最後の式になりうる場所のとき MAYBE_LAST_STMT.
 *                         末尾再帰処理が可能かも.
 */
EvalResult eval_expr_node( CRB_Thread* thread,
                           LocalEnvironment& env, const Expression* expr,
                           MaybeLastStmt maybe_last_stmt )
{
    assert( expr );
/*
    // test for tail-cal. DEBUG
    printf( "%s: line %d, column %d\n", __func__,
            expr->loc.line_number, expr->loc.column_number );
*/
    EvalResult result( CRB_NIL_VALUE );

    // operator
    switch (expr->m_type)
    {
    // リテラル
    DISPATCH(BOOLEAN_LITERAL,   eval_boolean_expression, LiteralExpression)
    DISPATCH(INT_LITERAL,       eval_int_expression, LiteralExpression)
    DISPATCH(FLOAT_LITERAL,     eval_double_expression, LiteralExpression)
    DISPATCH(NULL_EXPRESSION,   eval_nil_expression, LiteralExpression)

    DISPATCH(STRING_EXPRESSION, eval_string_expression, StringExpr)
    DISPATCH(LAMBDA_EXPR, eval_lambda_expr, LambdaExprNode)
    DISPATCH(VARIABLE_REF, eval_variable_ref, VariableRef)  // 右辺値
    DISPATCH(OBJECT_SLOT_REF, eval_object_slot_ref, ObjectSlotRef)  // 右辺値
    DISPATCH(ARRAY_EXPR, eval_array_ctor_expr, ArrayExpr)
    DISPATCH(HASH_EXPR, eval_hash_ctor_expr, HashExpr)

    DISPATCH(ASSIGN_EXPRESSION, eval_assign_expression, AssignExpression)
    DISPATCH(ASSIGN_OP_EXPR, eval_assign_op_expr, AssignOpExpr)
    DISPATCH(LOGICAL_AND_EXPRESSION, eval_binary_expression, LogicalOpExpr)
    DISPATCH(LOGICAL_OR_EXPRESSION, eval_binary_expression, LogicalOpExpr)
    DISPATCH(METHOD_CALL_EXPR,  eval_method_call_expr, MethodCallNode)
    DISPATCH(UNARY_NOT_EXPR,    eval_unary_not_expr, UnaryExpr)
    DISPATCH(YIELD_EXPR,        eval_yield_expr, FunctionCallNode)
    DISPATCH(SUPER_EXPR,        eval_super_expr, FunctionCallNode)
    DISPATCH(IF_EXPR,           eval_if_expr, IfStatement)
    DISPATCH(CASE_EXPR,         eval_case_expr, CaseExpr)

    // 文 //////////////////////////////////////////////////////////////

    DISPATCH(WHILE_STATEMENT, execute_while_statement, WhileStatement)
    DISPATCH(FOR_STATEMENT, execute_for_statement, ForStatement)
    DISPATCH(RETURN_STATEMENT, execute_return_statement, ReturnStatement)
    DISPATCH(NEXT_STATEMENT, execute_return_statement, ReturnStatement)
    DISPATCH(BREAK_STATEMENT, execute_return_statement, ReturnStatement)
/*
    case RETRY_STATEMENT:
        result = execute_retry_statement( thread, env,
                           static_cast<ReturnStatement*>(expr), return_mask );
        break;
*/
    DISPATCH(METHOD_DEFINITION, exec_method_definition, MethodDefinition)
    DISPATCH(CLASS_DEFINITION, exec_class_definition, ClassDefinitionNode)
    DISPATCH(MODULE_DEFINITION, exec_module_definition, ModuleDefinitionNode)
    DISPATCH(BEGIN_RESCUE_STMT, exec_begin_rescue_stmt, BeginRescueStmt)
    //DISPATCH(EXTEND_STMT, exec_extend_stmt, ExtendStmt)
    //DISPATCH(ATTR_ACCESSOR_STMT, exec_attr_accessor_stmt, AttrAccessorStmt)
    //DISPATCH(ATTR_READER_STMT, exec_attr_accessor_stmt, AttrAccessorStmt)
    //DISPATCH(ATTR_WRITER_STMT, exec_attr_accessor_stmt, AttrAccessorStmt)

    DISPATCH(BLOCK_NODE, eval_block_node, BlockNode)
    DISPATCH(UNWIND_PROTECT_NODE, eval_unwind_protect_node, UnwindProtectNode)

    default:
        abort(); // bug bug bug
    }

#ifndef NDEBUG
    if ( !is_last_stmt && result.type == NORMAL_EVAL_RESULT ) {
        assert( result.value.type() != CRB_TAIL_CALL_VALUE );
    }
#endif // !NDEBUG

    // rescue ... ensure 節対策
    if ( (return_mask & RETURN_FORCE_EVAL) != 0 ) {
        // for tail call
        while ( result.type == RETURN_EVAL_RESULT ||
                                          result.type == BREAK_EVAL_RESULT ) {
            assert( result.value.type() == CRB_RETURN_VALUE );

            CRB_ReturnValue* jump = result.value.u.return_value;
            if ( jump->exit_value.type() != CRB_TAIL_CALL_VALUE )
                break;

            EvalResult x = crb_method_call( thread, env,
                                     jump->exit_value.u.tail_call->receiver,
                                     jump->exit_value.u.tail_call->method_name,
                                     jump->exit_value.u.tail_call->arguments,
                                            //&jump->exit_value.u.tail_call->block_arg,
                                     jump->exit_value.u.tail_call->caller_loc,
                                     false );
            if ( x.type == EXCEPTION_EVAL_RESULT ) {
                crb_object_release( &result.value );
                return x;
            }
            else if ( x.type == RETURN_EVAL_RESULT ||
                                             x.type == BREAK_EVAL_RESULT ) {
                crb_object_release( &result.value );
                result = x; // return/breakも切り替える
                // もう一度点検
            }
            else {
                assert( x.type == NORMAL_EVAL_RESULT );
                assert( x.value.type() != CRB_TAIL_CALL_VALUE );

                crb_object_release( &jump->exit_value );
                jump->exit_value = x.value;
                break;
            }
        }
    }

    return result;
}


/**
 * 文並びを実行. 値は最後に実行した文の値.
 * @param is_last_stmt    RETURN_FORCE_EVAL の場合, 強制的に評価して返す
 */
EvalResult crb_execute_statement_list( CRB_Thread* thread,
                                       LocalEnvironment& env,
                                       const StatementList* list,
                                       //unsigned int return_mask,
                                       MaybeLastStmt is_last_stmt )
{
    assert( thread );
    printf("%s: enter.\n", __func__); // DEBUG

    // 文が一つもないときがある
    // if x then ... end でのelse節など. voidではない
    EvalResult result( CRB_NIL_VALUE );

    if (!list)
        return result;

    StatementList::const_iterator it = list->cbegin();
    for ( ; it != list->cend(); it++ ) {
        bool last_stmt = (it + 1) == list->cend();
        result = eval_expr_node( thread, env, *it,
                                    return_mask, is_last_stmt && last_stmt );
        if ( last_stmt )
            break;

        // sequence point
#ifndef NDEBUG
        if ( result.type == NORMAL_EVAL_RESULT ) {
            assert( result.value.type() != CRB_TAIL_CALL_VALUE );
        }
#endif // !NDEBUG

        if ( result.type != NORMAL_EVAL_RESULT )  // next, break, return
            break;

        // 最後の文以外は値を捨てる
        crb_object_release(&result.value);
    }

    return result;
}



// Local variables:
// tab-width: 4
// c-basic-offset: 4
// indent-tabs-mode: nil
// End:
