﻿// -*- coding:utf-8-with-signature; mode:c++; tab-width:4; -*-
// SPDX-License-Identifier: LGPL-3.0-or-later
/*
  minilang -- A small programming language, minilang interpreter.
  Copyright (c) 2011-2013, 2023 Netsphere Laboratories, Hisashi Horikawa.
  All rights reserved.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

// class Array

#include <cstdio>
#include <cstring>
#include "DBG.h"
#include "execute.h"
#include <cassert>
#include <unicode/ustdio.h>

using namespace std;
using namespace icu;


/**
 * Array.new
 */
static EvalResult array_s_new( CRB_Thread* thread,
                               LocalEnvironment& env,
                               CRB_Value& receiver,
                               const ScriptLocation& loc )
{
    return crb_class_create_instance<CRB_Array>( thread, env, receiver, { },
                                                 nullptr, loc );
}


static EvalResult array_size( CRB_Thread* thread,
                              LocalEnvironment& env,
                              CRB_Value& receiver,
                              const ScriptLocation& loc )
{
    CRB_Array* self = static_cast<CRB_Array*>(receiver.u.object_value);

    EvalResult result( CRB_INT_VALUE );
    mpz_set_si(result.value.u.int_value, self->list.size() );

    return result;
}


static EvalResult array_delete_at( CRB_Thread* thread,
                                   LocalEnvironment& env,
                                   CRB_Value& receiver,
                                   const ScriptLocation& loc )
{
    CRB_Interpreter* const interp = thread->interp;
    CRB_Array* self = static_cast<CRB_Array*>(receiver.u.object_value);

    if ( !crb_object_isa(interp, env["pos"], interp->class_integer) ) {
        return crb_raise_type_error( thread, env, loc, env["pos"],
                                     "pos", "Integer" );
    }

    int pos = mpz_get_si(env["pos"].u.int_value);
    if ( pos < 0 || pos >= self->list.size() )
        return EvalResult(CRB_NIL_VALUE);

    CRB_ValueList::iterator i = self->list.begin() + pos;
    EvalResult result( *i ); // refer 不要
    self->list.erase( i );

    return result;
}


/** 別の配列と破壊的に結合する. */
static EvalResult array_concat( CRB_Thread* thread,
                                   LocalEnvironment& env,
                                   CRB_Value& receiver,
                                   const ScriptLocation& loc )
{
    CRB_Interpreter* const interp = thread->interp;
    CRB_Array* const self = static_cast<CRB_Array*>(receiver.u.object_value);

    if ( !crb_object_isa(interp, env["other"], interp->class_array) ) {
        return crb_raise_type_error( thread, env, loc, env["other"],
                                     "other", "Array" );
    }

    CRB_Array* other = static_cast<CRB_Array*>(env["other"].u.object_value);

    CRB_ValueList::const_iterator i;
    for ( i = other->list.begin(); i != other->list.end(); i++ ) {
        self->list.push_back( *i );
        crb_object_refer( self->list.back() );
    }

    return EvalResult( crb_object_refer(receiver) );
}


static EvalResult array_push( CRB_Thread* thread, LocalEnvironment& env,
                              CRB_Value& receiver,
                              const ScriptLocation& loc )
{
    //CRB_Interpreter* const interp = thread->interp;
    CRB_Array* const self = static_cast<CRB_Array*>(receiver.u.object_value);

    self->list.push_back( crb_object_refer(env["obj"]) );

    return EvalResult( crb_object_refer(receiver) );
}


void class_array_add_methods( CRB_Thread* thread, LocalEnvironment& env )
{
    CRB_Interpreter* const interp = thread->interp;

    crb_interp_create_class( thread, env,
                             "::Array",
                             interp->class_object, ScriptLocation(),
                             &interp->class_array );

    // singleton methods
    CRB_add_native_function( interp,
                crb_object_create_singleton_class(interp, interp->class_array),
                "new", { },
                array_s_new );

    // instance methods
    CRB_add_native_function( interp, interp->class_array,
                             "size", { }, array_size );

    CRB_add_native_function( interp, interp->class_array,
                             "delete_at", {"pos"}, array_delete_at );

    CRB_add_native_function( interp, interp->class_array,
                             "concat", {"other"}, array_concat );

    CRB_add_native_function( interp, interp->class_array,
                             "push", {"obj"}, array_push );
}



// Local variables:
// tab-width: 4
// c-basic-offset: 4
// indent-tabs-mode: nil
// End:
