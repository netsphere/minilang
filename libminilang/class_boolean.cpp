﻿// -*- coding:utf-8-with-signature; mode:c++; tab-width:4; -*-

/*
  minilang -- a minilang interpreter.

  Copyright (c) 2011-2013 HORIKAWA Hisashi. All rights reserved.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "CRB.h"
#include "DBG.h"
#include "execute.h"
#include <cstring>

using namespace std;


static EvalResult class_boolean_to_s( CRB_Thread* thread,
                                      LocalEnvironment& env,
                                      CRB_Value& receiver,
                                      const ScriptLocation& loc )
{
    EvalResult result( crb_string_new(thread->interp,
        new icu::UnicodeString(receiver.u.boolean_value ? "true" : "false")) );

    return result;
}

/*
static EvalResult class_boolean_istrue( CRB_Thread* thread,
                                        LocalEnvironment& env,
                                        CRB_Value& receiver,
                                        const ScriptLocation& loc )
{
    EvalResult r( CRB_BOOLEAN_VALUE );
    // r.type = NORMAL_EVAL_RESULT;
    // r.value.type = CRB_BOOLEAN_VALUE;
    r.value.u.boolean_value = receiver.u.boolean_value;

    return r;
}
*/

void class_boolean_add_methods( CRB_Thread* thread, LocalEnvironment& env )
{
    CRB_Interpreter* interp = thread->interp;

/*
    crb_interp_create_class( thread, env,
                             "::Boolean",
                             interp->class_object, ScriptLocation(),
                             &interp->class_boolean );
*/
    ●● true / false インスタンスの特異メソッドにする.
    CRB_add_native_function( interp, interp->class_boolean, "to_s", { },
                             class_boolean_to_s );
    //CRB_add_native_function( interp, interp->class_boolean, "true?", { },
    //                         class_boolean_istrue );
}



// Local variables:
// tab-width: 4
// c-basic-offset: 4
// indent-tabs-mode: nil
// End:
