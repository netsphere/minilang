﻿// -*- coding:utf-8-with-signature; mode:c++; tab-width:4; -*-

/*
  minilang -- a minilang interpreter.

  Copyright (c) 2011-2013 HORIKAWA Hisashi. All rights reserved.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "support.h"
#include "execute.h"
#include "unicode/ucnv.h"
#include "class_byte_array.h"

using namespace std;
using namespace icu;


/////////////////////////////////////////////////////////////////////////////
// class Encoding

static EvalResult encoding_initialize( CRB_Thread* thread,
                                       LocalEnvironment& env,
                                       CRB_Value& receiver,
                                       const ScriptLocation& loc )
{
    CRB_Interpreter* const interp = thread->interp;

    if ( !crb_object_isa(interp, env["name"], interp->class_string) ) {
        return crb_raise_type_error( thread, env, loc, env["name"],
                                     "name", "String" );
    }

    CRB_String* name = static_cast<CRB_String*>(env["name"].u.object_value);

    UErrorCode err = U_ZERO_ERROR;
    UConverter* conv = ucnv_openU(name->value->getTerminatedBuffer(), &err);
    if ( !U_SUCCESS(err) ) {
        return crb_runtime_error( thread, env, ARGUMENT_ERROR, loc,
                                  _("encoding name 'name' is wrong") );
    }

    ucnv_close(conv);

    if ( *name->value == "cp932" ) {
        // icuの"cp932" はjis78. 
        name = crb_string_new( interp, new UnicodeString("ms932") );
    }
    else
        crb_object_refer(name);

    crb_object_assign_ivar( receiver.u.object_value, "name", name );
    
    return EvalResult(CRB_NIL_VALUE);
}


static EvalResult encoding_compare( CRB_Thread* thread,
                                    LocalEnvironment& env,
                                    CRB_Value& receiver,
                                    const ScriptLocation& loc )
{
    CRB_Interpreter* const interp = thread->interp;

    if ( !crb_object_isa(interp, env["other"], interp->class_encoding) ) {
        return crb_raise_type_error( thread, env, loc, env["other"],
                                     "other", "Encoding" );
    }

    CRB_String* name1 = static_cast<CRB_String*>(crb_object_find_ivar(receiver, "name")->u.object_value);
    CRB_String* name2 = static_cast<CRB_String*>(crb_object_find_ivar(env["other"], "name")->u.object_value);

    UErrorCode err = U_ZERO_ERROR;
    UConverter* conv1 = ucnv_openU(name1->value->getTerminatedBuffer(), &err);
    assert( U_SUCCESS(err) );

    UConverter* conv2 = ucnv_openU(name2->value->getTerminatedBuffer(), &err);
    assert( U_SUCCESS(err) );

    const char* int_name1 = ucnv_getName(conv1, &err);
    const char* int_name2 = ucnv_getName(conv2, &err);

    int r = int_name1 - int_name2;

    ucnv_close(conv1);
    ucnv_close(conv2);

    EvalResult result(CRB_INT_VALUE);
    mpz_set_si(result.value.u.int_value, r);

    return result;
}


/////////////////////////////////////////////////////////////////////////////
// class Encoding::Converter

/** 
 * 文字コードの変換. バイト列が文字の区切りで終わっているとは限らない. 
 * 最後に flush すること
 */
struct CRB_Converter: public CRB_Object 
{
    /** converter 自体が内部状態を持つ */
    UConverter* src_conv;

    UConverter* dest_conv;

    CRB_Converter( CRB_Class* klass ): CRB_Object(klass), 
                                     src_conv(nullptr), dest_conv(nullptr) { }
    
    ~CRB_Converter() {
        if ( src_conv ) {
            ucnv_close( src_conv );
            src_conv = nullptr;
        }
        if ( dest_conv ) {
            ucnv_close( dest_conv );
            dest_conv = nullptr;
        }
    }
};


static EvalResult converter_s_new( CRB_Thread* thread,
                                   LocalEnvironment& env,
                                   CRB_Value& receiver,
                                   const ScriptLocation& loc )
{
    // CRB_Interpreter* const interp = thread->interp;
    return crb_class_create_instance<CRB_Converter>( thread, env,
                            receiver,
                            {env["src_enc"], env["dest_enc"], env["options"]},
                            nullptr,
                            loc );
}


static EvalResult converter_convert( CRB_Thread* thread,
                                     LocalEnvironment& env,
                                     CRB_Value& receiver,
                                     const ScriptLocation& loc )
{
    CRB_Interpreter* const interp = thread->interp;
    CRB_Converter* self = static_cast<CRB_Converter*>(receiver.u.object_value);

    EvalResult r = crb_object_coerce( thread, env, 
                           *crb_object_find_ivar(receiver, "source_encoding"),
                           "to_s", interp->class_string,
                           loc );
    assert( r.type == NORMAL_EVAL_RESULT );
    UnicodeString* src_enc = static_cast<CRB_String*>(r.value.u.object_value)->value;

    r = crb_object_coerce( thread, env, 
                       *crb_object_find_ivar(receiver, "destination_encoding"),
                       "to_s", interp->class_string,
                       loc );
    assert( r.type == NORMAL_EVAL_RESULT );
    UnicodeString* dest_enc = static_cast<CRB_String*>(r.value.u.object_value)->value;

    const char* src;
    intptr_t srclen;
    if ( crb_object_isa(interp, env["source_bytes"], interp->class_string) ) {
        if ( *src_enc != "UTF-16") {
            return crb_runtime_error( thread, env, ARGUMENT_ERROR, loc,
                                      _("String is UTF-16") );
        }
                               
        CRB_String* s = static_cast<CRB_String*>(env["source_bytes"].u.object_value);
        src = (const char*) s->value->getTerminatedBuffer();
        srclen = s->value->length() * 2;
    }
    else if ( crb_object_isa(interp, env["source_bytes"], interp->class_byte_array) ) {
        CRB_ByteArray* s = static_cast<CRB_ByteArray*>(env["source_bytes"].u.object_value);
        src = (const char*) s->ptr;
        srclen = s->size;
    }
    else {
        return crb_raise_type_error( thread, env, loc, env["source_bytes"],
                                     "source_bytes", "String/ByteArray" );
    }

    EvalResult result( CRB_OBJECT_VALUE );

    if ( *dest_enc == "UTF-16" ) {
        // to UTF-16
        UErrorCode status = U_ZERO_ERROR;

        if ( !self->src_conv ) {
            self->src_conv = ucnv_openU( src_enc->getTerminatedBuffer(),
                                         &status );
            assert( U_SUCCESS(status) );
        }

        CRB_String* res = crb_string_new( interp, new UnicodeString() );

        UChar* const buf = (UChar*) malloc( 1000 * sizeof(UChar) );
        assert( buf );

        const char* p = src;
        while ( p < src + srclen ) {
            UChar* q = buf;
            status = U_ZERO_ERROR;
            ucnv_toUnicode( self->src_conv, &q, buf + 1000, &p, src + srclen, 
                            nullptr,
                            false, &status );
/*
            if ( status == U_TRUNCATED_CHAR_FOUND ) {
                // 入力バイト列が途中で切れていた
                self->temp = malloc( src+ srclen - p
                break;
            }
*/
            if ( !U_SUCCESS(status) && status != U_BUFFER_OVERFLOW_ERROR ) {
                free( buf );
                ucnv_close( self->src_conv );
                self->src_conv = nullptr;
                delete res;
                return crb_runtime_error( thread, env, CRB_RUNTIME_ERROR, loc,
                                          _("converter error") );
            }

            res->value->append( buf, q - buf );
        }

        free( buf );
        // ucnv_close( conv );     closeしない
        result.value.u.object_value = res;
    }
    else {
        // from any to !UTF-16
        UErrorCode status = U_ZERO_ERROR;

        if ( !self->src_conv ) {
            self->src_conv = ucnv_openU( src_enc->getTerminatedBuffer(),
                                         &status );
            assert( U_SUCCESS(status) );

            self->dest_conv = ucnv_openU( dest_enc->getTerminatedBuffer(),
                                          &status );
            assert( U_SUCCESS(status) );
        }

        int buflen = 500;
        char* outbuf = nullptr;

        const char* p = src;
        char* q = outbuf; // 後ろへずらしていく
        while (p < src + srclen ) {
            buflen *= 2;
            outbuf = (char*) realloc( outbuf, buflen );

            status = U_ZERO_ERROR;
            ucnv_convertEx( self->dest_conv, self->src_conv, 
                            &q, outbuf + buflen, 
                            &p, src + srclen, 
                            nullptr, nullptr, nullptr, nullptr,
                            false, false, 
                            &status );
            if ( !U_SUCCESS(status) && status != U_BUFFER_OVERFLOW_ERROR ) {
                free( outbuf );
                ucnv_close( self->src_conv );
                self->src_conv = nullptr;
                ucnv_close( self->dest_conv );
                self->dest_conv = nullptr;
                return crb_runtime_error( thread, env, CRB_RUNTIME_ERROR, loc,
                                          _("converter error") );
            }
        }

        // closeしない

        CRB_ByteArray* ary = new CRB_ByteArray( interp->class_byte_array );
        ary->ptr = (unsigned char*) outbuf;
        ary->own = true;
        ary->capacity = buflen;
        ary->size = q - outbuf;
        result.value.u.object_value = ary;
    }

    return result;
}


void class_encoding_add_methods( CRB_Thread* thread, LocalEnvironment& env )
{
    CRB_Interpreter* const interp = thread->interp;

    crb_interp_create_class( thread, env, "::Encoding",
                             interp->class_object,
                             ScriptLocation(), &interp->class_encoding ); 

    // singleton methods

    // instance methods
    CRB_add_native_function( interp, interp->class_encoding,
                             "initialize", {"name"},
                             encoding_initialize );

    CRB_add_native_function( interp, interp->class_encoding,
                             "<=>", {"other"},
                             encoding_compare );

    ///////////////////////////////////////////////////////////////////
    // Encoding::Converter

    CRB_Class* klass = nullptr;
    crb_interp_create_class( thread, env, "::Encoding::Converter",
                             interp->class_object,
                             ScriptLocation(), &klass );
    assert( klass );

    // singleton methods
    CRB_add_native_function( interp, 
                             crb_object_create_singleton_class(interp, klass),
                             "new", {"src_enc", "dest_enc", "?options"},
                             converter_s_new );

    // instance methods
    CRB_add_native_function( interp, klass,
                             "convert", {"source_bytes"},
                             converter_convert );
}




// Local variables:
// tab-width: 4
// c-basic-offset: 4
// indent-tabs-mode: nil
// End:
