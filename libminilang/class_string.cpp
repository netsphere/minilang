﻿// -*- coding:utf-8-with-signature; mode:c++; tab-width:4; -*-

/*
  minilang -- a minilang interpreter.

  Copyright (c) 2011-2013 HORIKAWA Hisashi. All rights reserved.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "support.h"
#include "CRB.h"
#include "DBG.h"
#include "execute.h"
#include <cstring>
#include <cassert>
#include <unicode/brkiter.h>
#include <unicode/schriter.h>

using namespace std;
using namespace icu;


#if 0
/** CRB_Stringインスタンスを生成して返す. 内容の文字列は複製しない */
CRB_String* crb_string_new( CRB_Interpreter* interp, UnicodeString* str )
{
    assert( interp );
    assert( str );

    CRB_String* ret = new CRB_String(interp, str);
    // ret->is_literal = false;

    return ret;
}
#endif // 0


#if 0
/** 新しいStringオブジェクトを生成して返す */
static CRB_String* chain_string( CRB_Interpreter* inter,
                                 const CRB_Value& left, const CRB_Value& right )
{
    UnicodeString* r = new UnicodeString(*static_cast<CRB_String*>(left.u.object_value)->value);
    r->append(*static_cast<CRB_String*>(right.u.object_value)->value);

    return crb_string_new( inter, r );
}
#endif // 0


/////////////////////////////////////////////////////////////////////
// class String

CRB_String::CRB_String( CRB_Interpreter* interp, UnicodeString* s ):
    CRB_Object(interp->class_string), value(s)
{
    assert( value );
    // printf("%s ctor\n", __func__); // DEBUG
}


/**
 * String#concat
 * 他の文字列と破壊的に結合する
 */
static EvalResult string_concat( CRB_Thread* thread,
                               LocalEnvironment& env,
                               CRB_Value& receiver,
                               const ScriptLocation& loc )
{
    // printf("%s: enter.\n", __func__); // DEBUG

    CRB_Interpreter* const interp = thread->interp;
    CRB_String* self = static_cast<CRB_String*>(receiver.u.object_value);

    if ( !crb_object_isa(interp, env["other"], interp->class_string) ) {
        // 暗黙の型変換.
        EvalResult rightv = crb_object_coerce( thread, env, env["other"],
                                               "to_s",
                                             interp->class_string,
                                             loc );
        if (rightv.type == EXCEPTION_EVAL_RESULT)
            return rightv;
        assert( rightv.type == NORMAL_EVAL_RESULT );

        self->value->append(*static_cast<CRB_String*>(rightv.value.u.object_value)->value);
        crb_object_release( &rightv.value );
    }
    else {
        self->value->append(*static_cast<CRB_String*>(env["other"].u.object_value)->value);
    }

    return EvalResult( crb_object_refer(receiver) );
}


/**
 * String#<=>   ロケールを無視して、文字列を比較する.
 * @return x > y のとき +1
 */
static EvalResult string_op_compare( CRB_Thread* thread,
                                     LocalEnvironment& env,
                                     CRB_Value& receiver,
                                     const ScriptLocation& loc )
{
    CRB_Interpreter* const interp = thread->interp;

    if ( !crb_object_isa(interp, env["other"], interp->class_string) ) {
        return crb_raise_type_error( thread, env, loc, env["other"],
                                     "other", "String" );
    }

    UnicodeString emp("");

    UnicodeString* leftv = receiver.u.object_value ?
           static_cast<CRB_String*>(receiver.u.object_value)->value : &emp;
    UnicodeString* rightv = env["other"].u.object_value ?
           static_cast<CRB_String*>(env["other"].u.object_value)->value : &emp;

    EvalResult result( CRB_INT_VALUE );
    // result.type = NORMAL_EVAL_RESULT;

    mpz_set_si( result.value.u.int_value, leftv->compare(*rightv) );

    return result;
}


static EvalResult string_s_new( CRB_Thread* thread,
                                LocalEnvironment& env,
                                CRB_Value& receiver,
                                const ScriptLocation& loc )
{
    return crb_class_create_instance<CRB_String>( thread, env, receiver,
                                              {env["value"]}, nullptr, loc );
}


static EvalResult string_initialize( CRB_Thread* thread,
                                     LocalEnvironment& env,
                                     CRB_Value& receiver,
                                     const ScriptLocation& loc )
{
    CRB_Interpreter* const interp = thread->interp;
    CRB_String* self = static_cast<CRB_String*>(receiver.u.object_value);

    if (env["value"].type() == CRB_NIL_VALUE ) {
        delete self->value;
        self->value = new UnicodeString("");
    }
    else {
        if ( crb_object_isa(interp, env["value"], interp->class_string) ) {
            delete self->value;
            self->value = new UnicodeString(
               *static_cast<CRB_String*>(env["value"].u.object_value)->value );
        }
        else {
            // 暗黙の型変換
            EvalResult r = crb_object_coerce(thread, env, env["value"], "to_s",
                                           interp->class_string, loc );
            if (r.type == EXCEPTION_EVAL_RESULT )
                return r;
            delete self->value;
            self->value = new UnicodeString(
                 *static_cast<CRB_String*>(r.value.u.object_value)->value );
        }
    }

    return EvalResult( CRB_NIL_VALUE );
}


/** コードポイント単位での長さを返す */
static EvalResult string_size( CRB_Thread* thread,
                               LocalEnvironment& env,
                               CRB_Value& receiver,
                               const ScriptLocation& loc )
{
    EvalResult result( CRB_INT_VALUE );
    mpz_set_si( result.value.u.int_value,
                static_cast<CRB_String*>(receiver.u.object_value)->value->length() );

    return result;
}


static EvalResult string_index( CRB_Thread* thread,
                               LocalEnvironment& env,
                               CRB_Value& receiver,
                               const ScriptLocation& loc )
{
    CRB_Interpreter* const interp = thread->interp;
    CRB_String* self = static_cast<CRB_String*>(receiver.u.object_value);

    if ( env["pos"].type() != CRB_NIL_VALUE &&
               !crb_object_isa(interp, env["pos"], interp->class_integer) ) {
        return crb_raise_type_error( thread, env, loc, env["pos"],
                                     "pos", "Integer" );
    }

    if ( crb_object_isa(interp, env["pattern"], interp->class_regexp) ) {
        CRB_ArgValues args;
        arg.normal.push_back(receiver);
        return crb_method_call( thread, env, env["pattern"], "=~", &args,
                                //nullptr,
                                loc, false );
    }
    else if ( !crb_object_isa(interp, env["pattern"], interp->class_string) ) {
        return crb_raise_type_error( thread, env, loc, env["pattern"],
                                     "pattern", "String/Regexp" );
    }

    int pos = env["pos"].type() == CRB_NIL_VALUE ? 0 :
                                           mpz_get_si(env["pos"].u.int_value);
    int r = self->value->indexOf(
             *static_cast<CRB_String*>(env["pattern"].u.object_value)->value,
             pos );

    EvalResult result( CRB_NIL_VALUE );
    if ( r >= 0 ) {
        result.value.set_type( CRB_INT_VALUE );
        mpz_set_si( result.value.u.int_value, r );
    }

    return result;
}


////////////////////////////////////////////////////////////////////////
// String::BreakIterator

struct String_BreakIterator: public CRB_Object
{
    BreakIterator* iter;

    String_BreakIterator( CRB_Class* klass ): CRB_Object(klass), iter(nullptr)
    { }

    ~String_BreakIterator() {
        delete iter;
    }
};


/** BreakIterator.new str, method */
static EvalResult string_bi_s_new( CRB_Thread* thread,
                                   LocalEnvironment& env,
                                   CRB_Value& receiver,
                                   const ScriptLocation& loc )
{
    EvalResult result =
        crb_class_create_instance<String_BreakIterator>( thread, env,
                                  receiver,
                                  {env["str"], env["method"]}, nullptr, loc );
    if ( result.type == EXCEPTION_EVAL_RESULT )
        return result;

    String_BreakIterator* self =
              static_cast<String_BreakIterator*>(result.value.u.object_value);

    UErrorCode err = U_ZERO_ERROR;
    self->iter = BreakIterator::createCharacterInstance( Locale::getDefault(),
                                                        err );
    if ( !U_SUCCESS(err) ) {
        crb_object_release( &result.value );
        return crb_runtime_error( thread, env, CRB_RUNTIME_ERROR, loc,
                                  "internal error" );
    }
    self->iter->setText( *static_cast<CRB_String*>(env["str"].u.object_value)->value );

    return result;
}


static EvalResult string_bi_next( CRB_Thread* thread,
                                  LocalEnvironment& env,
                                  CRB_Value& receiver,
                                  const ScriptLocation& loc )
{
    String_BreakIterator* self =
                 static_cast<String_BreakIterator*>(receiver.u.object_value);
    CRB_Interpreter* interp = thread->interp;

    CRB_Value next = *crb_object_find_ivar( receiver, "next" );
    CRB_String* str = static_cast<CRB_String*>(
                    crb_object_find_ivar( receiver, "str" )->u.object_value);

    int idx = mpz_get_si(next.u.int_value);
    if ( idx < 0 || idx >= str->value->length() ) {
        return crb_runtime_error( thread, env, STOP_ITERATION, loc,
                                  _("already reach the end") );
    }

    int next2 = self->iter->next();
    assert( next2 != BreakIterator::DONE );

    EvalResult result( crb_string_new(interp,
                           new UnicodeString(*str->value, idx, next2 - idx)) );
    CRB_Value nextv( CRB_INT_VALUE );
    mpz_set_si( nextv.u.int_value, next2 );
    crb_object_assign_ivar( receiver.u.object_value, "next", nextv );

    return result;
}


////////////////////////////////////////////////////////////////////////
// String::CharacterIterator

struct String_CharacterIterator: public CRB_Object
{
    CharacterIterator* iter;

    String_CharacterIterator( CRB_Class* klass ):
        CRB_Object(klass), iter(nullptr)
    { }

    ~String_CharacterIterator() {
        delete iter;
    }
};


/** CharacterIterator.new str, method */
static EvalResult string_ci_s_new( CRB_Thread* thread,
                                   LocalEnvironment& env,
                                   CRB_Value& receiver,
                                   const ScriptLocation& loc )
{
    EvalResult result =
        crb_class_create_instance<String_CharacterIterator>( thread, env,
                                 receiver,
                                 {env["str"], env["method"]}, nullptr, loc );
    if ( result.type == EXCEPTION_EVAL_RESULT )
        return result;

    String_CharacterIterator* self =
          static_cast<String_CharacterIterator*>(result.value.u.object_value);

    self->iter = new StringCharacterIterator(
                *static_cast<CRB_String*>(env["str"].u.object_value)->value );

    return result;
}


static EvalResult string_ci_next( CRB_Thread* thread,
                                  LocalEnvironment& env,
                                  CRB_Value& receiver,
                                  const ScriptLocation& loc )
{
    // CRB_Interpreter* const interp = thread->interp;
    String_CharacterIterator* self =
               static_cast<String_CharacterIterator*>(receiver.u.object_value);

    CRB_Value next = *crb_object_find_ivar( receiver, "next" );
    CRB_String* str = static_cast<CRB_String*>(
                    crb_object_find_ivar(receiver, "str")->u.object_value);

    int idx = mpz_get_si(next.u.int_value);
    if ( idx < 0 || idx >= str->value->length() ) {
        return crb_runtime_error( thread, env, STOP_ITERATION, loc,
                                  _("already reach the end") );
    }

    EvalResult result( CRB_INT_VALUE );
    mpz_set_ui( result.value.u.int_value, self->iter->next32PostInc() );

    CRB_Value nextv( CRB_INT_VALUE );
    mpz_set_si( nextv.u.int_value, self->iter->getIndex() );
    crb_object_assign_ivar( receiver.u.object_value, "next", nextv );

    return result;
}


void class_string_add_methods( CRB_Thread* thread, LocalEnvironment& env )
{
    CRB_Interpreter* const interp = thread->interp;

    crb_interp_create_class( thread, env, "::String",
                             interp->class_object,
                             ScriptLocation(), &interp->class_string );

    // singleton methods
    CRB_add_native_function( interp,
               crb_object_create_singleton_class(interp, interp->class_string),
               "new", {"?value"},
               string_s_new );

    // instance methods
    CRB_add_native_function( interp, interp->class_string,
                             "initialize", {"?value"},
                             string_initialize );

    CRB_add_native_function( interp, interp->class_string,
                             "concat", {"other"}, string_concat );

    CRB_add_native_function( interp, interp->class_string, "<=>", {"other"},
                             string_op_compare );
    CRB_add_native_function( interp, interp->class_string, "size", { },
                             string_size );
    CRB_add_native_function( interp, interp->class_string, "index",
                             {"pattern", "?pos"},
                             string_index );


    /////////////////////////////////////////////////////////////////////////
    // BreakIterator

    crb_interp_create_class( thread, env, "::Enumerator",
                             interp->class_object,
                             ScriptLocation(), &interp->class_enumerator );

    CRB_Class* klass;
    crb_interp_create_class( thread, env, "::String::BreakIterator",
                             interp->class_enumerator,
                             ScriptLocation(), &klass );
    assert( klass );

    // singleton methods
    CRB_add_native_function( interp,
                             crb_object_create_singleton_class(interp, klass),
                             "new", {"str", "method"},
                             string_bi_s_new );

    CRB_add_native_function( interp, klass, "next", { }, string_bi_next );

    /////////////////////////////////////////////////////////////////////////
    // CharacterIterator

    klass = nullptr;
    crb_interp_create_class( thread, env, "::String::CharacterIterator",
                             interp->class_enumerator,
                             ScriptLocation(), &klass );
    assert( klass );

    // singleton methods
    CRB_add_native_function( interp,
                             crb_object_create_singleton_class(interp, klass),
                             "new", {"str", "method"},
                             string_ci_s_new );

    CRB_add_native_function( interp, klass, "next", { }, string_ci_next );
}



// Local variables:
// tab-width: 4
// c-basic-offset: 4
// indent-tabs-mode: nil
// End:
