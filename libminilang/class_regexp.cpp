﻿// -*- coding:utf-8-with-signature; mode:c++; tab-width:4; -*-

/*
  minilang -- a minilang interpreter.

  Copyright (c) 2011-2013 HORIKAWA Hisashi. All rights reserved.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


// 正規表現クラス

#include "support.h"
#include "CRB.h"
#include "DBG.h"
#include "execute.h"
#include <string.h>
#include <assert.h>
#include <unicode/regex.h>

using namespace std;


/** クラス名は末尾に"p"が付く */
struct CRB_Regexp: CRB_Object 
{
    icu::RegexPattern* pattern;

    /** create_instance 用 */
    CRB_Regexp( CRB_Class* klass ): CRB_Object(klass), pattern(nullptr) { }
/*
    CRB_Regexp(CRB_Interpreter* interp, RegexPattern* pat): 
        CRB_Object(interp, CRB_REGEXP_CLASS), regex(pat) 
    {
        // printf("%s ctor\n", __func__); // DEBUG
    }
*/
    ~CRB_Regexp() {
        // printf("%s dtor\n", __func__); // DEBUG
        delete pattern;
    }
};


/** 正規表現のマッチ情報。名前が今ひとつ */
struct CRB_MatchData: CRB_Object 
{
    icu::RegexMatcher* matcher;
    bool matched;

    CRB_MatchData( CRB_Class* klass ): CRB_Object(klass), matcher(nullptr) { }

    CRB_MatchData(CRB_Interpreter* interp, icu::RegexMatcher* m, bool matched_): 
        CRB_Object(interp->class_match_data), matcher(m), matched(matched_)
    {
        // printf("%s ctor\n", __func__); // DEBUG
    }

    ~CRB_MatchData() {
        // printf("%s dtor\n", __func__); // DEBUG
        delete matcher;
    }
};


/**
 * Regexp.new string, option = nil
 */
static EvalResult class_regexp_s_new( CRB_Thread* thread,
                                   LocalEnvironment& env,
                                   CRB_Value& receiver,  // クラスがレシーバになる
                                   const ScriptLocation& loc )
{
    return crb_class_create_instance<CRB_Regexp>( thread, env, receiver, 
                               {env["string"], env["option"]}, nullptr, loc );
}


static EvalResult regexp_initialize( CRB_Thread* thread,
                                     LocalEnvironment& env,
                                     CRB_Value& receiver, 
                                     const ScriptLocation& loc )
{
    CRB_Interpreter* interp = thread->interp;

    if ( !crb_object_isa(interp, env["string"], interp->class_string) ) {
        return crb_raise_type_error( thread, env, loc, env["string"],
                                     "string", "String" );
    }

    CRB_Regexp* self = static_cast<CRB_Regexp*>(receiver.u.object_value);
    CRB_String* pat_str = static_cast<CRB_String*>(env["string"].u.object_value);

    UErrorCode status = U_ZERO_ERROR;
    self->pattern = icu::RegexPattern::compile( *pat_str->value, 0, status );
    if ( U_FAILURE(status) ) {
        delete self->pattern;
        self->pattern = nullptr;

        return crb_runtime_error( thread, env, REGEXP_ERROR, loc,
                           icu::UnicodeString(_("Regex compiling error: ")) +
                                  *pat_str->value );
    }

    return EvalResult( CRB_NIL_VALUE );
}


static EvalResult class_regexp_to_s( CRB_Thread* thread,
                                     LocalEnvironment& env,
                                     CRB_Value& receiver,
                                     const ScriptLocation& loc )
{
    CRB_Regexp* self = static_cast<CRB_Regexp*>(receiver.u.object_value);

    return EvalResult( crb_string_new(thread->interp,
                    new icu::UnicodeString(self->pattern->pattern())) );
}


/** Regexp#=~ str */
static EvalResult class_regexp_match( CRB_Thread* thread,
                                      LocalEnvironment& env,
                                      CRB_Value& receiver,
                                      const ScriptLocation& loc )
{
    CRB_Interpreter* interp = thread->interp;

    if ( !crb_object_isa(interp, env["string"], interp->class_string) ) {
        return crb_raise_type_error( thread, env, loc, env["string"],
                                     "string", "String" );
    }

    CRB_Regexp* self = static_cast<CRB_Regexp*>(receiver.u.object_value);

    UErrorCode err = U_ZERO_ERROR;
    icu::RegexMatcher* matcher = self->pattern->matcher( *static_cast<CRB_String*>(env["string"].u.object_value)->value, err );
    if ( U_FAILURE(err) ) {
        delete matcher;
        return crb_runtime_error( thread, env, ARGUMENT_ERROR, loc,
                                  _("Argument match error") );
    }

    err = U_ZERO_ERROR;
    bool matched = matcher->find(0, err);
    if ( U_FAILURE(err) ) {
        delete matcher;
        return crb_runtime_error( thread, env, ARGUMENT_ERROR, loc,
                                  _("Argument match error") );
    }

    return EvalResult( new CRB_MatchData(interp, matcher, matched) );
}


//////////////////////////////////////////////////////////////////////
// MatchData

static EvalResult match_data_s_new( CRB_Thread* thread,
                                    LocalEnvironment& env,
                                    CRB_Value& receiver,  // クラスがレシーバになる
                                    const ScriptLocation& loc )
{
    return crb_class_create_instance<CRB_MatchData>( thread, env, receiver, 
                                                     { }, nullptr, loc );
}


/*
static EvalResult match_data_initialize( CRB_Thread* thread,
                                         CRB_Value& receiver,
                                         CRB_ValueList& args,
                                         const ScriptLocation& loc )
{
    return EvalResult( CRB_NIL_VALUE );
}
*/


static EvalResult class_match_data_to_s( CRB_Thread* thread,
                                         LocalEnvironment& env,
                                         CRB_Value& receiver,
                                         const ScriptLocation& loc )
{
    CRB_MatchData* match = static_cast<CRB_MatchData*>(receiver.u.object_value);

    return EvalResult( crb_string_new(thread->interp,
       new icu::UnicodeString(match->matched ? "(matched)" : "(unmatched)")) );
}


void class_regexp_add_methods( CRB_Thread* thread, LocalEnvironment& env )
{
    CRB_Interpreter* const interp = thread->interp;

    // class Regexp
    // CRB_Class* klass = nullptr;
    crb_interp_create_class( thread, env, "::Regexp", 
                             interp->class_object, ScriptLocation(), 
                             &interp->class_regexp );
    CRB_Class* regexp_s = crb_object_create_singleton_class( interp, 
                                                        interp->class_regexp );

    CRB_add_native_function( interp, regexp_s, "new", {"string", "?option"},
                             class_regexp_s_new );

    CRB_add_native_function( interp, interp->class_regexp, 
                             "initialize", {"string", "?option"},
                             regexp_initialize );
    CRB_add_native_function( interp, interp->class_regexp, 
                             "to_s", { }, class_regexp_to_s);
    CRB_add_native_function( interp, interp->class_regexp, 
                             "=~", {"string"}, 
                             class_regexp_match );

    ////////////////////////////////////////////////////////////////////
    // class MatchData

    crb_interp_create_class( thread, env, "::MatchData",
                             interp->class_object, ScriptLocation(),
                             &interp->class_match_data );
    CRB_Class* match_data_s = crb_object_create_singleton_class( interp, 
                                                   interp->class_match_data );

    CRB_add_native_function( interp, match_data_s, "new", { }, match_data_s_new );

    // CRB_add_native_function( interp, klass, "initialize", { }, 
    // match_data_initialize );
    CRB_add_native_function( interp, interp->class_match_data, "to_s", { }, 
                             class_match_data_to_s );
}



// Local variables:
// tab-width: 4
// c-basic-offset: 4
// indent-tabs-mode: nil
// End:
