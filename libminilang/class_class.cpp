﻿// -*- coding:utf-8-with-signature; mode:c++; tab-width:4; -*-

/*
  minilang -- a minilang interpreter.

  Copyright (c) 2011-2013 HORIKAWA Hisashi. All rights reserved.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// class Class

#include "support.h"
#include "CRB.h"
#include "DBG.h"
#include "execute.h"
#include <cassert>
#include <algorithm>

using namespace std;

/**
 * クラスの継承関係にもとづく検索リスト (モジュールのリスト) を生成する
 * 多重継承で共通のスーパークラスがあるとき (菱形継承), すべてのサブクラスの後に,
 * その共通のスーパークラスを追加する.
 * CL: `class-precedence-list` 関数や Python とは異なる。これらは多重継承でのスーパクラスの順序も制約にする C3 Linearization で順序化し、より強い制約。
 */
void crb_create_method_search_graph(MethodSearchGraph* graph,
                                     const CRB_Class* klass )
{
    assert( graph );
    assert( klass );

    graph->push_back( klass );
    MethodSearchGraph::iterator p;

    // モジュールは, 原則として, スーパークラスとの間に挟む
    MethodSearchGraph::const_iterator it;
    for ( it = klass->included_modules.cbegin();
                               it != klass->included_modules.cend(); it++ ) {
        if ( (p = find_if(graph->begin(), graph->end(),
                     [&](const CRB_Module* x)->bool{ return x->name == (*it)->name; })) !=
                                                         graph->end() ) {
            // 最初にincludeしたクラスの右へ移動 = 左側のモジュールを削除.
            graph->erase(p);
        }
        graph->push_back( *it );
    }

    if ( klass->superclass ) {
        if ( (p = find_if(graph->begin(), graph->end(),
                    [&](const CRB_Module* x)->bool{ return x->name == klass->superclass->name; })) !=
             graph->end() ) {
            // 菱形で, すでに登録ずみ  => 一番右へ
            graph->erase(p);
        }
        crb_create_method_search_graph( graph, klass->superclass );
    }
}


//////////////////////////////////////////////////////////////////////////
// class Class

CRB_Class::CRB_Class( CRB_Class* klass,
                      const icu::UnicodeString& class_name, CRB_Class* super ):
    CRB_ClassDescription(klass, class_name), superclass(super)
{
    // printf("new class: %s\n", name_.c_str() ); // DEBUG
}


/**
 * Class.new superclass = Object
 * 無名クラスを作る
 */
static EvalResult class_s_new( CRB_Thread* thread,
                               LocalEnvironment& env,
                               CRB_Value& receiver,
                               const ScriptLocation& loc )
{
    return crb_class_create_instance<CRB_Class>( thread, env, receiver,
                                          {env["superclass"]}, nullptr, loc );
}


static EvalResult class_initialize( CRB_Thread* thread,
                                    LocalEnvironment& env,
                                    CRB_Value& receiver,
                                    const ScriptLocation& loc )
{
    CRB_Interpreter* const interp = thread->interp;
    CRB_Class* superclass;

    if ( env["superclass"].type() != CRB_NIL_VALUE ) {
        if ( !crb_object_isa(interp, env["superclass"],
                                                    interp->class_class) ) {
            return crb_raise_type_error( thread, env, loc, env["superclass"],
                                     "superclass", "Class" );
        }
        superclass = static_cast<CRB_Class*>(env["superclass"].u.object_value);
    }
    else
        superclass = interp->class_object;

    static_cast<CRB_Class*>(receiver.u.object_value)->superclass = superclass;

    return EvalResult( CRB_NIL_VALUE );
}


static EvalResult class_superclass( CRB_Thread* thread,
                                    LocalEnvironment& env,
                                    CRB_Value& receiver,
                                    const ScriptLocation& loc )
{
    return EvalResult( crb_object_refer(
              static_cast<CRB_Class*>(receiver.u.object_value)->superclass) );
}


static EvalResult class_class_to_s( CRB_Thread* thread,
                                    LocalEnvironment& env,
                                    CRB_Value& receiver,
                                    const ScriptLocation& loc )
{
    return EvalResult( crb_string_new(thread->interp,
                                      new icu::UnicodeString(static_cast<CRB_Class*>(receiver.u.object_value)->name)) );
}


/** Class#include module */
static EvalResult class_include( CRB_Thread* thread,
                                 LocalEnvironment& env,
                                 CRB_Value& receiver,
                                 const ScriptLocation& loc )
{
    CRB_Interpreter* const interp = thread->interp;

    if ( !crb_object_isa(interp, env["module"], interp->class_module) ) {
        return crb_raise_type_error( thread, env, loc, env["module"],
                                     "module", "Module" );
    }
    CRB_Module* mod = static_cast<CRB_Module*>(env["module"].u.object_value);

    CRB_Class* self = static_cast<CRB_Class*>(receiver.u.object_value);
    if ( find(self->included_modules.begin(),
              self->included_modules.end(),
              mod) == self->included_modules.end() )
        self->included_modules.push_back(mod);

    return EvalResult( CRB_NIL_VALUE );
}


/**
 * Class#new
 * ユーザ定義クラスのインスタンスを生成する。
 */
static EvalResult class_class_new( CRB_Thread* thread,
                                      LocalEnvironment& env,
                                      CRB_Value& receiver,
                                      const ScriptLocation& loc )
{
    CRB_Value* args = env.search_local_var("args");

    CRB_Array* ary;
    ary = args && args->type() == CRB_OBJECT_VALUE ?
              static_cast<CRB_Array*>(args->u.object_value) : nullptr;

    return crb_class_create_instance<CRB_Object>( thread, env, receiver,
                                   (ary ? ary->list : CRB_ValueList()),
                                                  &env["block"], loc );
}


void class_class_add_methods( CRB_Thread* thread, LocalEnvironment& env )
{
    CRB_Interpreter* const interp = thread->interp;
    CRB_Class* klass = interp->class_class;
    assert(klass);

    // singleton methods
    CRB_add_native_function( interp,
                             crb_object_create_singleton_class(interp, klass),
                             "new", {"?superclass"},
                             class_s_new );

    // インスタンスメソッド
    CRB_add_native_function( interp, klass, "initialize", {"?superclass"},
                             class_initialize );

    CRB_add_native_function( interp, klass, "superclass", { },
                             class_superclass );

    CRB_add_native_function( interp, klass, "new", {"*args", "&block"},
                             class_class_new );

    CRB_add_native_function( interp, klass, "to_s", { },
                             class_class_to_s );

    // 一つだけ指定できる [incompat] rubyでは複数指定できる
    // [incompat] ruby では Module のメソッド.
    CRB_add_native_function( interp, klass, "include", {"module"},
                             class_include );
}



// Local variables:
// tab-width: 4
// c-basic-offset: 4
// indent-tabs-mode: nil
// End:
