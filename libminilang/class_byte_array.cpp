﻿// -*- coding:utf-8-with-signature; mode:c++; tab-width:4; -*-
// SPDX-License-Identifier: LGPL-3.0-or-later
/*
  minilang -- A small programming language, minilang interpreter.
  Copyright (c) 2011-2013, 2023 Netsphere Laboratories, Hisashi Horikawa.
  All rights reserved.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "support.h"
#include "class_byte_array.h"

using namespace std;


static EvalResult byte_array_s_new( CRB_Thread* thread,
                                    LocalEnvironment& env,
                                    CRB_Value& receiver,
                                    const ScriptLocation& loc )
{
    return crb_class_create_instance<CRB_ByteArray>( thread, env, receiver, { },
                                                     nullptr, loc );
}


void class_byte_array_add_methods( CRB_Thread* thread, LocalEnvironment& env )
{
    CRB_Interpreter* const interp = thread->interp;

    // CRB_Class* klass = nullptr;
    crb_interp_create_class( thread, env,
                             "::ByteArray",
                             interp->class_object, ScriptLocation(),
                             &interp->class_byte_array );

    // singleton methods
    CRB_add_native_function( interp,
                   crb_object_create_singleton_class(interp, interp->class_byte_array),
                   "new", { },
                   byte_array_s_new );

    // instance methods

}


// Local variables:
// tab-width: 4
// c-basic-offset: 4
// indent-tabs-mode: nil
// End:
