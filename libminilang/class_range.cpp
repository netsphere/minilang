﻿// -*- coding:utf-8-with-signature; mode:c++; tab-width:4; -*-

/*
  minilang -- a minilang interpreter.

  Copyright (c) 2011-2013 HORIKAWA Hisashi. All rights reserved.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "CRB.h"
#include "DBG.h"
#include "execute.h"
#include <string.h>
#include <assert.h>
using namespace std;


///////////////////////////////////////////////////////////////////////
// Range




///////////////////////////////////////////////////////////////////////
// Range::Iterator




void class_range_add_methods( CRB_Thread* thread, LocalEnvironment& env ) 
{
    CRB_Interpreter* const interp = thread->interp;

    ////////////////////////////////////////////////////////////////////
    // class Range

    CRB_Class* range;
    crb_interp_create_class( thread, env, "::Range", interp->class_object,
                             ScriptLocation(), &range );
    // CRB_Class* range_s = crb_object_create_singleton_class( interp, range );


    ////////////////////////////////////////////////////////////////////
    // class Range::Iterator

    CRB_Class* range_i;
    crb_interp_create_class( thread, env, 
                             "::Range::Iterator", interp->class_object,
                             ScriptLocation(), &range_i );
    // CRB_Class* range_i_s = crb_object_create_singleton_class( interp, range_i );
}



// Local variables:
// tab-width: 4
// c-basic-offset: 4
// indent-tabs-mode: nil
// End:
