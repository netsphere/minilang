﻿// -*- coding:utf-8-with-signature; mode:c++; tab-width:4; -*-
// SPDX-License-Identifier: LGPL-3.0-or-later
/*
  minilang -- A small programming language, minilang interpreter.
  Copyright (c) 2011-2013, 2023 Netsphere Laboratories, Hisashi Horikawa.
  All rights reserved.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

// 手書きのlexer.

#ifndef LEXER_H__
#define LEXER_H__ 1

#include <unicode/unistr.h>
#include <assert.h>

struct Token
{
    int token_type;
    icu::UnicodeString lexstr;

    int first_line;
    int first_column;

    Token(int token): token_type(token) { }

    Token(int token, const icu::UnicodeString& str):
        token_type(token), lexstr(str) { }
};


//class Parser;

class Lexer
{
protected:
    class Parser* const parser;

public:
    Lexer( Parser* p ): parser(p) {
        assert(parser);
    }

    virtual ~Lexer() { }

    virtual bool set_input_file( const icu::UnicodeString& filename,
                                 const char* encoding ) = 0;

    virtual bool set_input_file( FILE* fp, const char* encoding_ ) = 0;

    virtual bool set_input_string( const char* string, const char* encoding ) = 0;

    /**
     * 1トークン進める.
     * 呼出し側でtokenをdeleteすること。
     */
    virtual void receive( Token** token ) = 0;

    // virtual int line_number() const = 0;
    // virtual int column_number() const = 0;

    virtual bool iseof() const = 0;
};


Lexer* crb_create_lexer( Parser* parser );
void crb_lexer_cleanup();

int yylex( union YYSTYPE* yylval, struct YYLTYPE* yylloc, Parser* myparser );


#endif



// Local variables:
// tab-width: 4
// c-basic-offset: 4
// indent-tabs-mode: nil
// End:
