﻿// -*- coding:utf-8-with-signature; mode:c++; tab-width:4; -*-
// SPDX-License-Identifier: LGPL-3.0-or-later
/*
  minilang -- A small programming language, minilang interpreter.
  Copyright (c) 2011-2013, 2023 Netsphere Laboratories, Hisashi Horikawa.
  All rights reserved.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef COMPILE_H__
#define COMPILE_H__ 1


#include "ast.h"
#include <unicode/unistr.h>
#include <vector>
//#include "lexer.h"


typedef void (*ErrorHandler)( int line_number, int column_number,
                               const char* format, ... );

class Parser
{
public:
    class Lexer* lexer;

    ErrorHandler error_handler;
    int error_count;

    static void cleanup();

    icu::UnicodeString block_tag;
    int block_tag_num;

    enum { RETURN_TAG = 1, NEXT_TAG = 2, BREAK_TAG = 4 };
    std::vector<icu::UnicodeString> return_tags;
    std::vector<icu::UnicodeString> next_tags;
    std::vector<icu::UnicodeString> break_tags;

    void push_tag(unsigned int tag);
    icu::UnicodeString pop_tag(unsigned int tag);

public:
    Parser();
    ~Parser();

    bool compile_file( const icu::UnicodeString& filename );

    bool compile_string( const char* string );

    void set_error_handler( ErrorHandler proc );

    ParsedScript* detach_parsed_script();

/*
    void compile_error( int line_number, int column_number,
                        const char* format, ... );
*/

private:
    static void default_error_handler( int line_number, int column_number,
                                       const char* format, ... );

    friend void yyerror( const YYLTYPE* yylloc, Parser* myparser,
                         ParsedScript* driver, const char* msg );

    /** コンパイルした結果の AST */
    ParsedScript* parsed_script;
};

/* error.c */
/*
void crb_compile_error( int line_number, int column_number,
		        const char* format, ... );
*/




/*
typedef enum {
    PARSE_ERR = 1,
    CHARACTER_INVALID_ERR,
    // FUNCTION_MULTIPLE_DEFINE_ERR,
    COMPILE_ERROR_COUNT_PLUS_1
} CompileError;
*/



#endif


// Local variables:
// tab-width: 4
// c-basic-offset: 4
// indent-tabs-mode: nil
// End:
