/* -*- mode:c++ -*- */
// SPDX-License-Identifier: LGPL-3.0-or-later
/*
  minilang -- A small programming language, minilang interpreter.
  Copyright (c) 2011-2013, 2023 Netsphere Laboratories, Hisashi Horikawa.
  All rights reserved.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

/* いったん、
Ruby 言語のサブセットを作る
仕様: http://web.archive.org/web/20160320072847/https://www.ipa.go.jp/files/000011432.pdf
*/

// Bison 3.2: C++ `position.hh`, `stack.hh` が廃止.
%require "3.7"

%{
// parse.cpp の冒頭に出力される。

//#include "support.h"
//#include "CRB.h"  
#include <stdio.h>
#include "ast.h"
#include <unicode/unistr.h>
#include <unicode/ustdio.h>
#include <string>  // for int_value
#include <assert.h>
#include "create_ast.h"
#include "parser.h"
#include "lexer.h"
using namespace std;
using namespace icu;

#define CRB_REGEXP_CLASS     "::Regexp"

#ifndef NDEBUG
  #define YYDEBUG 1
#endif // !NDEBUG
%}

// parse.hh を出力
%defines

// location tracking
%locations
 //%define api.location.file "loc.h"

// 再入可能 (reentrant)
%define api.pure full

 // push parser
%define api.push-pull both

  //%debug    // obsoleted by %define parse.trace
%define parse.trace

  //%error-verbose
%define parse.error verbose


// `yylex()` と `yyparse()` の両方に追加. `%lex-param` + `%parse-param` と同じ.
%param { class Parser* myparser }

  // pure parser のみ. 追加の yylex() argument
  //%lex-param { Parser* myparser }

// 追加の yyparse() の引数.
%parse-param { struct ParsedScript* driver }


// union YYSTYPE 型
%union {
    icu::UnicodeString* lexstr;
    ParameterList*   parameter_list;
    OptParamList*    opt_param_list; // OptParamList = ptr_list<OptionalParamNode*> 
    ArgumentList*    argument_list;

    Expression*      expression;
    LambdaExprNode*  lambda_expr;
    VariableRef*     variable_ref;
    WhenClauseList*  when_list;

    StatementList*   statement_list;
    CondList*        elsif;  // CondList = ptr_list<CondBlock*>
    RescueList*      rescue_list;
    AssociationList* association_list;
    
    DefMethodName*   def_method_name;
    MethodArgument*    method_argument;

    //AttrAccessorStmt* slot_def;
    SlotSpecList*     slot_spec_list;
    ModuleStmtList*   module_stmt_list;
    StringList*       str_list;
}

  // このセクションが必要!
%destructor { delete $$; } <lexstr>
%destructor { delete $$; } <parameter_list>
%destructor { delete $$; } <opt_param_list>
%destructor { delete $$; } <argument_list>
%destructor { delete $$; } <expression>
%destructor { delete $$; } <lambda_expr>
%destructor { delete $$; } <variable_ref>
%destructor { delete $$; } <when_list>

%destructor { delete $$; } <statement_list>
%destructor { delete $$; } <elsif>
%destructor { delete $$; } <rescue_list>
%destructor { delete $$; } <association_list>
%destructor { delete $$->specializer; delete $$->method_name;
     delete $$; } <def_method_name>
%destructor { delete $$; } <method_argument>
%destructor { delete $$; } <slot_spec_list>
%destructor { delete $$->block; delete $$->include_list; delete $$->extend_list;
     delete $$->slot_specs;
     delete $$; } <module_stmt_list>
%destructor { delete $$; } <str_list>


%{
extern void yyerror( const YYLTYPE* yylloc, Parser* myparser,
                     ParsedScript* driver, const char* msg );
%}


  // %token TK_UNINITIALIZED
%token TK_RESERVED
%token <lexstr>  TK_INT_LITERAL
%token <lexstr>  TK_DOUBLE_LITERAL
%token <lexstr>  TK_STRING_LITERAL
  // heredoc のまま EOF に達した場合のため. 
%token TK_HEREDOC
%token <lexstr>  TK_REGEX_LITERAL
%token <lexstr>  TK_IDENTIFIER TK_CONST_IDENT TK_CLASSVAR_IDENT TK_IVAR_IDENT TK_QUALIFIED_CONST
  // メソッド名は小文字始まりのみ. [incompat]
%token <lexstr>  TK_METHOD_NAME
%token <lexstr>  TK_SYMBOL  TK_KEY
%token TK_EMBD_EXPR_OPEN
%token TK_DEF  TK_SUPER
%token TK_IF TK_MOD_IF TK_THEN TK_ELSE TK_ELSIF TK_WHILE TK_FOR 
       TK_RETURN TK_BREAK  TK_NEXT 
       TK_NIL TK_CLASS TK_MODULE TK_SELF  TK_LAMBDA_ARROW_OP TK_LAMBDA
%token TK_ALIAS  TK_PRIVATE  TK_PROTECTED  TK_REQUIRE  TK_UNLESS  TK_MOD_UNLESS  TK_UNTIL
  // Ruby ではメソッド
%token TK_ATTR_ACCESSOR  TK_ATTR_READER  TK_ATTR_WRITER 
  // Ruby ではメソッド
%token TK_INCLUDE TK_EXTEND
%token TK_BEGIN TK_RESCUE TK_ENSURE  
%token TK_VAR_ASSIGNMENT "=>"
%token TK_DO TK_END TK_YIELD
%token '('
%token ')'
  // "{" が曖昧.
%token TK_HASH_OPEN  TK_BLOCK_OPEN
%token '}'
%token TK_TERM
%token ','
%token '='
%token <lexstr> TK_ASSIGN_OP
%token TK_DOT2 ".."
%token TK_LOGICAL_AND "&&"
%token TK_LOGICAL_OR "||"
%token <lexstr> TK_COMPARE_OP
%token <lexstr> TK_EQNE_OP
  // %token TK_MATCH_OP "=~"   -> TK_EQNE_OP にまとめる
%token TK_IN "in"
%token '+'
%token '-'
  // "*" は文脈によって, TK_MUL, TK_SPLAT
%token TK_MUL
%token TK_SPLAT
%token TK_DIV "/"
%token TK_MOD "%"
%token TK_POWER
%token TK_LSHIFT "<<"
%token TK_RSHIFT ">>"
%token TK_TRUE "true"
%token TK_FALSE "false"
%token TK_METHOD_PARENTHESIS TK_NO_PARENTHESIS
%token TK_METHOD_SQUARE_BRACKET
%token TK_CASE TK_WHEN
%token TK_DEFINED

// 文並び
%type <statement_list> statement_list
%type <expression> statement   if_modifier

// メソッド定義
%type <expression>  method_definition
%type <parameter_list> parameter_list_opt parameter_list param_normal_list
%type <opt_param_list> param_key_list
%type <def_method_name> def_object_and_method_name

// 式
%type <lexstr> qualified_const
%type <expression>  expression  operator_expression  conditional_operator_expr
            range_constructor  logical_or_expression  logical_and_expression
            equality_expression relational_expression
            bitwise_or_expr bitwise_and_expr bitwise_shift_expr
            additive_expression multiplicative_expression
            power_expr  unary_expr
            primary_expression  primary_expr_except_method_name 
%type <association_list> association_list_opt association_list 
%type <lexstr> assign_op

// リテラル
%type <expression> literal  string_expr

// メソッド呼び出し
%type <expression> method_invocation_without_parentheses 
            primary_method_invocation 
%type <argument_list> operator_expr_list arg_normal_list
%type <method_argument> method_argument  method_argument_opt
%type <association_list> arg_key_association_list
%type <lambda_expr> block_arg_opt block_arg block_arg2
%type <parameter_list> block_param_list_opt

// ラムダ式
%type <lambda_expr> lambda_expr 

// クラス・モジュール定義
%type <expression>   class_definition
%type <lexstr> superclass_opt
%type <expression>    module_definition
%type <slot_spec_list>  attr_accessor_stmt
%type <variable_ref> mod_include_stmt  mod_extend_stmt
%type <module_stmt_list> mod_stmt_list
%type <str_list> symbol_list

// begin-rescue文
%type <expression>  begin_rescue_statement
%type <rescue_list> rescue_clause_list
%type <lexstr> exception_class_opt 
%type <variable_ref> non_const_var_ref variable_assignment_opt left_hand_side
%type <statement_list> ensure_clause_opt

// if式
%type <expression> if_expression
%type <statement_list> else_clause_opt 
%type <elsif> elsif_list

// case式
%type <expression> case_expression
%type <when_list> when_clause_list when_cond_clause_list
//%type <argument_list> when_pattern_list

// その他の文
%type <expression> while_statement for_statement
            return_statement break_statement next_statement /*retry_statement*/
%type <expression>  operator_expr_opt 


%%


////////////////////////////////////////////////////////////////////////////
// 文並び

/** トップレベル */
program :
    statement_list {
        driver->stmt_list = $1;
    }

/** 文並び */
statement_list :
    statement            {
        $$ = new StatementList();
        if ($1)
            $$->push_back($1);
    }
  | statement_list TK_TERM statement            {
        // assert($3);  `AST_chain_statement_list()` 内でカバー
        $$ = AST_chain_statement_list($1, $3);
    }
  ;


/** 
 * 文または式文. 文は "(" ")" で囲んで式とすることができないもの.
 * statement_listからのみ参照される.
 * [incompat] Rubyでは class-definition, method-definition も, 
 *            primary-expression になっている。
 */
statement :
    class_definition  
  | module_definition
  | method_definition
    /* 式文 */
  | expression
  | expression if_modifier      {
        $$ = AST_create_if_statement( $2, 
                AST_create_statement_list($1), nullptr, nullptr, @2, driver );
    }
  | while_statement
  | for_statement
  | return_statement
  | return_statement if_modifier    {
        $$ = AST_create_if_statement( $2, 
                AST_create_statement_list($1), nullptr, nullptr, @2, driver );
    }
  | break_statement
  | break_statement if_modifier     {
        $$ = AST_create_if_statement( $2, 
                AST_create_statement_list($1), nullptr, nullptr, @2, driver );
    }
  | next_statement
  | next_statement if_modifier   {
        $$ = AST_create_if_statement( $2, 
                AST_create_statement_list($1), nullptr, nullptr, @2, driver );
    }
/*
  | retry_statement
  | retry_statement if_modifier  {
        $$ = AST_create_if_statement( $2, 
                         AST_create_statement_list($1), nullptr, nullptr, @2 );
    }
*/
  | begin_rescue_statement
  | %empty         { $$ = nullptr; }
  | error {
        $$ = nullptr;
    }


if_modifier :  
    TK_MOD_IF operator_expression   {
        $$ = $2; 
    }
  | TK_MOD_UNLESS operator_expression {
        $$ = new UnaryExpr(UNARY_NOT_EXPR, @1, driver, $2);
    }


////////////////////////////////////////////////////////////////////////
// メソッド定義

method_definition :
    TK_DEF def_object_and_method_name parameter_list_opt { myparser->push_tag(Parser::RETURN_TAG); } statement_list TK_END   {
        $$ = new MethodDefinition(@1, driver,
                                  $2->specializer, $2->method_name, $3, $5,
                                  myparser->pop_tag(Parser::RETURN_TAG) );
        delete $2;
    }
  | TK_DEF def_object_and_method_name '=' '(' parameter_list ')'
          { myparser->push_tag(Parser::RETURN_TAG); } statement_list TK_END   {
        $$ = new MethodDefinition(@1, driver,
                        $2->specializer, &$2->method_name->append('='), $5, $8,
                        myparser->pop_tag(Parser::RETURN_TAG) );
        delete $2;
    }
  | TK_DEF def_object_and_method_name '=' parameter_list TK_TERM
          { myparser->push_tag(Parser::RETURN_TAG); } statement_list TK_END   {
        $$ = new MethodDefinition(@1, driver,
                        $2->specializer, &$2->method_name->append('='), $4, $7,
                        myparser->pop_tag(Parser::RETURN_TAG) );
        delete $2;
    }


 // クラスメソッド  def ClassName.foo, def self.foo
 // 特異メソッド def foo.bar
def_object_and_method_name :
    TK_METHOD_NAME      { 
        $$ = new DefMethodName(nullptr, $1);
    }
  | TK_METHOD_NAME '.' TK_METHOD_NAME  {
        /*        if (*$1 != "self") {
            yyerror( &@1, myparser, driver, _("must be 'self'") );
            YYERROR;
            } */
        $$ = new DefMethodName( 
                        new VariableRef(@1, driver, LOCAL_VAR, $1), $3 );
    }
  | TK_CONST_IDENT '.' TK_METHOD_NAME {
        $$ = new DefMethodName(new VariableRef(@1, driver, CONST_VAR, $1), $3 );
    }


parameter_list_opt :
    TK_NO_PARENTHESIS parameter_list TK_TERM {
        $$ = $2;
    }
  | TK_METHOD_PARENTHESIS ')'  {
        $$ = nullptr;
    }
  | TK_METHOD_PARENTHESIS parameter_list ')'  {
        $$ = $2;
    }
  | TK_TERM   { $$ = nullptr; }


/**
 * [ParameterList*] 仮引数リスト. 必ずrestパラメータ, blockパラメータという順.
 */
parameter_list :
                                       '&' TK_IDENTIFIER {
        $$ = new ParameterList();
        $$->block_param = $2;
    }
  | param_normal_list
  | param_normal_list               ',' '&' TK_IDENTIFIER {
        $$ = $1;
        $$->block_param = $4;
    }
  |                 param_key_list {
        $$ = new ParameterList();
        $$->key_params = $1;
    }
  |                 param_key_list ',' '&' TK_IDENTIFIER {
        $$ = new ParameterList();
        $$->key_params = $1;
        $$->block_param = $4;
    }
  | param_normal_list ',' param_key_list {
        $$ = $1;
        $$->key_params = $3;
    }
  | param_normal_list ',' param_key_list ',' '&' TK_IDENTIFIER {
        $$ = $1;
        $$->key_params = $3;
        $$->block_param = $6;
    }
    ;

/** 
 * ブロック引数の仮引数内で, 
 * "do | x = s | t | ... end" というのは曖昧なので、デフォルト値としては 
 * additive_expression のみを許可する. x = (s | t) と書け. 
 */
param_normal_list :
    // ruby: ここにインスタンス変数は書けない. OK
    TK_IDENTIFIER          {
        $$ = new ParameterList();
        // このなかで変数名の重複チェック
        AST_param_chain_required($$, $1); 
    }
  | TK_IDENTIFIER '=' additive_expression             {
        $$ = new ParameterList();
        AST_param_chain_optional( $$, $1, $3 );
    }
  | TK_SPLAT TK_IDENTIFIER {
        $$ = new ParameterList();
        $$->rest_param = $2;
    }
  | param_normal_list ',' TK_IDENTIFIER            {
        // このなかで optional との順序チェック
        $$ = AST_param_chain_required($1, $3);
    }
  | param_normal_list ',' TK_IDENTIFIER '=' additive_expression  {
        $$ = AST_param_chain_optional( $1, $3, $5 );
    }
  | param_normal_list ',' TK_SPLAT TK_IDENTIFIER {
        $$ = $1;
        if ($$->rest_param) {
            yyerror(&@3, myparser, driver, "duplicate splat param");
            YYERROR;
        }
        $$->rest_param = $4;
    }

  // hoge:  
param_key_list :
    TK_KEY {
        $$ = new OptParamList();
        $$->push_back(new OptionalParamNode($1, nullptr));
    }
  | param_key_list ',' TK_KEY {
        $$ = $1;
        $$->push_back(new OptionalParamNode($3, nullptr));
    }
    ;


////////////////////////////////////////////////////////////////////////
// 式

expression :
    method_invocation_without_parentheses
  | operator_expression
    ;


qualified_const :
    TK_CONST_IDENT
  | TK_QUALIFIED_CONST
    ;


assign_op :
    TK_ASSIGN_OP  
  | '=' { $$ = nullptr; }


/**
 * 構文規則 11.4.1 演算子式
 * 後置 if を置けない場所の式. 実引数など */
operator_expression :  
    conditional_operator_expr
  | left_hand_side assign_op expression           {
        if ( $2 ) {
            //VariableRef* left_copy = $1->clone();
            $$ = new AssignOpExpr(@1, driver, $1, $2, $3);
  /*            
            // 特別扱い: a ||= hoge    => a || (a = hoge)
            // `&&=` も同様.
            // obj.foo ||= true は obj.foo || (obj.foo = true) と解釈し、
            // `foo=` メソッドは呼び出されないかもしれない。
            if ( *$2 == "||=" ) {
                $$ = new LogicalOpExpr(LOGICAL_OR_EXPRESSION, @1, driver,
                                $1,
                                new AssignExpression(@1, driver, left_copy, $3));
            }
            else if ( *$2 == "&&=" ) {
                $$ = new LogicalOpExpr(LOGICAL_AND_EXPRESSION, @1, driver,
                                $1,
                                new AssignExpression(@1, driver, left_copy, $3));
            }
            else {
                $$ = new AssignExpression(@1, driver, $1,
                               AST_create_method_call1(left_copy,
                                       UnicodeString(*$2, 0, $2->length() - 1),
                                       $3, @1, driver));
            }
            
            delete $2;
  */
        }
        else
            $$ = new AssignExpression( @1, driver, $1, /*$2,*/ $3);
    }
    ;


/**
 * 構文規則 11.5.2 条件演算子式
 * 3項演算子 */
conditional_operator_expr :
    range_constructor
  | range_constructor '?' operator_expression ':' operator_expression {
        $$ = AST_create_if_statement( $1, AST_create_statement_list($3), 
                         nullptr, AST_create_statement_list($5), @1, driver );
	}


range_constructor :
    logical_or_expression
  | logical_or_expression ".." logical_or_expression  {  // non-associativity
        $$ = AST_create_range_ctor($1, $3, @1, driver );
	}


/* 左を評価し、真なら左を返し、偽なら右を返す。true/falseを返すのではない。 */
logical_or_expression :
    logical_and_expression
  | logical_or_expression "||" logical_and_expression  {  // left
        $$ = new LogicalOpExpr( LOGICAL_OR_EXPRESSION, @1, driver, $1, $3);
    }


logical_and_expression :
    equality_expression
  | logical_and_expression "&&" equality_expression  {  // left
        $$ = new LogicalOpExpr(LOGICAL_AND_EXPRESSION, @1, driver, $1, $3);
    }


/** <=> == != =~ !~ */
equality_expression :
    relational_expression
  | relational_expression TK_EQNE_OP relational_expression {  // non-assoc
        // 3値論理を実装できるように, "!="もメソッドにする。  => 特例処理を置かない
        if (*$2 == "!~") {
            $$ = new UnaryExpr(UNARY_NOT_EXPR, @1, driver,
                        AST_create_method_call1($1, "=~", $3, @1, driver));
        }
        else
            $$ = AST_create_method_call1($1, *$2, $3, @1, driver );

        delete $2;
    }


/**
 * > >= < <= 
 * non-assoc. [incompat] Rubyはleft-assoc
 */
relational_expression :
    bitwise_or_expr
  | bitwise_or_expr TK_COMPARE_OP bitwise_or_expr   { 
        $$ = AST_create_method_call1($1, *$2, $3, @1, driver );
        delete $2;
    }


bitwise_or_expr :
    bitwise_and_expr
  | bitwise_or_expr '|' bitwise_and_expr { // left
        $$ = AST_create_method_call1($1, "|", $3, @1, driver );
	}
  | bitwise_or_expr '^' bitwise_and_expr { // left
        $$ = AST_create_method_call1($1, "^", $3, @1, driver );
	}


bitwise_and_expr :
    bitwise_shift_expr
  | bitwise_and_expr '&' bitwise_shift_expr { // left
        $$ = AST_create_method_call1($1, "&", $3, @1, driver );
	}


bitwise_shift_expr :
    additive_expression
  | bitwise_shift_expr "<<" additive_expression { // left
        $$ = AST_create_method_call1($1, "<<", $3, @1, driver );
	}
  | bitwise_shift_expr ">>" additive_expression { // left
        $$ = AST_create_method_call1($1, ">>", $3, @1, driver );
	}


additive_expression :
    multiplicative_expression
  /*  | "-" multiplicative_expression {
        $$ = AST_create_method_call0($2, "-@", @1, driver );
        } */
  | additive_expression '+' multiplicative_expression        {
        $$ = AST_create_method_call1($1, "+", $3, @1, driver );
    }
  | additive_expression '-' multiplicative_expression           {
        $$ = AST_create_method_call1($1, "-", $3, @1, driver );
    }


multiplicative_expression :
    power_expr    // "- x * x" => - (x * x)
  | multiplicative_expression TK_MUL power_expr           {
        $$ = AST_create_method_call1($1, "*", $3, @1, driver );
    }
  | multiplicative_expression "/" power_expr          {
        $$ = AST_create_method_call1($1, "/", $3, @1, driver );
    }
  | multiplicative_expression "%" power_expr          {
        $$ = AST_create_method_call1($1, "%", $3, @1, driver );
    }


/** 
 * 単項マイナス. 
 * "method - x" が method( -x) or method( (-x) ) か曖昧 
 * "- x * x"   => - (x * x) とする。式の最初の単項マイナスの順位はもっと下げる 
unary_minus_expr : ●これをすっ飛ばす
    power_expr
  | "-" power_expr  {  // - - x というのは不可
        $$ = AST_create_method_call0($2, "-@", @1, driver );
    }
*/

  /* Ruby と Crystal で優先順位が違う! JavaScript のように括弧を必須にする */
power_expr :
    unary_expr
  | primary_expression TK_POWER power_expr   {    // right
        $$ = AST_create_method_call1($1, "**", $3, @1, driver );
    }


unary_expr :
    primary_expression
  | '+' unary_expr {
        $$ = $2;    // "+@" はない [incompat]
    }
  | '-' unary_expr {
        $$ = AST_create_method_call0($2, "-@", @1, driver );
    }
  | '~' unary_expr  {      // bitwise NOT
        $$ = AST_create_method_call0($2, "~", @1, driver );
    }
  | '!' unary_expr   	{
        $$ = new UnaryExpr(UNARY_NOT_EXPR, @1, driver, $2);
    }

/* JavaScript では:
UnaryExpr    単項+ - ~ ! delete void typeof
   ↓
UpdateExpr   ++ --
   ↓
LeftHandSideExpr
   = NewExpr | CallExpr

CallExpr   [], .method  super
   ↓
CoverCallExpressionAndAsyncArrowHead
   ↓
MemberExpression
*/

  /** 一次式. */
primary_expression :
    left_hand_side  {
        if ($1->m_type == OBJECT_SLOT_REF) {
            ObjectSlotRef* s = static_cast<ObjectSlotRef*>($1);
            $$ = new MethodCallNode(s->m_loc, s->m_root,
                                    s->object,
                                    s->name, 
                                    s->bracket_arguments,
                                    nullptr);
            //$1->m_loc = nullptr; $1->m_root = nullptr;
            s->name = nullptr;
            s->object = nullptr; s->bracket_arguments = nullptr;
            delete s;
        }
        else
            $$ = $1; // 型が違う
    }
  | primary_expr_except_method_name
  | primary_method_invocation
    ;

  /* left_hand_side += ... と書けるので、ここでメソッド呼び出しにはできない */
left_hand_side :
    /* 二つ合わせて VARNAME.
       ローカル変数か実引数のないメソッド呼出しか, 区別つかない */
    non_const_var_ref 
  | qualified_const {
        $$ = new VariableRef( @1, driver, CONST_VAR, $1);
    }
  | primary_expression '[' method_argument ']'  {
        $$ = new ObjectSlotRef(@1, driver, $1, LOCAL_VAR, new UnicodeString("[]"), $3);
    }
  | primary_expression TK_METHOD_SQUARE_BRACKET method_argument ']'  {
        // ここにもキーワード引数を書ける
        $$ = new ObjectSlotRef(@1, driver, $1, LOCAL_VAR, new UnicodeString("[]"), $3);
    }
  | primary_expression '.' TK_METHOD_NAME {
        // ruby: ブロックの後ろに '.' で繋げてよい.
        $$ = new ObjectSlotRef(@1, driver, $1, LOCAL_VAR, $3, nullptr);
    }
/* メソッド名は小文字始まりのみ。プロパティはメソッド. getter はない.
  | primary_expression '.' TK_CONST_IDENT {
        $$ = new ObjectSlotRef(@1, driver, $1, CONST_VAR, $3, nullptr);
    } */
    ;    


/** 一次式の半分
    "(...)[x] = xxx" を実現するため, primary-expression を二つに分ける */
primary_expr_except_method_name :
    if_expression
  | case_expression
  | TK_IDENTIFIER TK_METHOD_PARENTHESIS method_argument_opt ')' 
                    { myparser->push_tag(Parser::BREAK_TAG); } block_arg_opt {
        Expression* expr =
            AST_create_function_call_expression( $1, $3, $6, @1, driver );
        UnicodeString tag = myparser->pop_tag(Parser::BREAK_TAG); 
        $$ = $6 ? new BlockNode(@5, driver, tag, expr) : expr;
    }
  | TK_YIELD TK_METHOD_PARENTHESIS method_argument_opt ')'   {
        // ruby: yield にブロック実引数は禁止.
        if ($3 && $3->block_arg) {
            yyerror( &@3, myparser, driver, "syntax error: block-arg for yield is forbidden" );
            YYERROR;
        }
        Expression* expr = new FunctionCallNode(YIELD_EXPR, @1, driver, $3 );
        $$ = expr; 
    }
  | TK_SUPER TK_METHOD_PARENTHESIS method_argument_opt ')'  {
        // ruby: super にブロック実引数は禁止.
        if ($3 && $3->block_arg) {
            yyerror( &@3, myparser, driver, "syntax error: block-arg for super is forbidden" );
            YYERROR;
        }
        Expression* expr = new FunctionCallNode(SUPER_EXPR, @1, driver, $3);
        $$ = expr; 
    }
  | primary_expression '.' TK_METHOD_NAME TK_METHOD_PARENTHESIS 
           method_argument_opt ')' { myparser->push_tag(Parser::BREAK_TAG); } block_arg_opt   {
        MethodArgument* args = ($5 != nullptr || $8 == nullptr) ? $5 : new MethodArgument();
        Expression* expr = new MethodCallNode(@1, driver, $1, $3, args, $8);
        UnicodeString tag = myparser->pop_tag(Parser::BREAK_TAG); 
        $$ = $8 ? new BlockNode(@8, driver, tag, expr) : expr;
    }
  | '(' expression ')'          {
        $$ = $2;
    }
  | TK_SELF          {
        // selfへの代入かどうかは, eval.cc:assign_variable() で調べる
        $$ = new VariableRef( @1, driver, LOCAL_VAR, new UnicodeString("self"));
    }
  | TK_TRUE           {
        $$ = AST_create_boolean_expression(true, @1, driver );
    }
  | TK_FALSE           {
        $$ = AST_create_boolean_expression(false, @1, driver );
    }
  | TK_NIL            {
        $$ = new LiteralExpression( NULL_EXPRESSION, @1, driver );
    }
  | lambda_expr { $$ = $1; // 型が違う
    }
  | '[' method_argument_opt ']'    { //    array_constructor
        // キーワード引数は不可
        if ($2 && $2->key && $2->key->size() > 0) {
            yyerror( &@2, myparser, driver, "syntax error: key args are forbidden" );
            YYERROR;
        }
        $$ = new ArrayExpr(@1, driver, $2 ? $2->normal : nullptr);
    }
  | TK_HASH_OPEN association_list_opt '}'    {  // hash_constructor
        $$ = new HashExpr(@1, driver, $2);
    }
  | literal
    ;

/** 
 * 構文規則 11.3 一次式メソッド呼出し
 * "method[1] = ..."   (operator []=) と method [1] (argument array) が曖昧
 *   => 空白の有無で判断する
 * 実引数なしでいきなりブロックを書くのは, 最初の実引数がハッシュ, と曖昧
 *    "method {...}"    => method() {...}  or  method({...})
 *   => ブロックと見做す. ruby と同じ
 */
primary_method_invocation :
    TK_IDENTIFIER TK_NO_PARENTHESIS { myparser->push_tag(Parser::BREAK_TAG); } block_arg    {
        $$ = new BlockNode(@4, driver, myparser->pop_tag(Parser::BREAK_TAG),
                           AST_create_function_call_expression( $1, nullptr, $4, 
                                                                @1, driver ));
    }
  | TK_LAMBDA TK_NO_PARENTHESIS block_arg2   {
        // lambda と proc はまるで挙動が異なる。無茶苦茶。`break` は例外発生
        // $3 のなかで `next`, `return` を手当てする。
        $$ = $3;
    }
  | TK_YIELD {
        $$ = new FunctionCallNode(YIELD_EXPR, @1, driver, nullptr);
    }
/* ruby: yield にブロック実引数は禁止.
  | TK_YIELD TK_NO_PARENTHESIS { myparser->push_tag(Parser::BREAK_TAG); } block_arg {
        $$ = new BlockNode(@4, driver, myparser->pop_tag(Parser::BREAK_TAG),
                    new FunctionCallNode(YIELD_EXPR, @1, driver, nullptr, nullptr, $4));
    }
*/
  | TK_SUPER {
        $$ = new FunctionCallNode(SUPER_EXPR, @1, driver, nullptr);
    }
/* ruby: super にブロック実引数は禁止.
  | TK_SUPER TK_NO_PARENTHESIS { myparser->push_tag(Parser::BREAK_TAG); } block_arg {
        $$ = new BlockNode(@4, driver, myparser->pop_tag(Parser::BREAK_TAG),
                    new FunctionCallNode(SUPER_EXPR, @1, driver, nullptr, nullptr, $4) );
    }
*/
  | primary_expression '.' TK_METHOD_NAME TK_NO_PARENTHESIS { myparser->push_tag(Parser::BREAK_TAG); } block_arg   {
        $$ = new BlockNode(@6, driver, myparser->pop_tag(Parser::BREAK_TAG),
                           new MethodCallNode(@1, driver, $1, $3,
                                              new MethodArgument(), $6));
    }
  ;


association_list_opt :
    %empty  { $$ = nullptr; }
  | association_list
    ;


/** "{" "}" の省略記法があるため、後置ifと混同しないために、key は expression 不可.
 TODO: double splat */
association_list :
    operator_expression "=>" operator_expression   	{
        $$ = AST_create_association_list($1, $3);
    }
  | TK_KEY operator_expression {
        // ruby 1.9. 大文字始まりでも, ただのシンボルで, 定数ではない
        $$ = AST_create_association_list(
                                new VariableRef(@1, driver, SYMBOL, $1), $2 );
    }
  | association_list ',' operator_expression "=>" operator_expression   {
        $$ = AST_chain_association_list($1, $3, $5);
    }
  | association_list ',' TK_KEY operator_expression   {
        $$ = AST_chain_association_list( $1, 
                                new VariableRef(@3, driver, SYMBOL, $3),
                                $4 );
    }


///////////////////////////////////////////////////////////////////////////
// リテラル

/** リテラル. TODO: %表記 */
literal :
    TK_INT_LITERAL   	{
        LiteralExpression* expression = 
                             new LiteralExpression(INT_LITERAL, @1, driver);
        string s;
        $1->toUTF8String(s);
        delete $1;
        mpz_init_set_str(expression->u.int_value, s.c_str(), 10 );
        $$ = expression;
	}
  | TK_DOUBLE_LITERAL   	{
      LiteralExpression* expr = 
                            new LiteralExpression(FLOAT_LITERAL, @1, driver);
        u_sscanf( $1->getTerminatedBuffer(), "%lf", &expr->u.double_value );
        delete $1;
        $$ = expr;
	}
  | string_expr 
  | TK_REGEX_LITERAL            {
        VariableRef* receiver = new VariableRef( @1, driver,
                                        VariableType::CONST_VAR,
                                        new UnicodeString( CRB_REGEXP_CLASS));
        StringExpr* expr = new StringExpr( @1, driver );
        expr->literals.push_back( $1 );
        $$ = AST_create_method_call1(receiver, "new", expr, @1, driver );
    }
  | TK_SYMBOL    {
        $$ = new VariableRef(@1, driver, SYMBOL, $1);
    }


/** 文字列式. 構造を持つ */
string_expr :
    TK_STRING_LITERAL   {
        $$ = AST_create_string_expr($1, @1, driver );
    }
  | string_expr TK_EMBD_EXPR_OPEN expression '}' TK_STRING_LITERAL {
        $$ = AST_chain_string_expr( static_cast<StringExpr*>($1), $3, $5);
    }


////////////////////////////////////////////////////////////////////////
// メソッド呼び出し

/** 
 * カッコなしの実引数 (一つ以上) を持つメソッド. 実引数が一つもないのは primary_method_invocation.
 * primary_expression より低くする.
 *     "method x . y" が (method x) . y か method (x . y) か [ruby: 後者]
 */
method_invocation_without_parentheses :
    TK_IDENTIFIER TK_NO_PARENTHESIS method_argument
                        { myparser->push_tag(Parser::BREAK_TAG); } block_arg_opt {
        Expression* expr =
            AST_create_function_call_expression( $1, $3, $5, @1, driver );
        UnicodeString tag = myparser->pop_tag(Parser::BREAK_TAG); 
        $$ = $5 ? new BlockNode(@5, driver, tag, expr) : expr;
    }
  | primary_expression '.' TK_METHOD_NAME TK_NO_PARENTHESIS method_argument 
                        { myparser->push_tag(Parser::BREAK_TAG); } block_arg_opt {
        Expression* expr = new MethodCallNode(@1, driver, $1, $3, $5, $7);
        UnicodeString tag = myparser->pop_tag(Parser::BREAK_TAG); 
        $$ = $7 ? new BlockNode(@7, driver, tag, expr) : expr;
    }
  | TK_YIELD TK_NO_PARENTHESIS method_argument
               /* { myparser->push_tag(Parser::BREAK_TAG); } block_arg_opt*/ {
        // ruby: yield にブロック実引数は禁止.                    
        if ($3 && $3->block_arg) {
            yyerror( &@3, myparser, driver, "syntax error: block-arg for yield is forbidden" );
            YYERROR;
        }
        $$ = new FunctionCallNode(YIELD_EXPR, @1, driver, $3 );
    }
  | TK_SUPER TK_NO_PARENTHESIS method_argument
              /*   { myparser->push_tag(Parser::BREAK_TAG); } block_arg_opt */ {
        // ruby: super にブロック実引数は禁止.
        if ($3 && $3->block_arg) {
            yyerror( &@3, myparser, driver, "syntax error: block-arg for super is forbidden" );
            YYERROR;
        }
        $$ = new FunctionCallNode(SUPER_EXPR, @1, driver, $3 );
    }

block_arg :
    TK_DO block_param_list_opt { myparser->push_tag(Parser::NEXT_TAG); } statement_list TK_END           {
        $$ = AST_create_lambda_expr($2, $4, myparser->pop_tag(Parser::NEXT_TAG),  @1, driver );
    }
  | TK_BLOCK_OPEN block_param_list_opt { myparser->push_tag(Parser::NEXT_TAG); } statement_list '}'  {
        $$ = AST_create_lambda_expr($2, $4, myparser->pop_tag(Parser::NEXT_TAG), @1, driver );
    }

block_arg2 :
    TK_DO block_param_list_opt { myparser->push_tag(Parser::NEXT_TAG | Parser::RETURN_TAG); } statement_list TK_END           {
        $$ = AST_create_lambda_expr($2, $4, myparser->pop_tag(Parser::NEXT_TAG | Parser::RETURN_TAG),  @1, driver );
    }
  | TK_BLOCK_OPEN block_param_list_opt { myparser->push_tag(Parser::NEXT_TAG | Parser::RETURN_TAG); } statement_list '}'  {
        $$ = AST_create_lambda_expr($2, $4, myparser->pop_tag(Parser::NEXT_TAG | Parser::RETURN_TAG), @1, driver );
    }

block_arg_opt :
    block_arg
  | %empty  { $$ = nullptr; }


block_param_list_opt :
    '|' '|'          {
        $$ = nullptr;
    }
  | "||"           {
        $$ = nullptr;
	}
  | '|' parameter_list '|'            {
        $$ = $2;
	}
  | %empty   { $$ = nullptr; }
  ;

  // 汎用の式リスト. 実引数は arg_normal_list
operator_expr_list :
    operator_expression            {
        $$ = AST_create_argument_list( NORMAL_ARG, $1 );
    }
  | operator_expr_list ',' operator_expression           {
        $$ = AST_chain_argument_list( $1, NORMAL_ARG, $3 );
    }


method_argument_opt :
    method_argument
  | %empty  { $$ = nullptr; }
  ;


/** 
 * 実引数. 後置ifがあるので、expressionノードは不可
 * TODO: Ruby仕様では, ここに command がある. 
 *     11.3 Method invocation expressions   -- command
 *     11.3.2 Method arguments   -- argument-list
 */
method_argument :
    method_invocation_without_parentheses {
        $$ = new MethodArgument(AST_create_argument_list(NORMAL_ARG, $1),
                                nullptr, nullptr);
    }
  |                                             '&' operator_expression {
        $$ = new MethodArgument(nullptr, nullptr, $2);
    }
  | arg_normal_list {
        $$ = new MethodArgument($1, nullptr, nullptr);
    }
  | arg_normal_list                         ',' '&' operator_expression {
        // pp %w(homu mami mado).map(&:upcase)
        // '&' の後ろはシンボルに限らない。例えばλ式でもよい.
        $$ = new MethodArgument($1, nullptr, $4);
    }
  |                 arg_key_association_list    {
        $$ = new MethodArgument(nullptr, $1, nullptr);
    }
  |                 arg_key_association_list ',' '&' operator_expression {   
        $$ = new MethodArgument(nullptr, $1, $4);
    }
  | arg_normal_list ',' arg_key_association_list  {
        // キーワード引数と両方指定できる
        $$ = new MethodArgument($1, $3, nullptr);
    }
  | arg_normal_list ',' arg_key_association_list ',' '&' operator_expression {   
        $$ = new MethodArgument($1, $3, $6);
    }
    ;

  // 実引数の通常引数部. "*" を好きな場所に複数書けることに注意.
arg_normal_list :
    operator_expression            {
        $$ = AST_create_argument_list( NORMAL_ARG, $1 );
    }
  | TK_SPLAT operator_expression {  
        $$ = AST_create_argument_list(SPLAT_ARG, $2);
    }
  | arg_normal_list ',' operator_expression           {
        $$ = AST_chain_argument_list( $1, NORMAL_ARG, $3 );
    }
  | arg_normal_list ',' TK_SPLAT operator_expression           {
        $$ = AST_chain_argument_list( $1, SPLAT_ARG, $4 );
    }

/** 実引数のキーワード引数部. キーをシンボルに限定 [incompat]
    ruby: foo(x: 1) スタイルはシンボルになる名前限定。foo("x".to_sym => 2) は OK
 TODO: double splat */
arg_key_association_list :
    TK_SYMBOL "=>" operator_expression {
        $$ = AST_create_association_list( 
                                new VariableRef(@1, driver, SYMBOL, $1), $3);
    }
  | TK_KEY operator_expression {
        // ruby 1.9
        $$ = AST_create_association_list( 
                                new VariableRef(@1, driver, SYMBOL, $1), $2);
    }
  | arg_key_association_list ',' TK_SYMBOL "=>" operator_expression {
        // リテラルでのキーの重複はエラー. [[incompat]]
        $$ = AST_chain_association_list($1, 
                                new VariableRef(@3, driver, SYMBOL, $3), $5);
    }
  | arg_key_association_list ',' TK_KEY operator_expression {
        $$ = AST_chain_association_list($1, 
                                new VariableRef(@3, driver, SYMBOL, $3), $4);
    }


////////////////////////////////////////////////////////////////////////
// ラムダ式

// Ruby: lambda do |x, y| ... end  これは単にメソッド呼出し
//       "->" (x, y) { ... }    仮引数のカッコは省略できる! 引数なしがある
lambda_expr :
    TK_LAMBDA_ARROW_OP parameter_list_opt TK_BLOCK_OPEN { myparser->push_tag(Parser::NEXT_TAG | Parser::RETURN_TAG); } statement_list '}'  {
        $$ = AST_create_lambda_expr($2, $5, myparser->pop_tag(Parser::NEXT_TAG | Parser::RETURN_TAG), @1, driver );
    }


////////////////////////////////////////////////////////////////////////
// クラス定義

class_definition :
    TK_CLASS TK_CONST_IDENT superclass_opt TK_TERM mod_stmt_list TK_END {
        $$ = new ClassDefinitionNode(@1, driver, $2, $3, $5);
    }
/* 冗長. def ClassName.foo, def self.foo を使え.
   または, extend を使え
  | TK_CLASS "<<" operator_expression TK_TERM statement_list TK_END {
        $$ = AST_create_class_definition( nullptr, nullptr, $3, $5, 
                                          @1, driver );
    } */


superclass_opt :
    TK_COMPARE_OP qualified_const  	  {
        /* Rubyではidentではなく式. それはダメだろう
	     class C < (A + X); end とかでも通る [incompat] */
        if (*$1 != "<") {
            yyerror( &@1, myparser, driver, "syntax error: must '<'" );
            YYERROR;
        }
        delete $1;
        $$ = $2;
	}
  | %empty         { $$ = nullptr; }


////////////////////////////////////////////////////////////////////////
// モジュール定義

module_definition :
    TK_MODULE TK_CONST_IDENT TK_TERM mod_stmt_list TK_END {
        $$ = new ModuleDefinitionNode(MODULE_DEFINITION, @1, driver, $2, $4);
    }

  // special: include, extend 
mod_stmt_list :
    statement            {
        $$ = new ModuleStmtList(AST_create_statement_list($1), NULL, NULL, NULL);
    }
  | mod_include_stmt {
        $$ = new ModuleStmtList(NULL, $1, NULL, NULL);
    }
  | mod_extend_stmt {
        $$ = new ModuleStmtList(NULL, NULL, $1, NULL);
    }
  | attr_accessor_stmt {
        $$ = new ModuleStmtList(NULL, NULL, NULL, $1 );
    }
  | mod_stmt_list TK_TERM statement            {
        $$ = $1;
        if (!$$->block) $$->block = new StatementList();
        AST_chain_statement_list($$->block, $3);
    }
  | mod_stmt_list TK_TERM mod_include_stmt {
        $$ = $1;
        if (!$$->include_list)
            $$->include_list = new ptr_list<VariableRef*>();
        $$->include_list->push_front($3); // 下が先に検索
    }
  | mod_stmt_list TK_TERM mod_extend_stmt {
        $$ = $1;
        if (!$$->extend_list) $$->extend_list = new ptr_vector<VariableRef*>();
        $$->extend_list->push_back($3);
    }
  ;

  // Module#include 文
mod_include_stmt :
    TK_INCLUDE qualified_const {
        $$ = new VariableRef(@2, driver, CONST_VAR, $2);
    }
  ;

  // モジュール直下の extend 文. (クラス以外の extend はメソッド扱い.)
mod_extend_stmt :
    TK_EXTEND qualified_const {
        $$ = new VariableRef(@2, driver, CONST_VAR, $2); 
    }
  | TK_EXTEND TK_SELF  {
        // モジュール定義直下にしか書けない. (`self` はモジュールに限る.)
        $$ = new VariableRef(@2, driver, LOCAL_VAR, new UnicodeString("self") ); 
    }

attr_accessor_stmt :
    TK_ATTR_ACCESSOR symbol_list { 
        $$ = AST_slot_spec_list_from_sym_list($2, SlotSpec::ACCESSOR);
    }
  | TK_ATTR_READER symbol_list  {
        $$ = AST_slot_spec_list_from_sym_list($2, SlotSpec::READER);
    }
  | TK_ATTR_WRITER symbol_list  {
        $$ = AST_slot_spec_list_from_sym_list($2, SlotSpec::WRITER);
    }

symbol_list :
    TK_SYMBOL { $$ = new StringList(); $$->push_back($1); }
  | symbol_list ',' TK_SYMBOL {
        $$ = $1; $$->push_back($3);
    }
  ;


////////////////////////////////////////////////////////////////////////
// begin-rescue文

/** 
 * [incompat] Rubyでは文ではなくbegin式 
 * rescue か ensure のいずれかを書かなければならない. */
begin_rescue_statement :
    TK_BEGIN statement_list rescue_clause_list ensure_clause_opt TK_END {
        Expression* expr = new BeginRescueStmt(@1, driver, $2, $3);
        $$ = $4 ? new UnwindProtectNode(@4, driver, expr, $4) : expr;
    }
  | TK_BEGIN statement_list TK_ENSURE statement_list TK_END {
        $$ = new UnwindProtectNode(@1, driver, $2, $4);
    }
    ;

  // rescue は 1ヶ以上
rescue_clause_list :
    TK_RESCUE exception_class_opt variable_assignment_opt TK_TERM 
                                                  statement_list      {
        $$ = AST_create_rescue_list($2, $3, $5, @1, driver );
    }
  | rescue_clause_list TK_RESCUE exception_class_opt 
                           variable_assignment_opt TK_TERM statement_list    {
        $$ = AST_chain_rescue_list($1, $3, $4, $6, @2, driver );
    }


exception_class_opt :
    qualified_const
  | %empty  { $$ = nullptr; }


variable_assignment_opt :
    "=>" non_const_var_ref   	{
        $$ = $2;
	}
  | %empty   	{ $$ = nullptr; }


ensure_clause_opt :
    TK_ENSURE statement_list  	{
        $$ = $2;
    }
  | %empty          { $$ = nullptr; }


non_const_var_ref :  // left-hand
    TK_IDENTIFIER  {
        $$ = new VariableRef( @1, driver, LOCAL_VAR, $1);
    }
/* | qualified_const {
        $$ = AST_create_variable_ref( CONST_VAR, $1, @1, driver );
        } */
  | TK_IVAR_IDENT {
        $$ = new VariableRef( @1, driver, INSTANCE_VAR, $1);
    }
  | TK_CLASSVAR_IDENT {
        $$ = new VariableRef( @1, driver, CLASS_VAR, $1);
    }        


////////////////////////////////////////////////////////////////////////
// if式

/** if式. ただし、後置ifがあるので、実引数としては書けないようにする
 * unless 式には elsif 節はない. 何だって! */
if_expression :
    TK_IF operator_expression then_or_term statement_list else_clause_opt 
                                                                    TK_END  {
        $$ = AST_create_if_statement($2, $4, nullptr, $5, @1, driver );
    }
  | TK_IF operator_expression then_or_term statement_list elsif_list 
                                                    else_clause_opt TK_END  {
        $$ = AST_create_if_statement($2, $4, $5, $6, @1, driver );
    }
  | TK_UNLESS operator_expression then_or_term statement_list else_clause_opt 
                                                                    TK_END  {
        $$ = AST_create_if_statement(new UnaryExpr(UNARY_NOT_EXPR, @1, driver, $2),
                                     $4, nullptr, $5, @1, driver );
    }
  ;

then_or_term :
    TK_THEN 
  | TK_TERM
    ;

else_clause_opt :
    TK_ELSE statement_list          {
        $$ = $2;
    }
  | %empty   { $$ = nullptr; }

elsif_list :
    TK_ELSIF operator_expression TK_TERM statement_list          {
        $$ = AST_create_cond_list($2, $4 );
    }
  | elsif_list TK_ELSIF operator_expression TK_TERM statement_list           {
        $$ = AST_chain_cond_list($1, $3, $5 );
    }


////////////////////////////////////////////////////////////////////////
// case式

case_expression :
    TK_CASE expression when_clause_list else_clause_opt TK_END {
        $$ = new CaseExpr(@1, driver, $2, $3, $4);
    }
    // 条件式を書ける
  | TK_CASE            when_cond_clause_list else_clause_opt TK_END {
        $$ = new CaseExpr(@1, driver, nullptr, $2, $3);
    }      

when_clause_list :
    // 値リスト
    TK_WHEN operator_expr_list then_or_term statement_list {
        $$ = AST_create_when_clause($2, $4 );
    }
  | when_clause_list TK_WHEN operator_expr_list then_or_term statement_list {
        $1->push_back(new WhenClause(@2, driver, $3, $5));
        $$ = $1;
    }

when_cond_clause_list :
    // 条件式
    TK_WHEN operator_expression then_or_term statement_list {
        $$ = AST_create_when_clause(AST_create_argument_list(NORMAL_ARG, $2), $4 );
    }
  | when_cond_clause_list TK_WHEN operator_expression then_or_term statement_list {
        $1->push_back(new WhenClause(@2, driver, AST_create_argument_list(NORMAL_ARG, $3), $5));
        $$ = $1;
    }
    

////////////////////////////////////////////////////////////////////////
// その他の文

/** while ... "do" は禁止 [incompat] */
while_statement :
    TK_WHILE operator_expression TK_TERM
                { myparser->push_tag(Parser::BREAK_TAG);
                  myparser->push_tag(Parser::NEXT_TAG);} statement_list TK_END {
        Expression* expr = new BlockNode(@5, driver,
                                myparser->pop_tag(Parser::NEXT_TAG), $5);
        $$ = new WhileStatement(@1, driver, $2,
                                new StatementList {expr},
                                myparser->pop_tag(Parser::BREAK_TAG));
    }


/** 
 * for var in 1..2; ... end. 
 * var は構文上, インスタンス変数も可.
 [incompat] `do` は置けない。`for j in o do ...` がブロック引数になる。
      Ruby: `{ }` と `do` の優先順位が違う.
            method-invocation-without-parentheses 構文規則に ...-with-do-block
 */
for_statement :
    TK_FOR non_const_var_ref TK_IN operator_expression TK_TERM /*for_do*/
                { printf("in for loop\n"); 
                    myparser->push_tag(Parser::BREAK_TAG);
                  myparser->push_tag(Parser::NEXT_TAG);} statement_list TK_END {
        Expression* expr = new BlockNode(@7, driver,
                                myparser->pop_tag(Parser::NEXT_TAG), $7);
        $$ = new ForStatement( @1, driver, $2, $4,
                               new StatementList {expr},
                               myparser->pop_tag(Parser::BREAK_TAG) );
    }

/*for_do :
  TK_TERM | TK_DO ; */

/**
 * return, next, break の実引数は一つだけ.
 * [incompat] 多値は, Ruby も真の多値ではなく, 不採用
 */
operator_expr_opt :
    %empty          { $$ = nullptr; }
  | operator_expression
    ;


/** 後置if があるので、expression ノードは置けない */
return_statement :
    TK_RETURN operator_expr_opt           {
        if (myparser->return_tags.size() < 1) {
            yyerror( &@1, myparser, driver, "`return` not in a method");
            YYERROR;
        }
        $$ = new ReturnStatement( RETURN_STATEMENT, @1, driver, $2,
                                  myparser->return_tags.back());
    }

break_statement :
    TK_BREAK operator_expr_opt         {
        if (myparser->break_tags.size() < 1) {
            yyerror( &@1, myparser, driver, "`break` not in a loop");
            YYERROR;
        }
        $$ = new ReturnStatement( BREAK_STATEMENT, @1, driver, $2,
                                  myparser->break_tags.back());
    }

next_statement :
    TK_NEXT operator_expr_opt          {
        if (myparser->next_tags.size() < 1) {
            yyerror( &@1, myparser, driver, "`next` not in a loop");
            YYERROR;
        }
        $$ = new ReturnStatement( NEXT_STATEMENT, @1, driver, $2,
                                  myparser->next_tags.back());
    }


/*
retry_statement :
    TK_RETRY  {
        $$ = AST_create_return_statement( RETRY_STATEMENT, nullptr, @1 );
    }
*/



%%

// Local variables:
// tab-width: 4
// c-basic-offset: 4
// indent-tabs-mode: nil
// End:
