﻿
// SPDX-License-Identifier: LGPL-3.0-or-later
/*
  minilang -- A small programming language, minilang interpreter.
  Copyright (c) 2011-2013, 2023 Netsphere Laboratories, Hisashi Horikawa.
  All rights reserved.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <assert.h>
#include <vector>
#include "ptr_map.h"
#include <unicode/unistr.h>


enum LexMode
{
    EXPR_HEAD = 1,
    BE_METHOD_NAME,

    // 項の後ろで, メソッド名の直後以外
    AFTER_EXPR,

    EXPR_OR_NEWLINE,

    // メソッド名の直後
    AFTER_METHOD_NAME,

    COMMENT,
    COMMENT_ML,
    STRING_LITERAL_STATE,
    STRING_HEREDOC,
    REGEX_LITERAL_STATE,
};

enum BracketType {
    HASH_OPEN = 1,
    LAMBDA_OPEN,
    EMBD_EXPR_OPEN
};

class State;

class StateMachine
{
public:
    StateMachine();
    ~StateMachine();

    void add_state( const LexMode& mode, State* state );

    void enter_mode( const LexMode& next_mode, const icu::UnicodeString& data = "");
    void push_mode( const LexMode& next_mode, const icu::UnicodeString& data = "");
    void pop_mode();

    void reset( LexMode init );

    // 現在の状態の #process() を呼び出す.
    // @return false  error: illegal character
    bool update();

    // 現在の状態
    LexMode mode;

    /** '{' が埋め込み式か, ハッシュ/ラムダ式か */
    std::vector<BracketType> bracket_stack;

    // lexer からも参照する
    std::vector<std::pair<Token*, icu::UnicodeString> > heredoc_guards;

private:
    ptr_map<LexMode, State*> states;
    // 現在の mode と紐づく Stateインスタンス参照
    State* state;
    std::pair<LexMode, icu::UnicodeString> _next_mode;

    std::vector< std::pair<LexMode, icu::UnicodeString> > mode_stack;
};


class Lexer_impl;

class State
{
protected:
    StateMachine* const fsm; // 所有しない
    Lexer_impl* const lexer; // 所有しない

protected:
    State(StateMachine* sm, Lexer_impl* lexer_): fsm(sm), lexer(lexer_) {
        assert(sm);
        assert(lexer_);
    }

public:
    virtual ~State() { }

    virtual bool process() = 0;
    virtual void on_entry( const icu::UnicodeString& data ) { }
    virtual void on_exit() { }
};
