﻿// -*- coding:utf-8-with-signature; mode:c++; tab-width:4; -*-
// SPDX-License-Identifier: LGPL-3.0-or-later

#include "pch.h"

/*
  minilang -- A small programming language, minilang interpreter.
  Copyright (c) 2011-2013, 2023 Netsphere Laboratories, Hisashi Horikawa.
  All rights reserved.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

// AST を生成する

//#include "CRB.h"
#include "DBG.h"
#include "ast.h"
#include "create_ast.h"
#include "parse.hh"
#include <unicode/unistr.h>
#include <assert.h>
#include <algorithm>
#include <stdexcept>
using namespace std;
using namespace icu;

#define CRB_RANGE_CLASS "::Range"


static void stmtlist_set_parent( StatementList* stmtlist, AST_Node* parent )
{
    assert( parent );
    if (stmtlist) {
        for_each( stmtlist->begin(), stmtlist->end(),
                  [&](AST_Node* e) { e->parent = parent; } );
    }
}

static void arguments_set_parent( ArgumentList* normal,
                                  AssociationList* key_args,
                                  AST_Node* parent )
{
    assert( parent );

    if (normal) {
        for_each( normal->begin(), normal->end(),
                  [&](Argument* e){ e->expr->parent = parent; } );
    }

    if (key_args) {
        for_each( key_args->begin(), key_args->end(),
                  [&](KeyValueNode* kv){
                      if (kv->key)  // double splat の場合は NULL
                          kv->key->parent = parent;
                      kv->value->parent = parent;
                  });
    }
}


/** 仮引数リスト. 必須パラメータ */
ParameterList* AST_param_chain_required( ParameterList* list,
                                         UnicodeString* identifier )
{
    assert( identifier );

    if (list->optional_params.size() > 0)
        throw logic_error("syntax error"); // TODO: ちゃんとする
    list->params.push_back(identifier);
    // ●●TODO: 名前の重複をチェック
    // ●● &rest の後ろの場合もエラー

    return list;
}

ParameterList* AST_param_chain_optional( ParameterList* list,
                                 UnicodeString* identifier, Expression* expr )
{
    assert( identifier );

    OptionalParamNode* param = new OptionalParamNode(identifier, expr);
    list->optional_params.push_back( param );
    // ●●TODO: 名前の重複をチェック
    // ●● &rest の後ろの場合もエラー

    return list;
}


/** 実引数リストノードを生成 */
ArgumentList* AST_create_argument_list( ArgumentType type,
                                        Expression* expression )
{
    ArgumentList* al = new ArgumentList();
    al->push_back( new Argument(type, expression) );
    // expression->parent = al;

    return al;
}


/** 末尾に実引数ノードを追加 */
ArgumentList* AST_chain_argument_list( ArgumentList* list, ArgumentType type,
                                       Expression* expr )
{
    list->push_back( new Argument(type, expr) );
    return list;
}


StatementList* AST_create_statement_list( Expression* statement )
{
    StatementList* sl = new StatementList();
    if (statement)
        sl->push_back(statement);

    return sl;
}


StatementList* AST_chain_statement_list( StatementList* list,
                                         Expression* statement )
{
    assert( list );
    // printf("%s: st = %d\n", __func__, (statement ? statement->type : 0) ); // DEBUG

    if (statement)
        list->push_back(statement);

    return list;
}


AssociationList* AST_create_association_list( Expression* key,
                                              Expression* value )
{
    AssociationList* expr = new AssociationList();
    expr->push_back( new KeyValueNode(key, value) );

    return expr;
}


AssociationList* AST_chain_association_list( AssociationList* list,
                                             Expression* key,
                                             Expression* value )
{
    // ●●TODO: リテラルでのキーの重複はエラーにすること。

    list->push_back( new KeyValueNode(key, value) );
    return list;
}


//////////////////////////////////////////////////////////////////////////
// 関数・メソッド呼出し

FunctionCallNode::FunctionCallNode( AST_NodeType t,
                                    const YYLTYPE& loc, ParsedScript* root,
                                    MethodArgument* args_ ) :
        Expression(t, loc, root), args(args_)
{
    if (args) {
        arguments_set_parent(args->normal, args->key, this);
        if (args->block_arg)
            args->block_arg->parent = this;
    }
}


/** ヘルパー関数. method_nameは定数 */
MethodCallNode* AST_create_method_call1(
                       Expression* receiver, const UnicodeString& method_name,
                       Expression* arg1,
                       const YYLTYPE& loc, ParsedScript* root )
{
    ArgumentList* arguments = new ArgumentList();
    arguments->push_back( new Argument(NORMAL_ARG, arg1) );

    return new MethodCallNode( loc, root, receiver,
                               new UnicodeString(method_name),
                               new MethodArgument(arguments, nullptr, nullptr),
                               nullptr);
}


MethodCallNode* AST_create_method_call0( Expression* receiver,
                                     const char* method_name,
                                     const YYLTYPE& loc, ParsedScript* root )
{
    return new MethodCallNode( loc, root, receiver,
                               new UnicodeString(method_name),
                               nullptr, nullptr );
}

MethodCallNode* AST_create_function_call_expression(
                        icu::UnicodeString* func_name,
                        MethodArgument* arguments, LambdaExprNode* block,
                        const YYLTYPE& loc, ParsedScript* root )
{
    // 実引数内のブロックとブロック実引数の重複チェックは MethodCallNode() 内で
    // おこなう
    if (block != nullptr && arguments == nullptr)
        arguments = new MethodArgument();

    return new MethodCallNode(loc, root,
                new VariableRef(loc, root, LOCAL_VAR, new icu::UnicodeString("self")),
                func_name, arguments, block);
}


///////////////////////////////////////////////////////////////////////////
// リテラル

MethodCallNode* AST_create_range_ctor( Expression* front, Expression* back,
                                   const YYLTYPE& loc, ParsedScript* driver )
{
    VariableRef* receiver = new VariableRef( loc, driver,
                                             VariableType::CONST_VAR,
                                             new UnicodeString(CRB_RANGE_CLASS));

    ArgumentList* args = new ArgumentList();
    args->push_back( new Argument(NORMAL_ARG, front) );
    args->push_back( new Argument(NORMAL_ARG, back) );

    return new MethodCallNode( loc, driver, receiver, new UnicodeString("new"),
                               new MethodArgument(args, nullptr, nullptr),
                               nullptr );
}


ArrayExpr::ArrayExpr( const YYLTYPE& loc, ParsedScript* root,
                      ArgumentList* args ):
                                Expression(ARRAY_EXPR, loc, root), list(args)
{
    if (args)
        arguments_set_parent(args, nullptr, this);
}

HashExpr::HashExpr( const YYLTYPE& loc, ParsedScript* root, AssociationList* args ):
                                Expression(HASH_EXPR, loc, root), list(args)
{
    // NULL のことがある
    if (args)
        arguments_set_parent(nullptr, args, this);
}

LiteralExpression* AST_create_boolean_expression( bool value,
                                       const YYLTYPE& loc, ParsedScript* root )
{
    LiteralExpression* expr = new LiteralExpression( BOOLEAN_LITERAL,
                                                     loc, root );
    expr->u.boolean_value = value;

    return expr;
}


StringExpr* AST_create_string_expr( UnicodeString* str,
                                    const YYLTYPE& loc, ParsedScript* root )
{
    StringExpr* expr = new StringExpr( loc, root );
    expr->literals.push_back(str);

    return expr;
}


StringExpr* AST_chain_string_expr( StringExpr* string_expr,
                                   Expression* embd_expr,
                                   UnicodeString* str )
{
    assert( string_expr );
    assert( embd_expr );

    string_expr->embeds.push_back(embd_expr);
    embd_expr->parent = string_expr;
    string_expr->literals.push_back(str);

    return string_expr;
}


ObjectSlotRef::ObjectSlotRef(const YYLTYPE& loc, ParsedScript* root,
                  Expression* obj,
                  VariableType var_type, icu::UnicodeString* name,
                  MethodArgument* args):
        VariableRef(OBJECT_SLOT_REF, loc, root, var_type, name),
        object(obj), bracket_arguments(args)
{
    assert(object);
    assert( *name != "" );

    object->parent = this;
    if (bracket_arguments) {
        arguments_set_parent(bracket_arguments->normal, bracket_arguments->key,
                             this);
    }
}


//////////////////////////////////////////////////////////////////////////
// 制御構造: 分岐, case-when, loop, 脱出

BlockNode::BlockNode( const YYLTYPE& loc, ParsedScript* root,
                      const icu::UnicodeString& name_, StatementList* block_):
        Expression(BLOCK_NODE, loc, root), name(name_), body(block_) {
    assert( name != "" );
    stmtlist_set_parent(body, this);
}

BlockNode::BlockNode( const YYLTYPE& loc, ParsedScript* root,
                      const icu::UnicodeString& name_, Expression* block_):
        Expression(BLOCK_NODE, loc, root), name(name_) {
    assert( name != "" );
    body = AST_create_statement_list(block_);
    stmtlist_set_parent(body, this);
}


WhileStatement::WhileStatement( const YYLTYPE& loc, ParsedScript* root,
                    Expression* cond, StatementList* blk,
                    const icu::UnicodeString& block_tag_):
                Expression(WHILE_STATEMENT, loc, root),
                condition(cond), block(blk), block_tag(block_tag_) {
    condition->parent = this;
    stmtlist_set_parent(block, this);
}

ForStatement::ForStatement( const YYLTYPE& loc, ParsedScript* root,
                  VariableRef* var_ref_, Expression* range_,
                                StatementList* block_,
                                const UnicodeString& block_tag_ ):
            Expression(FOR_STATEMENT, loc, root),
            var_ref(var_ref_), range(range_), block(block_),
            block_tag(block_tag_) {
    var_ref->parent = this;
    range->parent = this;
    stmtlist_set_parent( block, this );
}

CondBlock::CondBlock(const YYLTYPE& loc, ParsedScript* root, Expression* cond, StatementList* blk):
    AST_Node(COND_BLOCK_NODE, loc, root), condition(cond), block(blk)
{
    condition->parent = this;
    stmtlist_set_parent(block, this);
}

IfStatement* AST_create_if_statement( Expression* condition,
                                      StatementList* then_block,
                                      CondList* elsif_list,
                                      StatementList* else_block,
                                      const YYLTYPE& loc, ParsedScript* driver )
{
    assert( condition );
    assert( then_block );

    if ( !elsif_list )
        elsif_list = new CondList();

    // if-then 節と elsif 節を区別しない
    CondBlock* bl = new CondBlock(loc, driver, condition, then_block);
    elsif_list->push_front(bl);

    IfStatement* st = new IfStatement( loc, driver );

    CondList::iterator i;
    for ( i = elsif_list->begin(); i != elsif_list->end(); i++ )
        (*i)->parent = st;

    st->cond_list = elsif_list;

    st->else_block = else_block;
    stmtlist_set_parent( else_block, st );

    return st;
}


CondList* AST_chain_cond_list( CondList* list, Expression* add_expr,
                               StatementList* block )
{
    assert( list );
    assert(add_expr);

    CondBlock* bl = new CondBlock(add_expr->m_loc, add_expr->m_root,
                                  add_expr, block);
    list->push_back(bl);
    return list;
}


CondList* AST_create_cond_list( Expression* expr, StatementList* block )
{
    CondList* ei = new CondList();
    ei->push_back(new CondBlock(expr->m_loc, expr->m_root, expr, block));
    return ei;
}

WhenClause::WhenClause(const YYLTYPE& loc, ParsedScript* root,
                       ArgumentList* pat, StatementList* b) :
    AST_Node(WHEN_CLAUSE_NODE, loc, root), patterns(pat), block(b)
{
    assert(patterns);

    for_each(patterns->begin(), patterns->end(),
             [&](Argument* a) { a->expr->parent = this; });
    stmtlist_set_parent(block, this);
}

// 値版と条件式版がある
// @param cond 条件式版は NULL
CaseExpr::CaseExpr( const YYLTYPE& loc, ParsedScript* root,
            Expression* cond, WhenClauseList* when_list_, StatementList* elseb):
        Expression(CASE_EXPR, loc, root),
        condition(cond), when_list(when_list_), else_block(elseb)
{
    assert( when_list );

    if (cond)
        cond->parent = this;

    WhenClauseList::iterator i;
    for ( i = when_list->begin(); i != when_list->end(); ++i )
        (*i)->parent = this;

    stmtlist_set_parent( else_block, this );
}


WhenClauseList* AST_create_when_clause( ArgumentList* condition,
                                        StatementList* block )
{
    assert(condition);

    WhenClauseList* list = new WhenClauseList();

    Argument* arg = *condition->begin();
    WhenClause* wc = new WhenClause(arg->expr->m_loc, arg->expr->m_root, condition, block);
    list->push_back(wc);

    return list;
}


////////////////////////////////////////////////////////////////////////
// 例外処理

RescueBlock::RescueBlock( const YYLTYPE& loc, ParsedScript* root,
                          icu::UnicodeString* klass, VariableRef* var,
                          StatementList* blk):
        AST_Node(RESCUE_BLOCK_NODE, loc, root),
        class_name(klass), var_ref(var), block(blk)
{
    if (var_ref)
        var_ref->parent = this;
    stmtlist_set_parent( block, this );
}

UnwindProtectNode::UnwindProtectNode( const YYLTYPE& loc, ParsedScript* root,
                                      Expression* block_,
                                      StatementList* cleanup_block_ ):
        Expression(UNWIND_PROTECT_NODE, loc, root),
        cleanup_block(cleanup_block_)
{
    assert(cleanup_block);

    if (!block_)
        block = nullptr;
    else {
        block = AST_create_statement_list(block_);
        stmtlist_set_parent(block, this);
    }

    stmtlist_set_parent(cleanup_block, this);
}

UnwindProtectNode::UnwindProtectNode( const YYLTYPE& loc, ParsedScript* root,
                                      StatementList* block_,
                                      StatementList* cleanup_block_ ):
        Expression(UNWIND_PROTECT_NODE, loc, root),
        block(block_), cleanup_block(cleanup_block_)
{
    assert(cleanup_block);

    stmtlist_set_parent(block, this);
    stmtlist_set_parent(cleanup_block, this);
}


// ruby: rescue または ensure のいずれかを書かなければならない。
// `ensure` のみの場合は UnwindProtectNode のみで囲む.
// @param rescues  必須.
BeginRescueStmt::BeginRescueStmt( const YYLTYPE& loc, ParsedScript* root,
                     StatementList* tryb, RescueList* rescues):
    Expression(BEGIN_RESCUE_STMT, loc, root),
    try_block(tryb), rescue_list(rescues)
{
    assert(rescue_list);

    // try ブロック
    stmtlist_set_parent( try_block, this );

    if (rescue_list) {
        RescueList::iterator i;
        for ( i = rescue_list->begin(); i != rescue_list->end(); ++i )
            (*i)->parent = this;
    }
}


/**
 * rescue節
 * @param var_ref  構文上, インスタンス変数や定数のこともある
 */
RescueList* AST_create_rescue_list( UnicodeString* class_name,
                                    VariableRef* var_ref,
                                    StatementList* block,
                                    const YYLTYPE& loc, ParsedScript* driver )
{
    assert( block );

    RescueBlock* b = new RescueBlock( loc, driver, class_name, var_ref, block);
    RescueList* rl = new RescueList();
    rl->push_back(b);
    return rl;
}


RescueList* AST_chain_rescue_list( RescueList* list,
                                   UnicodeString* class_name,
                                   VariableRef* var_ref,
                                   StatementList* block,
                                   const YYLTYPE& loc, ParsedScript* driver )
{
    assert( block );

    RescueBlock* b = new RescueBlock( loc, driver, class_name, var_ref, block);
    list->push_back(b);
    return list;
}


//////////////////////////////////////////////////////////////////////////
// メソッド・クラス定義

// こちらは, `next` 用の暗黙のブロックを作らない.
LambdaExprNode::LambdaExprNode(AST_NodeType type, const YYLTYPE& loc,
                        ParsedScript* root,
                        ParameterList* params, StatementList* blk):
        Expression(type, loc, root), param_list(params), block(blk)
{
    if (param_list ) {
        for_each( param_list->optional_params.begin(),
                  param_list->optional_params.end(),
                  [&](OptionalParamNode* e) {
                      if (e->default_value)
                          e->default_value->parent = this;
                  });
    }
    stmtlist_set_parent(block, this);
}


MethodDefinition::MethodDefinition( const YYLTYPE& yyl, ParsedScript* root,
                                    VariableRef* specializer_,   // opt
                                    icu::UnicodeString* method_name_,
                                    ParameterList* param_list_,  // opt
                                    StatementList* block_,
                                    const UnicodeString& block_tag_ ):
        LambdaExprNode( METHOD_DEFINITION, yyl, root, param_list_,
                        new StatementList {
                            new BlockNode(yyl, root, block_tag_, block_) }),
        specializer(specializer_), method_name(method_name_)
        //return_tag(block_tag_)
{
    assert(*method_name != "");
    if (specializer)
        specializer->parent = this;
}


/**
 * ラムダ式.
 * `next` 用の暗黙のブロックで囲む
 */
LambdaExprNode* AST_create_lambda_expr( ParameterList* param_list,
                                StatementList* block,
                                const UnicodeString& block_tag_,
                                const YYLTYPE& loc, ParsedScript* driver )
{
    // `next` をブロックする
    BlockNode* block_node = new BlockNode(loc, driver, block_tag_, block);
    StatementList* wrap = AST_create_statement_list(block_node);
    LambdaExprNode* expr = new LambdaExprNode( LAMBDA_EXPR, loc, driver,
                                               param_list, wrap);
    //stmtlist_set_parent( wrap, expr );
    //expr->block_tag = block_tag_;

    return expr;
}

ModuleDefinitionNode::ModuleDefinitionNode( AST_NodeType t,
                          const YYLTYPE& loc, ParsedScript* root,
                          icu::UnicodeString* mod_name, ModuleStmtList* blk ):
                Expression( t, loc, root ), name(mod_name),
                block(blk->block), include_list(blk->include_list), extend_list(blk->extend_list), slot_specs(blk->slot_specs) {
    assert( *mod_name != "" );
    assert(block);
    stmtlist_set_parent( block, this );

    delete blk;
}

/*
AttrAccessorStmt::AttrAccessorStmt( AST_NodeType t,
                      const YYLTYPE& loc, ParsedScript* root,
                      ArgumentList* args ):
                Expression(t, loc, root), arguments(args)
{
    assert(arguments);
    arguments_set_parent(args, nullptr, this);
}
*/

// symbol_list は解放する
SlotSpecList* AST_slot_spec_list_from_sym_list(StringList* sym_list,
                                               SlotSpec::SlotType t)
{
    SlotSpecList* ret = new SlotSpecList();
    while (sym_list->size() > 0) {
        auto s = new SlotSpec {.type = t, .name = sym_list->detach(sym_list->begin()) };
        ret->push_back(s);
    }

    delete sym_list;
    return ret;
}



// Local variables:
// tab-width: 4
// c-basic-offset: 4
// indent-tabs-mode: nil
// End:
