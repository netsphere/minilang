﻿// -*- coding:utf-8-with-signature; mode:c++; tab-width:4; -*-
// SPDX-License-Identifier: LGPL-3.0-or-later
/*
  minilang -- A small programming language, minilang interpreter.
  Copyright (c) 2011-2013, 2023 Netsphere Laboratories, Hisashi Horikawa.
  All rights reserved.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef CREATE_AST_H__
#define CREATE_AST_H__ 1

#include "ast.h"
#include <unicode/unistr.h>


// 仮引数リスト
ParameterList* AST_param_chain_required( ParameterList* list,
                                         icu::UnicodeString* identifier );

ParameterList* AST_param_chain_optional( ParameterList* list,
                                         icu::UnicodeString* identifier, Expression* expr );


//////////////////////////////////////////////////////////////////////////
// 関数・メソッド呼出し

// 実引数
ArgumentList* AST_create_argument_list( ArgumentType type,
                                        Expression* expression );

ArgumentList* AST_chain_argument_list( ArgumentList* list, ArgumentType type,
                                       Expression* expr );

StatementList* AST_create_statement_list( Expression* statement );

StatementList* AST_chain_statement_list(StatementList* list,
                                        Expression* statement);


AssociationList* AST_create_association_list( Expression* key,
					      Expression* value );

AssociationList* AST_chain_association_list( AssociationList* list,
					     Expression* key,
					     Expression* value );


///////////////////////////////////////////////////////////////////////////
// リテラル

MethodCallNode* AST_create_range_ctor( Expression* front, Expression* back,
                                   const YYLTYPE& loc, ParsedScript* driver );

LiteralExpression* AST_create_boolean_expression( bool value,
                                     const YYLTYPE& loc, ParsedScript* root );

StringExpr* AST_create_string_expr( icu::UnicodeString* str,
                                    const YYLTYPE& loc, ParsedScript* root );

StringExpr* AST_chain_string_expr( StringExpr* string_expr,
                                   Expression* embd_expr,
                                   icu::UnicodeString* str );


MethodCallNode* AST_create_method_call1( Expression* receiver,
                                     const icu::UnicodeString& method_name,
                                     Expression* arg1,
                                     const YYLTYPE& loc, ParsedScript* root );

/** ヘルパー関数. method_nameは定数 */
MethodCallNode* AST_create_method_call0( Expression* receiver,
                                     const char* method_name,
                                     const YYLTYPE& loc, ParsedScript* root );


/** 関数呼び出し => selfをレシーバとするメソッド呼び出し
 * @param block ブロック引数. 実引数 `&block` と両方指定されているときはエラー
 */
MethodCallNode* AST_create_function_call_expression( icu::UnicodeString* func_name,
                                                 MethodArgument* arguments,
                                                 LambdaExprNode* block,
                                                 const YYLTYPE& loc,
                                                 ParsedScript* root );


//////////////////////////////////////////////////////////////////////////
// 制御構造: 分岐, case-when, loop, 脱出

IfStatement* AST_create_if_statement( Expression* condition,
                                   StatementList* then_block,
                                   CondList* elsif_list,
                                   StatementList* else_block,
                                   const YYLTYPE& loc, ParsedScript* driver );

CondList* AST_chain_cond_list( CondList* list,
                               Expression* addr_expr,
                               StatementList* block );

CondList* AST_create_cond_list( Expression* expr, StatementList* block );

WhenClauseList* AST_create_when_clause( ArgumentList* condition,
                                        StatementList* block );


////////////////////////////////////////////////////////////////////////
// 例外処理

RescueList* AST_create_rescue_list( icu::UnicodeString* class_name,
                                    VariableRef* var_name,
                                    StatementList* block,
                                    const YYLTYPE& loc, ParsedScript* driver );

RescueList* AST_chain_rescue_list( RescueList* list,
                                   icu::UnicodeString* class_name,
                                   VariableRef* var_ref,
                                   StatementList* block,
                                   const YYLTYPE& loc, ParsedScript* driver );


//////////////////////////////////////////////////////////////////////////
// メソッド・クラス定義

// symbol_list は解放する
SlotSpecList* AST_slot_spec_list_from_sym_list(StringList* Sym_list,
                                               SlotSpec::SlotType t);

LambdaExprNode* AST_create_lambda_expr( ParameterList* param_list,
                                StatementList* block,
                                const icu::UnicodeString& block_tag,
                                const YYLTYPE& loc, ParsedScript* driver );



#endif


// Local variables:
// tab-width: 4
// c-basic-offset: 4
// indent-tabs-mode: nil
// End:
