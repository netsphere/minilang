﻿// -*- mode:c++; -*-

#include "pch.h"

/*
  minilang -- a minilang interpreter.

  Copyright (c) 2011-2013 HORIKAWA Hisashi. All rights reserved.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// 手書きのlexer.
// ICU を用いて, Unicode でマッチさせる.

#ifndef _WIN32
  #define _XOPEN_SOURCE 700
  #define _POSIX_C_SOURCE 200809L
  #undef _REENTRANT
  #undef _GNU_SOURCE
#endif // !_WIN32
#define _FILE_OFFSET_BITS 64 // 内部で __USE_FILE_OFFSET64 をセットさせる
// _LARGEFILE64_SOURCE は古いので, 定義しない
//#include "support.h"
#include "lexer.h"
#ifndef _WIN32
  #include <sys/types.h>
  #include <sys/stat.h>
  #include <sys/mman.h>
  #include <fcntl.h>
  #include <unistd.h>
#endif // _WIN32
  //#include <unicode/regex.h>
#define PCRE2_CODE_UNIT_WIDTH 16  // UTF-16
#include <pcre2.h>
#include <unicode/errorcode.h>
#include <unicode/ustdio.h>
#include <assert.h>
#include <string>
#include "ptr_list.h"
#include "ptr_map.h"
#include "ast.h"    // YYSTYPEが必要とする
#include "parse.hh"
#include "parser.h"
#include <string.h>
#include <vector>
#include "state.h"
using namespace std;
using namespace icu;


/////////////////////////////////////////////////////////////////////////
// class Lexer_impl

class Lexer_impl: public Lexer
{
public:
    Lexer_impl( Parser* parser );
    virtual ~Lexer_impl();

    virtual bool set_input_file( const UnicodeString& filename,
                                 const char* encoding ) override;

    virtual bool set_input_file( FILE* fp, const char* encoding_ ) override;

    virtual bool set_input_string( const char* string, const char* encoding ) override;

    virtual void receive( Token** token ) override;

    virtual bool iseof() const override;

    //void forward_to( int new_pos );
    void forward( int step );

    UnicodeString source;

    // ファイル先頭からの位置
    int pos;

    // 行頭からの位置
    int col;

    bool match( const pcre2_code* matcher); //, int advance = 0 );
    bool match( const UnicodeString& text );

    UnicodeString matched_substring(int index = 0) const;
    int matched_length() const;

    // queue に追加
    void send( int token );
    void send( int token, const UnicodeString& str );
    int queue_size() const {
        return queue.size();
    }

private:
    // string filename;
    string encoding;
    int lineno;

    StateMachine stateMachine;

    ptr_list<Token*> queue;

    pcre2_match_data* match_data;
    bool _match_dirty;  // match() に成功したら forward() すること.

    void reset_stream();
};


bool Lexer_impl::set_input_file( const UnicodeString& filename,
                                 const char* encoding_ )
{
    assert( encoding_ );
    encoding = encoding_;

#ifdef _WIN32
    .....;
#else
    string r;
    filename.toUTF8String(r);
    int fd = open( r.c_str(), O_RDONLY );
#endif // _WIN32
    if (fd < 0) {
        char buf[100];
        int err = errno;
        int rr = strerror_r(err, buf, sizeof buf);
        assert( rr == 0 );
        fprintf(stderr, "%s: open() failed '%s': %s\n",
                __func__, r.c_str(), buf);
        return false;
    }

    struct stat sb;
    if (fstat(fd, &sb) < 0) {
        perror("fstat() failed.");
        close(fd);
        return false;
    }

    void* map = mmap( nullptr, sb.st_size, PROT_READ, MAP_SHARED, fd, 0);
    if (map == MAP_FAILED) {
        perror("mmap() failed.");
        close(fd);
        return false;
    }

    source = UnicodeString( static_cast<char*>(map), sb.st_size, encoding_ );
    munmap(map, sb.st_size );

    reset_stream();
    return true;
}


/** close は呼び出し側で行うこと. */
bool Lexer_impl::set_input_file( FILE* fp, const char* encoding_ )
{
    assert( fp );
    assert( encoding_ );

    encoding = encoding_;

    // stdio などもあり得る. EOFまで読む.
    int bufsiz = 1000;
    char* buf = (char*) malloc( bufsiz );
    assert( buf );

    char* p;
    size_t r;
    for ( p = buf; ; p += 1000 ) {
        r = fread( p, 1, 1000, fp );
        if ( r < 1000 )
            break;

        bufsiz *= 2;
        buf = (char*) realloc( buf, bufsiz );
        assert( buf );
    }

    if ( ferror(fp) ) {
        perror("file read error.");
        free(buf);
        return false;
    }

    source = UnicodeString( buf, p + r - buf, encoding_ );

    reset_stream();
    return true;
}


bool Lexer_impl::set_input_string( const char* string, const char* encoding_ )
{
    assert( string );
    assert( encoding_ );

    encoding = encoding_;
    source = UnicodeString( string, encoding_ );

    reset_stream();
    return true;
}


void Lexer_impl::forward( int step )
{
    assert( step >= 0 );
    int new_pos = pos + step ;

    while ( pos < new_pos ) {
        if ( source[pos] == '\r' && source[pos + 1] == '\n' ) {
            lineno++;
            col = 0;
            pos++; // 2文字
        }
        else if ( source[pos] == '\r' || source[pos] == '\n' ||
                  source[pos] == 0x2028 || source[pos] == 0x2029 ) {
            lineno++;
            col = 0;
        }
        else
            col++;

        pos++;
    }

    if (match_data) {
        pcre2_match_data_free(match_data); match_data = NULL;
    }
    _match_dirty = false;
}


// 一時的な参照を返す。
UnicodeString Lexer_impl::matched_substring(int idx) const
{
    assert( match_data );
    PCRE2_SIZE* ovec = pcre2_get_ovector_pointer(match_data);
    return source.tempSubString(ovec[idx * 2], ovec[idx * 2 + 1] - ovec[idx * 2]);
}

int Lexer_impl::matched_length() const
{
    assert( match_data );
    PCRE2_SIZE* ovec = pcre2_get_ovector_pointer(match_data);
    return ovec[1] - ovec[0];
}


/**
 * @return マッチしたとき true
 */
bool Lexer_impl::match( const pcre2_code* matcher )//, int advance )
{
    assert( matcher );
    assert( !_match_dirty );

    if ( !match_data )
        match_data = pcre2_match_data_create(128, NULL);

    int rc = pcre2_match(matcher,
                         (PCRE2_SPTR16) source.getBuffer(), source.length(),
                         pos /*+ advance*/, PCRE2_ANCHORED,
                         match_data, NULL);
    if (rc < 0) {
        switch (rc) {
        case PCRE2_ERROR_NOMATCH:
            return false;
        default:
            fprintf(stderr, "internal error: Matching error %d\n", rc);
            abort();
        }
    }

    _match_dirty = true;
    return true;
}


bool Lexer_impl::match( const UnicodeString& text )
{
    assert( !_match_dirty );
    bool ret = source.compare(pos, text.length(), text) == 0;
    _match_dirty = ret;
    return ret;
}


bool Lexer_impl::iseof() const
{
    return pos >= source.length();
}

void Lexer_impl::send( int token )
{
    Token* t = new Token(token);
    t->first_line = lineno; t->first_column = col;
    queue.push_back(t);
}


void Lexer_impl::send( int token, const UnicodeString& str )
{
    Token* t = new Token(token, str);
    t->first_line = lineno; t->first_column = col;
    queue.push_back(t);

    if (token == TK_HEREDOC)
        stateMachine.heredoc_guards.push_back(make_pair(t, str));
}


void Lexer_impl::receive( Token** token )
{
    while ( queue.size() == 0 || stateMachine.heredoc_guards.size() > 0 ) {
        if ( iseof() ) {
            //queue.push_back(new Token(YYEOF));
            send(YYEOF);
            //*token = new Token(YYEOF);
            break;
        }

        if ( !stateMachine.update() ) {
            parser->error_handler( lineno, col, "illegal character: U+%x",
                                   source[pos] );
            forward(1);
            continue;
        }
    }

    *token = queue.front();
    queue.detach_front();
}


/////////////////////////////////////////////////////////////////////////
// class StateMachine

StateMachine::StateMachine(): mode(EXPR_HEAD), state(nullptr)
{
}

StateMachine::~StateMachine()
{
}

void StateMachine::reset( LexMode init )
{
    mode = init;
    ptr_map<LexMode, State*>::const_iterator it = states.find(mode);
    assert( it != states.end() );
    state = it->second;
    _next_mode = make_pair(init, "");

    bracket_stack.clear();
    heredoc_guards.clear();
    mode_stack.clear();
}


// State から呼び出される
void StateMachine::enter_mode( const LexMode& next_mode,
                             const UnicodeString& data )
{
    if (mode != next_mode)
        state->on_exit();   // send() の順番があるので, exit だけおこなう

    // ここでは何もしない。最後に呼び出したものをイキにする。
    _next_mode = make_pair(next_mode, data);
}


void StateMachine::push_mode( const LexMode& next_mode, const UnicodeString& data)
{
    mode_stack.push_back( pair<LexMode, UnicodeString>(mode, data) );
    enter_mode(next_mode, data);
}


void StateMachine::pop_mode()
{
    pair<LexMode, UnicodeString> orig_mode = mode_stack.back();
    mode_stack.pop_back();
    enter_mode( orig_mode.first, orig_mode.second );
}


// @param state 所有する.
void StateMachine::add_state( const LexMode& mode, State* state )
{
    states.insert(make_pair(mode, state) );
}


// @return false  error: illegal character
bool StateMachine::update()
{
    assert(state);

    bool ret = state->process();

    LexMode next_mode = _next_mode.first;
    UnicodeString data = _next_mode.second;
    if (mode != next_mode) {
        ptr_map<LexMode, State*>::const_iterator it = states.find(next_mode);
        assert( it != states.end() );
        printf("mode changed: %d to %d\n", mode, next_mode); // DEBUG
        mode = next_mode; state = it->second;
        _next_mode = make_pair(mode, "");

        state->on_entry( data );
    }

    return ret;
}


/////////////////////////////////////////////////////////////////////////
// class BaseState

class BaseState: public State
{
public:
    BaseState(StateMachine* sm, Lexer_impl* l): State(sm, l) { }
    virtual bool process() override;
    bool check_const();

protected:
    bool check_ident( int kind );
};


class ExprHead: public BaseState
{
public:
    ExprHead(StateMachine* sm, Lexer_impl* l): BaseState(sm, l) { }
    virtual bool process() override;
};


class AfterExpr: public BaseState
{
public:
    AfterExpr(StateMachine* sm, Lexer_impl* l): BaseState(sm, l) { }
    virtual bool process() override;
};


class BeMethodName: public BaseState
{
public:
    BeMethodName(StateMachine* sm, Lexer_impl* l): BaseState(sm, l) { }
    virtual bool process() override;
};


class ExprOrNewline: public BaseState
{
public:
    ExprOrNewline(StateMachine* sm, Lexer_impl* l): BaseState(sm, l) { }
    virtual bool process() override;
};


/** primary '.' method_name の直後 */
class AfterMethodName: public BaseState
{
public:
    AfterMethodName(StateMachine* sm, Lexer_impl* l): BaseState(sm, l) { }
    virtual bool process() override;
};


class Comment: public State
{
public:
    Comment(StateMachine* sm, Lexer_impl* l): State(sm, l) { }
    virtual bool process() override;
};

class CommentML: public State
{
public:
    CommentML(StateMachine* sm, Lexer_impl* l): State(sm, l) { }
    virtual bool process() override;
};


class StringLiteralState: public State
{
public:
    StringLiteralState(StateMachine* sm, Lexer_impl* l): State(sm, l) { }
    virtual bool process() override;
    virtual void on_entry( const UnicodeString& data ) override;
    virtual void on_exit() override;

protected:
    UnicodeString delim;
    UnicodeString accumulator;
};


class StringHeredocState: public StringLiteralState
{
public:
    StringHeredocState(StateMachine* sm, Lexer_impl* l):
                                StringLiteralState(sm, l) { }
    virtual bool process() override;
    //virtual void on_entry( const UnicodeString& data ) override;
    virtual void on_exit() override;
};


class RegexLiteralState: public State
{
public:
    RegexLiteralState(StateMachine* sm, Lexer_impl* l): State(sm, l) { }
    virtual bool process() override;
    virtual void on_entry( const UnicodeString& data ) override;
    virtual void on_exit() override;

private:
    UnicodeString delim;
    UnicodeString accumulator;
};


// \u{...}はサポートしていない。=> \x{...}
// \p{L|Nd} も不可. => [\p{L}\p{Nd}]
static const UnicodeString CapitalLetter = "[\\p{Lu}\\p{Lt}]";
static const UnicodeString NonCapitalLetter = "[\\p{Ll}\\p{Lm}\\p{Lo}_\\x{3400}-\\x{4db5}\\x{4e00}-\\x{9fcc}]";
static const UnicodeString UnicodeLetter = "(" + CapitalLetter + "|" + NonCapitalLetter + ")";
static const UnicodeString IdentifierPart = UnicodeString("(") + UnicodeLetter + "|[\\p{Mn}\\p{Mc}\\p{Nd}\\p{Nl}])*";

// icuでは matcher と sourceを結びつけるので, スレッドごとにmatcherインスタ
// ンスを作らなければならない. => static にできない.
static pcre2_code* ident_re = NULL;

// `::` のない const.
static pcre2_code* const_re;
// `::` のある const.
static pcre2_code* qualified_const_re;
static pcre2_code* ivar_ident_re;      // @ivar
static pcre2_code* classvar_ident_re;  // @@foo
static pcre2_code* symbol_re;
static pcre2_code* key_re;
static pcre2_code* op_method_name_re;

static pcre2_code* newline_re;
static pcre2_code* whitespace_re;
static pcre2_code* begin_ml_comment_re;
static pcre2_code* ws_or_nl_re;

// リテラル
static pcre2_code* float_re;
static pcre2_code* int_re;
static pcre2_code* start_string_re;
static pcre2_code* heredoc_re;

static pcre2_code* nl_and_dot_re;
static pcre2_code* lambda_arrow_re;


// 先頭マッチを強制
static pcre2_code* re_compile(const UnicodeString& re_pattern)
{
    int err_num;
    PCRE2_SIZE err_offset;
    pcre2_code* re = pcre2_compile((PCRE2_SPTR16) re_pattern.getBuffer(),
                                   re_pattern.length(),
                                PCRE2_ANCHORED | PCRE2_UTF | PCRE2_NO_UTF_CHECK | PCRE2_MULTILINE,
                                &err_num,
                                &err_offset,
                                NULL);
    assert( re != NULL);
    return re;
}


static void setup_matchers()
{
    // シンボル ////////////////////////////////////////////////////////

    // Ruby: メソッド名は大文字始まりも可。
    // Crystal: 大文字始まりは構文エラー. こちらに合わせる. [incompat]
    static const UnicodeString ident_re_str = NonCapitalLetter + IdentifierPart + "[!?]?";
    ident_re = re_compile(ident_re_str);

    // ::のないconst
    static const UnicodeString ConstIdent_str = CapitalLetter + IdentifierPart;
    const_re = re_compile( ConstIdent_str );

    // ::のないconstは含まない.
    static const UnicodeString qualified_const_re_str =
                UnicodeString("::") + ConstIdent_str + "(::" + ConstIdent_str + ")*|" +
                ConstIdent_str + "(::" + ConstIdent_str + ")+" ;
    qualified_const_re = re_compile(qualified_const_re_str);

    static const UnicodeString ivar_ident_re_str =
                UnicodeString("\\@") + UnicodeLetter + IdentifierPart ;
    ivar_ident_re = re_compile(ivar_ident_re_str);

    static const UnicodeString classvar_ident_re_str =
                UnicodeString("\\@\\@") + UnicodeLetter + IdentifierPart ;
    classvar_ident_re = re_compile(classvar_ident_re_str);

    // ユーザ定義可能な演算子    [incompat] missing "`" "+@"
    static const UnicodeString op_method_name_re_str =
                "^|&|\\||<<|>>|<=>|===|==|!=|=~|>|>=|<|<=|\\+|-|\\*|\\/|%|\\*\\*|~|-@|\\[\\]|\\[\\]=";
    op_method_name_re = re_compile( op_method_name_re_str );

    symbol_re = re_compile(
                UnicodeString("\\:(") + ident_re_str + ")|" +
                "\\:(" + qualified_const_re_str + ")|" +
                "\\:(" + ConstIdent_str + ")|" +
                "\\:(" + classvar_ident_re_str + ")|" +
                "\\:(" + ivar_ident_re_str + ")|" +
                "\\:(" + op_method_name_re_str + ")" );

    key_re = re_compile(ident_re_str + "\\:|" + ConstIdent_str + "\\:");

    // 空白・コメント ///////////////////////////////////////////////////////

    // U+2028 <LS>  U+2029 <PS>
    static const UnicodeString newline_re_str = "(\\r\\n|[\\r\\n\\x{2028}\\x{2029}])";
    newline_re = re_compile( newline_re_str );

    // JavaScript はいくつか追加している:  \f 改ページ  \v 垂直タブ   u+FEFF
    // PCRE2  \v (0x0b) が改行文字にもマッチしてしまう!
    //     Unicode では U+000C <FF> と U+000B <VT> は mandatory break property
    //     だが, \v を改行全般にマッチさせるのはやりすぎで, bug では?
    // Zs: 0020  00A0  1680  2000..200A  202F  205F  3000
    static const UnicodeString whitespace_re_str =
                        "[\\t\\x{FEFF}\\x{000b}\\x{000c}\\p{Zs}]";
    whitespace_re = re_compile( whitespace_re_str + "+");

    begin_ml_comment_re = re_compile( "=begin([ \t]+.*)?\n" );

    static const UnicodeString ws_or_nl_re_str =
        UnicodeString("(") + whitespace_re_str + "+|" + newline_re_str + ")";
    ws_or_nl_re = re_compile( ws_or_nl_re_str );

    // リテラル  ///////////////////////////////////////////////////////

    // 小数点以下は省略不可. 1.to_s => "1" "." "to_s"
    float_re = re_compile( "[0-9]*\\.[0-9]+([eE][+\\-]?[0-9]+)?" );

    // "_"を挟んでよい. ただし "1_" のように "_" で終わるのは不可.
    // "012" => 10  "0077" => 63    8進数!
    // "0x12" => 18  16進数!
    int_re = re_compile( "[1-9][0-9]*(_[0-9]+)*|0+" );

    // ドキュメントに記載がないが "%Q|" もOK.
    start_string_re = re_compile( UnicodeString("\\%Q?[([{<|]") );

    heredoc_re = re_compile( UnicodeString("<<-?(") +
                             UnicodeLetter + IdentifierPart + ")" );
    // Ruby: 改行は高々一つのみ。
    nl_and_dot_re = re_compile( newline_re_str + whitespace_re_str + "*\\." );
    lambda_arrow_re = re_compile("->[ \t]*");
}


Lexer_impl::Lexer_impl( Parser* parser ):
    Lexer(parser), match_data(NULL), _match_dirty(false)
{
    ErrorCode status;

    if ( !ident_re )
        setup_matchers();

    stateMachine.add_state(EXPR_HEAD, new ExprHead(&stateMachine, this));
    stateMachine.add_state(BE_METHOD_NAME, new BeMethodName(&stateMachine, this));
    stateMachine.add_state(AFTER_EXPR, new AfterExpr(&stateMachine, this));
    stateMachine.add_state(EXPR_OR_NEWLINE,
                           new ExprOrNewline(&stateMachine, this));
    stateMachine.add_state(AFTER_METHOD_NAME,
                           new AfterMethodName(&stateMachine, this));
    stateMachine.add_state(COMMENT, new Comment(&stateMachine, this));
    stateMachine.add_state(COMMENT_ML, new CommentML(&stateMachine, this));
    stateMachine.add_state(STRING_LITERAL_STATE,
                           new StringLiteralState(&stateMachine, this));
    stateMachine.add_state(STRING_HEREDOC,
                           new StringHeredocState(&stateMachine, this));
    stateMachine.add_state(REGEX_LITERAL_STATE,
                           new RegexLiteralState(&stateMachine, this));
}


Lexer_impl::~Lexer_impl()
{
    if (match_data) {
        pcre2_match_data_free(match_data); match_data = NULL;
        _match_dirty = false;
    }
}


void Lexer_impl::reset_stream()
{
    pos = 0;
    lineno = 1;
    col = 0;
    stateMachine.reset( EXPR_HEAD );
}


/** 未実装のキーワード [incompat] */
static const UnicodeString reserved[] = {
    UnicodeString("__LINE__"),
    UnicodeString("__ENCODING__"),
    UnicodeString("__FILE__"),
    "__dir__",
    UnicodeString("and"), // Crystal もない. 禁止 => && [incompat]
    "defined?",  // Crystal にもない.
    UnicodeString("not"), // Crystal もない. 禁止 => !  [incompat]
    UnicodeString("or"),  // Crystal もない. 禁止 => || [incompat]
    "redo",  // Crystal にもない.
             // Ruby: 今いる関数/クロージャの実行部の最初に跳ぶだけ (仮引数は再
             //       評価されない. 本当に不要 [incompat]
    "retry", // Ruby: 例外が発生した場所の begin節を再実行する.
             //         -> Smalltalk の `#retry` メソッドと同じ.
             //       CommonLisp や R とセマンティクスが異なる。
             //         -> これは Smalltalk の `#resume` メソッドと同じ.
             // Crystal: ない.
             // 継続がないと実装できない.
    UnicodeString("undef"),  // Crystal もない. 禁止
    UnicodeString(""),
};
// Crystal のみのキーワード
//   -> 確かに, is_a?(), nil?(), respond_to?() 単数形は, あったほうがよい.
// See https://crystal-lang.org/reference/1.8/crystal_for_rubyists/
//     abstract  as   as?  asm   enum  fun  instance_sizeof   is_a?
//     lib  macro   nil?  of  out  pointerof  responds_to?   select
//     sizeof   struct   type  typeof uninitialized  union  verbatim  with


struct IdentAndNextMode {
    UnicodeString const str;
    int token1;
    int token2;
    LexMode next_mode;

    IdentAndNextMode( const char* str_, int t1, int t2, const LexMode& m ):
        str(str_), token1(t1), token2(t2), next_mode(m) { }
};


static const IdentAndNextMode ident_and_next_mode[] = {
    IdentAndNextMode("alias", TK_ALIAS, TK_ALIAS, EXPR_HEAD),
    // Crystal: macro `property`
    // Ruby: method Module#attr_accessor
    IdentAndNextMode("attr_accessor", TK_ATTR_ACCESSOR, TK_ATTR_ACCESSOR, EXPR_HEAD),
    // Crystal: macro `getter`
    IdentAndNextMode("attr_reader", TK_ATTR_READER, TK_ATTR_READER, EXPR_HEAD),
    // Crystal: macro `setter`
    IdentAndNextMode("attr_writer", TK_ATTR_WRITER, TK_ATTR_WRITER, EXPR_HEAD),
    IdentAndNextMode("begin", TK_BEGIN, TK_BEGIN, EXPR_HEAD),
    // `break` はブロックから抜けるので、戻り値の実引数を取れる.
    // (`return` と同じ.)
    IdentAndNextMode("break", TK_BREAK, TK_BREAK, EXPR_OR_NEWLINE),
    IdentAndNextMode("case", TK_CASE, TK_CASE, EXPR_HEAD),
    IdentAndNextMode("class", TK_CLASS, TK_CLASS, EXPR_HEAD),
    IdentAndNextMode("def", TK_DEF, TK_DEF, BE_METHOD_NAME),
    IdentAndNextMode("do", TK_DO, TK_DO, EXPR_HEAD),
    IdentAndNextMode("else", TK_ELSE, TK_ELSE, EXPR_HEAD),
    IdentAndNextMode("elsif", TK_ELSIF, TK_ELSIF, EXPR_HEAD),
    IdentAndNextMode("end", TK_END, TK_END, AFTER_EXPR),
    IdentAndNextMode("ensure", TK_ENSURE, TK_ENSURE, EXPR_OR_NEWLINE),
    // Ruby では Object#extend メソッド. 型を解析するためにキーワードにする✓
    IdentAndNextMode("extend", TK_EXTEND, TK_EXTEND, EXPR_HEAD),
    IdentAndNextMode("false", TK_FALSE, TK_FALSE, AFTER_EXPR),
    IdentAndNextMode("for", TK_FOR, TK_FOR, EXPR_HEAD),
    IdentAndNextMode("if", TK_IF, TK_MOD_IF, EXPR_HEAD),
    IdentAndNextMode("in", TK_IN, TK_IN, EXPR_HEAD),
    // Ruby では Module#include メソッド. 型を解析するためにキーワードにする✓
    IdentAndNextMode("include", TK_INCLUDE, TK_INCLUDE, EXPR_HEAD),
    // lambda だけキーワード. [[incompat]] proc はまた随分挙動が異なる
    IdentAndNextMode("lambda", TK_LAMBDA, TK_LAMBDA, AFTER_METHOD_NAME),
    IdentAndNextMode("module", TK_MODULE, TK_MODULE, EXPR_HEAD),
    // `next` は, クロージャから抜けるので、戻り値とする実引数を取る.
    // (`return` と同じ.)
    IdentAndNextMode("next", TK_NEXT, TK_NEXT, EXPR_OR_NEWLINE),
    IdentAndNextMode("nil", TK_NIL, TK_NIL, AFTER_EXPR),
    IdentAndNextMode("private", TK_PRIVATE, TK_PRIVATE, EXPR_HEAD),
    IdentAndNextMode("protected", TK_PROTECTED, TK_PROTECTED, EXPR_HEAD),
    IdentAndNextMode("require", TK_REQUIRE, TK_REQUIRE, EXPR_HEAD),
    IdentAndNextMode("rescue", TK_RESCUE, TK_RESCUE, EXPR_OR_NEWLINE),
    IdentAndNextMode("return", TK_RETURN, TK_RETURN, EXPR_OR_NEWLINE),
    IdentAndNextMode("self", TK_SELF, TK_SELF, AFTER_EXPR),
    IdentAndNextMode("super", TK_SUPER, TK_SUPER, AFTER_METHOD_NAME),
    IdentAndNextMode("then", TK_THEN, TK_THEN, EXPR_HEAD),
    IdentAndNextMode("true", TK_TRUE, TK_TRUE, AFTER_EXPR),
    // Syntax sugar for `if not`
    IdentAndNextMode("unless", TK_UNLESS, TK_MOD_UNLESS, EXPR_HEAD),
    // Syntax sugar for `while not`
    IdentAndNextMode("until", TK_UNTIL, TK_UNTIL, EXPR_HEAD),
    IdentAndNextMode("when", TK_WHEN, TK_WHEN, EXPR_HEAD),
    IdentAndNextMode("while", TK_WHILE, TK_WHILE, EXPR_HEAD),
    IdentAndNextMode("yield", TK_YIELD, TK_YIELD, AFTER_METHOD_NAME),
    IdentAndNextMode("", 0, 0, EXPR_HEAD),
};


struct TokenAndNextMode {
    UnicodeString const str;
    int token;
    LexMode next_mode;

    TokenAndNextMode(const char* str_, int t, const LexMode& m):
        str(str_), token(t), next_mode(m) { }
};


// 上から順にテストしていく。長いものから先に書くこと。
// TODO: 文字列の要否を持たせる
static const TokenAndNextMode op_and_next_mode[] = {
    // 禁止 [incompat]
    TokenAndNextMode("`", TK_RESERVED, EXPR_HEAD),
    // range-ctorとしては禁止. [incompat]
    TokenAndNextMode("...", TK_RESERVED, EXPR_HEAD),

    TokenAndNextMode("..", TK_DOT2, EXPR_HEAD),
    TokenAndNextMode("(", '(', EXPR_HEAD),
    TokenAndNextMode(")", ')', AFTER_EXPR),
    // TokenAndNextMode("{", '{', EXPR_HEAD),
    // TokenAndNextMode("}", '}', AFTER_EXPR),    特別扱いする
    TokenAndNextMode("[", '[', EXPR_HEAD),
    TokenAndNextMode("]", ']', AFTER_EXPR),
    TokenAndNextMode(";", TK_TERM, EXPR_HEAD),
    TokenAndNextMode(",", ',', EXPR_HEAD),

    // 特別扱いする
    //TokenAndNextMode("->", TK_LAMBDA_ARROW_OP, AFTER_METHOD_NAME), // 直後の "{" はブロック
    TokenAndNextMode("&&=", TK_ASSIGN_OP, EXPR_HEAD),
    TokenAndNextMode("||=", TK_ASSIGN_OP, EXPR_HEAD),
    TokenAndNextMode("&&", TK_LOGICAL_AND, EXPR_HEAD),
    TokenAndNextMode("||", TK_LOGICAL_OR, EXPR_HEAD),
    TokenAndNextMode("<<", TK_LSHIFT, EXPR_HEAD),
    TokenAndNextMode(">>", TK_RSHIFT, EXPR_HEAD),
    // 仮引数でも使う
    TokenAndNextMode("**", TK_POWER, EXPR_HEAD),

    TokenAndNextMode("===", TK_EQNE_OP, EXPR_HEAD),
    TokenAndNextMode("<=>", TK_EQNE_OP, EXPR_HEAD),
    TokenAndNextMode("==", TK_EQNE_OP, EXPR_HEAD),
    TokenAndNextMode("!=", TK_EQNE_OP, EXPR_HEAD),
    TokenAndNextMode("=~", TK_EQNE_OP, EXPR_HEAD),
    TokenAndNextMode("!~", TK_EQNE_OP, EXPR_HEAD),

    TokenAndNextMode("=>", TK_VAR_ASSIGNMENT, EXPR_HEAD),

    TokenAndNextMode(">=", TK_COMPARE_OP, EXPR_HEAD),
    TokenAndNextMode(">", TK_COMPARE_OP, EXPR_HEAD),
    TokenAndNextMode("<=", TK_COMPARE_OP, EXPR_HEAD),
    TokenAndNextMode("<", TK_COMPARE_OP, EXPR_HEAD),

    TokenAndNextMode("+=", TK_ASSIGN_OP, EXPR_HEAD),
    TokenAndNextMode("-=", TK_ASSIGN_OP, EXPR_HEAD),
    TokenAndNextMode("*=", TK_ASSIGN_OP, EXPR_HEAD),
    TokenAndNextMode("/=", TK_ASSIGN_OP, EXPR_HEAD),
    TokenAndNextMode("%=", TK_ASSIGN_OP, EXPR_HEAD),
    TokenAndNextMode("+", '+', EXPR_HEAD),
    TokenAndNextMode("-", '-', EXPR_HEAD),
    // "*" は特別扱い
    // TokenAndNextMode("*", TK_SPLAT, EXPR_HEAD),
    TokenAndNextMode("%", TK_MOD, EXPR_HEAD),
    // "/"は特別扱い

    TokenAndNextMode("!", '!', EXPR_HEAD),
    TokenAndNextMode("~", '~', EXPR_HEAD),

    // これと別に, (改行 + ".") を特別扱い
    TokenAndNextMode(".", '.', BE_METHOD_NAME),

    TokenAndNextMode("&=", TK_ASSIGN_OP, EXPR_HEAD),
    TokenAndNextMode("|=", TK_ASSIGN_OP, EXPR_HEAD),
    //TokenAndNextMode("&", '&', EXPR_HEAD), 特別扱い
    TokenAndNextMode("|", '|', EXPR_HEAD),

    TokenAndNextMode("=", '=', EXPR_HEAD),
    TokenAndNextMode("?", '?', EXPR_HEAD),
    TokenAndNextMode(":", ':', EXPR_HEAD),
    TokenAndNextMode("$", '$', EXPR_HEAD), // 型指定に使う. [incompat]

    TokenAndNextMode("", 0, EXPR_HEAD),
};


//////////////////////////////////////////////////////////////////////
// class BaseState

// ident とマッチした場合, send & forward する
bool BaseState::check_ident( int kind )
{
    assert( kind == 0 || kind == 1 );

    if ( !lexer->match(ident_re) )
        return false;

    const UnicodeString ident = lexer->matched_substring();
    bool found = false;
    LexMode next_mode;
    int token;
    const IdentAndNextMode* p = ident_and_next_mode;
    for ( ; p->token1 != 0; p++ ) {
        if ( ident == p->str ) {
            token = kind == 0 ? p->token1 : p->token2;
            lexer->send( token );
            lexer->forward( ident.length() );
            next_mode = p->next_mode;
            found = true; break;
        }
    }
    if ( !found ) {
        const UnicodeString* pp = reserved;
        for ( ; *pp != UnicodeString(""); pp++ ) {
            if (ident == *pp) {
                token = TK_RESERVED;
                lexer->send( token );
                lexer->forward( ident.length() );
                next_mode = EXPR_HEAD;
                found = true; break;
            }
        }
    }
    if ( !found ) {
        // キーワード以外
        token = TK_IDENTIFIER;
        lexer->send( token, ident );
        lexer->forward( ident.length() );
        next_mode = AFTER_METHOD_NAME;
    }

    if ( next_mode == AFTER_METHOD_NAME || token == TK_SELF ) {
        // ident(...), super(...), yield(...)  空白を入れない
        if ( lexer->match("(") ) {
            lexer->send(TK_METHOD_PARENTHESIS);
            lexer->forward(1);
            fsm->enter_mode(EXPR_HEAD);
            return true;
        }
        else if ( lexer->match("[") ) {
            // self[...]も
            lexer->send( TK_METHOD_SQUARE_BRACKET );
            lexer->forward(1);
            fsm->enter_mode(EXPR_HEAD);
            return true;
        }
    }

    fsm->enter_mode( next_mode );
    return true;
}


// @return match したら true
bool BaseState::check_const()
{
    if ( lexer->match(qualified_const_re) ) {
        if (fsm->mode == AFTER_METHOD_NAME)
            lexer->send(TK_NO_PARENTHESIS);

        // 長いほうからマッチさせる
        const UnicodeString substring = lexer->matched_substring();
        lexer->send( TK_QUALIFIED_CONST, substring );
        lexer->forward( substring.length() );

        if ( lexer->match("[") ) {
            lexer->send( TK_METHOD_SQUARE_BRACKET );
            lexer->forward(1);
            fsm->enter_mode( EXPR_HEAD );
        }
        else
            fsm->enter_mode( AFTER_EXPR );

        return true;
    }

    if ( lexer->match(key_re) ) { // ident, const より前にする
        if (fsm->mode == AFTER_METHOD_NAME)
            lexer->send(TK_NO_PARENTHESIS);

        const UnicodeString substring = lexer->matched_substring();
        lexer->send( TK_KEY, UnicodeString(substring, 0, substring.length() - 1) );
        fsm->enter_mode(EXPR_HEAD); // 改行はスルー
        lexer->forward( substring.length() );
        return true;
    }

    if ( lexer->match(const_re) ) {
        if (fsm->mode == AFTER_METHOD_NAME)
            lexer->send(TK_NO_PARENTHESIS);

        lexer->send( TK_CONST_IDENT, lexer->matched_substring() );
        lexer->forward( lexer->matched_length() );

        if ( lexer->match("[") ) {
            lexer->send( TK_METHOD_SQUARE_BRACKET );
            lexer->forward(1);
            fsm->enter_mode( EXPR_HEAD );
        }
        else
            fsm->enter_mode( AFTER_EXPR );

        return true;
    }

    return false;
}


/**
 * @return matchしたらtrue
 * mode == AFTER_METHOD_NAME のときは, dollar_sign を適宜挿入.
 * newline は各サブクラスで実装すること
 */
bool BaseState::process()
{
    // printf("%s enter.\n", __func__);

    if ( lexer->match(classvar_ident_re) ) {
        if (fsm->mode == AFTER_METHOD_NAME)
            lexer->send(TK_NO_PARENTHESIS);

        lexer->send( TK_CLASSVAR_IDENT,
              UnicodeString(lexer->matched_substring(), 2) );
        lexer->forward( lexer->matched_length() );

        if ( lexer->match("[") ) {
            lexer->send( TK_METHOD_SQUARE_BRACKET );
            lexer->forward(1);
            fsm->enter_mode( EXPR_HEAD );
        }
        else
            fsm->enter_mode( AFTER_EXPR );

        return true;
    }

    if ( lexer->match(ivar_ident_re) ) {
        if (fsm->mode == AFTER_METHOD_NAME)
            lexer->send(TK_NO_PARENTHESIS);

        const UnicodeString substring = lexer->matched_substring();
        lexer->send( TK_IVAR_IDENT, UnicodeString(substring, 1) );
        lexer->forward( substring.length() );

        if ( lexer->match("[") ) {
            lexer->send( TK_METHOD_SQUARE_BRACKET );
            lexer->forward(1);
            fsm->enter_mode( EXPR_HEAD );
        }
        else
            fsm->enter_mode( AFTER_EXPR );

        return true;
    }
    if ( lexer->match(symbol_re) ) {
        if (fsm->mode == AFTER_METHOD_NAME)
            lexer->send(TK_NO_PARENTHESIS);

        const UnicodeString substring = lexer->matched_substring();
        lexer->send( TK_SYMBOL, UnicodeString(substring, 1) );
        fsm->enter_mode(AFTER_EXPR);
        lexer->forward( substring.length() );
        return true;
    }
    if ( lexer->match(float_re) ) {
        if (fsm->mode == AFTER_METHOD_NAME)
            lexer->send(TK_NO_PARENTHESIS);

        const UnicodeString substring = lexer->matched_substring();
        lexer->send( TK_DOUBLE_LITERAL, substring );
        fsm->enter_mode(AFTER_EXPR);
        lexer->forward( substring.length() );
        return true;
    }
    if ( lexer->match(int_re) ) { // 実数より後ろ.
        if (fsm->mode == AFTER_METHOD_NAME)
            lexer->send(TK_NO_PARENTHESIS);

        lexer->send( TK_INT_LITERAL, lexer->matched_substring() );
        fsm->enter_mode(AFTER_EXPR);
        lexer->forward( lexer->matched_length() );
        return true;
    }

    if ( lexer->col == 0 && lexer->match(begin_ml_comment_re) ) {
        fsm->push_mode(COMMENT_ML);
        // 最後の改行文字の手前まで進める
        lexer->forward( lexer->matched_length() - 1 );
        return true;
    }

    // "%{"   x % {x:1} と曖昧. 書ける場所の制約がある
    if ( (fsm->mode == EXPR_HEAD || fsm->mode == AFTER_METHOD_NAME ||
          fsm->mode == EXPR_OR_NEWLINE) &&
         lexer->match(start_string_re) ) {
        if (fsm->mode == AFTER_METHOD_NAME) lexer->send(TK_NO_PARENTHESIS);

        const UnicodeString substring = lexer->matched_substring();
        fsm->enter_mode(STRING_LITERAL_STATE,
                          substring[substring.length() - 1]);
        lexer->forward( substring.length() );
        return true;
    }

    // "<<hoge"  x << hoge と曖昧.
    if ( (fsm->mode == EXPR_HEAD || fsm->mode == AFTER_METHOD_NAME ||
          fsm->mode == EXPR_OR_NEWLINE) &&
         lexer->match(heredoc_re) ) {
        if (fsm->mode == AFTER_METHOD_NAME) lexer->send(TK_NO_PARENTHESIS);

        const UnicodeString substring = lexer->matched_substring(1);
        // EOFに到達したとき、このトークンを返すことがある。
        // この中で heredoc_guards への追加を行う
        string s;
        printf("heredoc placeholder: %s\n", substring.toUTF8String(s).c_str()); // DEBUG
        assert( substring != "" );
        lexer->send(TK_HEREDOC, substring);

        fsm->enter_mode(AFTER_EXPR);
        lexer->forward( lexer->matched_length() ); // "<<" を含む長さ
        return true;
    }

    // メソッド呼出しの '.' の前に改行があってもよい.
    // heredoc のほうが優先.
    if ( fsm->heredoc_guards.size() == 0 && lexer->match(nl_and_dot_re) ) {
        // TODO: 間にコメントが入ると上手くいかない。コメントを空白に置き換えて
        //       再度流す必要がある
        lexer->send('.');
        lexer->forward( lexer->matched_length() );
        fsm->enter_mode(BE_METHOD_NAME);
        return true;
    }

    if ( lexer->match(newline_re) ) {
        if (fsm->heredoc_guards.size() > 0) {
            fsm->push_mode(STRING_HEREDOC, fsm->heredoc_guards.front().second);
        }
        else {
            if ( fsm->mode == AFTER_METHOD_NAME ||
                 fsm->mode == AFTER_EXPR || fsm->mode == EXPR_OR_NEWLINE) {
                printf("AfterMethodName / AfterExpr: match newline\n"); // DEBUG
                lexer->send(TK_TERM);
                fsm->enter_mode(EXPR_HEAD);
            }
        }
        lexer->forward( lexer->matched_length() );
        return true;
    }

    if ( lexer->match(lambda_arrow_re) ) {  // 空白を許容
        lexer->send(TK_LAMBDA_ARROW_OP); lexer->forward(lexer->matched_length());
        if ( lexer->match("(") ) {
            lexer->send(TK_METHOD_PARENTHESIS); lexer->forward(1);
            fsm->enter_mode(EXPR_HEAD);
        }
        else
            fsm->enter_mode(AFTER_METHOD_NAME);
        return true;
    }

    // 演算子. 実数より後ろ.
    const TokenAndNextMode* p = op_and_next_mode;
    for ( ; p->token != 0; p++ ) {
        if ( lexer->match(p->str) ) {
            lexer->send( p->token, p->str );
            fsm->enter_mode( p->next_mode );
            lexer->forward( p->str.length() );
            return true;
        }
    }

    if (lexer->match("&")) {
        if (fsm->mode == AFTER_METHOD_NAME)
            lexer->send(TK_NO_PARENTHESIS);
        lexer->send('&'); lexer->forward(1); fsm->enter_mode(EXPR_HEAD);
        return true;
    }

    if ( lexer->match("}") ) {
        lexer->send( '}' );

        if (fsm->bracket_stack.size() > 0) {
            BracketType b = fsm->bracket_stack.back();
            fsm->bracket_stack.pop_back();
            if ( b == EMBD_EXPR_OPEN )
                fsm->pop_mode();
            else
                fsm->enter_mode( AFTER_EXPR );
        }
        lexer->forward( 1 );
        return true;
    }
    else if ( lexer->match("\"") ) {  // Crystal: シングルクォートは文字列ではなく文字.
        if (fsm->mode == AFTER_METHOD_NAME)
            lexer->send(TK_NO_PARENTHESIS);

        fsm->enter_mode(STRING_LITERAL_STATE, "\"");
        lexer->forward(1);
        return true;
    }
    else if ( lexer->match(whitespace_re) ) {
        int l = lexer->matched_length();  lexer->forward( l );
        //printf("ws len = %d\n", l);  // DEBUG
        return true;
    }
    else if ( lexer->match("#") ) {
        // 行末までコメント
        fsm->push_mode(COMMENT);
        lexer->forward(1);
        return true;
    }

    return false;
}


/**
 * 改行は暗黙に継続行. 式を書けるところ
 */
bool ExprHead::process()
{
    if (BaseState::check_const() )
        return true;

    if ( check_ident(0) )
        return true;
    else if ( lexer->match("/") ) {
        // 正規表現リテラル. "/=" も正規表現の開始
        fsm->enter_mode(REGEX_LITERAL_STATE, "/");
        lexer->forward(1);
        return true;
    }
    else if ( lexer->match("{") ) {
        // ハッシュ
        printf("ExprHead::process(): match '{'\n"); // DEBUG
        lexer->send( TK_HASH_OPEN );
        fsm->bracket_stack.push_back( HASH_OPEN );
        lexer->forward(1);
        // EXPR_HEADのまま
        return true;
    }

    if ( BaseState::process() )
        return true;

    if ( lexer->match("*") ) {
        // "**" や "*=" は BaseState::process() で処理済み
        lexer->send( TK_SPLAT );
        lexer->forward(1);
        // EXPR_HEADのまま
        return true;
    }

    return false;  // fail
}


/**
 * 必ずメソッド名がくる場所. キーワードが無効になる (メソッド名として使える).
 */
bool BeMethodName::process()
{
    if ( lexer->match(ident_re) ) {
        lexer->send( TK_METHOD_NAME, lexer->matched_substring() );
        lexer->forward( lexer->matched_length() );

        if ( lexer->match("(") ) { // 空白なしの場合
            lexer->send(TK_METHOD_PARENTHESIS);
            lexer->forward(1);
            fsm->enter_mode(EXPR_HEAD);
        }
        else if ( lexer->match("[") ) {
            lexer->send( TK_METHOD_SQUARE_BRACKET );
            lexer->forward(1);
            fsm->enter_mode(EXPR_HEAD);
        }
        else
            fsm->enter_mode( AFTER_METHOD_NAME );

        return true;
    }
    else if ( lexer->match(op_method_name_re) ) {
        const UnicodeString substring = lexer->matched_substring();
        lexer->send( TK_METHOD_NAME, substring );
        lexer->forward( substring.length() );

        if ( lexer->match("(") ) {
            lexer->send(TK_METHOD_PARENTHESIS);
            lexer->forward(1);
            fsm->enter_mode(EXPR_HEAD);
        }
        else if ( lexer->match("[") ) {
            lexer->send( TK_METHOD_SQUARE_BRACKET );
            lexer->forward(1);
            fsm->enter_mode(EXPR_HEAD);
        }
        else
            fsm->enter_mode( AFTER_METHOD_NAME );

        return true;
    }
    else if ( lexer->match(whitespace_re) ) {
        // empty
        lexer->forward( lexer->matched_length() );
        return true;
    }

    if ( BaseState::process() )
        return true;

    return false;     // fail
}


/**
 * primary '.' method_name の直後
 *    method_name dollar_sign arguments  => dollar_signを挿入する
 */
bool AfterMethodName::process()
{
    printf("AfterMethodName::process(): enter.\n"); // DEBUG

    if (BaseState::check_const())
        return true;

    // mod_ifを特別扱い
    if ( lexer->match(ident_re) ) {  // まず先読み
        const UnicodeString ident = lexer->matched_substring();
        lexer->forward(0);  // match_data をクリア

        if (ident == "if" || ident == "unless" )
            return check_ident(1);
        else if (ident == "in" || ident == "end")
            return check_ident(1); // for i in 1..2

        // メソッド名と実引数の間に dollar_sign
        lexer->send(TK_NO_PARENTHESIS);

        return check_ident(1);  // 改めて前に進める
    }

    if ( lexer->match("(")) {
        // メソッド名と実引数の間に dollar_sign
        lexer->send(TK_NO_PARENTHESIS);

        lexer->send( '(' ); lexer->forward(1);
        fsm->enter_mode(EXPR_HEAD);
        return true;
    }

    // "/", "*" は特別扱い. BaseState ではマッチしない.
    // "/=", "*=" は処理する. 空白もこのなかで処理.
    if ( BaseState::process() ) {
        printf("AfterMethodName::process(): match in BaseState\n"); // DEBUG
        return true;
    }

    // p Proc.new {}.lambda?  空白があっても block
    if ( lexer->match("{")) {
        // メソッド名と実引数の間に dollar_sign
        lexer->send(TK_NO_PARENTHESIS);

        lexer->send( TK_BLOCK_OPEN );
        fsm->bracket_stack.push_back( LAMBDA_OPEN );
        lexer->forward(1);
        fsm->enter_mode(EXPR_HEAD);
        return true;
    }

    if ( lexer->match("/") ) { // 投機的にパースしないと分からない
        lexer->forward(1);
        if ( lexer->match(ws_or_nl_re) ) {
            lexer->send(TK_DIV);
            lexer->forward( lexer->matched_length() );
            fsm->enter_mode(EXPR_HEAD);
        }
        else {
            // メソッド名と実引数の間に dollar_sign
            lexer->send(TK_NO_PARENTHESIS);
            fsm->enter_mode(REGEX_LITERAL_STATE, "/");
        }
        return true;
    }

    if ( lexer->match("*") ) {
        // "**" や "*=" は BaseState::process() で処理済み
        lexer->send( TK_MUL );
        lexer->forward(1);
        fsm->enter_mode(EXPR_HEAD);
        return true;
    }

    return false;  // fail
}


/** 式の項が終わったところで, メソッド名の直後以外 */
bool AfterExpr::process()
{
    if ( check_ident(1) )
        return true;
    else if ( lexer->match("/") ) {
        lexer->send(TK_DIV);
        lexer->forward(1);
        fsm->enter_mode(EXPR_HEAD);
        return true;
    }
    else if ( lexer->match("{") ) {
        // メソッドの括弧を省略して, 最初の実引数としてハッシュ式は書けない. Rubyも同じ
        lexer->send( TK_BLOCK_OPEN );
        fsm->bracket_stack.push_back( LAMBDA_OPEN );
        lexer->forward(1);
        fsm->enter_mode(EXPR_HEAD);
        return true;
    }

    if ( BaseState::process() )  // このなかで '\n.' がある。
        return true;

    if ( lexer->match("*") ) {
        lexer->forward( 1 );
        // "**" や "*=" は BaseState::process() で処理済み
        if ( lexer->match(ws_or_nl_re) ) {
            lexer->send( TK_MUL );
            lexer->forward( lexer->matched_length() );
            fsm->enter_mode( EXPR_HEAD );
        }
        else {
            lexer->send( TK_SPLAT );
            fsm->enter_mode( EXPR_HEAD );
        }
        return true;
    }

    return false;  // fail
}


/**
 * returnやensureなど, 値を持たないが引数を持つ語の直後.
 * EXPR_HEAD と AFTER_EXPR の両方の性質
 */
bool ExprOrNewline::process()
{
    if (BaseState::check_const())
        return true;

    if ( check_ident(1) )
        return true;
    else if ( lexer->match("/") ) {
        // 必ず正規表現リテラル
        fsm->enter_mode(REGEX_LITERAL_STATE, "/");
        lexer->forward(1);
        return true;
    }

    if ( BaseState::process() )
        return true;

    if ( lexer->match("*") ) {
        // 必ずsplat
        lexer->send( TK_SPLAT );
        lexer->forward(1);
        fsm->enter_mode(EXPR_HEAD);
        return true;
    }

    // fail
    return false;
}


bool Comment::process()
{
    /** 改行を活かす場合とそうでない場合がある */
    if ( lexer->match(newline_re) ) {
        fsm->pop_mode();
        lexer->forward(0); // forwardしない
        return true;
    }
    else {
        lexer->forward(1);
        return true;
    }
}

bool CommentML::process()
{
    printf("pos = %d, %04x\n", lexer->pos, lexer->source[lexer->pos]);
    int tmp_pos;
    while (true) {
        // indexOf() の戻り値は絶対位置.
        tmp_pos = lexer->source.indexOf("\n=end", lexer->pos);
        if ( tmp_pos == -1) {
            fprintf(stderr, "`=end` missing./n");
            return false;
        }

        // =endhoge の場合を調べておく
        uint16_t ch = lexer->source[tmp_pos + 5];
        if ( ch >= 'a' && ch <= 'z' || ch >= 'A' && ch <= 'Z' ||
             ch >= '0' && ch <= '9' || ch == '_' ) {
            lexer->forward( tmp_pos + 5 - lexer->pos );
            continue;
        }
        break;
    }

    // 行末まで読みとばす. 改行なく EOF のことがある。
    tmp_pos = lexer->source.indexOf("\n", tmp_pos + 5);
    lexer->forward( tmp_pos == -1 ? lexer->source.length() - lexer->pos :
                                    tmp_pos + 1 - lexer->pos );
    fsm->pop_mode();

    return true;
}


////////////////////////////////////////////////////////////////////
// StringLiteralState

typedef map<UnicodeString, UnicodeString> StrPair;
static const StrPair delim_pairs = {
    {"(", ")"},
    {"[", "]"},
    {"{", "}"},
    {"<", ">"},
};

// @override
void StringLiteralState::on_entry( const UnicodeString& data )
{
    // printf("%s enter: pos = %d\n", __func__, lexer->pos);

    accumulator = "";

    assert( data != "" ) ;
    delim = data; // heredoc の場合は長い
    StrPair::const_iterator i = delim_pairs.find(delim);
    if (i != delim_pairs.end() )
        delim = i->second;

    string s;
    printf("delim = %s\n", delim.toUTF8String(s).c_str());
}


void StringLiteralState::on_exit()
{
    // printf("%s enter: pos = %d\n", __func__, lexer->pos);

    lexer->send( TK_STRING_LITERAL, accumulator );
    // lexer->forward( accumulator.length() );

    // printf("%s exit: pos = %d\n", __func__, lexer->pos);
}


bool StringLiteralState::process()
{
    //printf("str-literal: ch = %x ", lexer->source[lexer->pos]);

    if ( fsm->mode == STRING_LITERAL_STATE && lexer->match(delim) ) {
        fsm->enter_mode(AFTER_EXPR);  // heredoc の場合は pop
        lexer->forward( delim.length() );
        return true;
    }
    else if ( lexer->match(newline_re) ) {
        accumulator.append("\n");
        lexer->forward( lexer->matched_length() );
        return true;
    }
    else if ( lexer->match("\\\"") ) {
        accumulator.append("\"");
        lexer->forward(2);
        return true;
    }
    else if ( lexer->match("\\n") ) {
        accumulator.append("\n");
        lexer->forward(2);
        return true;
    }
    else if ( lexer->match("\\t") ) {
        accumulator.append("\t");
        lexer->forward(2);
        return true;
    }
    else if ( lexer->match("\\\\") ) {
        accumulator.append("\\");
        lexer->forward(2);
        return true;
    }
    else if ( lexer->match("#{") ) {
        fsm->push_mode( EXPR_HEAD, delim ); // flushされる
        lexer->send( TK_EMBD_EXPR_OPEN );
        fsm->bracket_stack.push_back( EMBD_EXPR_OPEN );
        lexer->forward(2);
        return true;
    }

    accumulator.append(lexer->source[lexer->pos]);
    lexer->forward(1);
    return true;
}


////////////////////////////////////////////////////////////////////
// StringHeredocState

bool StringHeredocState::process()
{
    //printf("str-heredoc: ch = %x ", lexer->source[lexer->pos]);

    if ( lexer->match(delim) ) {   // TODO: 改行の直後の確認
        fsm->pop_mode();
        lexer->forward( delim.length() );  // 後ろの改行文字を残す
        return true;
    }

    return StringLiteralState::process();
}

void StringHeredocState::on_exit()
{
    string s;
    printf("accumulator = %s\n", accumulator.toUTF8String(s).c_str());

    pair<Token*, UnicodeString> guard = fsm->heredoc_guards.front();
    fsm->heredoc_guards.erase(fsm->heredoc_guards.begin());
    guard.first->token_type = TK_STRING_LITERAL;
    guard.first->lexstr = accumulator;
}


////////////////////////////////////////////////////////////////////
// RegexLiteralState

void RegexLiteralState::on_entry( const UnicodeString& data )
{
    accumulator = "";

    assert( data != "" );
    delim = data;
    StrPair::const_iterator i = delim_pairs.find(delim);
    if ( i != delim_pairs.end() )
        delim = i->second;
}


void RegexLiteralState::on_exit()
{
    lexer->send( TK_REGEX_LITERAL, accumulator );
    // lexer->forward( accumulator.length() );
}


bool RegexLiteralState::process()
{
    if ( lexer->match(delim) ) {
        fsm->enter_mode(AFTER_EXPR);
        lexer->forward( delim.length() );
        return true;
    }
    else if ( lexer->match(newline_re) ) {
        accumulator.append("\n");
        lexer->forward( lexer->matched_length() );
        return true;
    }
    else if ( lexer->match("\\/") ) {
        accumulator.append("/");
        lexer->forward(2);
        return true;
    }
    else if ( lexer->match("\\\"") ) {
        accumulator.append("\"");
        lexer->forward(2);
        return true;
    }
    else if ( lexer->match("\\n") ) {
        accumulator.append("\n");
        lexer->forward(2);
        return true;
    }
    else if ( lexer->match("\\t") ) {
        accumulator.append("\t");
        lexer->forward(2);
        return true;
    }
    else if ( lexer->match("\\\\") ) {
        accumulator.append("\\");
        lexer->forward(2);
        return true;
    }
    else if ( lexer->match("#{") ) {
        fsm->push_mode( EXPR_HEAD, delim ); // flushされる
        lexer->send( TK_EMBD_EXPR_OPEN );
        fsm->bracket_stack.push_back( EMBD_EXPR_OPEN );
        lexer->forward(2);
        return true;
    }

    accumulator.append(lexer->source[lexer->pos]);
    lexer->forward(1);
    return true;
}


/////////////////////////////////////////////////////////////////////////

Lexer* crb_create_lexer( Parser* parser )
{
    return new Lexer_impl( parser );
}

void crb_lexer_cleanup()
{
    pcre2_code_free(ident_re);
    pcre2_code_free(const_re);
    pcre2_code_free(qualified_const_re);
    pcre2_code_free(ivar_ident_re);
    pcre2_code_free(classvar_ident_re);
    pcre2_code_free(symbol_re);
    pcre2_code_free(key_re);
    pcre2_code_free(op_method_name_re);

    pcre2_code_free(newline_re);
    pcre2_code_free(whitespace_re);
    pcre2_code_free(begin_ml_comment_re);
    pcre2_code_free(ws_or_nl_re);

    pcre2_code_free(float_re);
    pcre2_code_free(int_re);
    pcre2_code_free(start_string_re);
    pcre2_code_free(heredoc_re);

    pcre2_code_free(nl_and_dot_re);
    pcre2_code_free(lambda_arrow_re);
}


#ifndef NDEBUG
static struct TokenNamePair {
    int token;
    const char* name;
} tk_names[] = {
    {TK_TERM, "TK_TERM"},
    {TK_STRING_LITERAL,"TK_STRING_LITERAL"},
    {0, ""}
};
#endif // !NDEBUG


#ifndef NDEBUG
static void dump_token( const Token* token, YYSTYPE* yylval )
{
    assert( yylval );

    char buf[100];
    const TokenNamePair* p = tk_names;
    const char* token_name = nullptr;
    if ( token->token_type >= 0x21 && token->token_type <= 0x7e ) {
        sprintf(buf, "'%c'", token->token_type);
        token_name = buf;
    }
    else {
        for ( ; p->token; p++ ) {
            if ( p->token == token->token_type ) {
                token_name = p->name;
                break;
            }
        }
        if (!token_name) {
            sprintf(buf, "%d", token->token_type);
            token_name = buf;
        }
    }

    //UFILE* out = u_finit( stderr, nullptr, nullptr );
    string s;
    fprintf( stderr, "%s: %d:%d: token_id = %s, lexstr = '%s'\n", __func__,
               token->first_line, token->first_column,
               token_name,
             yylval->lexstr ? yylval->lexstr->toUTF8String(s).c_str() : "");
    //u_fclose(out);
}
#endif // !NDEBUG


int yylex( YYSTYPE* yylval, YYLTYPE* yylloc, Parser* myparser )
{
    assert( yylval );
    assert( myparser );
    assert( myparser->lexer );

    Token* token = nullptr;
    UnicodeString unistr;

    myparser->lexer->receive(&token);
    assert(token);

    switch (token->token_type) {
    // 識別子
    case TK_IDENTIFIER:
    case TK_SYMBOL:
    case TK_KEY:
    case TK_METHOD_NAME:
    case TK_CONST_IDENT:
    case TK_IVAR_IDENT:
    case TK_CLASSVAR_IDENT:
    case TK_QUALIFIED_CONST:
    // リテラル
    case TK_INT_LITERAL:
    case TK_DOUBLE_LITERAL:
    case TK_STRING_LITERAL:
    case TK_REGEX_LITERAL:
    // 演算子
    case TK_COMPARE_OP:
    case TK_EQNE_OP:
    case TK_ASSIGN_OP:
        yylval->lexstr = new UnicodeString(token->lexstr);
        break;
    default:
        yylval->lexstr = nullptr;
        break;
    }

#ifndef NDEBUG
dump_token( token, yylval );
#endif // !NDEBUG

    yylloc->first_line = token->first_line;
    yylloc->first_column = token->first_column;

    int tid = token->token_type;
    delete token;

    return tid;
}



// Local variables:
// tab-width: 4
// c-basic-offset: 4
// indent-tabs-mode: nil
// End:
