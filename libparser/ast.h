﻿// -*- coding:utf-8-with-signature; mode:c++; tab-width:4; -*-
// SPDX-License-Identifier: LGPL-3.0-or-later
/*
  minilang -- A small programming language, minilang interpreter.
  Copyright (c) 2011-2013, 2023 Netsphere Laboratories, Hisashi Horikawa.
  All rights reserved.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef AST_H__
#define AST_H__ 1

// スクリプトの抽象構文木 (AST).

#include "ptr_list.h"
#include "ptr_vector.h"
#include <unicode/unistr.h>
#include <gmp.h>
#include "noncopyable.h"
#include <assert.h>
#include <stdexcept>
#ifndef NDEBUG
  #include <stdlib.h>
#endif // !NDEBUG


// `parse.hpp` で定義される。循環参照になるので、コピーやむなし。
#if ! defined YYLTYPE && ! defined YYLTYPE_IS_DECLARED
typedef struct YYLTYPE YYLTYPE;
struct YYLTYPE
{
  int first_line;
  int first_column;
  int last_line;
  int last_column;
};
# define YYLTYPE_IS_DECLARED 1
# define YYLTYPE_IS_TRIVIAL 1
#endif


typedef ptr_vector<icu::UnicodeString*> StringList;

/**
 * 文並び
 * CL: Special Operator `PROGN`
 */
typedef ptr_vector<struct Expression*> StatementList;


// AST のトップレベル
struct ParsedScript: private noncopyable
{
    icu::UnicodeString filename;
    StatementList* stmt_list;

    ParsedScript(): stmt_list(nullptr) { }

    ~ParsedScript() {
        delete stmt_list;
    }
};


/**
 * operator
 * CL: 例外を投げる `signal` は関数. Special Operator `THROW` は別物。
 * Ruby にも throw(), catch() があるが、実装不要. [incompat]
 */
enum AST_NodeType
{
    // Expression /////////////////////////////

    // リテラル
    BOOLEAN_LITERAL = 1,        // LiteralExpression
    INT_LITERAL,                // LiteralExpression
    FLOAT_LITERAL,              // LiteralExpression
    NULL_EXPRESSION,            // `nil` LiteralExpression

    // オブジェクトの生成
    STRING_EXPRESSION,  // StringExpr
    LAMBDA_EXPR,        // LambdaExprNode
    VARIABLE_REF,       // VariableRef
    OBJECT_SLOT_REF,    // ObjectSlotRef
    ARRAY_EXPR,         // array_constructor. ArrayExpr
    HASH_EXPR,          // HashExpr

    ASSIGN_EXPRESSION,  // AssignExpression
    ASSIGN_OP_EXPR,     // AssignOpExpr
    LOGICAL_AND_EXPRESSION,     // // LogicalOpExpr
    LOGICAL_OR_EXPRESSION,      // LogicalOpExpr
    METHOD_CALL_EXPR,           // MethodCallNode
    UNARY_NOT_EXPR,             // UnaryExpr
    YIELD_EXPR,                 // FunctionCallNode
    SUPER_EXPR,                 // FunctionCallNode
    IF_EXPR,                    // IfStatement
    CASE_EXPR,                  // CaseExpr

    // 文 ////////////////////////////////////

    WHILE_STATEMENT,            // WhileStatement
    FOR_STATEMENT,              // ForStatement
    RETURN_STATEMENT,           // ReturnStatement
    BREAK_STATEMENT,            // ReturnStatement
    NEXT_STATEMENT,             // ReturnStatement

    METHOD_DEFINITION,          // MethodDefinition
    CLASS_DEFINITION,           // ClassDefinitionNode
    MODULE_DEFINITION,          // ModuleDefinitionNode
    BEGIN_RESCUE_STMT,          // BeginRescueStmt
    //INCLUDE_STMT,
    //EXTEND_STMT,
    //ATTR_ACCESSOR_STMT,
    //ATTR_READER_STMT,
    //ATTR_WRITER_STMT,

    BLOCK_NODE,                 // BlockNode
    UNWIND_PROTECT_NODE,        // ruby `ensure` 節. UnwindProtectNode

    // AST だけ差し込む ///////////////////////////

    COND_BLOCK_NODE,
    WHEN_CLAUSE_NODE,
    RESCUE_BLOCK_NODE,

    LAST
};

/*
https://product.st.inc/entry/2023/12/25/160504#%E5%8F%82%E8%80%83Ruby-%E3%83%91%E3%83%BC%E3%82%B5%E3%81%AE%E6%AF%94%E8%BC%83
Ruby パーサの比較

RubyVM::AST::Node
    サブクラスなし, Node クラスのみ。使いにくそう。
    [String] type "NODE_RESCUE", "NODE_WHEN" ほかすごい細かい
    [Array of Node] children はあるが, parent はない

RuboCop::AST::Node
    https://docs.rubocop.org/rubocop-ast/node_types.html  type, サブクラス対応
    [Symbol] type   parser gem の AST::Node#type と同じ
    parent がある。
    各ノードは別クラス. RescueNode, WhenNode などなど.

Prism::Node
    parent なし。
    各ノードは別クラス. RescueNode, WhenNode などなど.

parser gem, AST::Node
    https://zenn.dev/qnighy/articles/cc67193321b5e6
    サブクラスなし, Node クラスのみ。
    [Symbol] type はタグ :send, :rescue など
    children あり, タグ付きS式の引数の意味。parent なし
    -> S式として表示できる。
*/

/**
 * 抽象構文木 (AST) のノード
 *  - StatementList を子に持つノードは AST_Node のサブクラスにする
 *  - EVAL() するノードは, Expression のサブクラスにする
 */
struct AST_Node : private noncopyable
{
    const AST_NodeType m_type; // ● TODO: 名前を node_type などに変更する

    /**
     * 参照するだけ
     */
    struct AST_Node* parent;

    YYLTYPE m_loc;

    /** 参照するだけ */
    ParsedScript* m_root;

    AST_Node( AST_NodeType type, const YYLTYPE& loc, ParsedScript* root ):
        m_type(type), parent(nullptr) , m_loc(loc), m_root(root)
    {
        //m_loc.line_number = loc.first_line;
        //m_loc.column_number = loc.first_column;
        if (m_type >= LAST)
            abort(); // bug bug
    }

    virtual ~AST_Node() { }
};


/**
 * タグ付き形式のS式. EVAL() するノード
 * S式の定義: 1. アトム (単純データ, 空リスト) はS式
             2. S1, S2 がS式であるとき, (S1 . S2) は S式である.
 */
struct Expression: public AST_Node
{
    Expression( AST_NodeType type, const YYLTYPE& loc, ParsedScript* root ):
        AST_Node(type, loc, root) { }

    virtual ~Expression() {}

    //virtual Expression* clone() const = 0;
};


// 関数・メソッド呼出し //////////////////////////////////////////////////

enum ArgumentType {
    NORMAL_ARG,
    SPLAT_ARG,         // "*" 好きな場所に複数置ける
};


struct Argument: private noncopyable
{
    ArgumentType const type;
    Expression* const expr;

    Argument(ArgumentType t, Expression* e): type(t), expr(e) {
        assert(expr);
    }

    ~Argument() {
        delete expr;
    }
};


/** AST実引数リスト */
typedef ptr_list<Argument*> ArgumentList;

struct KeyValueNode: private noncopyable
{
    // double splat の場合は NULL にする.
    Expression* const key;

    Expression* const value;


    KeyValueNode(Expression* k, Expression* v): key(k), value(v) {
        //assert(key);
        assert(value);
    }

    ~KeyValueNode() {
        delete key;
        delete value;
    }
};


/** ハッシュの k => v (汎用) */
typedef ptr_list<KeyValueNode*> AssociationList;


// リテラル ////////////////////////////////////////////////////////////////

/** 配列式 */
struct ArrayExpr: public Expression
{
    // splat で展開がある.
    ArgumentList* const list;

    ArrayExpr( const YYLTYPE& loc, ParsedScript* root, ArgumentList* args );

    ~ArrayExpr() {
        delete list;
    }

    //virtual ArrayExpr* clone() const override;
};


/** ハッシュ式 */
struct HashExpr: public Expression
{
    // ここにも double splat を書いてよい.
    AssociationList* const list;

    HashExpr( const YYLTYPE& loc, ParsedScript* root, AssociationList* args );

    ~HashExpr() {
        delete list;
    }

    //virtual HashExpr* clone() const override;
};


/**
 * 文字列式.
 * 式を埋め込めるので, 構造を持つ
 */
struct StringExpr: public Expression
{
    // 交互に編み込む. 構文上, 最後はリテラルで終わる ("" であれ).
    StringList literals;
    ptr_vector<Expression*> embeds;

    StringExpr( const YYLTYPE& loc, ParsedScript* root ):
        Expression(STRING_EXPRESSION, loc, root ) { }

    //virtual StringExpr* clone() const override;
};


/** 即値 */
struct LiteralExpression: public Expression
{
    // 即値 nil のときは何も設定しない
    union {
        bool    boolean_value;

        // int     int_value;
        mpz_t int_value;

        double  double_value;  // TODO: こちらも多倍長に
    } u;

    LiteralExpression( AST_NodeType type_,
                       const YYLTYPE& loc, ParsedScript* root ):
            Expression(type_, loc, root) {
        assert( type_ == NULL_EXPRESSION || type_ == BOOLEAN_LITERAL ||
                type_ == INT_LITERAL || type_ == FLOAT_LITERAL );
    }

    ~LiteralExpression() {
        if ( m_type == INT_LITERAL )
            mpz_clear( u.int_value);
    }

    //virtual LiteralExpression* clone() const override;
};


/**
 * 変数の種類.
 * [incompat] グローバル変数はない.
 */
enum VariableType {
    LOCAL_VAR,  // メソッド名を兼ねる. 構文が曖昧  TODO: ruby と同様, 曖昧な `VCALL` とローカル変数で間違いない `LVAR` に分けるか?
    CONST_VAR,
    INSTANCE_VAR,
    CLASS_VAR,
    SYMBOL
};


/** 変数の参照. 評価するかは文脈による */
struct VariableRef: public Expression
{
    VariableType const var_type;
    icu::UnicodeString* name;

    /**
     * @param name_ 先頭の"@"や":"は取り除かれた, 変数やシンボル名.
     */
    VariableRef( const YYLTYPE& loc, ParsedScript* root,
                 VariableType var_type_, icu::UnicodeString* name_):
                Expression(VARIABLE_REF, loc, root), var_type(var_type_), name(name_) {
        assert( *name != "");
    }

    VariableRef( AST_NodeType type, const YYLTYPE& loc, ParsedScript* root,
                 VariableType var_type_, icu::UnicodeString* name_):
                Expression(type, loc, root), var_type(var_type_), name(name_) {
        assert( *name != "");
    }

    virtual ~VariableRef() {
        delete name;
    }

    //virtual VariableRef* clone() const override;
};


// 演算式 ////////////////////////////////////////////////////////////////////

/**
 * 代入式. スロットへの代入の場合, メソッド呼出し `obj.foo=`, `obj.[]=` になる.
 * CL: Special Form `SETQ`
        (setq var1 form1 var2 form2 ...)
 */
struct AssignExpression: public Expression
{
    // 左辺. 値ではなく参照を取得する. ObjectSlotRef の場合も同様.
    VariableRef* const lefthand;

    // 右辺
    Expression* const operand;

    AssignExpression( const YYLTYPE& loc, ParsedScript* root, VariableRef* var,
                      /*icu::UnicodeString* op_,*/ Expression* value ):
                Expression(ASSIGN_EXPRESSION, loc, root),
                lefthand(var), /*op(op_),*/ operand(value) {
        assert( lefthand );
        assert( operand );
        lefthand->parent = this; operand->parent = this;
    }

    ~AssignExpression() {
        delete lefthand;
        //delete op;
        delete operand;
    }

    //virtual AssignExpression* clone() const override;
};

// "+=" などの場合. `&&=`, `||=` が特別扱いが必要
struct AssignOpExpr : public Expression
{
    // 左辺. 値ではなく参照を取得する. ObjectSlotRef の場合も同様.
    VariableRef* const lefthand;

    icu::UnicodeString* const op;

    // 右辺
    Expression* const operand;

    AssignOpExpr( const YYLTYPE& loc, ParsedScript* root, VariableRef* var,
                  icu::UnicodeString* op_, Expression* value ):
                Expression(ASSIGN_OP_EXPR, loc, root),
                lefthand(var), op(op_), operand(value) {
        assert( lefthand );
        assert( op );
        assert( operand );
        lefthand->parent = this; operand->parent = this;
    }

    ~AssignOpExpr() {
        delete lefthand;
        delete op;
        delete operand;
    }
};

/**
 * 論理OR/ANDノード. 右辺は正格ではない (lazy evaluation).
 * CL: Macro `OR`. Emacs Lisp では special form になっている。
 */
struct LogicalOpExpr: public Expression
{
    Expression* const left;
    Expression* const right;

    LogicalOpExpr( AST_NodeType type_, const YYLTYPE& loc, ParsedScript* root,
                   Expression* left_, Expression* right_ ):
                Expression(type_, loc, root), left(left_), right(right_) {
        assert(left);
        assert(right);
        left->parent = this; right->parent = this;
    }

    ~LogicalOpExpr() {
        delete left;
        delete right;
    }

    //virtual LogicalOpExpr* clone() const override;
};


/** メソッド呼び出しではない単項 */
struct UnaryExpr: public Expression
{
    Expression* const operand;

    UnaryExpr( AST_NodeType type_, const YYLTYPE& loc, ParsedScript* root,
               Expression* value ):
                Expression(type_, loc, root), operand(value) {
        assert(operand);
        operand->parent = this;
    }

    ~UnaryExpr() {
        delete operand;
    }

    //virtual UnaryExpr* clone() const override;
};


// 関数・メソッド定義 ///////////////////////////////////////////////////////

struct OptionalParamNode: private noncopyable
{
    icu::UnicodeString* const var_name;

    // opt.
    Expression* const default_value;

    OptionalParamNode(icu::UnicodeString* var_name_, Expression* default_):
                var_name(var_name_), default_value(default_) {
        assert( var_name);
    }

    ~OptionalParamNode() {
        delete var_name;
        delete default_value;
    }
};

typedef ptr_list<OptionalParamNode*> OptParamList;

/**
 * 仮引数リスト. 必ず次の順序.
       ({var         仮引数
        [&optional   オプション引数, デフォルト値
        [&rest       残りを配列として受け取る
        [&key        キーワード引数, デフォルト値。暗黙に省略可能.
        [&aux        補助パラメータ. 局所変数を定義する. (Ruby にはない。)
 */
struct ParameterList: private noncopyable
{
    // 必須の仮引数. ruby: ここにインスタンス変数は書けない.
    StringList params;

    // オプション引数: 変数名とデフォルト値のペア
    OptParamList optional_params;

    /** restパラメータ名 */
    icu::UnicodeString* rest_param;

    // キーワード引数. 両立する!
    OptParamList* key_params;

    /**
     * opt. ブロックパラメータ名.
     * ブロックパラメータにデフォルト値は設定できない (Rubyも同じ)
     */
    icu::UnicodeString* block_param;

    ParameterList(): rest_param(nullptr), key_params(nullptr), block_param(nullptr)  { }

    ~ParameterList() {
        delete rest_param;
        delete key_params;
        delete block_param;
    }
};


/**
 * ラムダ式. メソッド定義は, このクラスのサブクラス.
 * CL: Macro `LAMBDA`
    (lambda lambda-list [[declaration* | documentation]] form*)
    == (function (lambda lambda-list [[declaration* | documentation]] form*))
    別の書き方:
    == #'(lambda lambda-list [[declaration* | documentation]] form*)
 */
struct LambdaExprNode: public Expression
{
    /** 仮引数 */
    ParameterList* const param_list;

    /** 本体 */
    StatementList* const block;

    // 暗黙のブロックタグ. `next` 用
    //     -> 明示的に BlockNode を使え
    //icu::UnicodeString next_tag;

    LambdaExprNode( AST_NodeType type, const YYLTYPE& loc, ParsedScript* root,
                    ParameterList* params, StatementList* blk);

    virtual ~LambdaExprNode() {
        delete param_list;
        delete block;
    }

    //virtual LambdaExprNode* clone() const override;
};


/**
 * メソッド定義
 * 特異メソッド (object-specialized method) 定義がある。
 * CL: Macro `DEFUN`
 * CL: Macro `DEFMETHOD`   -- implicit block で囲まれる.
 * 特異メソッドは、次のように引数名 `EQL` specializer でオブジェクト制約する.
 *   (defmethod withdraw ((account (eql *account-of-bank-president*)) amount)
 *     ...)
 */
struct MethodDefinition: public LambdaExprNode
{
    // 特異メソッドを定義するオブジェクト
    VariableRef* const specializer;

    /** メソッド名 */
    icu::UnicodeString* const method_name;

    // 暗黙のブロックタグ. `return` 用   -> BlockNode を挟む
    //icu::UnicodeString const return_tag;

    MethodDefinition( const YYLTYPE& yyl, ParsedScript* root,
                      VariableRef* specializer_,   // opt
                      icu::UnicodeString* method_name_,
                      ParameterList* param_list_,  // opt
                      StatementList* block_,
                      const icu::UnicodeString& block_tag );

    ~MethodDefinition() {
        delete specializer;
        delete method_name;
    }

    //virtual MethodDefinition* clone() const override;
};


// method_definition へ繋ぐだけ
// TODO: この構造体は内部利用だけ. <ast.h> から外す
struct DefMethodName
{
    // 所有しない
    VariableRef* const specializer;
    icu::UnicodeString* const method_name;

    DefMethodName( VariableRef* o, icu::UnicodeString* m ):
        specializer(o), method_name(m) { }

    ~DefMethodName() {
        // delete object;
        // delete method_name;
    }
};


// 関数・メソッド呼出し ///////////////////////////////////////////////////

/* 普通の引数とキーワード引数は併用できる
* (defun poem2 (punchline &key (rose-color 'red) (violet-color 'blue))
  (append (list 'roses 'are rose-color 'and 'violets 'are violet-color)
          punchline))
POEM2
* (poem2 '(ccrma director chris chafe plays the cello) :violet-color 'yellow)
(ROSES ARE RED AND VIOLETS ARE YELLOW CCRMA DIRECTOR CHRIS CHAFE PLAYS THE
 CELLO)
 */
struct MethodArgument
{
    // ruby: 実引数のほうは、splat を好きな場所に複数書ける.
    //   -> これではコンパイル時に "引数の数" エラーで弾けない。実行時にも確認
    //      が必要。
    ArgumentList* const normal;

    // ruby: 実引数のほうは, double splat を好きな場所に複数書ける.
    //   -> こちらも、余計な指定をコンパイル時に弾けない. うーむ....
    AssociationList* const key;

    /** opt. 実引数としてのラムダ式.
        変数も書ける   pp %w(homu mami mado).map(&:upcase) */
    struct Expression* block_arg;

    MethodArgument(): normal(nullptr), key(nullptr), block_arg(nullptr) { }

    MethodArgument(ArgumentList* n, AssociationList* k, Expression* b):
        normal(n), key(k), block_arg(b) { }

    ~MethodArgument() {
        delete normal;
        delete key;
        delete block_arg;
    }
};


// (式).hoge
struct ObjectSlotRef: public VariableRef
{
    Expression* object;

    // (opt.) obj[x] の場合, VariableRef#name = "[]"
    struct MethodArgument* bracket_arguments;

    ObjectSlotRef(const YYLTYPE& loc, ParsedScript* root,
                  Expression* obj,
                  VariableType var_type, icu::UnicodeString* name,
                  MethodArgument* args);

    ~ObjectSlotRef() {
        delete object;
        delete bracket_arguments;
    }

    //virtual ObjectSlotRef* clone() const override;
};


/**
 * (1) 無名関数の呼び出し
 *    CL: Function `FUNCALL`
 *        (funcall #'(lambda (x) (print (* 2 x))) 100)
 *    Ruby:
 *        Proc#call(*args)    クロージャを呼び出す
 * (2) Ruby の `yield` は、暗黙の引数のブロック引数をレシーバとして呼び出すだ
 *     け。ジェネレータではない
 * (3) `super`. スーパクラス (多重継承があることに注意.) の同名メソッドを呼び出
 *     す. 引数を省略した場合、現在のメソッドの引数を付ける.
 *    CL: Local Function `CALL-NEXT-METHOD`
 *        引数を省略した場合、現在のメソッドの引数になるのも同じ.
 */
struct FunctionCallNode: public Expression
{
    // 通常実引数 + キーワード実引数 + ブロック実引数
    MethodArgument* const args;

    FunctionCallNode( AST_NodeType t, const YYLTYPE& loc, ParsedScript* root,
                      MethodArgument* args );

    virtual ~FunctionCallNode() {
        delete args;
        //delete key_args;
        //delete block_arg;
    }

    //virtual FunctionCallNode* clone() const override;
};


/**
 * メソッド呼び出し
 *        BasicObject#__send__(name, *args)   レシーバ, 名前が必須
 *        Method#call(*args)  インスタンス を bind 済みのメソッドを呼び出す
 * CL: `funcall` でよさそう.
```
(defclass Foo () ())
(defmethod bar ((x Foo)) (format t "Foo bar~%"))
(setq x1 (make-instance 'Foo))
(funcall 'bar x1)
```
 */
struct MethodCallNode: public FunctionCallNode
{
    Expression* const receiver;

    icu::UnicodeString* const method_name;

    // @param block_arg ブロック引数. 実引数 `&block` と両方指定されているときは
    //                  エラー.
    MethodCallNode( const YYLTYPE& loc, ParsedScript* root,
                    Expression* receiver_, icu::UnicodeString* method_name_,
                    MethodArgument* args, LambdaExprNode* block_arg ):
                FunctionCallNode(METHOD_CALL_EXPR, loc, root, args),
                receiver(receiver_), method_name(method_name_)
    {
        assert( receiver );
        assert( method_name && *method_name != "" );

        if ( block_arg ) {
            if (args && args->block_arg) {
                throw std::logic_error("syntax error"); // TODO: ちゃんとする
            }
            args->block_arg = block_arg;
        }

        receiver->parent = this;
    }

    ~MethodCallNode() {
        delete receiver;
        delete method_name;
    }

    //virtual MethodCallNode* clone() const override;
};


// 制御構造: 分岐, case-when, loop, 脱出  ////////////////////////////////////

/** if-then 節, elsif 節 */
struct CondBlock: public AST_Node
{
    Expression* const condition;
    StatementList* const block;

    CondBlock(const YYLTYPE& loc, ParsedScript* root, Expression* cond, StatementList* blk);

    ~CondBlock() {
        delete condition;
        delete block;
    }
};


typedef ptr_list<CondBlock*> CondList;


/** if 式
 * CL: Macro `COND`  該当した条件の文の値, どれにも該当しなかったとき nil
  (cond ((= a 1) (setq a 2))
        ((= a 2) (setq a 3))
        ((and (= a 3) (floor a 2)))
        (t (floor a 3)))
 */
struct IfStatement: public Expression
{
    // if-then 節, elsif 節
    CondList* cond_list;

    // else 節
    StatementList* else_block;

    IfStatement( const YYLTYPE& loc, ParsedScript* root ):
        Expression(IF_EXPR, loc, root),
        cond_list(nullptr), else_block(nullptr)  { }

    ~IfStatement() {
        delete cond_list;
        delete else_block;
    }

    //virtual IfStatement* clone() const override;
};


/**
 * while 文. 戻り値は不定.
 * CL: `do` がある。
 */
struct WhileStatement: public Expression
{
    Expression*    const condition;
    StatementList* const block;

    // 暗黙のブロックタグ. break 用
    icu::UnicodeString const block_tag;

    WhileStatement( const YYLTYPE& loc, ParsedScript* root,
                    Expression* cond, StatementList* blk,
                    const icu::UnicodeString& block_tag_);

    ~WhileStatement() {
        delete condition;
        delete block;
    }

    //virtual WhileStatement* clone() const override;
};


/**
 * for文
 * CL: Macro `DOLIST`     CommonLisp では戻り値を決められる (省略時は nil).
(dolist (item '(1 2 3))
  (print item))

このマクロは暗黙の block で囲まれる
* (macroexpand '(dolist (x '(1 2 3)) (print x)) )
(BLOCK NIL
  (LET ((#:N-LIST373 '(1 2 3)))
    (TAGBODY
     #:START374
      (UNLESS (ENDP #:N-LIST373)
        (LET ((X (TRULY-THE (MEMBER 3 2 1) (CAR #:N-LIST373))))
          (SETQ #:N-LIST373 (CDR #:N-LIST373))
          (TAGBODY (PRINT X)))
        (GO #:START374))))
  NIL)
 */
struct ForStatement: public Expression
{
    // 構文上, インスタンス変数や定数のこともある
    VariableRef* const var_ref;

    /**
     * to_enumメソッドでnextメソッドを持つオブジェクト (iterator) を返すもの
     */
    Expression*    const range;

    StatementList* const block;

    // 暗黙のブロックタグ. break 用
    icu::UnicodeString const block_tag;

    ForStatement( const YYLTYPE& loc, ParsedScript* root,
                  VariableRef* var_ref_, Expression* range_,
                  StatementList* block_,
                  const icu::UnicodeString& break_tag );

    ~ForStatement() {
        delete var_ref;
        delete range;
        delete block;
    }

    //virtual ForStatement* clone() const override;
};


/**
 * 名前付きのレキシカルな exit point. Ruby の `break`, `next` を実現
 * するために使う。
 * CL: Special Operator `BLOCK`
 * established explicitly by `block` or implicitly by operators such as `loop`, `do` and `prog`.
 * `defun` にも implicit block が置かれる.
 */
struct BlockNode: public Expression
{
    icu::UnicodeString const name;

    /** 本体 */
    StatementList* body;

    BlockNode( const YYLTYPE& loc, ParsedScript* root,
               const icu::UnicodeString& name_, StatementList* block_);
    BlockNode( const YYLTYPE& loc, ParsedScript* root,
               const icu::UnicodeString& name_, Expression* block_);

    ~BlockNode() {
        //delete name;
        delete body;
    }

    //virtual BlockNode* clone() const override;
};


/**
 * return文, break または next文
 * [incompat] `redo`, `retry` はサポートしない。
  CL: Special Operator `RETURN-FROM`
 */
struct ReturnStatement: public Expression
{
    icu::UnicodeString const block_tag;   // not evaluated.

    // 戻り値
    Expression* const return_value;

    ReturnStatement( AST_NodeType type, const YYLTYPE& loc, ParsedScript* root,
                     Expression* expression,
                     const icu::UnicodeString& block_tag_):
                Expression(type, loc, root),
                block_tag(block_tag_), return_value(expression) {
        assert( type == RETURN_STATEMENT || type == BREAK_STATEMENT ||
                type == NEXT_STATEMENT );
        if (return_value)
            return_value->parent = this;
    }

    ~ReturnStatement() {
        //delete block_tag;
        delete return_value;
    }

    //virtual ReturnStatement* clone() const override;
};


// case - when 式
struct WhenClause: public AST_Node
{
    // 値版:     複数の値を書いてよい.
    // 条件式版: 複数の値は不可
    ArgumentList* const patterns;

    StatementList* const block;

    WhenClause(const YYLTYPE& loc, ParsedScript* root,
               ArgumentList* pat, StatementList* b);

    ~WhenClause() {
        delete patterns;
        delete block;
    }
};

typedef ptr_list<WhenClause*> WhenClauseList;


/**
 * いずれにもマッチしなければ nil を返す
 * CL: Macro `CASE`  それぞれの値と `eql` で比較
 *   if 式を数珠つなぎしたいときは `cond`
 */
struct CaseExpr: public Expression
{
    // 条件式版では NULL
    Expression* const condition;

    WhenClauseList* const when_list;

    StatementList* const else_block;

    CaseExpr( const YYLTYPE& loc, ParsedScript* root,
              Expression* cond, WhenClauseList* when, StatementList* elseb);

    ~CaseExpr() {
        delete condition;
        delete when_list;
        delete else_block;
    }

    //virtual CaseExpr* clone() const override;
};


// 例外処理 /////////////////////////////////////////////////////////////////

/**
 * いくつかの場所で使われる.
 *    規則 11.5   begin-expression
 *    規則 11.4.2 assignment-with-rescue-modifier
 *    規則 12.7   rescue-modifier-statement
 * tail-call をブロックするため, AST_Nodeから派生させる
 */
struct RescueBlock : public AST_Node
{
    /**
     * opt. 省略時は Exception を補足する.
     *      ruby: 省略時は StandardError. [incompat]
     */
    icu::UnicodeString* const class_name;

    /** opt. */
    VariableRef* const var_ref;

    StatementList* const block;

    RescueBlock( const YYLTYPE& loc, ParsedScript* root,
                 icu::UnicodeString* klass, VariableRef* var,
                 StatementList* blk );

    ~RescueBlock() {
        delete class_name;
        delete var_ref;
        delete block;
    }
};


/** rescue節リスト */
typedef ptr_list<RescueBlock*> RescueList;


/**
 * 例外を受け取る。
 * CL: Macro `HANDLER-CASE`
 *   キャッチできなかった場合, Ruby と同様に, 順次上に上がる.
 *   `handler-case` は例外発生時にスタックを巻き戻すため, restart できない.
 *
 * CL: Macro `HANDLER-BIND` 低レベルなマクロ.
 *   `handler-case` も `handler-bind` を使って実装される.
 *   warning 以外のエラーの場合, 補足しても、上に上がる!
 *
 * `ensure` 節は `UnwindProtectNode` で囲め.
 */
struct BeginRescueStmt: public Expression
{
    StatementList* const try_block;

    // ruby: rescue または ensure のいずれかを書く
    // ensure は UnwindProtectNode に追い出したので、必ず rescue がある.
    RescueList* const rescue_list;

    BeginRescueStmt( const YYLTYPE& loc, ParsedScript* root,
                     StatementList* tryb, RescueList* rescues);

    ~BeginRescueStmt() {
        delete try_block;
        delete rescue_list;
        //delete else_block;  // 例外が発生しなかった場合. その他の例外ではない.
                            // CL: handler-case で :no-error を指定できる。
                            // -> handler-bind にはない。
    }

    //virtual BeginRescueStmt* clone() const override;
};


/**
 * AST のみ: メソッド定義にも `ensure` 節を設けることができる。切り出す
 * CL: Special Operator `UNWIND-PROTECT`
 */
struct UnwindProtectNode: public Expression
{
    // 実行部
    StatementList* block;

    // 実行を保証する部分
    StatementList* const cleanup_block;

    UnwindProtectNode( const YYLTYPE& loc, ParsedScript* root,
                       Expression* block_,
                       StatementList* cleanup_block_ );

    UnwindProtectNode( const YYLTYPE& loc, ParsedScript* root,
                       StatementList* block_,
                       StatementList* cleanup_block_ );

    ~UnwindProtectNode() {
        delete block;
        delete cleanup_block;
    }

    //virtual UnwindProtectNode* clone() const override;
};


// モジュール・クラス定義 //////////////////////////////////////////////////

struct SlotSpec
{
    enum SlotType {
        READER, WRITER, ACCESSOR } ;

    // メソッドを定める
    SlotType type;

    // 先頭の "@" は含めない.
    icu::UnicodeString* const name;

    ~SlotSpec() {
        delete name;
    }
};

typedef ptr_vector<SlotSpec*> SlotSpecList;

// TODO: この構造体は内部利用だけ. <ast.h> から外す
struct ModuleStmtList {
    // 所有しない
    StatementList* block;

    // push_front() が必要.
    ptr_list<VariableRef*>* include_list;

    ptr_vector<VariableRef*>* extend_list;

    SlotSpecList* slot_specs;

    ModuleStmtList(StatementList* b, VariableRef* i, VariableRef* e,
                   SlotSpecList* s):
                block(b), slot_specs(s) {
        if (i) {
            include_list = new ptr_list<VariableRef*>();
            include_list->push_back(i);
        }
        else
            include_list = nullptr;

        if (e) {
            extend_list = new ptr_vector<VariableRef*>();
            extend_list->push_back(e);
        }
        else
            extend_list = nullptr;
    }
};


// モジュール (インタフェイス) 定義文.
struct ModuleDefinitionNode: public Expression
{
    icu::UnicodeString* const name;

    StatementList* const block;

    ptr_list<VariableRef*>* const include_list;

    ptr_vector<VariableRef*>* const extend_list;

    SlotSpecList* const slot_specs;

    ModuleDefinitionNode( AST_NodeType t,
                          const YYLTYPE& loc, ParsedScript* root,
                          icu::UnicodeString* mod_name, ModuleStmtList* blk );

    virtual ~ModuleDefinitionNode() {
        delete name;
        delete block;
        delete include_list;
        delete extend_list;
        delete slot_specs;
    }

    //virtual ModuleDefinitionNode* clone() const override;
};


/**
 * クラス定義文.
 * [incompat] class << object という構文は廃止。
 *            extend するか, def self.foo か def ClassName.foo と書け.
 * 特異メソッド (EQL method) を作るとき, 実行環境に無名クラスが出来るが、
 * 字面上で無名のクラスはない。
 * CL: Macro `DEFCLASS`
 */
struct ClassDefinitionNode: public ModuleDefinitionNode
{
    icu::UnicodeString* const superclass;

    ClassDefinitionNode( const YYLTYPE& loc, ParsedScript* root,
                         icu::UnicodeString* class_name,
                         icu::UnicodeString* super, ModuleStmtList* blk ):
            ModuleDefinitionNode(CLASS_DEFINITION, loc, root, class_name, blk),
            superclass(super) {
        assert(*class_name != "");
    }

    ~ClassDefinitionNode() {
        delete superclass;
        //delete expr;
    }

    //virtual ClassDefinitionNode* clone() const override;
};


/* モジュールの直下に書かれたことを確認し、モジュールのスーパクラスにしてしまう。
struct IncludeStmt: public Expression
{
    icu::UnicodeString* const mod_name;

    IncludeStmt( const YYLTYPE& loc, ParsedScript* root,
                 icu::UnicodeString* mod_name_):
                Expression(INCLUDE_STMT, loc, root), mod_name(mod_name_) {
        assert( *mod_name != "");
    }

    ~IncludeStmt() {
        delete mod_name;
    }
};
*/

// `Object#extend` あらゆるオブジェクトに特異メソッドを追加できる
//    -> メソッドとして処理する
/*
struct ExtendStmt: public Expression
{
    // `extend self` がありえる. `self` はモジュールでなければならない
    icu::UnicodeString* const mod_name;

    ExtendStmt( const YYLTYPE& loc, ParsedScript* root,
                 icu::UnicodeString* mod_name_):
                Expression(EXTEND_STMT, loc, root), mod_name(mod_name_) {
        assert( *mod_name != "");
    }

    ~ExtendStmt() {
        delete mod_name;
    }
};
*/



#endif



// Local variables:
// tab-width: 4
// c-basic-offset: 4
// indent-tabs-mode: nil
// End:
