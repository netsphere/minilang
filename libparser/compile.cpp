﻿// -*- coding:utf-8-with-signature; mode:c++; tab-width:4; -*-
// SPDX-License-Identifier: LGPL-3.0-or-later
/*
  minilang -- A small programming language, minilang interpreter.
  Copyright (c) 2011-2013, 2023 Netsphere Laboratories, Hisashi Horikawa.
  All rights reserved.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

// スクリプトのコンパイル

#include "ast.h"
#include "parser.h"
#include <unicode/unistr.h>
#include <unicode/uchar.h> // u_isprint()
#include <unicode/schriter.h>
#include <stdarg.h>
#include <assert.h>
#include "lexer.h"

using namespace std;
using namespace icu;


extern int yyparse( Parser* myparser, ParsedScript* driver );


Parser::Parser(): lexer(nullptr), block_tag_num(1), parsed_script(nullptr)
{
    error_handler = default_error_handler;

    block_tag = tmpnam(NULL);
    int r = block_tag.lastIndexOf("/"); // "/tmp/" が付いている
    if (r != -1)
        block_tag = UnicodeString(block_tag, r + 1);
}


Parser::~Parser()
{
    if (parsed_script) {
        delete parsed_script; parsed_script = NULL;
    }
    if (lexer) {
        delete lexer; lexer = nullptr;
    }
}


void Parser::cleanup()
{
    crb_lexer_cleanup();
}

void Parser::push_tag(unsigned int tag)
{
    UnicodeString r = block_tag + to_string(++block_tag_num).c_str();
    if (tag & RETURN_TAG) return_tags.push_back(r);
    if (tag & NEXT_TAG)   next_tags.push_back(r);
    if (tag & BREAK_TAG)  break_tags.push_back(r);
}

UnicodeString Parser::pop_tag(unsigned int tag)
{
    UnicodeString r;
    if (tag & RETURN_TAG) {
        r = return_tags.back();
        return_tags.pop_back();
    }
    if (tag & NEXT_TAG) {
        UnicodeString t = next_tags.back();
        assert( r == "" || r == t);
        r = t; next_tags.pop_back();
    }
    if (tag & BREAK_TAG) {
        UnicodeString t = break_tags.back();
        assert( r == "" || r == t );
        r = t; break_tags.pop_back();
    }

    return r;
}

/** parser または lexer から呼び出される */
void Parser::default_error_handler( int line_number, int column_number,
                                    const char* format, ... )
{
    va_list     ap;
    char buf[1000]; // TODO: 可変長

    va_start(ap, format);
    vsnprintf(buf, 1000 - 1, format, ap);
    fprintf(stderr, "%3d:%d:%s\n", line_number, column_number, buf);
    va_end(ap);

    // exit(1); // TODO: 一定回数までは続行
}


/** ファイルをコンパイルする */
bool Parser::compile_file( const UnicodeString& filename_ )
{
    if ( lexer )
        delete lexer;
    lexer = crb_create_lexer( this );

    if ( parsed_script )
        delete parsed_script;
    parsed_script = new ParsedScript();

    parsed_script->filename = filename_;
    bool r = lexer->set_input_file( filename_, "UTF-8" );
    if (!r) {
        fprintf(stderr, "file open error.\n");
        // delete lexer;
        return false;
    }

    parsed_script->stmt_list = nullptr;

    error_count = 0;
    if ( yyparse(this, parsed_script) || error_count > 0 ) {
        // BUGBUG. TODO:
        fprintf(stderr, "Syntax Error ! Error ! Error !\n");
        // delete lexer;
        return false;
    }

    // delete lexer;
    return true;
}


bool Parser::compile_string( const char* string )
{
    assert( string );

    if ( lexer )
        delete lexer;
    lexer = crb_create_lexer( this );

    if ( parsed_script )
        delete parsed_script;
    parsed_script = new ParsedScript();

    parsed_script->filename = "(string)";
    bool r = lexer->set_input_string( string, "UTF-8" );
    assert( r );

    parsed_script->stmt_list = nullptr;

    error_count = 0;
    if ( yyparse(this, parsed_script) || error_count > 0 ) {
        // BUGBUG
        fprintf(stderr, "Error ! Error ! Error !\n");
        // delete lexer;
        return false;
    }

    // delete lexer;
    return true;
}

ParsedScript* Parser::detach_parsed_script()
{
    ParsedScript* ps = parsed_script;
    parsed_script = NULL;
    return ps;
}

/** 引数はyyparse() と同じになる */
void yyerror( const YYLTYPE* yylloc, Parser* myparser, ParsedScript* driver,
              const char* msg )
{
    myparser->error_handler( yylloc->first_line, yylloc->first_column,
                             "%s", msg );
    myparser->error_count++;
}




// Local variables:
// tab-width: 4
// c-basic-offset: 4
// indent-tabs-mode: nil
// End:
