# -*- coding:utf-8 -*-


class Range
  # Range はコンテナではない
  # include Enumerable

  attr_reader :first
  attr_reader :last
  attr_reader :step

  def initialize first, last, step = 1
    if !(first <=> last)
      raise ArgumentError, "first or last isn't a comparable."
    end
    if first > last
      raise ArgumentError, "first is greater than last."
    end

    @first = first
    @last = last
    @step = step
  end


  def to_enum
    Iterator.new self
  end


  # [incompat]
  class Iterator < Enumerator

    def initialize range
      raise TypeError if !range.is_a?(Range)

      @range = range
      @next = @range.first
    end


    def has_next?
      @next <= @range.last
    end


    def next
      raise StopIteration, "already reach the end." if @next > @range.last

      result = @next
      @next += @range.step
      result
    end
  end # Range::Iterator

end # class Range


