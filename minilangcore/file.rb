# -*- coding:utf-8 -*-


require "file/file_io"
require "text_reader_writer"


class File
  class << self
    # ファイルを開く
    # File.open path, mode = "r", perm = 0o666, options = {}
    # File.open path, mode = "r", options = {}
    # File.open path, options = {}
    #
    # @param path ファイル名
    # @param mode 開くモード. "r"などや, "r:ext_encoding"
    # @param perm パーミション
    # @param options オプション [incompat]
    #     :encoding 文字コード.
    #
    # @return FileIO or TextReaderWriter
    #     モードまたはオプションで文字コードが指定されたときは TextReaderWriterオブジェクト
    #     を返す.
    #     modeに"t"が含まれていたときもTextReaderWriterオブジェクトを返す.
    def open path, *args, &block
      if !path.is_a?(String)
        if path.is_a?(Integer)
          raise RuntimeError, "file descriptor as file name is not supported"
        end
        raise TypeError, "path must be a String"
      end
      raise ArgumentError, "too many arguments" if args.size > 3 

      if args.size >= 1 && args[-1].is_a?(Hash)
        options = args[-1]
        args.pop
      else
        options = {}
      end
      raise ArgumentError, "too many arguments" if args.size > 2 

      mode = args.size >= 1 ? args[0] : "r"
      perm = args.size == 2 ? args[1] : 0o666

      raise TypeError, "mode must be a String" if !mode.is_a?(String)
      raise TypeError, "perm must be a Integer" if !perm.is_a?(Integer)

      # 文字コードが指定されているか
      if (pos = mode.index(":"))
        raise ArgumentError, "mode missing" if pos == 0
        encoding = mode[pos + 1, mode.size - pos - 1]
        mode = mode[0, pos]
        raise ArgumentError, "encoding missing" if encoding == ""
      else
        encoding = options[:encoding]
      end

      if encoding || mode.index("t")
        fp = TextReaderWriter.new( ::File::FileIO.new(path, mode, perm), 
                                   encoding )
      else
        fp = ::File::FileIO.new path, mode, perm
      end

      if block
        begin
          block.call fp
        ensure
          fp.close
        end
        nil
      else
        fp
      end
    end

    
    # 各行を引数としてブロックを呼び出す
    def foreach path, rs = /\r?\n|\r|\x2028|\x2029/, options = {}
      opt = {
        :encoding => "UTF-8" # 強制的にテキストモード
      }.merge options

      open path, opt do |fp|
        while line = fp.gets(rs)
          yield line
        end
      end
    end

    
    def readlines path, rs = /\r?\n|\r|\x2028|\x2029/, options = {}
      opt = {
        :encoding => "UTF-8" # 強制的にテキストモード
      }.merge options

      EachLine.new (open path, opt), rs
    end
  end # class << self

  
  class EachLine
    include ::Enumerable

    def initialize fp, rs = /\r?\n|\r|\x2028|\x2029/
      raise TypeError if !fp.is_a?(::TextReaderWriter)
      raise TypeError if !rs.is_a?(String) && !rs.is_a?(Regexp)
      @fp = fp
      @rs = rs
    end


    def each 
      while line = fp.gets(@rs)
        yield line
      end
    end
  end # class EachLine

end # class File
