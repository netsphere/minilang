# -*- coding: utf-8 -*-

require "basic_socket"


class Socket

  # UDP/IP または UNIXドメインの, データグラムソケット
  class DatagramSocketIO < ::BasicSocket

    def initialize afamily, protocol
    end


    # sendto() は, データグラムソケットのみ.
    # @param [Sockaddr] dest_sockaddr 送信先のアドレス
    def sendto msg, flags, dest_sockaddr
      raise # TODO: impl.
    end


    # データを読み込む.
    # 仕様としてはストリームソケットでも使えるが, 通常はデータグラムソケットで使用する.
    # @return [Array] データのバイト列 ByteArray と送信元アドレス Sockaddr の2要素の配列.
    def recvfrom max_length, flags
      raise # TODO: impl.
    end

  end

end # class Socket
