# -*- coding: utf-8 -*-

require "socket/sockaddr"


class Socket
  # ルート名前空間にあれこれ置かない [incompat]
  class Addrinfo
=begin
    # getaddrinfo() に与えるフラグ. AI_V4MAPPED, AI_ADDRCONFIG or AI_NUMERICHOST
    # AI_NUMERICSERV
    attr_reader :flags
=end

    # アドレスファミリ. AF_INET or AF_INET6.
    attr_reader :afamily

    # SOCK_STREAM or SOCK_DGRAM
    attr_reader :socktype
    
    attr_reader :protocol

    # struct sockaddr. Socket#connect() に渡す
    attr_reader :addr

    attr_reader :canonname

    def initialize sockaddr, afamily, socktype = 0, protocol = 0
      raise TypeError if !sockaddr.is_a?(Sockaddr)
      raise TypeError if !afamily.is_a?(Integer)
      raise TypeError if !socktype.is_a?(Integer)
      raise TypeError if !protocol.is_a?(Integer)

      @addr = sockaddr
      @afamily = family
      @socktype = socktype
      @protocol = protocol
    end
  end

end # class Socket

# for compat.
#::Addrinfo = ::Socket::Addrinfo
