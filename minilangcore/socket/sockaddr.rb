# -*- coding: utf-8 -*-

class Socket

  class Sockaddr
    attr_reader :afamily

    class << self
      # SockaddrUn, SockaddrIn, SockaddrIn6のいずれかを生成して返す [incompat]
      def parse bytes
        raise # TODO: impl.
      end
    end # class << self


    def initialize afamily
      raise TypeError if !afamily.is_a?(Integer)
      @afamily = afamily
    end
  end

    
  # UNIXドメインソケット (AF_UNIX)
  class SockaddrUn < Sockaddr
    attr_reader :path

    def initialize path
      raise TypeError if !path.is_a?(String)

      super Socket::AF_UNIX
      @path = path
    end
  end


  # IPv4. sockaddr_inのラッパ
  class SockaddrIn < Sockaddr
    attr_reader :port
    attr_reader :addr  # struct in_addr

    def initialize port, addr
      raise TypeError if !port.is_a?(Integer)
      raise TypeError if !addr.is_a?(String)

      super Socket::AF_INET
      @port = port
      @addr = addr
    end
  end


  # IPv6
  class SockaddrIn6 < Sockaddr
    attr_reader :port
    attr_reader :flowinfo
    attr_reader :addr  # struct in6_addr
    # 増えた
    attr_reader :scope_id

    def initialize port, flowinfo, addr, scope_id
      raise TypeError if !port.is_a?(Integer)
      raise TypeError if !flowinfo.is_a?(Integer)
      raise TypeError if !addr.is_a?(String)
      raise TypeError if !scope_id.is_a?(Integer)

      super Socket::AF_INET6
      @port = port
      @flowinfo = flowinfo
      @addr = addr
      @scope_id = scope_id
    end
  end

end # class Socket

