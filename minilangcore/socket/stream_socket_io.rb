# -*- coding:utf-8 -*-

require "basic_socket"


class Socket
  # [incompat]
  # stream socket
  class StreamSocketIO < ::BasicSocket

    def initialize afamily, protocol
      raise # TODO: impl.
    end


    # ストリームソケット専用
    def listen
      raise # TODO: impl.
    end


    # ストリームソケット専用
    # @return a pair of SocketIO, sockaddr
    #
    # example:
    #     # AF_INET6 は IPv4/IPv6両方を受け付ける
    #     # Windows XP & Windows Server 2003ではIPv6専用になってしまう。
    #     # Windows Vista以降は問題ない
    #     # See http://msdn.microsoft.com/en-us/library/windows/desktop/bb513665%28v=vs.85%29.aspx
    #     # Windows XPでも動くサーバを作るときは, setsockopt( ... IPV6_V6ONLY ) した
    #     # IPv6専用ソケットと IPv4専用ソケットを用意して, select() 等する.
    #
    #     addrs = Socket.getaddrinfo nil, "80", AF_INET6, SOCK_STREAM, nil,
    #                                AI_PASSIVE
    #     sock = Socket.new addrs[0].afamily, addrs[0].socktype, addrs[0].protocol
    #     sock.bind addrs[0].addr
    #     sock.listen 5
    #     io, clientaddr = sock.accept
    def accept
      raise # TODO: impl.
    end


    # 接続相手のアドレス
    # @return [Sockaddr] 接続相手のアドレス
    def getpeername
      raise # TODO: impl.
    end

  end # class StreamSocketIO

end # class Socket
