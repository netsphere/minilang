# -*- coding:utf-8 -*-

class File
  # [incompat] バイト列のread/write
  class FileIO < IO

    # @!method write bytes
    #     ファイルにバイト列を書き込む
    #     @param [ByteArray] 書き込むバイト列


    # @!method read max_length
    #     バイト列を読み込む
    #     @param [Integer] max_length 読み込む最大長 (バイト単位).
    #     @return [ByteArray, nil] 読み込んだバイト列.
    #         すでにEOFに達していたときは nil


    # @!method close
    #     IOを閉じる.
    #     @return self

  end

end # class File
