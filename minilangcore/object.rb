# -*- coding:utf-8 -*-


require "kernel"


class Object < BasicObject

  include Kernel

  # @!method instance_variable_get name
  #     インスタンス変数の値を得る
  #     @param [Symbol, String] name インスタンス変数名. 先頭の"@"が必要


  # @!method instance_variable_set name, value
  #     インスタンス変数を設定する
  #     @param [Symbol, String] name  インスタンス変数名. 先頭の"@"が必要.
  #     @return [Object] self


  # @!method instance_variables
  #     インスタンス変数の変数名の一覧
  #     @return [Array<Symbol>] インスタンス変数名の配列. シンボルには先頭の"@"も付く.


  # @!method class
  #     このオブジェクトのクラスを得る
  #     @return [Class] クラス


  # @!method methods
  #     このオブジェクトのメソッドの一覧
  #     @return [Array<Symbol>] メソッドの一覧


  # @!method is_a? klass
  #     @param [Module] klass 
  #     @return [Boolean] klass のインスタンスなら true


  def inspect
    r = "#<" + self.class.name + ":" + __id__
    ivars = instance_variables
    if !ivars.empty?
      r += (ivars.map do |ivar| 
              " " + ivar.to_s + "=" + instance_variable_get(ivar).inspect
            end).join("")
    end
    r + ">"
  end


  def to_s
    "#<" + self.class.name + ":" + __id__ + ">"
  end

end # class Object
