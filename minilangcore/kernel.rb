
module Kernel

  def print *args
    ::STDOUT.print *args
  end


  def puts *args
    args.each do |x| print x, "\n" end
  end


  # lambda do ... end -> Proc
  def lambda &block
    block
  end

end
