# -*- coding:utf-8 -*-


# BasicObjectにスーパークラスはない
class BasicObject
  def initialize 
    # empty
  end


  # 必要があれば偽にできるようにする. [incompat]
  # サブクラスでオーバーライドすること. 
  def true?
    true
  end


  # @!method __id__
  #     オブジェクトの id を返す
  #     @return [Integer] オブジェクトのid


  # @!method __send__ name, *args, &block
  #     メソッドを呼び出す
  #     @param [Symbol] name    メソッド名
  #     @param args             実引数
  #     @param block            ブロック引数

end # class BasicObject
