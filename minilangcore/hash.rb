# -*- coding: utf-8 -*-

require "enumerable"


# マップ
class Hash
  include Enumerable

  def inspect
    "{" + (map do |k, v| k.to_s + "=>" + v.to_s end).join(", ") + "}"
  end


  def to_enum
    Iterator.new self, :each
  end


  def enum_for method = :each
    raise TypeError if !method.is_a?(Symbol)
    Iterator.new self, method
  end


# [incompat]
#  def each
#    iter = to_enum
#    while iter.has_next?
#      yield *iter.next  # 2要素の配列ではなく, 2引数で渡す
#    end
#  end


  def to_a
    map do |x| x end
  end


  # [incompat]
  class Iterator < Enumerator

    def initialize hash, method
      raise TypeError if !hash.is_a?(::Hash)
      raise TypeError if !method.is_a?(Symbol)
      raise ArgumentError if method != :each

      @hash = hash
      @method = method
    end


    # @!method has_next?
    #     次のペアがあるか


    # @!method next
    #     次のペアを得る
    #     @return [Array] 次のペア
    #     @raise [StopIteration] すでに終端に達していたとき

  end # Hash::Iterator

end # class Hash



