# -*- coding:utf-8 -*-


require "comparable"
require "enumerable"


# 配列
class Array
  include Comparable
  include Enumerable

  # 配列を比較する. それぞれの要素を '<=>' 演算子で比較する
  # @param [Array] other 比較対象の配列
  def <=> other
    raise TypeError if !other.is_a?(Array)

    i1 = to_enum
    i2 = other.to_enum

    while i1.has_next? && i2.has_next?
      r = i1.next <=> i2.next
      return r if r != 0
    end

    return 1 if i1.has_next? && !i2.has_next?
    return -1 if !i1.has_next? && i2.has_next?
    0
  end


  # 文字列に変換する
  def to_s
    "[" + (map do |x| x.to_s end).join(", ") + "]"
  end


  # 各要素を 'inspect' した文字列に変換する
  def inspect
    "[" + (map do |x| x.inspect end).join(", ") + "]"
  end


  # イテレータを得る
  def to_enum
    Iterator.new self, :each
  end


  # イテレータを得る. メソッド名を指定できる
  # @param [Symbol] method   メソッド名
  def enum_for method = :each
    raise TypeError if !method.is_a?(Symbol)
    Iterator.new self, method
  end


  # @return [Boolean] 配列が空のとき true
  def empty?
    size == 0
  end


  # @!method size
  #     要素数を返す
  #     @return [Integer] 要素数


  # 配列の末尾から要素を取り除く
  # @return 取り除いた要素. 配列が空だったときは nil. (Common Lispと同じ.)
  def pop
    #raise TypeError if !n.is_a?(Integer)
    #raise ArgumentError if n > size

    if size > 0
      delete_at size - 1
    else
      nil
    end
  end


  # @!method push obj
  #     配列の末尾に要素objを追加する
  #     @return self


  def << obj
    push obj
  end


  # @!method delete_at pos
  #     posの要素を削除し, 以降の要素を詰める
  #     @param [Integer] pos 削除する位置.
  #     @return 削除した要素. posが範囲外だったときは nil.


  # @!method concat other
  #     otherを破壊的に結合する
  #     @param [Array] other 結合する配列
  #     @return [Array] self
  

  def + other
    raise TypeError if !other.is_a?(Array)
    Array.new.concat(self).concat(other)
  end


  # 内部の要素をコピーせずに, 単にselfを返す
  def to_a
    self
  end


  # 配列のイテレータ. [incompat]
  class Iterator < Enumerator
    # @param [Array] ary      配列
    # @param [Symbol] method  内部イテレータ名
    def initialize ary, method
      raise TypeError if !ary.is_a?(Array)
      raise TypeError if !method.is_a?(Symbol)
      if method != :each
        raise ArgumentError, "not supported method-name except :each" 
      end

      @ary = ary
      @method = method
      @next = 0
    end


    # @return [Boolean] 次の要素があるときは true
    def has_next?
      @next < @ary.size
    end


    # 次の要素を得る
    # @raise [StopIteration] すでに終端に達していたとき
    def next
      raise StopIteration, "already reach the end." if @next >= @ary.size 

      result = @ary[@next]
      @next += 1
      result
    end
  end # Array::Iterator

end # class Array


