# -*- coding: utf-8 -*-


# <=> メソッドを利用するmix-in
module Comparable

  # @!method == other
  #     'other' と一致しているか
  #     @param other 比較対象


  # @!method != other
  #     'other' と異なるか. != もメソッド. Ruby 1.9も同じ.
  #     @param other 比較対象


  # @!method < other
  #     'other' より小さいか


  # @!method <= other
  #     'other' より同じか小さいか


  # @!method > other
  #     'other' より大きいか


  # @!method >= other
  #     'other' より同じか大きいか


  # サブクラスでオーバーライドすること
  def <=> other
    raise "must override"
  end

end
