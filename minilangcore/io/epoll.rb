# -*- coding: utf-8 -*-

# Linux専用
class Epoll
  # Epollインスタンスを生成する
  def initialize 
    int epfd = epoll_create(10); // sizeは現在では無視される
  end

  # IOを追加する
  # @param events EPOLLIN   read()を待つとき
  def add iofd, events
    struct epoll_event event;
    event.events = events;
    epoll_ctl( epfd, EPOLL_CTL_ADD, iofd, &event );
  end

  def remove iofd
    epoll_ctl( epfd, EPOLL_CTL_DEL, iofd, nullptr );
  end

  # イベントを待つ
  def wait
  end

end
