# -*- coding: utf-8 -*-


module Enumerable
  # [incompat] Ruby では empty? == true でも真
  def true?
    return !empty?
  end


  def empty?
    raise "must override"
  end


  # サブクラスで, eachをオーバーライドするか, to_enumメソッドを用意すること
  def each
    iter = to_enum
    while iter.has_next?
      yield iter.next
    end
  end


  # すべての要素に演算を適用し, 一つの値にする
  def reduce sym = nil
    raise TypeError if sym && !sym.is_a?(Symbol)

    result = nil
    case sym
    when :maximum
      raise # TODO: impl.
    when :minimum
      raise # TODO: impl.
    when :sum
      raise # TODO: impl.
    when :product
      raise # TODO: impl.
    when :logical_and
      raise # TODO: impl.
    when :bitwise_and
      raise # TODO: impl.
    when :logical_or
      raise # TODO: impl.
    when :bitwise_or
      raise # TODO: impl.
    when :logical_xor
      raise # TODO: impl.
    when :bitwise_xor
      raise # TODO: impl.
    else
      raise ArgumentError, "unknown operator"  # [incompat] rubyではメソッド名
    end
  end


  # [incompat] reduceの別名ではない
  def inject init, sym = nil
    raise TypeError if sym && !sym.is_a?(Symbol)

    result = init
    if sym
      each do |x|
        result = result.__send__ sym, x
      end
    else
      each do |x|
        result = yield result, x
      end
    end

    result
  end


  def map
    result = []
    each do |x|
      result << (yield x)
    end
    result
  end


  def to_enum
    raise "must override"
  end


  def enum_for method = :each
    raise TypeError if !method.is_a?(Symbol)
    raise "must override"
  end

end # module Enumerable
