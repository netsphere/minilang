# -*- coding: utf-8 -*-

class Encoding
  # ハッシュのキーにできるようにする
  include Comparable

  attr_reader :name


  # @!method initialize name
  #     @param [String] name  文字コード名. Symbolは不可 [incompat]

  
  def to_s
    @name
  end


  def inspect
    "#<Encoding:" + to_s + ">"
  end


  # @!method <=> other
  #     文字コード名を正規化し, 比較する
  #     @param [Encoding] other 比較する文字コード

end # class Encoding
