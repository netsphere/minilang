# -*- coding:utf-8 -*-


class Module
  class << self
    # @!method new
    #     無名モジュールを生成する
  end


  def attr_reader *names
    names.each do |name|
      raise TypeError if !name.is_a?(Symbol)
      define_method name, 
                    (lambda do instance_variable_get("@#{name.to_s}") end)
    end
  end


  # @!method to_s
  #     文字列にする


  # @!method define_method name, method
  #     インスタンスメソッドを定義する
  #     @param [Symbol] name  メソッド名
  #     @param [Proc] method メソッド本体
end
