# -*- coding: utf-8 -*-


require "encoding"


# バイト列のみを格納できる mutable な配列. [incompat]
class ByteArray
  include Comparable
  include Enumerable

  # バイト列が本当にこの文字コードかは確かではない. for compat.
  attr_reader :encoding


  # このバイト列の文字コードが 'encoding' だとみなす
  # @param [String, Encoding] encoding  このバイト列の文字コード
  def force_encoding encoding
    if !encoding.is_a?(String) && !encoding.is_a?(Encoding)
      raise TypeError, "encoding must be a String or Encoding"
    end

    @encoding = encoding
    self
  end


  # バイト列を別の文字コードに変換し, 新しいString / ByteArrayを生成する
  # @param [String, Encoding] new_enc 新しい文字コード
  def encode new_enc
    if !new_enc.is_a?(String) && !new_enc.is_a?(Encoding)
      raise TypeError, "new_enc must be a String or Encoding"
    end

    if new_enc.to_s == "UTF-16"
      String.new self
    else
      conv = Encoding::Converter.new @encoding, new_enc
      conv.convert self
    end
  end

end # class ByteArray

