# -*- coding:utf-8 -*-


# 外部イテレータ
class Enumerator

  def has_next?
    raise "must override"
  end


  def next
    raise "must override"
  end
end
