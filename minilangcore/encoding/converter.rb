# -*- coding: utf-8 -*-


require "encoding"
require "byte_array"

class Encoding

  # iconvライブラリはruby 2.0で*廃止*された. 何たる非互換!!!
  # 文字コードを変換する
  class Converter
    attr_reader :source_encoding

    attr_reader :destination_encoding

    # @param [String, Encoding] source_encoding 変換元の文字コード
    # @param [String, Encoding] dest_encoding   変換先の文字コード
    def initialize source_encoding, dest_encoding, options = {}
      if !source_encoding.is_a?(String) && !source_encoding.is_a?(::Encoding) 
        raise TypeError 
      end
      if !dest_encoding.is_a?(String) && !dest_encoding.is_a?(::Encoding)
        raise TypeError 
      end

      @source_encoding = source_encoding
      @destination_encoding = dest_encoding
    end


    # @!method convert source_bytes
    #     バイト列を変換した新しいバイト列を生成する
    #     入力バイト列は, 文字の途中で切れているかもしれない. 内部に蓄える
    #     @param [ByteArray, String] source_bytes   変換元のバイト列 
    #     @return [ByteArray, String] 変換後のバイト列. UTF-16の場合だけString


    # 入力バイト列が文字の途中で切れていた場合
    def finish
      ........
    end
  end

end # class Encoding

