# -*- coding: utf-8 -*-


# クリティカルセクション. 再入可能.
class Mutex
  # @!method lock
  #     ロックする


  # @!method unlock
  #     ロックを解除する


  def synchronize
    m = Mutex.new

    m.lock  # blockされる
    begin
      result = yield
    ensure
      m.unlock
    end
    result
  end

end # class Mutex
