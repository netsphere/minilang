# -*- coding:utf-8 -*-

require "numeric"


# 整数. 
# 有理数とは除算などの意味が異なり, スーパークラス・サブクラスではない
class Integer < Numeric
  def denominator
    1
  end


  def numerator
    self
  end


  def to_i
    self
  end

end # Integer
