# -*- coding:utf-8 -*-


class NilClass
  def to_s
    ""
  end


  def inspect
    "nil"
  end


  # もはや変更できない
  def to_i
    0
  end

  
  def true?
    false
  end


  # 明示的に定義されたメソッドを除き, 常にnil を返す. [incompat]
  def method_missing name, *args
    raise TypeError if !name.is_a?(Symbol)
    nil
  end


  def respond_to? name
    raise TypeError if !name.is_a?(Symbol)
    true
  end
end
