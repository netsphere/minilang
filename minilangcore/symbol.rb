# -*- coding:utf-8 -*-

require "comparable"


class Symbol
  include Comparable

  # @!method to_s
  #     文字列に変換する.
  #     @return [String] 文字列に変換した結果. 先頭の":"は付かない


  def inspect
    ":" + to_s
  end


  def intern
    self
  end
end
