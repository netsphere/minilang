# -*- coding:utf-8 -*-

require "encoding/converter"


# IOと文字コード変換を分離する [incompat]
class TextReaderWriter

  attr_reader :external_encoding

  def initialize io, ext_encoding
    raise TypeError if !io.is_a?(IO)
    if !ext_encoding.is_a?(String) && !ext_encoding.is_a?(Encoding)
      raise TypeError, "ext_encoding must be a String or Encoding"
    end

    @io = io
    @external_encoding = ext_encoding
    @binary_mode = false

    @buf = ""
    @write_conv = ::Encoding::Converter.new "UTF-16", @external_encoding
    @read_conv = ::Encoding::Converter.new @external_encoding, "UTF-16"
  end


  # 破壊的メソッド. 改行の変換を抑制する. 文字コードとは関係ない.
  # binmode= ではないことに注意
  def binmode
    @binary_mode = true
  end


  # バイナリモード (改行の変換を抑制) かどうか. 文字コードとは関係ない
  def binmode?
    @binary_mode
  end


  def << str
    raise TypeError, "must use IO for binary" if str.is_a?(ByteArray)
    @io.write @write_conv.convert(str.to_s)
  end


  def print *args
    args.each do |x| 
      self << x.to_s # 暗黙の型変換
    end
    nil
  end


  # 改行を挟む
  def puts *args
    args.each do |x| print x, "\n" end
  end


  def each_line rs = /\r?\n|\r|\x2028|\x2029/
    while line = gets(rs)
      yield line
    end
    nil
  end


  def lines
    raise # TODO: impl.
  end


  # 1行読み込む. 改行文字は削除しない. 
  # TODO: IO.binmode? が偽のとき, CRLFをLFに変換する
  def gets rs = /\r?\n|\r|\x2028|\x2029/
    # ブロックしないように, read は1回のみ
    @buf.concat( @read_conv.convert @io.read(16000) )
    if (pos = @buf.index(rs))
      if @binary_mode
        r = @buf[0, pos + rs.size]  これでは不味い
        @buf = @buf[pos + 1, @buf.size - pos - 1]
      else
        ...........
      end
    else
      # 改行文字が見つからなかったときも, とりあえず何か返す
      r = @buf
      @buf = ""
    end
    r
  end


  def getc
  end

  def ungetc
  end


  def readchar
  end


  def readline
  end

  def readlines
  end


  def close
    @io.close
  end


  def method_missing name, *args
    raise TypeError if !name.is_a?(Symbol)
    @io.__send__ name, *args
  end


  def respond_to? name
    raise TypeError if !name.is_a?(Symbol)

    if self.methods.include?(name)
      true
    else
      @io.respond_to?(name)
    end
  end

end # class TextReaderWriter
