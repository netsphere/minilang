# -*- coding: utf-8 -*-


require "enumerator"


class String
  
  # String#each_codepoint 用のイテレータ
  class CharacterIterator < Enumerator
    def initialize str, method
      raise TypeError if !str.is_a?(String)
      if method != :each_codepoint
        raise ArgumentError, "supported only :each_codepoint" 
      end

      @str = str
      @method = method
      @next = 0
    end


    def has_next?
      @next >= 0 && @next < @str.size
    end


    # @!method next
    #     次の文字のコードポイントを得る. サロゲートペアのみを考慮.
    #     @return [Integer] コードポイント.

  end

end # class String
