# -*- coding: utf-8 -*-

class String
  # [incompat]
  # ロケールに基づく比較順序を提供する. sort() (sort_byではなく) に渡す
  # example:
  #     ["foo", "bar"].sort &Collator.new(:ja)
  #
  #     collator = Collator.new :ja
  #     [["hoge", 50], ["fuga", 20]].sort do |a, b| 
  #       collator.call a[0], b[0]
  #     end
  class Collator

    def call a, b
    .........
    end

  end # class Collator
end
