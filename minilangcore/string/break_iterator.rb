# -*- coding: utf-8 -*-


require "enumerator"


class String

  # String#each_codepoint と each_char では, next() が返すオブジェクトの型が違う
  # => クラスを分ける
  # 真の文字 (の文字列) をブロックに渡す
  # See http://www.unicode.org/reports/tr29/  
  #     Annex #29: Unicode Text Segmentation
  class BreakIterator < Enumerator

    def initialize str, method
      raise TypeError if !str.is_a?(String)
      raise ArgumentError, "supported only :each_char" if method != :each_char

      @str = str
      @method = method
      @next = 0
    end


    def has_next?
      @next >= 0 && @next < @str.size
    end

    
    # @!method next
    #     次の文字 (真の文字) を得る.
    #     サロゲートペア, UCHAR_CANONICAL_COMBINING_CLASS などの考慮
    #     @return [String] 次の文字.

  end # String::BreakIterator

end # class String

