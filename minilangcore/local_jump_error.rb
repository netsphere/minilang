# -*- coding:utf-8 -*-


require "exception"


# 書けない場所で return などしたとき
class LocalJumpError < StandardError
  # :break, :next, :return or :yield
  attr_reader :reason

  def initialize message, reason
    if reason != :break && reason != :next && reason != :return && 
       reason != :yield
      raise ArgumentError, "reason must be :break, :next or :return"
    end

    super message
    @reason = reason
  end

end # class LocalJumpError


