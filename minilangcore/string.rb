# -*- coding: utf-8 -*-


# Ruby 1.9の String は Container よりも IO に似たインターフェイス
class String
  # Enumerable ではない. "" が真, self[nth] -> String. ruby 1.9と同じ
  include Comparable

  # @!method <=> other
  #     他の文字列と比較する. 暗黙の型変換はおこなわない.


  # @!method size
  #     長さ (文字単位) を返す


  # @!method index pattern, pos = 0
  #     pattern を検索する
  #     @param [String, Regexp] pattern  検索パターン. 
  #         patternが正規表現のときは, pattern =~ self
  #     @param [Integer] pos   検索開始位置 (文字単位).
  #     @return [Integer, nil] 見つかればその位置 (文字単位) を, 見つからなければ nil を
  #         返す


  # 内部文字列をコピーせずに, 単にselfを返す
  def to_s
    self
  end


  # ruby 1.9 
  # each は廃止された. 何たる非互換!!


  # 1文字ずつ, 真の文字を返す.
  def each_char
    iter = enum_for :each_char
    while iter.has_next?
      yield iter.next
    end
  end


  # ブロックに, code point (文字列ではなく) を渡す
  def each_codepoint
    iter = enum_for :each_codepoint
    while iter.has_next?
      yield iter.next
    end
  end


  def each_line
    iter = enum_for :each_line
    while iter.has_next?
      yield iter.next
    end
  end


  def to_enum
    Iterator.new self, :each_line
  end


  def enum_for method = :each_line
    raise TypeError if !method.is_a?(Symbol)
    case method
    when :each_line
      Iterator.new self, method
    when :each_codepoint
      CharacterIterator.new self, method
    when :each_char
      BreakIterator.new self, method
    else
      raise ArgumentError, "not support: #{method}"
    end
  end


  def encoding
    Encoding.new "UTF-16"
  end


  # バイト列を変更せずに, 文字コードだけを変更する
  # mutableなので, エラーにするしかない
  def force_encoding encoding
    raise TypeError if !encoding.is_a?(String) && !encoding.is_a?(Encoding)

    raise "not support except UTF-16" if encoding.to_s != "UTF-16"
    # 何もしない
    self
  end


  # other_encodingで指定する文字コードのバイト列に変換し、それを返す
  # @param [String, Encoding] other_encoding  変換後の文字コード
  def encode other_encoding
    if !other_encoding.is_a?(String) && !other_encoding.is_a?(Encoding)
      raise TypeError, "other_encoding must be a String or Encoding"
    end

    if other_encoding.to_s == "UTF-16"
      return String.new self
    else
      conv = Encoding::Converter.new "UTF-16", other_encoding
      conv.convert self
    end
  end


  # [incompat]
  # @param [Symbol] form   :c, :kc, :d or :kd. Default is :kc
  def normalize form = :kc
    raise # TODO: impl.
  end


  # @!method concat other
  #     otherを破壊的に結合する
  #     @param [String, #to_s] other 末尾に結合する文字列
  #     @return [String] self


  def << other
    concat other
  end


  # 文字列を連結した新しい文字列を生成する
  def + other
    String.new.concat(self).concat(other)
  end


#TODO:
#  def += other
#    concat other
#  end


  # [incompat]
  class Iterator < Enumerator

    def initialize str, method
      raise TypeError if !str.is_a?(String)
      if method != :each_codepoint && method != :each_line
        raise ArgumentError, "only support :each_codepoint and :each_line"
      end

      @str = str
      @method = method
      @next = 0
    end


    def has_next?
      @next < @str.size
    end


    def next
      raise StopIteration, "already reach the end." if @next >= @str.size

      case @method
      when :each_line
        ..........
      when :each_codepoint
        .......
      else
        raise "internal error."
      end
    end

  end # String::Iterator

end # class String



