# -*- coding: utf-8 -*-


# Container とは型が違う
# Container -> [X]; IO -> [[X]]
# 文字コード関係は TextReaderWriter へ移動 [incompat]
# IO とそのサブクラスは, バイト列のread/writeをおこなう
class IO
  # Enumerable は 型が合わないのでinclude しない [incompat]

  # TextReaderWriterへ移動
  #def external_encoding


  class << self
    def select reads, writes, excepts, timeout
      raise # TODO: impl.
    end
  end


  def << str
    raise TypeError if !str.is_a?(ByteArray)
    write str
    self
  end


  # サブクラスでオーバーライドすること
  # @param [ByteArray] bytes 書き出すバイト列.
  def write bytes
    raise TypeError if !bytes.is_a?(ByteArray)
    raise "must override"
  end


  # サブクラスでオーバーライドすること
  # @return [ByteArray, nil] 読み込んだバイト列.
  #     すでにEOFに達していたときは, nil
  def read max_length
    raise TypeError if !max_length.is_a?(Integer)
    raise "must override"
  end


  def close
    raise "must override"
  end

end # class IO
