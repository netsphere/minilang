# -*- coding:utf-8 -*-


class Exception
  attr_reader :message

  # バックトレース
  attr_reader :backtrace

  # @!method initialize message = ""
  #     例外を発生する
  #     @param [String] message メッセージ

end


class StandardError < Exception
end


class SystemCallError < StandardError
  attr_reader :errno

  def initialize message, errno
    raise TypeError if !errno.is_a?(Integer)

    super message
    @errno = errno
  end
end
