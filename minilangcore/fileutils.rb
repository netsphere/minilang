# -*- coding: utf-8 -*-


# ファイル名とクラス名が合っていない
module FileUtils
  class << self
    # @!method copy src_file, dest_file, options = {}
    #     ファイルをコピーする
    #     @param [String] src_file コピー元のファイル名
    #     @param [String] dest_file コピー先のファイル名

  end # class << self

end # module FileUtils
