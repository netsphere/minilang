
#include "../ptr_vector.h"

using namespace std;

struct Foo {
};

int main() {
    ptr_vector<Foo*> var;
    var.push_back(new Foo);

    ptr_vector<Foo*>::iterator i;
    for (i = var.begin(); i != var.end(); i++) {
    }
    var.erase(var.begin(), var.end());
    
    var.push_back(new Foo);
    var.clear();
    
    return 0;
}

