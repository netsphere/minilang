
#include "../ptr_list.h"

using namespace std;

struct Foo {
};

int main() {
    ptr_list<Foo*> var;
    var.push_back(new Foo);

    ptr_list<Foo*>::iterator i;
    for (i = var.begin(); i != var.end(); i++) {
    }
    
    var.clear();
    return 0;
}

